To have an environnement with dolfinx :
guix time-machine -C myCurrentChannels.scm -- shell -C -m manifestDolfinxv06.scm -L recettes/
guix time-machine -C myCurrentChannels.scm -- shell -C -m manifestDolfinxv07.scm -L recettes/

To have python-mse environnement:
guix time-machine -C myCurrentChannels.scm -- shell -C -m manifestDolfinxv06.scm -m manifestOnlyMsev2.scm -L recettes/

a propos de la version d'openmpi petcs-openmpi:
-----------------------------------------------
assume: les environnement sont lancé avec le "time-machine -C myCurrentChannels.scm"
rappel: les librairie disponible dans un environnement guix sont dans la variable LIBRRARY_PATH (pas de libraririe LD_LIBRARY_PATH en guix, on se base sur la chaine de compilation qui écrit dans la librarie compuilée les pah et version vers les libraries à charger.).

Dans l'environnement "guix shell -C coreutils which  petsc-openmpi pkg-config gcc-toolchain@12.3.0", 
"objdump -p libpetcs.so"  fait apparaître la lib openmpi 4.1.6, ie elle est liée à cette verision de openmpi, de plus libmpi.so est dans le gnu/store mais non visible dans l'environnement (ie pas dans LIBRARY_PATH )

/gnu/store/kim85ijw6xh0134pljv5ihil8h29q79w-openmpi-4.1.6/lib

* guix shell -C coreutils which  openmpi pkg-config gcc-toolchain@12.3.0

objdump -p libmpi.so
/gnu/store/riwbcmb43xd5323fdcdlkw4lrsvcjv6a-openmpi-5.0.0-ucx-1.14.1-rocm-5.7.1/lib

* guix shell -C coreutils which  openmpi@4.1.6 pkg-config gcc-toolchain@12.3.0

objdump -p libmpi.so

/gnu/store/z1ys5am8m7zrz71xghnhr6yaiv71i1b8-openmpi-4.1.6/lib

On constatera (sisi) que "guix shell -C coreutils which  openmpi petsc-openmpi pkg-config gcc-toolchain" où libpetsc.so est liée à libmpi 4.1.6 et qu'il y a dans LIBRARY_PATH, libmpi 5.0.xxx;

Ce qui va poser de facheux problème de compil et de cohérence si on creer de nouvelles librariries utilisant LIBRARY_PATH/libmpi et LIBRARY_PATH/libpetsc 


C'est pourquoi les manifestes ici présent mentionnent explicitement la version openmpi@4.1.6. Peut-etre qur dans les prochaine verision de dépot guix cela sera inutil.

A propos de hdf5:
-----------------

il est bien difficil d'utiilser une seule version de hdf5:

hdf5-geos : 1.12.2

hdf5 last : 1.14.3

hdf5-parallel-openmpi : 1.14.3

python-h5py-geos : basé sur h5py-geos

il y a deux version de h5py:
python-h5py	3.9.0	out	llnl/geos.scm:454:2
python-h5py	3.8.0	out	gnu/packages/python-xyz.scm:2962:2

python-h5py: hdf5-1.10

netcdf : hdf5 last: 1.14.3

dans packages/math.scm
(define-public hdf5
  ;; Default version of HDF5.
  hdf5-1.10)


d'ou vient la version 1.10.9 ??
Headers are 1.10.9, library is 1.12.2
