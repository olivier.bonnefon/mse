;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2023 Inria

(define-module (inrae mse)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (gnu packages algebra)
  #:use-module (guix packages)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system python)
  #:use-module (guix build-system pyproject)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages gcc)
  #:use-module (guix git-download)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages python-build)
  #:use-module (gnu packages python)
  #:use-module (gnu packages libffi)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages check)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages ssh)
  #:use-module (non-free parmetis)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages simulation)
  #:use-module (gnu packages ninja)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-science)
  #:use-module (llnl geos)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages fltk)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages multiprecision)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages image)
  #:use-module (gnu packages swig))

(define-public mse
  (package
    (name "mse")
    (version "v2.2")
    (home-page "https://forgemia.inra.fr/olivier.bonnefon/mse/")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://forgemia.inra.fr/olivier.bonnefon/mse/-/archive/"
             version "/mse-" version ".tar.gz"))
       (sha256
        (base32 "0l7bpi8msv7ihn8pp79ms952yqp3a3b6wfbs5jz5f0n3av3gfc21"))))
    (build-system cmake-build-system)
    (arguments
     (list
      #:configure-flags #~`("-DBUILD_SHARED_LIBS=ON" ,(string-append
                                                       "-DPETSC_INCLUDE_DIRS="
                                                       #$(this-package-input
                                                          "python-petsc4py")
                                                       "/include"))
      #:validate-runpath? #f ;find why we need that
      #:phases #~(modify-phases %standard-phases
                   (add-before 'configure 'chdir-to-trunk/src
                     (lambda _
                       (chdir "trunk/src"))))))
    (synopsis "environnement mecha stat")
    (native-inputs (list pkg-config swig python-wheel))
    (inputs (list openmpi
                  openssh-sans-x
                  python
                  python-petsc4py
                  petsc-openmpi
                  dolfinx
                  python-numpy
                  python-ipython
                  python-fenics-ufl
                  python-fenics-basix
                  python-dolfinx
                  python-petsc4py
                  fenics-ffcx
                  python-mpi4py
                  openmpi
                  gmsh
                  python-meshio
                  python-fenics-ffcx
                  python-h5py@3.9))
    (description "simulation et estimations d'edps")
    (license license:lgpl3)))

(define-public python-mse
  (package/inherit mse
    (name "python-mse")
    (build-system python-build-system)
    (arguments
     '(#:phases (modify-phases %standard-phases
                  (add-before 'build 'chdir-to-trunk/
                    (lambda _
                      (chdir "trunk/")))
                 ;(add-after 'install 'stopp/
                 ;   (lambda _
                 ;     (exit 1)))
			  )))
    (synopsis "python-mse")
    (native-inputs (list pkg-config python-numpy python-setuptools))
    (inputs (list openmpi openssh-sans-x pymseMapTools pymseMatTools))
    (description "tools to build dofs map used by inrae.mse")
    ))

(define-public mseNetcdf
  (package/inherit netcdf
		   (name "mseNetcdf")
		   (inputs
     (list curl
           hdf4-alt
           hdf5-geos
           libjpeg-turbo
           libxml2
           unzip
           zlib))
		   ))

(define-public mseGmsh
  (package/inherit gmsh
		   (name "mseGmsh")
		   (propagated-inputs
     (list fltk
           gfortran
           glu
           gmp
           hdf5-geos
           libx11
           libxext
           mesa
           metis
           openblas
           opencascade-occt))
		   ))

(define-public mseMapTools
  (package/inherit python-mse
    (name "mseMapTools")
    (home-page "https://forgemia.inra.fr/olivier.bonnefon/mse/")
    (build-system cmake-build-system)
    (arguments (list #:configure-flags #~`("-DBUILD_SHARED_LIBS=ON")
                     #:phases #~(modify-phases %standard-phases
                                  (add-before 'configure 'chdir-to-trunk/mseMapTools
                                    (lambda _
                                      (chdir "trunk/mseMapTools"))))))
    (synopsis "tools to build dofs map")
    (native-inputs (list pkg-config))
    (inputs (list openmpi openssh-sans-x))
    (description "tools to build dofs map used by inrae.mse")
    (license license:lgpl3)))

(define-public mseMatTools
  (package/inherit python-mse
    (name "mseMatTools")
    (home-page "https://forgemia.inra.fr/olivier.bonnefon/mse/")
    (build-system cmake-build-system)
    (arguments (list #:configure-flags #~`("-DBUILD_SHARED_LIBS=ON" ,(string-append
                                                                      "-DPETSC_INCLUDE_DIRS="
                                                                      #$(this-package-input
                                                                         "python-petsc4py")
                                                                      "/include"))
                     #:phases #~(modify-phases %standard-phases
                                  (add-before 'configure 'chdir-to-trunk/mseMapTools
                                    (lambda _
                                      (chdir "trunk/mseMatTools")))
				    )))
    (synopsis "tools for matrix building map")
    (native-inputs (list pkg-config))
    (inputs (list openssh-sans-x python-petsc4py petsc-openmpi openmpi))
    (description "tools to build matrix used by inrae.mse")
    (license license:lgpl3)))

(define-public mseSafranTools
  (package/inherit python-mse
    (name "mseSafranTools")
    (home-page "https://forgemia.inra.fr/olivier.bonnefon/mse/")
    (build-system cmake-build-system)
    (arguments (list #:configure-flags #~`("-DBUILD_SHARED_LIBS=ON" )
                     #:phases #~(modify-phases %standard-phases
                                  (add-before 'configure 'chdir-to-trunk/mseSafranTools
                                    (lambda _
                                      (chdir "trunk/mseSafranTools")))
				    )))
    (synopsis "tools to export safran to dolfinx")
    (native-inputs (list pkg-config))
    (inputs (list openssh-sans-x mseNetcdf mseMapTools))
    (description "tools to export safran to dolfinx")
    (license license:lgpl3)))


(define-public pymseMapTools
  (package/inherit mseMapTools
    (name "pymseMapTools")
    (home-page "https://forgemia.inra.fr/olivier.bonnefon/mse/")
    (build-system cmake-build-system)
    (arguments (list #:configure-flags #~`("-DBUILD_SHARED_LIBS=ON -DCMAKE_LIBRARY_PATH=$GUIX_ENVIRONMENT/lib/ -DCMAKE_INCLUDE_PATH=$GUIX_ENVIRONMENT/include/")
                     #:phases #~(modify-phases %standard-phases
                                  (add-before 'configure 'chdir-to-trunk/mseMapTools/Frontend
                                    (lambda _
                                      (chdir "trunk/mseMapTools/Frontend"))))))
    (synopsis "python interface on mseMapTools")
    (native-inputs (list pkg-config swig python-wheel))
    (inputs (list mseMapTools
                  openmpi
                  openssh-sans-x
                  python
                  python-numpy
                  python-ipython
                  mseMapTools))
    (description "python interface on mseMapTools for inrae.mse")
    (license license:lgpl3)))

(define-public pymseMatTools
  (package/inherit mseMatTools
    (name "pymseMatTools")
    (home-page "https://forgemia.inra.fr/olivier.bonnefon/mse/")
    (build-system cmake-build-system)
    (arguments (list #:configure-flags #~`("-DBUILD_SHARED_LIBS=ON -DCMAKE_LIBRARY_PATH=$GUIX_ENVIRONMENT/lib/ -DCMAKE_INCLUDE_PATH=$GUIX_ENVIRONMENT/include/" ,
                                           (string-append
                                            "-DPETSC_INCLUDE_DIRS="
                                            #$(this-package-input
                                               "python-petsc4py") "/include"))
                     #:phases #~(modify-phases %standard-phases
                                  (add-before 'configure 'chdir-to-trunk/mseMapTools/Frontend
                                    (lambda _
                                      (chdir "trunk/mseMatTools/Frontend"))))))
    (synopsis "python interface on mseMatTools")
    (native-inputs (list pkg-config swig python-wheel))
    (inputs (list mseMatTools
                  openmpi
                  openssh-sans-x
                  python
                  petsc-openmpi
                  python-petsc4py
                  python-numpy
                  python-ipython
                  mseMatTools))

    (description "python interface on mseMatTools for inrae.mse")
    (license license:lgpl3)))

(define-public pymseSafranTools
  (package/inherit mseSafranTools
    (name "pymseSafranTools")
    (home-page "https://forgemia.inra.fr/olivier.bonnefon/mse/")
    (build-system cmake-build-system)
    (arguments (list #:configure-flags #~`("-DBUILD_SHARED_LIBS=ON -DCMAKE_LIBRARY_PATH=$GUIX_ENVIRONMENT/lib/ -DCMAKE_INCLUDE_PATH=$GUIX_ENVIRONMENT/include/" )
                     #:phases #~(modify-phases %standard-phases
                                  (add-before 'configure 'chdir-to-trunk/mseSafranTools/Frontend
                                    (lambda _
                                      (chdir "trunk/mseSafranTools/Frontend"))))))
    (synopsis "python interface on mseSafranTools")
    (native-inputs (list pkg-config swig python-wheel))
    (inputs (list mseSafranTools
                  openssh-sans-x
                  python
                  python-numpy
                  python-ipython))

    (description "python interface on mseSafranTools for inrae.mse")
    (license license:lgpl3)))

(define-public python-fenics-ufl
  (package
    (name "python-fenics-ufl")
    (version "2023.1.0")
    (home-page "https://github.com/FEniCS/ufl.git")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0nd02wp8v557xisgyajvdk5idhp54swwzcdb6pvqglvnh2zf0d10"))))
    (build-system pyproject-build-system)
    (native-inputs (list python-pytest))
    (inputs (list python-numpy))
    (synopsis "UFL is part of the FEniCS Project")
    (description
     "The Unified Form Language (UFL) is a domain specific language for declaration
      of finite element discretizations of variational forms.  More precisely, it
      defines a flexible interface for choosing finite element spaces and
      defining expressions for weak forms in a notation close to mathematical notation.")
    (license license:lgpl3)))

(define-public python-fenics-ufl-2023-2
  (package
    (name "python-fenics-ufl")
    (version "2023.2.0")
    (home-page "https://github.com/FEniCS/ufl.git")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "11s5l62kbl82p59mv2c5vjcvzn1dr4zkpxjx3bygwiklaqwm72s9"))))
    (build-system pyproject-build-system)
    (native-inputs (list python-pytest))
    (inputs (list python-numpy))
    (synopsis "UFL is part of the FEniCS Project")
    (description
     "The Unified Form Language (UFL) is a domain specific language for declaration
      of finite element discretizations of variational forms.  More precisely, it
      defines a flexible interface for choosing finite element spaces and
      defining expressions for weak forms in a notation close to mathematical notation.")
    (license license:lgpl3)))

(define-public fenics-basix
  (package
    (name "fenics-basix")
    (version "0.6.0")
    (home-page "https://github.com/FEniCS/basix.git")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "14g3hxjzqa8f1kc9jgnpclr8isd1qky2vpy0afky5aih6gd2mfad"))))
    (build-system cmake-build-system)
    (arguments
     (list
      #:configure-flags #~`("-DBUILD_SHARED_LIBS=ON")
      #:tests? #f
      #:phases #~(modify-phases %standard-phases
                   (add-before 'configure 'chdir-to-cpp
                     (lambda _
                       (chdir "cpp"))))))
    (inputs (list openblas))
    (synopsis
     "Basix is one of the components of FEniCSx, alongside UFL, FFCx, and DOLFINx")
    (description
     "Basix is a finite element definition and tabulation runtime library.")
    (license license:lgpl3)))

(define-public fenics-basix-7
  (package
    (name "fenics-basix")
    (version "0.7.0")
    (home-page "https://github.com/FEniCS/basix.git")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "01c4kqc7pbl4f916y0n8cnb5vwci2649jbxm4spfigi1mr59ypm2"))))
    (build-system cmake-build-system)
    (arguments
     (list
      #:configure-flags #~`("-DBUILD_SHARED_LIBS=ON")
      #:tests? #f
      #:phases #~(modify-phases %standard-phases
                   (add-before 'configure 'chdir-to-cpp
                     (lambda _
                       (chdir "cpp"))))))
    (native-inputs (list nanobind python-nanobind))
    (inputs (list openblas))
    (synopsis
     "Basix is one of the components of FEniCSx, alongside UFL, FFCx, and DOLFINx")
    (description
     "Basix is a finite element definition and tabulation runtime library.")
    (license license:lgpl3)))

(define-public python-fenics-basix
  (package/inherit fenics-basix
    (name "python-fenics-basix")
    (build-system pyproject-build-system)
    (arguments (list #:tests? #f))
    (native-inputs (list cmake))
    (inputs (modify-inputs (package-inputs fenics-basix)
              (append openblas)
              (append fenics-basix)
              (append python-wheel)
              (append pybind11)
              (append python-numpy)
              (append python-scikit-build)))))
(define-public python-fenics-basix-7
  (package/inherit fenics-basix-7
    (name "python-fenics-basix")
    (build-system pyproject-build-system)
    (arguments (list #:tests? #f))
    (native-inputs (list cmake nanobind python-nanobind))
    (inputs (modify-inputs (package-inputs fenics-basix-7)
              (append openblas)
              (append fenics-basix-7)
              (append python-wheel)
              (append pybind11)
              (append python-numpy)
              (append python-scikit-build-core)
              (append python-scikit-build)))))

(define-public fenics-ffcx
  (package
    (name "fenics-ffcx")
    (version "0.6.0")
    (home-page "https://github.com/FEniCS/ffcx.git")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1q7kgbnx9nzqv02lsipy64ikxp8i72bahhjmxcwy3s4l5jy7fvh2"))))
    (build-system cmake-build-system)
    (arguments
     (list
      #:configure-flags #~`("-DBUILD_SHARED_LIBS=ON")

      #:tests? #f
      #:phases #~(modify-phases %standard-phases
                   (add-before 'configure 'chdir-to-cmake
                     (lambda _
                       (chdir "cmake"))))))
    (inputs (list python))
    (synopsis "FFCx is a new version of the FEniCS Form Compiler")
    (description
     "FFCx is a compiler for finite element variational forms.  From a
     high-level description of the form in the Unified Form
     Language (UFL), it generates efficient low-level C code that can
     be used to assemble the corresponding discrete operator (tensor).")
    (license license:lgpl3)))

(define-public fenics-ffcx-7
  (package
    (name "fenics-ffcx")
    (version "0.7.0")
    (home-page "https://github.com/FEniCS/ffcx.git")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "06kmqk87fy7mlfs80r676yjcwjrap8hr26w4gqv050lczpnanvxi"))))
    (build-system cmake-build-system)
    (arguments
     (list
      #:configure-flags #~`("-DBUILD_SHARED_LIBS=ON")

      #:tests? #f
      #:phases #~(modify-phases %standard-phases
                   (add-before 'configure 'chdir-to-cmake
                     (lambda _
                       (chdir "cmake"))))))
    (inputs (list python))
    (synopsis "FFCx is a new version of the FEniCS Form Compiler")
    (description
     "FFCx is a compiler for finite element variational forms.  From a
     high-level description of the form in the Unified Form
     Language (UFL), it generates efficient low-level C code that can
     be used to assemble the corresponding discrete operator (tensor).")
    (license license:lgpl3)))

(define-public python-fenics-ffcx
  (package/inherit fenics-ffcx
    (name "python-fenics-ffcx")
    (build-system pyproject-build-system)
    (arguments (list #:tests? #f))
    (inputs (modify-inputs (package-inputs fenics-ffcx)
              (append python-fenics-basix)
              (append python-fenics-ufl)
              (append python-cffi)
              (append python-numpy)
              (append fenics-ffcx)))))

(define-public python-fenics-ffcx-7
  (package/inherit fenics-ffcx-7
    (name "python-fenics-ffcx")
    (build-system pyproject-build-system)
    (arguments (list #:tests? #f))
    (inputs (modify-inputs (package-inputs fenics-ffcx-7)
              (append python-fenics-basix-7)
              (append python-fenics-ufl-2023-2)
              (append python-cffi)
              (append python-numpy)
              (append fenics-ffcx-7)))))

(define-public dolfinx
  (package
    (name "dolfinx")
    (version "0.6.0")
    (home-page "https://github.com/FEniCS/dolfinx")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1sarc8f5mwfq3f0mky14b1vaczs0v8dj49jsy847v8ssz03isj1m"))))
    (build-system cmake-build-system)
    (arguments
     (list
      #:configure-flags #~`("-DBUILD_SHARED_LIBS=ON"
                            "-DDOLFINX_UFCX_PYTHON=OFF"
                            "-DDOLFINX_ENABLE_ADIOS2=OFF"
                            "-DDOLFINX_SKIP_BUILD_TESTS=ON"
                            "-DDOLFINX_ENABLE_SLEPC=OFF"
                            ,(string-append "-DUFCX_INCLUDE_DIR="
                                            #$(this-package-input
                                               "fenics-ffcx") "/include")
                            ,(string-append "-DSCOTCH_ROOT="
                                            #$(this-package-input "pt-scotch"))
                            ,(string-append "-DPARMETIS_ROOT="
                                            #$(this-package-input "parmetis")))
      #:tests? #f ;because we do not know how to use
      #:phases #~(modify-phases %standard-phases
                   (add-before 'configure 'chdir-to-cpp
                     (lambda _
                       (chdir "cpp"))))))
    (native-inputs (list pkg-config python-pytest))
    (inputs (list openmpi
                  scotch
                  zlib
                  pt-scotch
                  parmetis
                  python-mpi4py
                  python-numpy
                  metis
                  hdf5-geos
                  python
                  pugixml
                  petsc-openmpi
                  boost
                  fenics-basix
                  fenics-ffcx))
    (synopsis "New version of DOLFIN and is being actively developed")
    (description
     "DOLFINx is the computational environment of FEniCSx and
     implements the FEniCS Problem Solving Environment in C++ and Python.")
    (license license:lgpl3)))

(define-public dolfinx-7
  (package
    (name "dolfinx")
    (version "0.7.0")
    (home-page "https://github.com/FEniCS/dolfinx")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1p87r76046xi7fvviywrs61dqwqisrdjn96sbslsfabpmlk8b5vw"))))
    (build-system cmake-build-system)
    (arguments
     (list
      #:configure-flags #~`("-DBUILD_SHARED_LIBS=ON"
                            "-DDOLFINX_UFCX_PYTHON=OFF"
                            "-DDOLFINX_ENABLE_ADIOS2=OFF"
                            "-DDOLFINX_SKIP_BUILD_TESTS=ON"
                            "-DDOLFINX_ENABLE_SLEPC=OFF"
                            ,(string-append "-DUFCX_INCLUDE_DIR="
                                            #$(this-package-input
                                               "fenics-ffcx") "/include")
                            ,(string-append "-DSCOTCH_ROOT="
                                            #$(this-package-input "pt-scotch"))
                            ,(string-append "-DPARMETIS_ROOT="
                                            #$(this-package-input "parmetis")))
      #:tests? #f ;because we do not know how to use
      #:phases #~(modify-phases %standard-phases
                   (add-before 'configure 'chdir-to-cpp
                     (lambda _
                       (chdir "cpp"))))))
    (native-inputs (list pkg-config python-pytest))
    (inputs (list openmpi
                  scotch
                  zlib
                  pt-scotch
                  parmetis
                  python-mpi4py
                  python-numpy
                  metis
                  hdf5-geos
                  python
                  pugixml
                  petsc-openmpi
                  boost
                  fenics-basix-7
                  fenics-ffcx-7))
    (synopsis "New version of DOLFIN and is being actively developed")
    (description
     "DOLFINx is the computational environment of FEniCSx and
     implements the FEniCS Problem Solving Environment in C++ and Python.")
    (license license:lgpl3)))

(define-public python-dolfinx
  (package/inherit dolfinx
    (name "python-dolfinx")
    (build-system python-build-system)
    (arguments (list #:tests? #f ;because we do not know how to use
                     #:phases #~(modify-phases %standard-phases
                                  (add-before 'build 'chdir-to-python
                                    (lambda _
                                      (chdir "python"))))))
    (native-inputs (modify-inputs (package-native-inputs dolfinx)
                     (append cmake)))
    (inputs (modify-inputs (package-inputs dolfinx)
              (append python-petsc4py openssh-sans-x)
              (append python-fenics-basix python-fenics-ufl python-fenics-ffcx)
              (append python-cffi pybind11)
              (append dolfinx)))))

(define-public python-dolfinx-7
  (package/inherit dolfinx-7
    (name "python-dolfinx")
    (build-system python-build-system)
    (arguments (list #:tests? #f ;because we do not know how to use
                     #:phases #~(modify-phases %standard-phases
                                  (add-before 'build 'chdir-to-python
                                    (lambda _
                                      (chdir "python"))))))
    (native-inputs (modify-inputs (package-native-inputs dolfinx)
                     (append cmake)))
    (inputs (modify-inputs (package-inputs dolfinx-7)
              (append python-petsc4py openssh-sans-x)
              (append python-fenics-basix-7 python-fenics-ufl-2023-2
                      python-fenics-ffcx-7)
              (append python-cffi pybind11)
              (append dolfinx-7)))))

(define-public nanobind
  (package
    (name "nanobind")
    (version "v?")
    (home-page "https://github.com/wjakob/nanobind")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit "b3b6f44e55948986e02cdbf67e04d9cdd11c4aa4")
             (recursive? #t)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0iqqivansbw92bkbfbs9i9xxpghcnvp93zdcyvcc95f8b3bqv5kb"))))
    (build-system cmake-build-system)
    (arguments
     (list
      #:phases #~(modify-phases %standard-phases
                   (replace 'check
                     (lambda _
		       (system "pwd")
                       (invoke "pytest")
		       ))
      )))
    (synopsis "nanobind for python bind")
    (native-inputs (list pkg-config swig python-wheel python-pytest))
    (inputs (list openmpi openssh-sans-x python ))
    (description "simulation et estimations d'edps")
    (license license:bsd-3)))

(define-public python-nanobind
  (package/inherit nanobind
    (name "python-nanobind")
    (build-system python-build-system)
    (arguments (list #:tests? #f))
    (synopsis "python-nanobind")
    (inputs (list nanobind))
    (description "simulation et estimations d'edps")
    (license license:bsd-3)))


