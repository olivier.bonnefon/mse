(specifications->manifest
  (list "python"
	"which"
        "less"
	"grep"
        "openssh-sans-x"
        "vim"
        "gcc-toolchain@12.3.0"
	"pkg-config"
	"cmake"
	"coreutils"
	"make"))
