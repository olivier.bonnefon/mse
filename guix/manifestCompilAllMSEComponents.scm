(specifications->manifest
  (list "python"
	"which"
        "less"
	"grep"
        "petsc-openmpi"
	"python-petsc4py"
	"openmpi@4.1.6"
        "openssh-sans-x"
        "vim"
        "gcc-toolchain@12.4.0"
	"pkg-config"
	"hdf5-geos"
	"cmake"
	"coreutils"
	"make"
	"eigen"
	"ddd"
	"gdb"
	"valgrind"
  	"swig"
	"python-numpy"
	"python-wheel"
	"python-setuptools"
        "python-matplotlib"
	"python-pyqt@5.15.9"; for visu with matplotlib (in viewTopo.py)
        "vim"
	"less"
	"sphinxbase"
	"python-sphinx"
	"python-breathe"
	"python-sphinx-gallery"
	"python-sphinx-rtd-theme"
	"python-pytest"
	"python-sympy"
	"python-scipy"
	"eigen"
	"paraview"
	"mseNetcdf"
	"gdal"))

