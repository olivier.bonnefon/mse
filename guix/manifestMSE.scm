(specifications->manifest
  (list "python"
        "python-mse@v2.2"
        "coreutils"
        "which"
        "less"
        "python-numpy"
        "petsc-openmpi"
        "python-petsc4py"
        "openmpi@4.1.6"
        "openssh-sans-x"
        "python-setuptools"
        "python-matplotlib"
	"python-pyqt@5.15.9"; for visu with matplotlib (in viewTopo.py)
        "gmsh"
        "python-meshio"
        "python-mpi4py"
        "vim"
        "gcc-toolchain@12.4.0"
	"python-scipy"
        "mseMapTools@v2.2"
        "mseMatTools@v2.2"
	"mseSafranTools@v2.2"
	"pymseMapTools@v2.2"
        "pymseMatTools@v2.2"
	"pymseSafranTools@v2.2"
	"python-sympy"
	"paraview"))

