import pymseMatTools.mseMatTools as MT
import numpy as np
import os
NCOL=20
NROW=20


Xcoords=np.tile(np.arange(start=0,stop=NCOL,step=1, dtype=np.double),NROW)
Ycoords=np.zeros(NCOL*NROW, dtype=np.double)
for i in range(20): Ycoords[i*NCOL:(i+1)*NCOL]=i

dofs=np.zeros(NCOL*NROW,np.double)
returnV=np.array([0],dtype=np.int32)
absPath=os.getcwd().split("trunk")[0]+"/trunk/mseMatTools/RASTER/"
MT.fillFromRaster(absPath+"/testRaster",dofs,Xcoords,Ycoords,returnV)
for i in range(20): print(dofs[i*NCOL:(i+1)*NCOL])
res=0
for i in range(10,20):
    if (dofs[i*NCOL] != 1 or dofs[i*NCOL+5] != 1):
        res=1
for i in [0,1]:
    if (dofs[i*NCOL+NROW-1] != 1 or dofs[i*NCOL+NROW-4] != 1 or dofs[i*NCOL+NROW-5] != 0):
        res=2
print("test value="+str(res))
def test_raster():
    return res
