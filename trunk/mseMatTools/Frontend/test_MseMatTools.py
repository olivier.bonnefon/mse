from petsc4py import PETSc
import pymseMatTools.mseMatTools as MT
print( MT.__file__)
print("TEST MATRIX")

n=10
aPetscMat1=PETSc.Mat()
aPetscMat1.createAIJ([n,n])
aPetscMat1.setUp()
aPetscMat1.assemble()
aPetscMat3=PETSc.Mat()
aPetscMat3.createAIJ([n,n])
aPetscMat3.setUp()
aPetscMat3.assemble()
MT.getPetscId(aPetscMat1);
for i in [0,1,3,4,5,9]:
    for j in [0,2,4,6,8]:
        aPetscMat1.setValue(i,j,i+j)
        aPetscMat3.setValue(i,j,i+j)

aPetscMat1.assemblyBegin() # Assembling the matrix makes it "useable".
aPetscMat1.assemblyEnd()
MT.printMat(aPetscMat1)
aPetscMat3.assemblyBegin() 
aPetscMat3.assemblyEnd()


m=5
aPetscMat2=PETSc.Mat()
aPetscMat2.createAIJ([m,m])
aPetscMat2.setUp()
aPetscMat2.assemble()
offsetRow=2
offsetCol=1
for i in [0,1,4]:
    for j in [0,2,4]:
        aPetscMat2.setValue(i,j,i+0.5)
        aPetscMat3.setValue(i+offsetRow,j+offsetCol,i+0.5,PETSc.InsertMode.ADD_VALUES)

aPetscMat2.assemblyBegin() 
aPetscMat2.assemblyEnd()
aPetscMat3.assemblyBegin() 
aPetscMat3.assemblyEnd()
MT.assembleMat(aPetscMat1,aPetscMat2,offsetRow,offsetCol)
MT.printMat(aPetscMat1)
MT.printMat(aPetscMat3)

aPetscMat3.axpy(-1,aPetscMat1)
print("the folowing matrix must be null:")
aPetscMat3.view()

if (aPetscMat3.norm() > 1e-10):
    print("BUG")
else:
    print("TEST MATRIX OK");

print("TEST VECTOR")

aPetscVec1=PETSc.Vec()
aPetscVec1.createSeq(n)
aPetscVec3=PETSc.Vec()
aPetscVec3.createSeq(n)

aPetscVec2=PETSc.Vec()
aPetscVec2.createSeq(m)

for i in [0,1,3,4,5,9]:
    aPetscVec1.setValue(i,i)
    aPetscVec3.setValue(i,i)

for i in [0,1,4]:
    aPetscVec2.setValue(i,i+0.5)
    aPetscVec3.setValue(i+offsetRow,i+0.5,PETSc.InsertMode.ADD_VALUES)

MT.assembleVec(aPetscVec1,aPetscVec2,offsetRow)

aPetscVec1.view()
aPetscVec3.view()

if (abs((aPetscVec3-aPetscVec1).norm()) > 1e-10):
    print("BUG")
    exit(1)
else:
    print("TEST VECTOR OK");

print("TEST LOAD_VEC")
import numpy as np
from numpy import linalg as LA
inArray=np.array([1.0,2.0,3.0], dtype=np.double)
MT.saveVec("testLoad",inArray);

outArray=np.array([0.0,1.0,1.0,4.0], dtype=np.double)
MT.loadVec("testLoad",outArray);
print(outArray)
outWait=np.array([1.0,2.0,3.0,4.0], dtype=np.double)
if (abs(LA.norm(outArray-outWait)) > 1e-10):
    exit(2)
outWait=np.array([-1.0,-2.0,-3.0,4.0], dtype=np.double)
outArray=np.array([0.0,.0,.0,4.0], dtype=np.double)

outBuf=np.array([0.0,0.0,0.0,4.0], dtype=np.double)
MT.addInPlaceVec("testLoad",outArray,outBuf,-1)
if (abs(LA.norm(outArray-outWait)) > 1e-10):
    exit(3)

#################
print("test addMatMapij")
# test addMatMapij
aPetscMat4=PETSc.Mat()
aPetscMat4.createAIJ([n,n])
aPetscMat4.setUp()
aPetscMat4.assemble()
aPetscMat5=PETSc.Mat()
aPetscMat5.createAIJ([n,n])
aPetscMat5.setUp()
aPetscMat5.assemble()
for i in range(n):
    aPetscMat4.setValue(i,i,1)
    aPetscMat5.setValue((i+1)%n,i,1)

aPetscMat4.assemblyBegin()
aPetscMat4.assemblyEnd()

aPetscMat5.assemblyBegin()
aPetscMat5.assemblyEnd()
iMap = np.array([0 for _ in range(10)],dtype=np.int32)
jMap = np.array([(i+1)%n for i in range(10)],dtype=np.int32)
# test
# Call addMatMapij with alpha=0
aPetscMat5.view()
print("matrix before changement")
aPetscMat4.view()
MT.addMatMapij(aPetscMat4,-1,aPetscMat5,iMap,jMap,0,1)
print("matrix after changement")
aPetscMat4.view()
if (aPetscMat4.norm() > 1e-10):
    print("BUG")
    exit(4)
#2nd test
#MT.addMatMapij(aPetscMat4,0,aPetscMat5,iMap,jMap,0,0)
#third test
#MT.addMatMapij(aPetscMat4,0,aPetscMat5,iMap,jMap,0,0)

###############
def test_mat():
    return 0
