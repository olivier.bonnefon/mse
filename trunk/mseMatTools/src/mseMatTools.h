#ifndef MAT_TOOLS_PETSC
#define MAT_TOOLS_PETSC
#include <petscmat.h>

/*Mat is from Petsc and is a pointer, 
so we copy only the value of pointer, the matrix is not duplicated.*/
void getPetscId(Mat aMat);
void assembleMat(Mat pMatOut, Mat pMatIn, int NOffset, int MOffset);
void assembleVec(Vec pVecOut,Vec pVecIn,int Offset);
/*
*#pMatOut = pMatOut + pMatIn[iMap[i],iMap[j]]
* memo: numpy.i export array in  (int *iMap, int iLength)
**/
void addMatMapij(Mat pMatOut, double alpha, Mat pMatIn, int *iMap, int iLength, int* jMap, int jLength, int iMask, int jMask);
void printMat(Mat aMat);
void saveVec(char* fileName,double *dVec, int dLength);
int loadVec(char* fileName,double *dVec, int dLength);
//do dVec+=coef*(content of fileName)
//dBuf must be allocated, min size is size of dVec
void addInPlaceVec(char* fileName,double *dVec, int dLength,double *dBuf,int dLengthBuf,double coef);
void fillFromRaster(char* fileName,
		    double *dVec1, int dLength1,
		    double *dVec2, int dLength2,
		    double *dVec3, int dLength3,
		    int* returnValue,int dL);
void readXYZ(double *dVec1, int dLength1);
#endif
