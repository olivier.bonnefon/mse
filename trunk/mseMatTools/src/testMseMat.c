#include "mseMatTools.h"
#include <petsc.h>

int main(int argc, char **argv){
    PetscErrorCode ierr;

     // Initialize PETSc
    ierr = PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL); CHKERRQ(ierr);
	     
    Mat A;
    MatCreate(PETSC_COMM_WORLD, &A);
    getPetscId(A); /*not tested*/
    return 0;
}
