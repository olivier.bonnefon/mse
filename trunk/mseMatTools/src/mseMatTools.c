#include "mseMatTools.h"
#include <petscmat.h>
#include "stdio.h"
#include <stdlib.h>
//#define mseMatToolsS_VERBOSE

void getPetscId(Mat aMat){
    printf("Begin getPetscId2\n");
    PetscInt N,M;
    MatGetSize(aMat,&N,&M);
    printf("size %i %i\n",N,M);
}

void assembleMat(Mat matOut, Mat matIn,int NOffset, int MOffset){
    PetscInt NO,MO;
    MatGetSize(matOut,&NO,&MO);
    PetscInt NI,MI,i,ii,iOffseted;
    const PetscInt *aj;
    PetscInt *ajOffseted;
    PetscInt nc;
    const PetscScalar *aa;
    MatGetSize(matIn,&NI,&MI);
    PetscMalloc1(MI, &ajOffseted);
#ifdef mseMatToolsS_VERBOSE
      printf("mseMatToolsS assembleMat No=%i Mo=%i Ni=%i+%i Mi=%i+%i.\n",NO,MO,NI,NOffset,MI,MOffset);
#endif
    if (NOffset+NI>NO || MOffset+MI>MO){
        printf("mseMatToolsS ERROR \n assembleMat wrong size or offset:\n");
        printf(" NOffset+NI %i > %i NO\n",NOffset+NI,NO);
        printf(" MOffset+MI %i > %i MO\n",MOffset+MI,MO);
        return;
    }
    //get values from matIn
    
    for (i = 0; i < NI; i++) {
        //nc non null in row i.
        //aj the col numbers
        //aa the values
        MatGetRow(matIn, i, &nc, &aj, &aa);
#ifdef mseMatToolsS_VERBOSE
            printf("mseMatToolsS inMat row %i, with %i nc\n",i,nc);
#endif
        for (ii=0;ii<nc;ii++)
            ajOffseted[ii]=aj[ii]+MOffset;
        iOffseted=i+NOffset;
        MatSetValues(matOut,1,&iOffseted,nc,ajOffseted,aa,ADD_VALUES);
    }
    MatAssemblyBegin(matOut, MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(matOut, MAT_FINAL_ASSEMBLY);
 
    //PetscErrorCode MatSetValues(Mat mat, PetscInt m, const PetscInt idxm[], PetscInt n, const PetscInt idxn[], const PetscScalar v[], InsertMode addv)
    //ADD_VALUES or INSERT_VALUES
    PetscFree(ajOffseted);
}

/*
*#pMatOut = pMatOut + pMatIn[iMap[i],iMap[j]]
**/
void addMatMapij(Mat matOut, double alpha, Mat matIn, int *iMap, int iLength, int *jMap, int jLength, int iMask, int jMask){
  PetscInt NO,MO;
  MatGetSize(matOut,&NO,&MO);
  PetscInt NI,MI,i,ii,iOffseted;
  const PetscInt *aj;
  PetscInt *ajOffseted;
  PetscInt nc;
  const PetscScalar *aa;
  MatGetSize(matIn,&NI,&MI);
  PetscMalloc1(MI, &ajOffseted);
  PetscScalar *copyaa;
  PetscMalloc1(NI, &copyaa);
#ifdef mseMatToolsS_VERBOSE
    printf("mseMatToolsS addMatMapij No=%i Mo=%i Ni=%i Mi=%i.\n",NO,MO,NI,MI);
    if (iMask && jMask)
      printf("   doing matOut[iMap[i],jMap[j]]+=matIn[i,j]\n");
    else if (iMask)
      printf("   doing matOut[iMap[i],j]+=matIn[i,j]\n");
    else if (jMask)
      printf("   doing matOut[i,jMap[j]]+=matIn[i,j]\n");
#endif

  for (i = 0; i < NI; i++) {
    //nc non null in row i.
    //aj the col numbers
    //aa the values
    MatGetRow(matIn, i, &nc, &aj, &aa);
    PetscMemcpy(copyaa,aa,nc*sizeof(aa));
		
#ifdef mseMatToolsS_VERBOSE
    printf("mseMatToolsS addMatMapij row %i, with %i nc\n",i,nc);
#endif
    for (ii=0;ii<nc;ii++){
      ajOffseted[ii]=jMask?jMap[aj[ii]]:aj[ii];
      copyaa[ii]=alpha* copyaa[ii];
    }
    iOffseted=iMask?iMap[i]:i;
    MatSetValues(matOut,1,&iOffseted,nc,ajOffseted,copyaa,ADD_VALUES);
  }
  MatAssemblyBegin(matOut, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(matOut, MAT_FINAL_ASSEMBLY);
 
  PetscFree(ajOffseted);
  PetscFree(copyaa);
}


void assembleVec(Vec vecOut,Vec vecIn,int Offset){
    PetscInt NO,NI;
    VecGetSize(vecOut,&NO);
    VecGetSize(vecIn,&NI);
#ifdef mseMatToolsS_VERBOSE
        printf("mseMatToolsS assembleVec No=%i Ni=%i .\n",NO,NI);
#endif
    if (Offset+NI>NO){
        printf("mseMatToolsS ERROR \n assembleVec wrong size or offset:\n");
        printf(" Offset+NI %i > %i NO\n",Offset+NI,NO);
        return;
    }
    PetscScalar *pV;
    VecGetArray(vecIn,&pV);
    for (int i = Offset; i < (NI+Offset) ; i++){
        VecSetValues(vecOut, 1, &i, pV+(i-Offset), ADD_VALUES);
    }
}
void printMat(Mat aMat){
  int i,j;
  printf("mseMatToolsS PRINTING MAT\n");
  PetscInt N,M;
  MatGetSize(aMat,&N,&M);
  printf("%i %i\n",N,M);
  const PetscInt *aj;
  const PetscScalar *aa;
  PetscInt *ajOffseted;
  PetscInt nc;
  for (i = 0; i < N; i++) {
    MatGetRow(aMat, i, &nc, &aj, &aa);
    for (j=0;j<nc;j++)
      printf("%i(%e) ",aj[j],aa[j]);
    printf("\n");
  }
}


void saveVec(char* fileName,double *dVec, int dLength){
#ifdef mseMatToolsS_VERBOSE
  printf("matToolsPetsc saveVec %s of size %i\n",fileName,dLength);
#endif
  FILE *pFb=fopen(fileName,"wb");
  if (!pFb){
	printf("ERROR mseMatTools saveVec can't open file %\n",fileName);
	return;
  }

  fwrite((char*)&dLength,sizeof(int),1,pFb);
  fwrite((char*)dVec, sizeof(double), dLength,pFb);
  fclose(pFb);
}
int loadVec(char* fileName,double *dVec, int dLength){
#ifdef mseMatToolsS_VERBOSE
  printf("matToolsPetsc loadVec %s in size %i\n",fileName,dLength);
#endif
  FILE *pFb=fopen(fileName,"rb");
  if (!pFb){
	printf("ERROR mseMatTools loadVec can't open file %s\n",fileName);
	return 1;
  }

  int dim;
  fread((char *)&dim, sizeof( int),1,pFb);
#ifdef mseMatToolsS_VERBOSE
  printf("matToolsPetsc loadVec loadind %i in %i.\n",dim,dLength);
#endif
  fread((char *)dVec, sizeof(double),dim,pFb);
  fclose(pFb);
  return 0;
}
/*
 * Read the content of fileName and add the content multipli by coef in dVec
 *
 *
 */
void addInPlaceVec(char* fileName,double *dVec, int dLength,double *dBuf,int dLangthBuf,double coef){
  int i;	
#ifdef mseMatToolsS_VERBOSE
  printf("matToolsPetsc addInPlaceVec %s in size %i\n",fileName,dLength);
  printf("matToolsPetsc addInPlaceVec %p %p\n",dVec,dBuf);
#endif
  FILE *pFb=fopen(fileName,"rb");
  int dim;
  fread((char *)&dim, sizeof( int),1,pFb);
#ifdef mseMatToolsS_VERBOSE
  printf("matToolsPetsc AddInPlaceVec loadind %i in %i.\n",dim,dLength);
#endif
  fread((char *)dBuf, sizeof(double),dim,pFb);
  fclose(pFb);
#ifdef mseMatToolsS_VERBOSE
  printf("matToolsPetsc loaded\n");
#endif
  for (i=0;i<dim;i++){
	  *dVec=*dVec+coef*(*dBuf++);
	  dVec++;
#ifdef mseMatToolsS_VERBOSE
  printf("matToolsPetsc %i \n",dim);
#endif
   }
}
void readXYZ(double *XYZ, int dLengthXYZ){
  printf("XYZ length=%d\n",dLengthXYZ);
}

int readRaster(char * rasterFile,int nDoubles,double *pData){
   
   FILE *pF=fopen(rasterFile,"r");
   if (!pF){
	   printf(" mseMatTools, readraster, can't open raster file %s\n",rasterFile);
	   return 1;
   }
   for (int k=0;k<nDoubles;k++){
     fscanf(pF,"%le",pData++);
     //printf("%le ",*(pData-1));
   }
   return 0;
}
#define PRINT_INFORASTER_FORMAT  printf("mseMatTools Info, fillFromRaster readRasterInfo format must be %s\n","Nrow\t Ncol\t rowMajeur\t xmin\t xmax\t ymin\t ymax\t XBeginLeft\t YBeginTop\t adjustOnMesh \n int\t int\t bool\t real\t real\t real\t real\t bool\t bool\t bool ");



/* about fillfromRaster
 *
 *
 * illustration with testRasterInfo.txt
 *  8	9	1	0.	0.	0.	0.	1	1	1
 *
 * testRaster:
 * ::::::::::::::
*  testRaster tij:
*  ::::::::::::::
*  1.0	1.0	1.0	0.	0.	0.	0.	0.	0.
*  1.0	1.0	1.0	0.	0.	0.	0.	0.	0.
*  1.0	1.0	1.0	0.	0.	0.	0.	0.	0.
*  1.0	1.0	1.0	0.	0.	0.	0.	0.	0.
*  0.0	0.0	0.0	0.	0.	0.	0.	0.	0.
*  0.0	0.0	0.0	0.	0.	0.	0.	0.	0.
*  0.0	0.0	0.0	0.	0.	0.	0.	0.	0.
*  0.0	0.0	0.0	0.	0.	0.	0.	1.	1.
 *	
 *
 *  ------------------------------> X
 *  |     t.0 t.1 t.2
 *  |      -
 *  |t0. | x | x | x | x   x   x   x   x  x    
 *  |      -
 *  |t1.   x
 *  |      -
 *  |      x
 *  |      -
 *  |      x
 *  |
 *  |      x
 *  |
 *  |      x
 *  |
 *  |      x
 *  |
 *  |      x
 *  |
 *  |
 *  |
 *  |
 *  |
 *  V
 * 
 *  Y
 *
 *
 *   dx=Xrange/Ncol
 *   dy=Yrange/Nrow
 *
 *
 *  int numCol=(int)(pX[k]-box[0])/dx+0.5;
 *  int numRow=(int)((pY[k]-box[2])/dy+0.5)
 *
 * leading on Px and Py a regular 20x20 grild:
 *111111100000000000000
 *111111100000000000000
 *111111100000000000000
 *111111100000000000000
 *111111100000000000000
 *111111100000000000000
 *111111100000000000000
 *111111100000000000000
 *111111100000000000000
 *111111100000000000000
 *100000000000000000000
 *100000000000000000000
 *100000000000000000000
 *100000000000000000000
 *100000000000000000000
 *100000000000000000000
 *100000000000000000000
 *100000000000000000000
 *100000000000000001111
 *100000000000000001111
 *
 *
 *
 *
 *
 *
 *
 */
void fillFromRaster(char* rasterFile,
		    double *pDataNodes, int dLength,
		    double *pX, int dLengthX,
		    double *pY, int dLengthY,
		    int* returnValue,int dL){
  *returnValue=0;
  if (dLength!= dLengthX || dLengthX != dLengthY){
    printf("mseMatTools Error, fillFromRaster dLength!= dLengthX or dLengthX != dLengthY\n");
    *returnValue=1;
    return;
  }
  int Nrow;
  int Ncol;
  int rowMajeur;//1 means raster is range along X axis.
  int adjustOnMesh=1;
  double box[4];
  int XBeginLeft,YBeginTop;
  char fileName[1024];
  //read info raster
  sprintf(fileName,"%sInfo.txt",rasterFile);
  FILE *pF=fopen(fileName,"r");
  if (!pF){
    printf("mseMatTools Error, fillFromRaster readRasterInfo can't find file %s\n",fileName);
    PRINT_INFORASTER_FORMAT;
    *returnValue=2;
    return;
  }
  int resScan=fscanf(pF,"%i%i%i%le%le%le%le%i%i",&Nrow,&Ncol,&rowMajeur,box,box+1,box+2,box+3,&XBeginLeft,&YBeginTop,&adjustOnMesh);
   if (resScan == 7){
    PRINT_INFORASTER_FORMAT 
    *returnValue=3;
    return;
  }
#ifdef mseMatToolsS_VERBOSE
   printf("mseMatTools info, fillFromRaster readRasterInfo done :\n \t %s Nrow=%i Ncol=%i rowMajeur=%i box= %le %le %le %le XBeginLeft=%i YBeginTop=%i adjustOnMesh=%i\n",rasterFile,Nrow,Ncol,rowMajeur,box[0],box[1],box[2],box[3],XBeginLeft,YBeginTop,adjustOnMesh);
#endif
   if (adjustOnMesh){
     box[0]=pX[0];
     box[1]=pX[0];
     box[2]=pY[0];
     box[3]=pY[0];
     for (int i=1;i<dLengthX;i++){
       if (pX[i]<box[0]) box[0]=pX[i];
       if (pX[i]>box[1]) box[1]=pX[i];
       if (pY[i]<box[2]) box[2]=pY[i];
       if (pY[i]>box[3]) box[3]=pY[i];
       
     }
#ifdef mseMatToolsS_VERBOSE
     printf("mseMatTools info, fillFromRaster adjusted box %le %le %le %le \n",box[0],box[1],box[2],box[3]);
#endif
   }
   
  double * pData=(double*)malloc(Nrow*Ncol*sizeof(double));
  if (!pData){
	  *returnValue=5;
	  printf("mseMatTools Error, can't alloc memory\n");
	  return;
  }
  if (readRaster(rasterFile,Nrow*Ncol,pData))
    {*returnValue=4;
	free(pData);
	return;}

  double dx=(box[1]-box[0])/(1.0*(Ncol-1));
  double dy=(box[3]-box[2])/(1.0*(Nrow-1));
#ifdef mseMatToolsS_VERBOSE
  printf("mseMatTools Info, fillFromRaster fillFromRaster dx=%e dy=%e\n",dx,dy);
#endif
  for (int k=0;k<dLength;k++){
    int numCol=XBeginLeft?(int)((pX[k]-box[0])/dx+0.5):(int)(box[1]-pX[k])/dx+0.5;
    if (numCol <0) numCol=0;
    if (numCol > Ncol-1) numCol=Ncol-1;
    int numRow=YBeginTop?(int)((pY[k]-box[2])/dy+0.5):(int)((box[3]-pY[k])/dy+0.5);
    if (numRow <0) numRow=0;
    if (numRow > Nrow-1) numRow=Nrow-1;
    if (!rowMajeur)
      pDataNodes[k]=pData[numCol*Nrow+numRow];
    else
      pDataNodes[k]=pData[numRow*Ncol+numCol];
  }
  free(pData);
#ifdef mseMatToolsS_VERBOSE
  printf("mseMatTools info, fillFromRaster end fillCovariable \n");
#endif
}

