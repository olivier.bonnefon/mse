Explore trajectory
------------------


.. autoclass:: mse.explorTrajectory.explorTrajectory
   :members: interpolToTime, rewind, replay, stopReplay
   :noindex:

