.. MSE is a software dedicated to the simulation and estimation of derministe system reaction-diffision PDE in the INRAE context. 


:gitmia_url: https://forgemia.inra.fr/olivier.bonnefon/mse/
   
.. toctree::
   :hidden:
   :maxdepth: 3
   
   ./about.rst
   ./mathContext/mathContext.rst
   ./tutorials/tutorials.rst
   ./sofwareStruc/modules.rst
   ./gallery/gallery.rst
   ./references.rst
   ./objStruct/modules.rst
   install_guide/index
   license
   

.. |imag1| image::  /figures/illustrationMecaStat.png
   :height: 150

.. |imag2| image::  /figures/maillageBassin.png
   :height: 150

.. |video1| image:: /figures/movies/optimGrippe.gif
   :height: 150


.. include:: about.rst                   

.. image:: /figures/illustrationMecaStat.png
        :height: 300
        :align: center 

.. csv-table:: 

   .. include:: teaser.rst, |video1|, |imag2|

.. include:: contacts.rst


