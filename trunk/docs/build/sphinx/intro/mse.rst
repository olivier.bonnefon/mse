Mecanistic-Statistical Environment (MSE)
========================================

It is a python-based library facilitating studies based on spatiotemporal models in epidemiology or population dynamics. This environment aims to be usable by non-experts in computation. The goal is to make partial differential equation models accessible and enable their adjustment. The BioSP research unit focuses on this research and engineering theme, with numerous studies and publications conducted. The objective now is to make this work available to the widest range of users.

The mechanistic-statistical approach involves comparing parameterized mechanistic models with observed or measured data. The aim is to best fit the model. To achieve this, an observation process is introduced to measure the likelihood of the data for a set of fixed parameters. This can involve an optimization problem or Bayesian inference."
