.. _mTethathContext_mse:


Algorithms and mathematical aspects
===================================


Reaction diffusion models
-------------------------
MSE works with models based on parabolic partial differential equation of the following form :eq:`eq:model1`:

.. math::
        :label: eq:model1

        \begin{equation}
        \left\{ \begin{array}{lcll}
        \partial _t U(t,x) &=& \Delta D(x,\Theta)U(t,x)+F(\Theta,U(t,x)) &, \text{ pour $x\in \Omega$, $t\in]0,T]$}\\
        U(0,x)&=&U_0(x,\Theta)&, \text{ pour $x\in \Omega$}\\
        \vec{\nabla} D(x,\Theta)U(t,x).\vec{n}&=&0&, \text{pour $x\in \delta \Omega$, $t\in]0,T]$}\\
        \end{array}
        \right.
        \end{equation}

Where :math:`\Omega` is the study zone of dimension 1 or 2. The bondary condition on :math:`\delta \Omega` depends on the study and will be adjustied in case of interaction with the border.

Diffusion term

The diffusion process is represented by the term :math:`\Delta D(x,\Theta)U(t,x)`. In MSE, this part of the model is implemented in the class :ref:`diffusion_class`.

Sources terms

The sources terms are contains in :math:`F(\Theta,U(t,x))`. These terms are implemented in the class :ref:`model_class`, which uses the model's database. It is also possible to defined new ones using a simple textual description. The relations between compartments are also defined in these terms.

Initial state

The initial state of the system :math:`U_0(x,\Theta)` is defined in the dynamical system class :ref:`ds_class`.

Observation Process
-------------------
At this step it can be useful to see the MSE simulator has the function :eq:`eq:simulator`:

 .. math::
        :label: eq:simulator

        \begin{equation}
        \left\{ \begin{array}{lcl}
                S&:& \mathbb{R}^p \mapsto ((t,\Omega) \mapsto \mathbb{R}^m)\\
                && \Theta \to S(\Theta), \text{ The funtion satisfyng (1) for $\Theta$}.
                \end{array}
         \right.
         \end{equation}

Let :math:`\mathcal{L}` in :eq:`eq:llh` be a function that can represent a likelihood, a log-likelihood, a contrat function. This function can describe an observation process. The implementation is in the class :ref:`obs_process_class`.

.. math::
        :label: eq:llh

        \begin{equation}
        \left\{ \begin{array}{lcl}
                \mathcal{L}&:& ((t,\Omega) \mapsto \mathbb{R}^m) \mapsto \mathbb{R}\\
                && U \to \mathcal{L}(U), \text{ eg: the likelihood of $U$}.
                \end{array}
         \right.
         \end{equation}

Optimisation, Maximum likelihood Edstimation  
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

MSE provides algorithms (BFGS, system adjoint, MCMC) to estimate the parameters of the model. It consists in analysising the function :eq:`eq:objectif`.

.. math::
        :label: eq:objectif

        \begin{equation}
        \left\{ \begin{array}{lcl}
        \mathcal{O}&:& (\mathbb{R}^p) \mapsto \mathbb{R}\\
         && \Theta \to \mathcal{O}(\Theta) =\mathcal{L}(S(\Theta)), \text{ eg : the likelihood of $\Theta$}.
          \end{array}
        \right.
        \end{equation}


Because optimisation requires computing the variation of the likelihood, MSE provides algorithms to perform this task.

Computing variations :math:`\partial_{\theta_i} \mathcal{O}(\Theta)` using adjoint method
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Assumptions and notations

- the likelihood in :eq:`eq:llh` can be written as :math:`\mathcal{L}(U)=\int_Q v(u)(x,t)` with :math:`Q=\Omega \times [0,T]`.,
- :math:`H` is the Hilbert functions  :math:`Q\to\mathbb{R}`, with the dot product :math:`<u,v>_H=\int_Q u.v`,
- :math:`v(u) \in H`,
- :math:`v'(u) \in L(H,H)` is :math:`\partial _u v(u)` such that :math:`v(u+\delta u)=v(u)+v'(u)(\delta u) + o(\delta u)`
- :math:`v'(u)` can be written as :math:`v'(u)=(\delta u)(x,t)=\tilde v'(u)(x,t) . \delta u(x,t) \in \mathbb{R}` 
- leading to :math:`\mathcal{L}'(U)(\delta u)=<\tilde v'(u),\delta u>_H`

Let :math:`S` play the role of a simulator parameterized by :math:`Theta`:

.. math::
   \begin{array}{llll}
   S  :&  \mathbb{R}^p \to H\\
   &\Theta \to S(\Theta)=u_\Theta
   \end{array}

Linearization of :math:`S` at :math:`\Theta`:

.. math::
   \begin{array}{llll}
   S'(\Theta) :&  \mathbb{R}^p \to H\\
   &d \to \lim_{e\to 0}\frac{S(\Theta+ed)-S(\Theta)}{e}
   \end{array}

Operator :math:`S'(\Theta)^*`:

.. math::
   \begin{array}{llll}
   S'(\Theta)^* :&  H \to \mathbb{R}^p\\
   & \text{such that } \forall (x,y)\in H\times\mathbb{R}^p, \langle x,S'(\Theta) (y)\rangle_H=\langle S'(\Theta)^*(x),y\rangle_{\mathbb{R}^p}
   \end{array}

## Using the implicit function theorem

:math:`\mathcal{O}` as an objective function of a parameter:

.. math::
   \begin{array}{llll}
   \mathcal{O}:&\mathbb{R}^p &\to &\mathbb{R}\\
   &a&\mapsto &\mathcal{O}(\Theta)=V(S(\Theta)) 
   \end{array}

### Link between :math:`\nabla \mathcal{O}(\Theta) \in \mathbb{R}^p` and :math:`\mathcal{O}'(\Theta) \in \mathbb{R}^{p*}`:

.. math::
   \begin{array}{llll}
   \mathcal{O}'(\Theta):&\mathbb{R}^p \to \mathbb{R}\\
   &\epsilon\to \langle \nabla \mathcal{O}(\Theta),\epsilon\rangle_{\mathbb{R}^p}\\
   \end{array}

That is, :math:`\mathcal{O}'(\Theta)^*=\nabla \mathcal{O}(\Theta)`. Taking into account function compositions, with :math:`\delta \Theta \in \mathbb{R}^p`:

.. math::
   \begin{array}{lllc}
   \mathcal{O}'(a)(\delta \Theta) &=V'(S(\Theta))S'(\Theta)(\delta \Theta)\\
   &=\langle \tilde v'(S(\Theta)),S'(\Theta)(\delta \Theta)\rangle_H
   \end{array}
   :label: eqnablaJFV

We then obtain:

.. math::
   \nabla \mathcal{O}(\Theta) = S'(\Theta)^* (\tilde v'(S(\Theta)))
   :label: eqnablaJ

### Adding the state function 

Let :math:`F:\mathbb{R}^p\times H \to H` such that :math:`S(\Theta)=u_\Theta` satisfies :math:`F(\Theta,u_\Theta)=0\in H`.

If we apply the implicit function theorem:

.. math::
   \partial_u F(\Theta,u_\Theta)\delta u +\partial_\Theta F(\Theta,u_\Theta)\delta \Theta=0

and

.. math::
   S'(\Theta)=\frac{\delta _u}{\delta_\Theta}=-\partial_uF(\Theta,u_\Theta)^{-1}\partial_\Theta F(\Theta,u_\Theta)

By substituting into :eq:`eqnablaJFV`:

.. math::
   \begin{array}{lllc}
   \mathcal{O}'(\Theta)(\delta a)&=\langle \tilde v'(u_\Theta),-\partial_u(F(\Theta,u_\Theta)^{-1}\partial_\Theta F(\Theta,u_\Theta)(\delta \Theta)\rangle_H\\
   &=\langle \underbrace{\partial_u(F(\Theta,u_\Theta)^{-1*}\tilde v'(u_\Theta)}_{\large- P},-\partial_\Theta F(\Theta,u_\Theta)(\delta \Theta)\rangle_H
   \end{array}

That is, :math:`P+\partial_u(F(\Theta,u_\Theta)^{-1*}\tilde v'(u_\Theta)=0`.

Writing the weak formulation of the adjoint equation, for all :math:`w \in H`:

.. math::
   \boxed{\langle \tilde v'(u_\Theta)+\partial _u F(\Theta,u_\Theta)^*p,w \rangle_H=0}
   :label: eq:eqAdjoint

:math:`P` is called the adjoint state.

.. math::
   \nabla \mathcal{O}(\Theta)_i=\nabla \mathcal{O}(\Theta).e_i=\langle P,\partial_\Theta F(\Theta,u_\Theta) e_i \rangle_H

### Details:

- :math:`\partial_uF (\Theta, u_\Theta)\in L(H,H)`
- :math:`\partial_uF (\Theta, u_\Theta)^*\in L(H,H)`
- Such that :math:`\langle \partial_uF (\Theta, u_\Theta)(u),w\rangle_H=\langle u,\partial_uF (\Theta, u_\Theta)^*(w)\rangle_H`

## Using the Lagrangian

Let :math:`L` be:

.. math::
   L(\Theta,u,p)=V(u)+\langle p,F(\Theta,u) \rangle_H

If :math:`u=u_\Theta`, then:

.. math::
   L(\Theta,u_\Theta,p)=\mathcal{O}(\Theta)

Taking the derivative:

.. math::
   \mathcal{O}'(\Theta)\delta \Theta=\partial_\Theta L(\Theta,u_\Theta,p)\delta \Theta+\partial_u L(\Theta,u_\Theta,p)\partial_\Theta u_\Theta\delta \Theta

We then choose :math:`p` such that for all :math:`\delta u`:

.. math::
   \partial_u L(\Theta,u_\Theta,p)\delta u=0

which means:

.. math::
   V'(u_\Theta)\delta u+\langle p,\partial_uF(\Theta,u_\Theta) \delta u \rangle_H=0

.. math::
   \langle \tilde v '(u_\Theta)+\partial_uF(\Theta,u_\Theta)^*p,\delta u \rangle_H=0

which corresponds to the adjoint equation. We then obtain:

.. math::
   \mathcal{O}'(\Theta) \delta \Theta=\partial_\Theta L(\Theta,u_\Theta,p)\delta \Theta

.. math::
   \boxed{\mathcal{O}'(\Theta) \delta \Theta=\langle p,\partial _\Theta F(\Theta,u_\Theta)\delta \Theta \rangle_H}
   :label: eq:nablaAdjoint

We can then deduce the components of :math:`\nabla \mathcal{O}(\Theta)` by choosing :math:`\delta \Theta=e_i`.

In the examples, we will explicitly define and solve/simulate system :eq:`eq:eqAdjoint` to obtain the vector :math:`\nabla \mathcal{O}(\Theta)`.

Remarks:

  - The adjoint system does not depend on the model parameterization! This means that it can be computed without considering the parameterization of the model (an important detail for implementation).

  - Moreover, the left-hand side of the adjoint system, the matrix in equation :eq:`eq:eqAdjoint`, does not depend on the observation process. This means that the construction of the linear system for simulating the adjoint state depends only on the model.

  - Equation :eq:`eq:nablaAdjoint` corresponds to computing an integral over :math:`Q`, regardless of the nature of the observation process, even if observations are discrete in time and/or space.
  
  - The computation of :math:`\nabla \mathcal{O}(\Theta)` reduces to simulating a linear PDE and computing :math:`p` integrals over :math:`Q`.


computing variation using the linear tangent equation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

bayesian analysis
#################


Models with interactions
------------------------
We first proposed a resolution model with interaction 2D1D, based on the following study: The influence of a line with fast diffusion on Fisher-KPP propagation, https://arxiv.org/abs/1210.3721 .
We then generalized this process with interaction 1D1D and 2D2D. We describe these processes in the following subsections.

Interaction 2d1d
~~~~~~~~~~~~~~~~
Let: 

- :math:`\delta` a 1d segment,
- :math:`\Omega` a rectangle based on :math:`\delta`,
- :math:`V` is defined in :math:`\Omega`,
- :math:`U` is defined on :math:`\delta`.

The system coupling :math:`\Omega` and `\delta` is:

.. math::
        :label: eq:2d1d

        \begin{equation}
        \left\{ \begin{array}{lcl}
        \partial_t U -D\partial_{xx}U &=& \boldsymbol {V}(x,0,t)-U&,\forall t>0,\forall x\in \delta, \\
        \partial_t\boldsymbol {V}-d\Delta \boldsymbol {V}&=&\boldsymbol {V}(1-\boldsymbol {V})&,\forall t>0,\forall (x,y)\in \Omega\\
        -d\partial_y\boldsymbol {V}(x,0,t)&=&U(x,t)-\boldsymbol {V}(x,0,t) &,\forall t>0,\forall x\in \delta
        \end{array}\right.
        \end{equation}

The interaction consists in adding the 1D sources terme :math:`\boldsymbol {V}(x,0,t)-U(x,t)` and the 2D flux :math:`U(x,t)-\boldsymbol {V}(x,0,t)`.

Interaction 1d1d
~~~~~~~~~~~~~~~~
Let :math:`\delta_1` and :math:`\delta_2` sharing the same geometry. This interaction consist in allowing the population transfert between them. :math:`U_1` and :math:`U_2` are respecrtively defined on :math:`\delta_1` and :math:`\delta_2`. The intercation in adding the terms:

.. math::
        \begin{equation}
          \left\{ \begin{array}{lcll}
            \partial _t U_1(t,x) &+=& \lambda(U_2(t,x)-U_1(t,x)) &, \text{ pour $x\in \delta_1 $, $t\in]0,T]$}\\
            \partial _t U_2(t,x) &+=& \lambda(U_1(t,x)-U_2(t,x)) &, \text{ pour $x\in \delta_2 $, $t\in]0,T]$}\\
          \end{array}
          \right.
        \end{equation}

Interaction 2d2d
~~~~~~~~~~~~~~~~
The connexion between two field :math:`(\Omega_1,\Omega_2)` sharing an border :math:`\delta` is defined as follow:

.. math::
        \begin{equation}
          \left\{ \begin{array}{lcll}
            \vec{\nabla} D_1(x,\Theta)U_1(t,x).\vec{n}&+=&\gamma(U_2(x,t)-U_1(x,t))&, \text{pour $x\in \delta $, $t\in]0,T]$}\\
            \vec{\nabla} D_2(x,\Theta)U_2(t,x).\vec{n}&+=&\gamma(U_1(x,t)-U_2(x,t))&, \text{pour $x\in \delta $, $t\in]0,T]$}\\
          \end{array}
          \right.
        \end{equation}

Implementations of interactions in MSE
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Without transfert, the variational formulation leads to the linear system :math:`M \left( \begin{array}{c}
\alpha  \\
\beta   \end{array} \right)=B`
with :math:`D1` and :math:`D2` two domains, :math:`M` the matrix:

.. math::
        M= \left( \begin{array}{cc}
        MD2 & MD2\_D1  \\
        MD1\_D2 & MD1   \end{array} \right)

where extract diagonal could be zero.
The transfert consists in adding :math:`addM` to `M`:

.. math::
        addM= \left( \begin{array}{cc}
        addMD2 & addMD2\_D1  \\
        addMD1\_D2 & addMD1   \end{array} \right)


**Matrices descritpion of the 2d1d interaction**

In the system :eq:`eq:2d1d`, we write the Green's formula on adding fluxes in :math:`\Omega`:

.. math::
        -\int_\Omega d\Delta V \psi_i =  \int_\Omega d\nabla V \nabla \psi_i - \int_\delta d\partial_n V \psi_i

Using the 2D1D transfer:

.. math::
        \int_\delta d\partial_n V \psi_i += \sum\beta_j\int_\delta\psi_j\psi_i - \sum\alpha_k\int_\delta\phi_k\psi_i

This leads to:

.. math::
        (addM2D)_{i,j} = \int_\delta\psi_j \psi_i \qquad (add2D1D)_{i,k} = -\int_\delta\phi_k \psi_i

Note that many :math:`\int_\delta\psi_j \psi_i` are null. Rewriting it using :math:`mass_{1d}`, we get:

.. math::
        \boxed{\begin{equation}
        \begin{array}{l}
        (addM2D)_{I_{1D2D}(i),I_{1D2D}(j)} = \int_\delta \psi_{I_{1D2D}(j)}\psi_{I_{1D2D}(i)} = \int_\delta\phi_j\phi_i = (mass_{1D})_{i,j}\\
        (addM2D1D)_{I_{1D2D[i]},k} = -\int_\delta\phi_i\phi_k = -(mass_{1D})_{i,k}
        \end{array}
        \end{equation}}

In the system :eq:`eq:2d1d`, we now Write the variational formulation of the source term in :math:`\delta`:

.. math::
        \int_\delta V \phi_i - \int_\delta U \phi_i = \sum \beta_j \int_\delta \psi_j \phi_i - mass_{1D} \alpha

If :math:`\psi` equals :math:`\phi` on :math:`\delta`, we get:


.. math::

        \boxed{\begin{equation}
        \begin{array}{l}
           addM1D = mass_{1D}\\
          (addM1D2D)_{i,I_{1D2D}(l)} = -(mass_{1D})_{i,l}
        \end{array}
        \end{equation}}



This interation is implemented in the class :ref:`interactions_class`.

