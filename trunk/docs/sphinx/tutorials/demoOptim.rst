.. _demo1_example:


   
Tutorial: simulation and estimation using optimisation algorithms
=================================================================

**Introduction:**

The goal of this study is to estimate the parameter :math:`\theta=(D=p1,R=p2)` of a KPP-Fisher equation. 
The steps are:

- build the simulator,
- run a simulation to generate data from a reference parameters values,
- choose an observation process,
- run the optimisation algorithm.


The model is:

 .. math::
        \begin{equation}
        \left\{ \begin{array}{lcll}
        \partial _t u(t,x) &=& \Delta p_1 u(x,t) +p_2 u(t,x) (1-u(t,x))&, \text{ for $x\in \Omega$, $t\in]0,T]$}\\
        U(0,x)&=&U_0(x)&, \text{ pour $x\in \Omega$}\\
        0&=&\vec{\nabla} D(x,\Theta)U(t,x).\vec{n}&, \text{pour $x\in \delta \Omega$, $t\in]0,T]$}\\
        \end{array}
        \right.
        \label{modeldemo1}
        \end{equation}


The following shows the python code to perform this study.


**Import mse and tools:**

.. code-block:: python

   from mse.study import study
   from mse.dynamicalSystem import dynamicalSystem,computeMass
   from mse.DISPERSIONS.diffusionModel import diffusionModel, txtDiffusionModel
   from mse.MODELS.models import txtModel
   from mse.system import system
   from mse.systemAdjoint import systemAdjoint
   from mse.obsProcess import quadraticDiff
   from mse.simulator import simulator,simulatorBackward
   from mse.timeDoOneStep import timeDoOneStep,timeDoOneStepBackward
   from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme,implicitLinearBackwardTimeScheme
   from mse.toolsEnv import computerEnv,MSEPrint
   from mse.explorTrajectory import explorTrajectory
   from os import  mkdir
   from os.path import isdir, exists
   import numpy as np
   import matplotlib.pyplot as plt
   import shutil


**Build a study**, it builds a directory containing study code and datas:

.. code-block:: python

   studName="KPP_OPTIM"
   nbParam=2
   aStudy=study(studName, nbParam)

**Choose a study zone from the geometries library**, it builds meshes:


.. code-block:: python
   
   aStudy.setGeometry("SQUARE",0.4)

**Build a dynamical system on 2d mesh:**

.. code-block:: python
   
   dim=2
   ds2d1=dynamicalSystem(aStudy,dim,"surface1.xdmf",nbComps=1)

**Add a reaction and diffusion term from the models library:**

.. code-block:: python

   # Diffusion
   aDiffModel=txtDiffusionModel(ds2d1, "diff2dIsotropP1")
   # source term is a predefined logistic
   m=txtModel(ds2d1,"logisticGrowthRParam")

**Build a simulator**, using implicit non-linear with fixed time step:

.. code-block:: python

   #time scheme: non-linear implicit 
   aTS=implicitNonLinearTimeScheme()
   #full implicit
   aTS.theta=1
   #simulator
   t0=0.
   t1=1.
   simulator(t0,t1,aStudy)
   #chose an fixed time stepping
   stepT=1e-2
   aTStep=timeDoOneStep(aStudy,aTS,stepT)

**Run a simulation:**

.. code-block:: python

   #set parameters value (p1,p2,p3)
   paramRef = [5., 1.]
   aStudy.setParam(paramRef)
   #run the simulation
   aStudy.simulator.doSimu()

In this example, the simulated trajectory :math:`u(x,t)` is looked as data. In the following, we will use an optimization algorithm to estimate the parameters that minimize the quadratic distance to this trajectory.

**Save this trajectory as reference:**

.. code-block:: python

   path=aStudy.absoluteStudyDir+"/UREF/"
   if(exists(path)):
      shutil.rmtree(path)
   shutil.copytree(aStudy.simuDir, path)

**Choose the observation process:**

.. code-block:: python
   
   #choose a predefiend observation process
   quaDiff=quadraticDiff(ds2d1)

**Optimisation using scipy.optimize.**

*build interface to scipy.optimize:*

.. code-block:: python
   
   from scipy.optimize import minimize, OptimizeResult
   #a call back function to see optimization steps
   globalCount=0

   def aCallableOpt(intermediate_result: OptimizeResult):
      """a callable use in intermediate odo a kdf  ozf zkf sofnqqk oajn
      """
      global globalCount
      globalCount+=1
      print("****************\nIn iteration"+str(globalCount)+": \n",intermediate_result)
      print("V = ",quaDiff.computeV())
      print("GradV = ",quaDiff.computeGradV())

   initParam = [10.,5.]
   #optimization computation of variation using adjoint system built within MSE
   optParam = minimize(quaDiff.sysAdj.valJAndGradJ, initParam, method="L-BFGS-B", bounds=[(0.01,12),(0.1,6.0)], jac=True, tol=1e-8, callback=aCallableOpt, options={"maxiter":100})

optimisation result:

- :math:`|\nabla J(\theta_0)|=10`
- :math:`|\nabla J(\theta^*)|=10^{-8}`
- :math:`J(\theta_0)=41`
- :math:`J(\theta^*)=5.10^{-16}`
- :math:`|\theta^*-\theta_{ref}|\sim 10^{-16}`

with 22 calls to the simulator.

The iterations of the Quasi-Newton algorithm is showed in the following figure:

.. image::  ../../figures/pathParameter17.png
      :height: 450


**Post-simu**: MSE records the simulation , allowing it to be replayed it for specific computations. In this example, it involves exporting states for visualization and calculating the total mass:**

.. code-block:: python

   #for visu, export simulation steps to vtk
   aStudy.simulator.exportSimuToVtk()

   #loop on simu to compute mass
   import numpy as np
   mass=np.zeros((ds2d1.nbComps),dtype=np.double)
   aExplorTraj=explorTrajectory(aStudy,aStudy.simuDir)
   aExplorTraj.rewind()
   while(aExplorTraj.replay()):
      computeMass(ds2d1,mass)
      print("mass "+str(aExplorTraj.curStep)+" = ",end="")
      for m in mass:
         print(m,end=", ")
      print("")


