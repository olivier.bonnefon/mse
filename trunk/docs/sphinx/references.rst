.. _references_mse:

References and applications
===========================

A mecanistico-statistique approach:
-----------------------------------

- Using genetic data to estimate diffusion rates in heterogeneous landscapes. Journal of Mathematical Biology, 2015 (https://doi.org/10.1007/s00285-015-0954-4).

Studies using MSE for simulations and mecanistico-statistique analysis:
-----------------------------------------------------------------------

- Modelling Population Dynamics in Realistic Landscapes with Linear Elements: A Mechanistic-Statistical Reaction-Diffusion Approach, PLOS ONE, 2016,  http://dx.doi.org/10.1371/journal.pone.0151217

- Dynamics of Aedes albopictus invasion Insights from a spatio-temporal model (Biological Invasion DOI: 10.1101/2021.09.24.461645 )

- Dating and localizing an invasion from post-introduction data and a coupled reaction-diffusion-absorption model. Août 2018. https://arxiv.org/abs/1808.00868


- More pests but less pesticide applications: Ambivalent effect of landscape complexity on conservation biological control, PLOS Computationalbiology (DOI: 10.1371/journal.pcbi.1009559)
