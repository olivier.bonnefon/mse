.. _download:


Download MSE
============

.. contents::
   :local:

Latest source release
---------------------

Latest source release is downloadable from here:

https://forgemia.inra.fr/olivier.bonnefon/mse

Install
=======

.. contents::
   :local:


.. include:: ../../../../README.md

