.. _interactions_class:
Interactions
------------

Interactions 2D1D
~~~~~~~~~~~~~~~~~
.. autoclass:: mse.INTERACTIONS.interaction.interaction2D1D
   :members: 
   :noindex:


Interactions 1D1D
~~~~~~~~~~~~~~~~~
.. autoclass:: mse.INTERACTIONS.interaction.interaction1D1D
        :members:
        :noindex:
