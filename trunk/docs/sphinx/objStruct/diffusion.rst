.. _diffusion_class:
Diffusion process
-----------------
Constant Diffusion
~~~~~~~~~~~~~~~~~~
.. autoclass:: mse.DISPERSIONS.diffusionModel.diffusionModel
        :members: enableAdjoint, setDiffCoef
        :noindex:

Diffusion with parameter
~~~~~~~~~~~~~~~~~~~~~~~~
.. autoclass:: mse.DISPERSIONS.diffusionModel.txtDiffusionModel
   :members: enableAdjoint, setDiffCoef
   :noindex:

