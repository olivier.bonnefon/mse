Linear algebra
--------------
.. automodule:: mse.linAlgTools
   :noindex:

.. autoclass:: mse.linAlgTools.matrix
   :members:
   :noindex:
   
