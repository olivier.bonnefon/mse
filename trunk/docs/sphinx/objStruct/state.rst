State
-----

A state is the mermory representation of the solution of the PDE at a fixed time. It allows to build the FEM représentation of the simulation.

.. automodule:: mse.state
   :noindex:

.. autoclass:: mse.state.state
   :members:
   :noindex:
   
