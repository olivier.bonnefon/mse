Object structure
================

.. include::  ./study.rst
.. include::  ./linAlgTools.rst
.. include::  ./diffusion.rst
.. include::  ./models.rst
.. include::  ./dynamicalSystem.rst
.. include::  ./state.rst
.. include::  ./obsprocess.rst
.. include::  ./systemAdjoint.rst
.. include::  ./exploreTraj.rst
.. include::  ./interactions.rst
