.. _obs_process_class:
Observation Processus
---------------------

.. autoclass:: mse.obsProcess.obsProcess
   :members: computeGradV
   :noindex:


Quadratic difference
~~~~~~~~~~~~~~~~~~~~
.. autoclass:: mse.obsProcess.quadraticDiff
   :members: computeV, computeTildevPrim
   :noindex:

