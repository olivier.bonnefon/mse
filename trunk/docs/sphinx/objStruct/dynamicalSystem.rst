.. _ds_class:
Dynamical System
----------------

.. automodule:: mse.dynamicalSystem
   :noindex:

.. autoclass:: mse.dynamicalSystem.dynamicalSystem
   :members: addSecondOrderTerm, addSourcesTerm, exportStateVtk
   :noindex:
   
.. autofunction:: mse.dynamicalSystem.computeMass
   :noindex:
