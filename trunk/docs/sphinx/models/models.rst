.. _models_mse:

Reaction diffusion models
=========================
MSE proposes a model based on parabolic partial differential equation of the following form :eq:`eq:model1`:

.. math::
        :label: eq:model1

        \begin{equation}
        \left\{ \begin{array}{lcll}
        \partial _t U(t,x) &=& \Delta D(x,\Theta)U(t,x)+F(\Theta,U(t,x)) &, \text{ pour $x\in \Omega$, $t\in]0,T]$}\\
        U(0,x)&=&U_0(x,\Theta)&, \text{ pour $x\in \Omega$}\\
        \vec{\nabla} D(x,\Theta)U(t,x).\vec{n}&=&0&, \text{pour $x\in \delta \Omega$, $t\in]0,T]$}\\
        \end{array}
        \right.
        \end{equation}

Where :math:`\Omega` is the study zone. The bondary condition on :math:`\delta \Omega` depends on the study and will be adjust in case of interaction with the border.

Diffusion process
-----------------

The diffusion process is the term :math:`\Delta D(x,\Theta)U(t,x)`. In MSE, this part of the model is implemented in the class :ref:`diffusion_class`.
