echo "ensure you are in mse/trunk directory ?"

TRUNK_MSE=$(pwd)/

rm -rf mseSafranTools/build/* mseSafranTools/FrontEnd/build/*  

echo "========================================================"
echo "===== 5) install mseSafranTools ========================"
echo "========================================================"
mkdir mseSafranTools/build
cd mseSafranTools/build
#because in guix, mseMapTools will be in the gnu, following option are not neceassry in guix cook
#the option -DCMAKE_SHARED_LINKER_FLAGS="${CMAKE_EXE_LINKER_FLAGS}  -Wl,-rpath,${MSEMAPTOOLS_LIBRARY_DIR}" add the ${MSEMAPTOOLS_LIBRARY_DIR} to RPATH of mseSafranTools.so, objdump -x /././././mseSafranTools.so allow to see that. -Wl prefix indique option for the linker.
cmake  -DCMAKE_INSTALL_PREFIX=$TRUNK_MSE/TEST_INSTAL/installmseSafranTools -DCMAKE_SHARED_LINKER_FLAGS="${CMAKE_EXE_LINKER_FLAGS}  -Wl,-rpath,${MSEMAPTOOLS_LIBRARY_DIR}" ..
#make VERBOSE=TRUE install
make install
echo "========================================================"
echo "===== 6) install python interface pymseSafranTools======"
echo "========================================================"
cd $TRUNK_MSE

mkdir mseSafranTools/Frontend/build
cd mseSafranTools/Frontend/build
cmake ..  -DMSE_SAFRAN_TOOLS_INSTALL_PATH=$TRUNK_MSE/TEST_INSTAL/installmseSafranTools -DCMAKE_INSTALL_PREFIX=$TRUNK_MSE/TEST_INSTAL/installpymseSafranTools
make
export PYTHONPATH=$PYTHONPATH:$TRUNK_MSE/TEST_INSTAL/installpymseSafranTools/lib/python3.10/site-packages/
make test
echo "to check this:"
python3 -c "import pymseSafranTools.mseSafranTools as mp"

cd $TRUNK_MSE
