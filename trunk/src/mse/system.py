class system:
    def __init__(self,aStudy) -> None:
        #ds
        self.dynamicalSystems=[]
        self.nbDofs=0
        #relations beteween dss
        self.interactions=[]
        self.study=aStudy
        self.study.system=self
    #called from study only
    def _addDS(self,aDs):
        aDs.beginDofsIndexInSystem=self.nbDofs
        self.dynamicalSystems.append(aDs)
        self.nbDofs=self.nbDofs+aDs.nbDofs
        aDs.indexInSystem=len(self.dynamicalSystems)-1
    def addInteraction(self,aI):
        self.interactions.append(aI)
    def saveState(self):
        for ds in self.dynamicalSystems:
            ds.saveState()
    def restoreState(self):
        for ds in self.dynamicalSystems:
            ds.restoreState()
 
