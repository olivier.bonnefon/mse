from mse.toolsEnv import computerEnv,MSEPrint,MSEPrint_0,MSEError
from dolfinx.fem import Function,Constant
from dolfinx.io import VTKFile
from ufl import grad
from dolfinx.fem import Function, assemble_scalar, Constant, form
from petsc4py.PETSc import ScalarType
#aName is used in the model description
#covariable.femCov is the covariable usable in the fem varf
from math import *
import numpy as np
import pymseMatTools.mseMatTools as MT
from os.path import isfile
from mse.exception import mseFileErr,mseError

class builderCovariable:
    doVtk=0
    def __init__(self,aName,aDs):
        self.name=aName
        self.study=aDs.study
        self.dS=aDs
        self.study=aDs.study


class builderCovariableFromRaster(builderCovariable):
    def __init__(self,aName,aDs,pathToRaster):
        super().__init__(aName,aDs)
        self.pathRaster=pathToRaster
        if (not isfile(pathToRaster) or not isfile(pathToRaster+str("Info.txt"))):
            MSEPrint(" Erreur, cant find files "+pathToRaster+ " and "+pathToRaster+str("Info.txt"))

    def build(self):
        returnV=np.array([0],dtype=np.int32)
        XX= self.dS.vSpace.tabulate_dof_coordinates()[:,0]
        YY= self.dS.vSpace.tabulate_dof_coordinates()[:,1]
        aDFunc=self.dS.utm1.comps[0]
        MT.fillFromRaster(self.pathRaster,aDFunc.x.array,XX,YY,returnV)
        if (returnV[0]):
            MSEPrint(" failed to import raster. ERROR ="+str(returnV[0]))
            return returnV[0]
        MT.saveVec(self.study.covDir+self.name,aDFunc.x.array)
        if (self.doVtk):
            f=VTKFile(self.dS.dolfinMesh.comm, self.study.exportDir+self.name+".pvd","w")
            f.write_function(aDFunc)
            f.close()
 



#
# the menber self.femCov must takes place in the varf
#
#
#
#
class covariable:
    myPrint=MSEPrint_0
    def verbose(aBool):
        if (aBool):
            covariable.myPrint=MSEPrint
        else:
            covariable.myPrint=MSEPrint_0
    def __init__(self,aName,aDS) -> None:
        covariable.myPrint(" buid cov named "+aName)
        self.dS=aDS
        self.name=aName
        self.study=aDS.study
        self.femCov=None
        self.gradCov=None
    def exportVtk(self,fileName=None):
        f=None
        if (fileName):
            f=VTKFile(self.dS.dolfinMesh.comm, fileName+".pvd","w")
        else:
            f=VTKFile(self.dS.dolfinMesh.comm, self.dS.study.exportDir+self.name+".pvd","w")
        f.write_function(self.femCov)
        f.close()
    def setValue(self):
        if (MT.loadVec(self.study.covDir+self.name,self.femCov.x.array)):
            covariable.myPrint("Erreur : calling MT.loadVec")
            raise mseFileErr("Covariable load failed" + self.study.covDir+self.name) 

    def computeMass(self):
        if (self.femCov):
            return assemble_scalar(form(self.femCov*self.dS.dx))
        else: 
            raise mseError("Covariable empty")
    def save(self):
        if (self.femCov != None):
            if (MT.saveVec(self.study.covDir+"ds"+str(self.dS.id)+"/"+self.name,self.femCov.x.array)):
                covariable.myPrint("Erreur : covariable cant save in "+self.study.covDir+"ds"+str(self.dS.id)+"/"+self.name)
                raise mseFileErr("Covariable wrtie failed" + self.study.covDir+"ds"+str(self.dS.id)+"/")
    def load(self):
        if (self.femCov != None):
            if (MT.loadVec(self.study.covDir+"ds"+str(self.dS.id)+"/"+self.name,self.femCov.x.array)):
                covariable.myPrint("Erreur : covariable not intialised")
                raise mseFileErr("Covariable load failed" + self.study.covDir+"ds"+str(self.dS.id)+"/"+self.name) 



class covariableFromExpression(covariable):
    def __init__(self,aName,aDS) -> None:
        super().__init__(aDS,aName)
        self.femCov=Function(self.dS.vSpace)
        self.femCov.x.array.fill(0.0)
        self.gradCov=None
    def setValue(self,aExpression):
        def fct_express(x):
            return eval(aExpression)
        self.femCov.interpolate(fct_express)
    def computeGrad(self):
        if ( self.gradCov is None):
            self.gradCov=grad(self.femCov)


class covariableCst(covariable):
    def __init__(self,aName,aDS) -> None:
        super().__init__(aDS,aName)
        self.femCov=Constant(self.dS.dolfinMesh,ScalarType(0.0))
        self.gradCov=0
    def setValue(self,value):
        self.femCov.value=value
    def save(self):
        pass
    def load(self):
        pass
    def exportVtk(self,fileName=None):
        pass
class covariableFromCovFile(covariable):
    def __init__(self,aDs,aName,aSubDir=".") -> None:
        super().__init__(aDs,aName)
        self.femCov=Function(self.dS.vSpace)
        self.femCov.x.array.fill(0.0)
        self.gradCov=None
        self.subDir=aSubDir
        fileWillLoad=self.study.covDir+"ds"+str(self.dS.id)+"/"
        fileWillLoad=fileWillLoad+self.subDir+"/"
        fileWillLoad=fileWillLoad+self.name
        if (MT.loadVec(fileWillLoad,self.femCov.x.array)):
            covariable.myPrint("Info: covariableFromCovFile " +self.name+" not intialised")

    # setValue(self,aTime)
    #
    #Pour les covariable , variable en temps, on creer un fichier timeCovName.txt contenant deux colonnes (index de fichier covariable, temps):
    #       0 0.0
    #       1 1.0
    #       2 1.4
    #       3 2.0
    #   par exemple pour les donnée de températures.
    #   A l'instar de l'explorTraj, la mise a jours de la covariable pendant la simulation, constera à chercher l'interval de temp correspondant au pas de simu et a faire l'interpolation, (ou le pas précédent)
    def setValue(self,aTime):
        covariable.myPrint("at time="+str(aTime))
        prevFile=""
        curFile=""
        covDir=self.study.covDir+"ds"+str(self.dS.id)+"/"+self.subDir+"/"
        f_Time = open(covDir+"/time"+self.name+".txt")
        if (not f_Time):
            MSEError(" Erreur, no time file for covariable "+covDir+"/time"+self.name+".txt")
            return
        line = f_Time.readline()
        covariable.myPrint("line = "+line)
        curT=float(line.split()[1])
        prevT=curT-1
        curFile=int(line.split()[0])
        while line := f_Time.readline():
            prevT=curT
            prevFile=curFile
            curT=float(line.split()[1])
            curFile=int(line.split()[0])
            if (curT-aTime>-1e-16): 
                break
        f_Time.close()
        aCoef=0
        if (abs(aTime-prevT) < 1e-16):
            if(MT.loadVec(covDir+"/"+self.name+str(prevFile)+"",self.femCov.x.array)): raise mseFileErr("") 
            covariable.myPrint("load "+covDir+"/"+self.name+str(prevFile)+"")
        elif(abs(curT-aTime)<1e-16):
            if(MT.loadVec(covDir+"/"+self.name+str(curFile)+"",self.femCov.x.array)): raise mseFileErr("")
            covariable.myPrint("load "+covDir+"/"+self.name+str(curFile)+"")
        else:
            aCoef=(aTime-prevT)/(curT-prevT)
            covariable.myPrint("coef="+str(aCoef))
            #print("covariable aCoef="+str(aCoef))
            #print(str(aCoef))
            if (aCoef <0 or aCoef > 1):
                if (aCoef>1):
                    aCoef=1
                else:
                    aCoef=0
            covariable.myPrint("corrected coef="+str(aCoef))
            covariable.myPrint("with files "+covDir+"/"+self.name+str(prevFile)+" " +str(curFile)) 
            if(MT.loadVec(covDir+"/"+self.name+str(prevFile)+"",self.dS.bufState.comps[0].x.array)): raise mseFileErr("")
            #use self.dS.bufState.comps[0] as buffer
            if(MT.loadVec(covDir+"/"+self.name+str(curFile)+"",self.femCov.x.array)): raise mseFileErr("")
            self.femCov.x.array[:]=aCoef*self.femCov.x.array[:]+(1-aCoef)*self.dS.bufState.comps[0].x.array[:]
