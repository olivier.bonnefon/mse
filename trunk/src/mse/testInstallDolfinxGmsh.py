#
# teste l'instalation de gmsh, mshio, dolfinx, petsc
#
# si correct doit afficher les messages suivants:
'''
python3 testInstallDolfinxGmsh.py 
Invalid MIT-MAGIC-COOKIE-1 keyInfo    : Meshing 1D...
Info    : [  0%] Meshing curve 1 (Line)
Info    : [ 10%] Meshing curve 2 (Nurb)
Info    : [ 10%] Meshing curve 3 (Nurb)
Info    : [ 20%] Meshing curve 4 (Nurb)
Info    : [ 20%] Meshing curve 5 (Nurb)
Info    : [ 20%] Meshing curve 6 (Nurb)
Info    : [ 30%] Meshing curve 7 (Nurb)
Info    : [ 30%] Meshing curve 8 (Nurb)
Info    : [ 40%] Meshing curve 9 (Nurb)
Info    : [ 40%] Meshing curve 10 (Line)
Info    : [ 40%] Meshing curve 11 (Nurb)
Info    : [ 50%] Meshing curve 12 (Nurb)
Info    : [ 50%] Meshing curve 13 (Nurb)
Info    : [ 50%] Meshing curve 14 (Nurb)
Info    : [ 60%] Meshing curve 15 (Nurb)
Info    : [ 60%] Meshing curve 16 (Nurb)
Info    : [ 70%] Meshing curve 17 (Nurb)
Info    : [ 70%] Meshing curve 18 (Nurb)
Info    : [ 70%] Meshing curve 19 (Nurb)
Info    : [ 80%] Meshing curve 20 (Nurb)
Info    : [ 80%] Meshing curve 21 (Line)
Info    : [ 90%] Meshing curve 22 (Nurb)
Info    : [ 90%] Meshing curve 23 (Nurb)
Info    : [ 90%] Meshing curve 24 (Nurb)
Info    : [100%] Meshing curve 25 (Nurb)
Info    : [100%] Meshing curve 26 (Nurb)
Info    : Done meshing 1D (Wall 0.0855715s, CPU 0.081878s)
Info    : Meshing 2D...
Info    : Meshing surface 1 (Plane, Frontal-Delaunay)
Info    : Done meshing 2D (Wall 0.265209s, CPU 0.247575s)
Info    : 12513 nodes 24949 elements
Info    : Writing './MESHES/GLS/mesh.med'...
Info    : Done writing './MESHES/GLS/mesh.med'
Info    : Running '/home/olivierb/.local/bin/gmsh -2 ./MESHES/GLS/mesh.med' [Gmsh 4.9.3, 1 node, max. 1 thread]
Info    : Started on Thu Dec 15 13:48:35 2022
Info    : Reading './MESHES/GLS/mesh.med'...
Info    : Reading MED file V4.1.0 using MED library V4.1.0
Info    : Reading 3-D unstructured mesh 'create_mesh'
Info    : Done reading './MESHES/GLS/mesh.med'
Info    : Meshing 1D...
Info    : Done meshing 1D (Wall 2.3944e-05s, CPU 5.7e-05s)
Info    : Meshing 2D...
Info    : Done meshing 2D (Wall 1.3027e-05s, CPU 3e-05s)
Info    : 12412 nodes 24822 elements
Info    : Writing './MESHES/GLS/mesh.msh'...
Info    : Done writing './MESHES/GLS/mesh.msh'
Info    : Stopped on Thu Dec 15 13:48:35 2022 (From start: Wall 0.0433681s, CPU 0.2985s)
solving Linear System

Linear System Solved
'''

#
#
#
#
#



import MESH_IMPORT_GMSH.read as reader
import MESH_IMPORT_GMSH.buildMeshes as buildMeshes

import study
import dynamicalSystem
import system
import simulator
from dolfinx.io import XDMFFile,VTKFile
from ufl import Measure,TrialFunction,TestFunction
from petsc4py.PETSc import ScalarType
from dolfinx.fem import form,assemble_scalar,Constant,form,Expression
from dolfinx.fem.petsc import assemble_matrix, assemble_vector, apply_lifting, create_vector, set_bc
from dolfinx.fem import Function

import numpy as np
import math
from petsc4py import PETSc
import time
import implicitTimeScheme

reader.verbose=0
buildMeshes.verbose=0

aTopo=reader.geomTopoReader("./GEOMETRY/GLS/")
aMeshesDir="./MESHES/GLS/"
buildMeshes.buildXdmf(aTopo,aMeshesDir,7e3)
aSystem=system.system()
aTS=implicitTimeScheme.implicitTimeScheme(aSystem)
aStudy=study.study(aTopo,aMeshesDir,aSystem,aTS)
ds2d1=dynamicalSystem.dynamicalSystem(aStudy,1,2,aMeshesDir+"surface1.xdmf")
mesh2d=ds2d1.dolfinMesh
vtkf = VTKFile(mesh2d.comm, "SIMU/mesh2d.pvd", "w")
vtkf.write(mesh2d)
vtkf.close()
N=ds2d1.nbDofs
S=TrialFunction(ds2d1.vSpace)
I=TrialFunction(ds2d1.vSpace)
u=TrialFunction(ds2d1.vSpace)
v=TestFunction(ds2d1.vSpace)

Stm1=Function(ds2d1.vSpace)
Itm1=Function(ds2d1.vSpace)
IConvol=Function(ds2d1.vSpace)
resS = Function(ds2d1.vSpace)
FBuf=Function(ds2d1.vSpace)

def f_fct1(x):
    nElem=x.shape[1]
    values = np.zeros((1, nElem))
    for i in range(nElem):
            values[0][i]=1
    return values

def f_fct0(x):
    nElem=x.shape[1]
    values = np.zeros((1, nElem))
    return values

def f_fctI0(x):
    nElem=x.shape[1]
    print(nElem)
    values = np.zeros((1, nElem))
    for i in range(nElem):
        if (math.fabs(x[0,i]-382644) + math.fabs(x[1,i]-953296)<25000):
            values[0][i]=0.001
    return values



vtkf = VTKFile(mesh2d.comm, "SIMU/SI0.pvd", "w")
vtkf.write_function(Stm1,0)
vtkf.write_function(Itm1,1)
vtkf.close()


dx=ds2d1.dx
dt=0.01
theta=0.5
t=0
T=35
lambda0=1e-4
alpha=Constant(mesh2d, ScalarType(1/dt))
alphaTheta=Constant(mesh2d, ScalarType(1/dt+1-theta))
thetaConvol=Constant(mesh2d, ScalarType(0.5))
mI=alphaTheta*u*v*dx
mS=alpha*u*v*dx
mSForm=form(mS)
#mMass=u*v*dx
#mMassForm=form(mMass)
matS=assemble_matrix(mSForm)
matS.assemble()
#matS.view()


solver = PETSc.KSP().create(mesh2d.comm)
solver.setOperators(matS)
solver.setType(PETSc.KSP.Type.PREONLY)
solver.getPC().setType(PETSc.PC.Type.LU)
rhsL=dt*Stm1*v*dx
rhsLL=form(rhsL)
rhsS=assemble_vector(rhsLL)
print("solving Linear System\n")
solver.solve(rhsS,resS.vector)
print("Linear System Solved\n")
