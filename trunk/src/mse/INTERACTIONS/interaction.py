import pymseMapTools.mseMapTools as mc
import numpy as np
from mse.linAlgTools import matrix
from mse.toolsEnv import MSEPrint, MSEPrint_0

def buildMap1DTo2D(aDS1d,aDS2d,index1DTo2d):
    coord1D=aDS1d.vSpace.tabulate_dof_coordinates()
    nDouble1D=1;
    sh1D=coord1D.shape
    for l in sh1D:
        nDouble1D=nDouble1D*l
    coord2D=aDS2d.vSpace.tabulate_dof_coordinates()
    nDouble2D=1;
    sh2D=coord2D.shape
    for l in sh2D:
        nDouble2D=nDouble2D*l
    mc.buildMapc(aDS1d.vSpace.tabulate_dof_coordinates().reshape((nDouble1D)),aDS2d.vSpace.tabulate_dof_coordinates().reshape((nDouble2D)),index1DTo2d,1e-9)
def _checkMap(aDS1d,aDS2d,index1DTo2d):
    x1d=aDS1D.vSpace.tabulate_dof_coordinates()
    x2d=aDS2D.vSpace.tabulate_dof_coordinates()
    for i1d in range(index1DTo2D.shape[0]):
        i2d=index1DTo2D[i1d]
        if (i2d != -1):
            if (abs(x1d[i1d,0]-x2d[i2d,0])>1e-9 or abs(x1d[i1d,1]-x2d[i2d,1])>1e-9):
                MSEPrint("BUG: "+str(i1d)+","+str(i2d)+" -> ("+str(x1d[i1d,0])+","+str(x1d[i1d,1])+")!=("+str(x2d[i2d,0])+","+str(x2d[i2d,1])+")")
            else:
                MSEPrint(str(i1d)+","+str(i2d)+" -> ("+str(x1d[i1d,0])+","+str(x1d[i1d,1])+")==("+str(x2d[i2d,0])+","+str(x2d[i2d,1])+")")
        else:
            MSEPrint(str(i1d)+" has no 2d node")
# without interaction the ds1,ds2 locks are:
# Matrix:M=|M1  ,M12| 
#          |M21,M2  |
#
# interaction add matrix:
# addM=|addM1  ,addM12|
#      |addM21,addM2  |

class interaction:
    myPrint=MSEPrint_0
    def verbose(aBool):
        if (aBool):
            myPrint=MSEPrint
        else:
            myPrint=MSEPrint_0
    def __init__(self, aStudy, aDS1, aDS2, aCompsInvolved=None) -> None:
        self.study=aStudy
        self.study.system.addInteraction(self)
        self.dS1=aDS1
        self.dS2=aDS2
        if (aCompsInvolved is None):
            self.compsInvolved=np.ones(self.dS1.nbComps)
        else:
            self.compsInvolved=aCompsInvolved
        #build sparce empty mat with right size
        self.addM1=matrix()
        self.addM1.setDim(self.dS1.nbDofs,self.dS1.nbDofs)
        self.addM2=matrix()
        self.addM2.setDim(self.dS2.nbDofs,self.dS2.nbDofs)
        self.addM12=matrix()
        self.addM12.setDim(self.dS1.nbDofs,self.dS2.nbDofs)
        self.addM21=matrix()
        self.addM21.setDim(self.dS2.nbDofs,self.dS1.nbDofs)
    def mulCoef(self, coefBlockLine1, coefBlockLine2):
        """Multiply the matrix add by a coef
        """
        self.addM1.scale(coefBlockLine1)
        self.addM12.scale(coefBlockLine1)
        self.addM2.scale(coefBlockLine2)
        self.addM21.scale(coefBlockLine2)

###
#
# See interaction2D1D.tex for a readable tex version.
#
# do 2D1D interaction
# V : 2D \Omega
# U : 1D \delta
#
# Dofs: (dof2D,dof1D)
# without this interaction the sustem is :
# Matrix:M=|M2D  ,M2D1D| 
#          |M1D2D,M1D  |
#
# 2D1D interaction add matrix:
# addM=|addM2D  ,addM2D1D|
#      |addMAD2D,addM1D  |
#
#Apply this interaction consist in doing M=M+addM
#
#
# Le couplage 2D1D s'ecrit:
# Dans \Omega, un terme de flux: \partial_n V += U-V sur \delta.
# Dans \delta, un terme source : \partial_t U += V-U dans \delta
#
#Vue du 2D, la varf s'ecrit: u*v - \Delta u . v = utm1*v
#
#
# * Vue du 2D (blocs M2D et M2D1D), il s'agit de traiter le terme de flux.
#    l'ecriture de la formulation variationelle: \phi_i fct de base EF:
#    \int_O [V*\phi_i - \Delta V . \phi_i] = \int_O [Vtm1*\phi_i]
#    -\int_O \Delta V \phi_i =  \int_O \grad V \grad \phi_i - int_d \partial_n V.\phi_i
#
#    si phi(x \in \delta) = 0, le couplage n'a pas d'impact.
#
#    sinon phi(x \in \delta) = 1 sur le bord alors i = index1DTo2d[j] ie, index1DTo2d[j] est le dof 2D correspondant au dof 1D j
# 
#          la varf devient:
#          \int_O \Delta V \phi_i = - \int_O \grad V \grad \phi_i - int_d V.\phi_i + int_d U.\phi_i
#          **) les termes "- int_d V.\phi_i" entrent dans M2D, sous forme :
#               condition au bords 
#               ou 
#               d'ajout (addM2D)_{index1DTo2d[j],index1DTo2d[k]} = Mass1D_{j,k} avec j,k index de dofs 1D.
#          **) les termes "int_d U.phi_i" entrent dans M2D1D, sous la forme:
#               (addM2D1D)_{index1D2D[j],k} = -Mass1D_{j,k} j k index de dofs 1d
#
# * Vue du 1D (blocs M1D2D et M1D), il s'agit d'ajouter le terme source. 
#       **) les termes \partial_t U += -U entrent dans M1D sous la forme addM1D = Mass1D
#       **) les termes \partial_t U += V entrent dans (addM1D2D)_{i,index1D2D[j]} = -Mass1D_{i,j} avec j k index 1D
#
#
###


class interaction2D1D(interaction):
    """Class used to simulate interaction 2D1D

    :param study aStudy: the study
    :param dynamicalSystem aDS2d: the 2d dynamical system
    :param dynamicalSystem aDS1d: the 1d dynamical system
    :param numpy.array aCompsInvolved=NONE: the list of the compement involved, if all are involved the value is None
    """
    def __init__(self, aStudy, aDS2d, aDS1d, aCompsInvolved=None) -> None:
        super().__init__(aStudy,aDS2d, aDS1d,aCompsInvolved)
        self.dS2D=aDS2d
        self.dS1D=aDS1d
        if (self.dS2D.geoDim != 2 or self.dS1D.geoDim != 1):
            interaction.myPrint("ERROR in interaction2D1D, wrong dimension " +str(self.dS2D.geoDim)+" "+str(self.dS1D.geoDim))
            print("exit"+"ERROR in interaction2D1D, wrong dimension " +str(self.dS2D.geoDim)+" "+str(self.dS1D.geoDim))
            exit()
        #first we build the compartment-block-matrix 
        self.index1DTo2D=np.zeros((self.dS1D.nbDofsPerComp,),dtype=np.int32)
        buildMap1DTo2D(self.dS1D,self.dS2D,self.index1DTo2D)
        self.addM2D=matrix()
        self.addM2D.setDim(self.dS2D.nbDofsPerComp,self.dS2D.nbDofsPerComp)
        self.addM1D=matrix()
        self.addM1D.setDim(self.dS1D.nbDofsPerComp,self.dS1D.nbDofsPerComp)
        self.addM2D1D=matrix()
        self.addM2D1D.setDim(self.dS2D.nbDofsPerComp,self.dS1D.nbDofsPerComp)
        self.addM1D2D=matrix()
        self.addM1D2D.setDim(self.dS1D.nbDofsPerComp,self.dS2D.nbDofsPerComp)
        self.addM2D.addMatMapij(1.0,self.dS1D.massMat,self.index1DTo2D,self.index1DTo2D,1,1)
        self.addM1D.axpy(1.0,self.dS1D.massMat)
        self.addM2D1D.addMatMapij(-1.0,self.dS1D.massMat,self.index1DTo2D,self.index1DTo2D,1,0)
        self.addM1D2D.addMatMapij(-1.0,self.dS1D.massMat,self.index1DTo2D,self.index1DTo2D,0,1)
        #next, we sample compartment-block-matrix if the compartment is involved in interaction
        for i in range(self.dS1.nbComps):
            if (self.compsInvolved[i]):
                #print("inter2D1D i="+str(i))
                #self.addM1=self.addM2D
                self.addM1.assembleMat(self.addM2D,i*self.dS1.nbDofsPerComp,i*self.dS1.nbDofsPerComp)           
                #self.addM2=self.addM1D
                self.addM2.assembleMat(self.addM1D,i*self.dS2.nbDofsPerComp,i*self.dS2.nbDofsPerComp)
                #self.addM12=self.addM2D1D
                self.addM12.assembleMat(self.addM2D1D,i*self.dS1.nbDofsPerComp,i*self.dS2.nbDofsPerComp)
                #self.addM21=self.addM1D2D
                self.addM21.assembleMat(self.addM1D2D,i*self.dS2.nbDofsPerComp,i*self.dS1.nbDofsPerComp)
###
# CLASS INTERACTION 1D1D
###
class interaction1D1D(interaction):
    """Class used to simulate interaction 2D1D

    :param study aStudy: the study
    :param dynamicalSystem aDS1d1: the first 1d dynamical system
    :param dynamicalSystem aDS1d2: the second 1d dynamical system
    :param numpy.array aCompsInvolved=NONE: the list of the compement involved, if all are involved the value is None
    """
    def __init__(self, aStudy, aDS1d1, aDS1d2, aCompsInvolved=None) -> None:
        super().__init__(aStudy,aDS1d1,aDS1d2,aCompsInvolved)
        self.dS1D1=aDS1d1
        self.dS1D2=aDS1d2
        if (self.dS1D1.geoDim != 1 or self.dS1D1.geoDim != 1):
            interaction.myPrint("ERROR in interaction2D1D, wrong dimension " +str(self.dS2D.aGeoDim)+" "+self.dS1D.aGeoDim)
            exit()
        # ?
        self.c1d2d=1
        self.c2d1D=1
        #
        self.indexG1ToG2=np.zeros((self.dS1D1.nbDofsPerComp,),dtype=np.int32)
        self.indexG2ToG1=np.zeros((self.dS1D2.nbDofsPerComp,),dtype=np.int32)# dS1D1.nbDof == dS1D2.nbDof
        buildMap1DTo2D(self.dS1D1,self.dS1D2,self.indexG1ToG2)
        buildMap1DTo2D(self.dS1D2,self.dS1D1,self.indexG2ToG1)
        #must be done for each compartment
        #self.addM2.axpy(1.0,self.dS1D2.massMat)
        #self.addM1.axpy(1.0,self.dS1D1.massMat)
        #self.addM21.addMatMapij(-1.0,self.dS1D2.massMat,self.indexG2ToG1,self.indexG2ToG1,0,1)
        #self.addM12.addMatMapij(-1.0,self.dS1D1.massMat,self.indexG1ToG2,self.indexG1ToG2,0,1)

class interactionEdgesConnect(interaction):
#  make edges connection
# assume only one vertex is shared
#
#
    def __init__(self, aStudy, aDs1, aDs2, aCompsInvolved=None) -> None:
        
        if (aDs1.geoDim != 1 or aDs2.geoDim != 1):
            interaction.myPrint("ERROR in interactionEdgeConnexion, wrong dimension " +str(ads1.aGeoDim)+" "+ads2.aGeoDim)
            return
        self.index1To2=np.zeros((aDs1.nbDofsPerComp,),dtype=np.int32)
        buildMap1DTo2D(aDs1,aDs2,self.index1To2)
        cmp=0
        self.i1=-1
        self.i2=-1
        for i in self.index1To2:
            if (i != -1):
                if (self.i2>=0):
                    interaction.myPrint("InteractionEdgeConnexion, edges shared more than one vertex, interaction ignored")
                    return
                self.i2=i
                self.i1=cmp
            cmp+=1
        if (self.i2==-1):
            interaction.myPrint("InteractionEdgeConnexion, edges don't shared  vertex, interaction ignored")
            return
        super().__init__(aStudy,aDs1,aDs2,aCompsInvolved)
        self.addM1Block=matrix()
        self.addM1Block.setDim(self.dS1.nbDofsPerComp,self.dS1.nbDofsPerComp)
        self.addM2Block=matrix()
        self.addM2Block.setDim(self.dS2.nbDofsPerComp,self.dS2.nbDofsPerComp)
        self.addM12Block=matrix()
        self.addM12Block.setDim(self.dS1.nbDofsPerComp,self.dS2.nbDofsPerComp)
        self.addM21Block=matrix()
        self.addM21Block.setDim(self.dS2.nbDofsPerComp,self.dS1.nbDofsPerComp)
        self.addM2Block.setAij(self.i2,self.i2,1.0)
        self.addM1Block.setAij(self.i1,self.i1,1.0)
        self.addM21Block.setAij(self.i2,self.i1,-1.0)
        self.addM12Block.setAij(self.i1,self.i2,-1.0)
        #next, we sample compartment-block-matrix if the compartment is involved in interaction
        for i in range(self.dS1.nbComps):
            if (self.compsInvolved[i]):
                #self.addM1=self.addM2D
                self.addM1.assembleMat(self.addM1Block,i*self.dS1.nbDofsPerComp,i*self.dS1.nbDofsPerComp)
                #self.addM2=self.addM1D
                self.addM2.assembleMat(self.addM2Block,i*self.dS2.nbDofsPerComp,i*self.dS2.nbDofsPerComp)
                #self.addM12=self.addM2D1D
                self.addM12.assembleMat(self.addM12Block,i*self.dS1.nbDofsPerComp,i*self.dS2.nbDofsPerComp)
                #self.addM21=self.addM1D2D
                self.addM21.assembleMat(self.addM21Block,i*self.dS2.nbDofsPerComp,i*self.dS1.nbDofsPerComp)
####
###
# CLASS INTERACTION 2D2D
###
#class interaction2D2D(interaction):
#    """Presentation of class
#
#    more details
#
#    :param study aStudy: the study
#    :param dynamicalSystem aDS2d1: the first 2d dynamical system
#    :param dynamicalSystem aDS2d2: the second 2d dynamical system
#    :param numpy.array aCompsInvolved=NONE: the list of the compement involved, if all are involved the value is None
#    """
#    def __init__(self, aStudy, aDS2d1, aDS2d2, aCompsInvolved=None) -> None:
#        super().__init__(aStudy,aDS1d1,aDS1d2,aCompsInvolved)
#        self.dS2D1=aDS2d1
#        self.dS2D2=aDS2d2
#        if (self.dS1D1.geoDim != 2 or self.dS1D1.geoDim != 2):
#            print("ERROR in interaction2D1D, wrong dimension " +str(self.dS2D.aGeoDim)+" "+self.dS1D.aGeoDim)
#            exit()
#        # ?
#        self.c1d2d=1
#        self.c2d1D=1
#        #
#        #TODO: change all
#        self.indexG1ToG2=np.zeros((self.dS1D1.nbDofsPerComp,),dtype=np.int32)
#        self.indexG2ToG1=np.zeros((self.dS1D2.nbDofsPerComp,),dtype=np.int32)# dS1D1.nbDof == dS1D2.nbDof
#        buildMap1DTo2D(self.dS1D1,self.dS1D2,self.indexG1ToG2)
#        buildMap1DTo2D(self.dS1D2,self.dS1D1,self.indexG2ToG1)
#        self.addM2=matrix()
#        self.addM2.setDim(self.dS1D2.nbDofsPerComp,self.dS1D2.nbDofsPerComp)
#        self.addM1=matrix()
#        self.addM1.setDim(self.dS1D1.nbDofsPerComp,self.dS1D1.nbDofsPerComp)
#        self.addM21=matrix()
#        self.addM21.setDim(self.dS1D2.nbDofsPerComp,self.dS1D1.nbDofsPerComp)
#        self.addM12=matrix()
#        self.addM12.setDim(self.dS1D1.nbDofsPerComp,self.dS1D1.nbDofsPerComp)
#        self.addM2.axpy(1.0,self.dS1D2.massMat)
#        self.addM1.axpy(1.0,self.dS1D1.massMat)
#        self.addM21.addMatMapij(-1.0,self.dS1D2.massMat,self.indexG2ToG1,self.indexG2ToG1,0,1)
#        self.addM12.addMatMapij(-1.0,self.dS1D1.massMat,self.indexG1ToG2,self.indexG1ToG2,0,1)
#        # END TODO
