from mse.linAlgTools import matrix,vector
from dolfinx.fem import form,Constant
from petsc4py.PETSc import ScalarType
from dolfinx.fem.petsc import assemble_matrix,assemble_vector
from mse.toolsEnv import computerEnv, MSEPrint, MSEPrint_0  ,MSEError
from sympy import *
from os.path import exists
"""
The model has to built the linearized matrix and the rhs.
The computation is done if :
-> The time-stepping ask to update it, because the current state change.

The computation will not be updated if the model is lenear and time invariant (flag self.computeMatOnce=1)
"""

def strForDolf(strFile, unknownName="abcdefgh"):
    """Fonction qui adapte une chaine de charactere pour lecture par Dolfinix

    Utilise bibliothèque sympy
    a,$a = symbols(" a $a") puis expr.subs(), et enfin str(expr)


    :param str strFile: chaine de caractère auquel on ajoutera le char '$' devant les variables
    :return str: chaine de char avec les '$' devant les var 'a,b,c,d,e,f'.
    """
    return(str(sympify(strFile).subs(list(zip(symbols(' '.join(unknownName)), symbols(' '.join(['$'+k for k in unknownName])))))))

from re import sub

# Fonction add sufix and prefix to upcase words ie covariable
#suffix = "]"
#prefix = "self.dS.dicCov["
#nouveau_texte = ajouter_suffixe_majuscule(texte, prefixe, suffixe)
def formatCovariables(chaine, prefixe, suffixe):
    # L'expression reguliere cherche des mots entierement en majuscules
    regex = r'\b[A-Z]+\b'
    # Fonction de remplacement qui ajoute le suffixe
    def ajouter_suffixe(match):
        return prefixe + match.group(0) + suffixe
    # Utilise re.sub() pour remplacer les mots en majuscules par eux-memes avec le suffixe ajoute
    return sub(regex, ajouter_suffixe, chaine)


class model:
    #chose your Debug mode
    #myPrint=MSEPrint
    myPrint=MSEPrint_0
    def verbose(aBool):
        if (aBool):
            model.myPrint=MSEPrint
        else:
            model.myPrint=MSEPrint_0
    def __init__(self,aDS) -> None:
        self.dS=aDS
       # if the model is lenear and cst in time, mat and rhs are built only once if computeMat/RhsOnce is 1
        #these flags must be set by user
        self.computeMatOnce=0
        self.computeRhsOnce=0
        self.matComputed=False
        self.isLinear=0
        self.nbComps=aDS.nbComps
        #self.blocks=[[None]*nbComps]*nbComps
        self.Fk=None
        self.Ftm1=None
        self.mat=None
        self.Rhs=None
        self.vecBuf=vector()
        self.vecBuf.setDim(self.dS.nbDofs)
    # ABOUT LINEAR PART
    # build the block matrix corresponding to the effect of the compartiment compj on the compartiment compi
    # must be overload in the model implementation
    # _ method because must be protected 
    def updateBlockMat(self,compi,compj):
        raise VirtualException()
    # build the matrix of the model by assembling block
    # can be overload if the matrix is constant ie the model is linear
    def updateLinearisedMat(self):
        if ((self.mat is None ) or (not self.computeMatOnce) or (not self.matComputed)):
            self.matComputed=True
            self.mat=matrix()
            self.mat.setDim(self.dS.nbDofs,self.dS.nbDofs)
            for i in range(self.nbComps):
                for j in range(self.nbComps):
                    block=self.updateBlockMat(i,j)
                    #block=self.blocks[i][j]
                    if (block):
                        self.mat.assembleMat(block,i*self.dS.nbDofsPerComp,j*self.dS.nbDofsPerComp)
    def getLinearisedMat(self):
        self.updateLinearisedMat()
        return self.mat
    #ABOUT RHS
    # update the Fk[compi] with vector
    def updateBlockFk(self,compi):
        raise VirtualException()
    def updateBlockFtm1(self,compi):
        raise VirtualException()
    def updateFtm1(self):
        if (self.Ftm1 is None):
            self.Ftm1=vector()
            self.Ftm1.setDim(self.nbComps*self.dS.nbDofsPerComp)
        self.Ftm1.zero()
        for i in range(self.nbComps):
            blockVec=self.updateBlockFtm1(i)
            self.Ftm1.assembleVec(blockVec,i*self.dS.nbDofsPerComp)
    def updateFk(self):
        if (self.Fk is None):
            self.Fk=vector()
            self.Fk.setDim(self.nbComps*self.dS.nbDofsPerComp)
        self.Fk.zero()
        for i in range(self.nbComps):
            blockVec=self.updateBlockFk(i)
            self.Fk.assembleVec(blockVec,i*self.dS.nbDofsPerComp)
    #assume that self.mat is up to date
    def _updateRhs(self):
        self.Rhs.zero()
        self.updateFk()
        #myPrint("debug _updateRhs",self.Fk.petscVec.array)
        if (self.mat):
            self.vecBuf.setValuesFromState(self.dS.uk)
            self.mat.mult(self.vecBuf,self.Rhs)
        else:
            model.myPrint("warning, model has not linear part")
        self.Rhs.scale(-1.0)
        self.Rhs.axpy(1.0,self.Fk)
        
    # return F(\Theta,U_k)-\partial_UF(t,U_k)(U_k)  see models.tex.
    def buildRhs(self):
        if ((self.Rhs is None) or (not self.computeRhsOnce)):
            if (self.Rhs is None):
                self.Rhs=vector()
                self.Rhs.setDim(self.dS.nbDofs)
            self._updateRhs()
    def getRhs(self):
        return self.Rhs
    def getFtm1(self):
        return self.Ftm1
    def enableAdjoint(self):
        """Function to check if derivate file to param exist, else create it
        """
        raise VirtualException()
class txtModel(model):
    """Class used to add source term in model.

    The model is stored in a txt file and applied to a dynamical system.


    :param dynamicalSystem aDs: class allows applying a PDE to a geometry
    :param str aTxtFile: file which contain source term, file = aTxtFile+".txt". An other file with partial derivate can be create in the same directory, file = aTxtFile+"Dv.txt".
"""
    #note that strModel is val passed, the string is not set calling this function
    def _subFXForm(self,strModel,X):
        for j in range(self.nbComps):
            strModel=strModel.replace("$"+txtModel.unknownName[j],"self.dS."+X+".comps["+str(j)+"]")
        code="form(("+strModel+")"
        model.myPrint("X ->"+code)
        return code
    unknownName="abcdefgh"
    def getParam(self,i):
        return self.dS.study.param[i]
    def __init__(self,aDs,aTxtFile) -> None:
        super().__init__(aDs)
        if (exists(aTxtFile+".txt")):
            self.txtFile=aTxtFile
        else:
            self.txtFile=computerEnv.modelsDir+aTxtFile #aTxtFile form is "affineGrowth"
        model.myPrint("Looking for model  "+self.txtFile)
        try:
            f = open(self.txtFile+".txt", "r")
        except IOError:
            MSEError("IOError in txtModel when reading "+self.txtFile+".txt")
        try:
            fd = open(self.txtFile+"Dv.txt", "r")
        except FileNotFoundError:
            self.dvFileExist=False
            model.myPrint("Création fichier "+self.txtFile+"Dv.tx")
            fd = open(self.txtFile+"Dv.txt", "w") #open file in mod write
            for numComps in range(self.nbComps):
                strF=f.readline()
                strF=strF.split('=')[1].strip()#.replace("^","**")
                for k in range(self.nbComps):
                    #On utilise Sympy pour calculer derivee et ecrire dans le fichier
                    #search the derivate of "strline"
                    grad1s=diff(strF,txtModel.unknownName[k])
                    grad1=str(grad1s).replace("**","^")
                    #write in fic
                    fd.write("d"+txtModel.unknownName[k]+"f"+txtModel.unknownName[numComps]+"="+grad1+"\n")
            f.close()
            fd.close()
            f = open(self.txtFile+".txt", "r")
            try:
                fd = open(self.txtFile+"Dv.txt", "r")
            except IOError:
                MSEError("IOError in txtModel when reading "+self.txtFile+"Dv.txt")
        except IOError:
            MSEError("IOError in txtModel when reading "+self.txtFile+"Dv.txt")
        self.varfs=[None]*(self.nbComps*(self.nbComps+1))
        self.varfsTm1=[None]*self.nbComps
        #for the comp number numComp \in {0,..,self.nbComps-1}:
        #self.varfs[numComp*(self.nbComps+1)] contains the varf F_numComp for the rhs
        #self.varfs[numComp*(self.nbComps+1)+(j+1)] contains the derivative d_jF_numComp, j \in {0,..,self.nbComps-1}
        #strkeys=r"[\s\+\-\*/\=\(\)\^]+"
        for numComps in range(self.nbComps):
            ##READ SOURCE TERM
            strline=f.readline().split('=')[1].strip()
            strline=strForDolf(strline, self.unknownName)
            strline=strline.replace('^','**')
            ##replace param
            for count,p in enumerate(self.dS.paramD): 
                strline=strline.replace('p'+str(count+1),'self.dS.paramD['+str(count)+']')
            model.myPrint(" read txt model F"+txtModel.unknownName[numComps]+"="+strline)
            #### replace COVARIABLES
            strline=formatCovariables(strline, "self.dS.dicCov[\"", "\"]")
            model.myPrint(strline)
            #### BUILD VARF USING FK
            code="self.varfs["+str(numComps)+"*(self.nbComps+1)]="+self._subFXForm(strline,"uk")+"*self.dS.v*self.dS.dx)"
            exec(compile(code, 'sumstring', 'exec'))
            #### BUILD VARF USING UTM1
            code="self.varfsTm1["+str(numComps)+"]="+self._subFXForm(strline,"utm1")+"*self.dS.v*self.dS.dx)"
            model.myPrint(code)
            exec(compile(code, 'sumstring', 'exec'))
            ## READ DERIVATIVES
            for k in range(self.nbComps):
                strline=fd.readline().split('=')[1].strip() #modif harry f to fd
                strline=strForDolf(strline,self.unknownName)
                #strline=strline.replace('^','**')
                ##replace param
                for count,p in enumerate(self.dS.paramD): 
                    strline=strline.replace('p'+str(count+1),'self.dS.paramD['+str(count)+']')
                strline=formatCovariables(strline, "self.dS.dicCov[\"", "\"]")
                model.myPrint("txtModel: read txt model d"+txtModel.unknownName[k]+"F"+txtModel.unknownName[numComps]+"="+strline)
                code="self.varfs["+str(numComps)+"*(self.nbComps+1)+"+str(k)+"+1]="+self._subFXForm(strline,"uk")+"*self.dS.u*self.dS.v*self.dS.dx)"
                exec(compile(code, 'sumstring', 'exec'))
        aDs.addSourcesTerm(self)
    def updateBlockMat(self,compi,compj):
        model.myPrint(str(compi)+" "+str(compj))
        return matrix(assemble_matrix(self.varfs[compi*(self.nbComps+1)+compj+1]))
    def updateBlockFk(self,compi):
        return vector(assemble_vector(self.varfs[compi*(self.nbComps+1)]))
    def updateBlockFtm1(self,compi):
        return vector(assemble_vector(self.varfsTm1[compi]))
    def enableAdjoint(self):
        """Function used for the adjoint system, check whether all the necessary files exist, and if not, creates them.
        """
        try:
            f = open(self.txtFile+".txt", "r")
        except IOError:
            self.myPrint("IOError in txtModel when reading "+self.txtFile+".txt")
            return 
        try:
            fp = open(self.txtFile+"Dp"+".txt", "r")
        except FileNotFoundError:
            #open file in mod write
            fp = open(self.txtFile+"Dp.txt", "w") 
            for numComp in range(self.nbComps):
                strF=f.readline()
                strF=strF.split('=')[1].strip()
                for k in range(self.dS.study.nbParam):
                    #we used Sympy to calculate Dv et wrote it in the file
                    #search the derivate of "strline"
                    grad1s=diff(strF,"p"+str(k+1))
                    grad1=str(grad1s).replace("**","^")
                    #write in fic
                    fp.write("d"+"p"+str(k+1)+"f"+self.unknownName[numComp]+"="+grad1+"\n")
        except IOError:
            self.myPrint("IOError in txtModel when reading "+self.txtFile+"Dp.txt")
            return 
        f.close()
        fp.close()
#
# f(u)=r(K-u)
# obsolet
class affineGrowth(model):
    def __init__(self,aDs,aR,aK) -> None:
        super().__init__(aDs)
        self.R=Constant(aDs.dolfinMesh, ScalarType(aR))
        self.K=Constant(aDs.dolfinMesh, ScalarType(aK))
        #self.bilinearForm=None
        #self.rhs=None
        self.islinear=1
        aux=-self.R*self.dS.u*self.dS.v*self.dS.dx
        self.bilinearForm=form(aux)
        self.block=matrix(assemble_matrix(self.bilinearForm))
        self.updateLinearisedMat()
        aux2=self.R*self.K*self.dS.v*self.dS.dx
        self.Fkblock=vector(assemble_vector(form(aux2)))
        aux3=self.R*(self.K-self.dS.utm1[0])*self.dS.v*self.dS.dx
        self.FTm1block=vector(assemble_vector(form(aux3)))
        self.buildRhs()
        self.computeMatOnce=1
        self.computeRhsOnce=1
        aDs.addSourcesTerm(self)
    def updateBlockMat(self,compi,compj):
        if (compi==compj):
            return self.block
        return None
    def updateBlockFk(self,compi):
        return self.Fkblock
    def updateBlockFtm1(self,compi):
        return self.FTm1block
#
# f(u)=G
# obsolet
class growth(model):
    def __init__(self,aDs,aG) -> None:
        super().__init__(aDs)
        self.G=Constant(aDs.dolfinMesh, ScalarType(aG))
        #self.bilinearForm=None
        #self.rhs=None
        self.islinear=1
        aux2=self.G*self.dS.v*self.dS.dx
        self.Fkblock=vector(assemble_vector(form(aux2)))
        self.buildRhs()
        self.computeMatOnce=1
        self.computeRhsOnce=1
        aDs.addSourcesTerm(self)
    def updateBlockMat(self,compi,compj):
        return None
    def updateBlockFk(self,compi):
        return self.Fkblock
    def updateBlockFTm1(self,compi):
        return self.Fkblock
