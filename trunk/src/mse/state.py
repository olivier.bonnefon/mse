from dolfinx.io import VTKFile
from dolfinx.fem import Function
import pymseMatTools.mseMatTools as MT
from numpy.linalg import norm as npnorm
from mse.exception import mseFileErr

class state:
    def __init__(self,aDs):
        self.dS=aDs
        #the compartiments of the model-dynamicalSystem
        self.comps=[]
        self.nbComps=self.dS.nbComps
        for i in range(self.dS.nbComps):
            self.comps.append(Function(self.dS.vSpace))
        self.exportFiles=[]
    def __new__(cls, *args):
        return object.__new__(cls)
    def initFilesToExportVtk(self,filePrefix):
        """ 
        prepar vtk export in pvd file for animation

        :param string filePrefix : prefix name for vtk file 

        """
        for i in range(self.nbComps):
            self.exportFiles.append(VTKFile(self.dS.dolfinMesh.comm, filePrefix+"_comp"+str(i)+".pvd","w"))
    def closeFilesToExportVtk(self):
        """
        close the file containing vtk animation
        """
        for f in self.exportFiles:
            f.close()
        self.exportFiles=[]
    def exportVtk(self,nStep):
        """
        build a vtk file with self and add in animation file

        :param real nStep: the number of step or time
        """
        indexComp=0
        for indexComp, f in enumerate(self.exportFiles):
            f.write_function(self.comps[indexComp],nStep)
    def vectorToState(self,aVec,curIndexInVec):
        from mse.linAlgTools import vector
        lcurDof=curIndexInVec
        for comp in self.comps:
            comp.x.array[:]=aVec.petscVec.array[lcurDof:lcurDof+self.dS.nbDofsPerComp]
            lcurDof=lcurDof+self.dS.nbDofsPerComp
    def scale(self,alpha):
        """
        self=alpha*self
        """
        for comp in self.comps:
            comp.x.array[:]=alpha * comp.x.array[:]
    def zero(self):
        """
        self=0
        """
        for comp in self.comps:
            comp.x.array[:]=0
    def axpy(self,aOtherState,alpha):
        """
        self=self+alpha*otherState
        
        """
        for (comp,otherComp) in zip(self.comps,aOtherState.comps):
            comp.x.array[:]=comp.x.array[:]+alpha*otherComp.x.array[:]
    def setFromState(self,aOtherState):
        for i in range(self.nbComps):
            self.comps[i].x.array[:]=aOtherState.comps[i].x.array[:]
    def save(self,filePrefix,numStep):
        numComp=0
        for comp in self.comps:
            MT.saveVec(filePrefix+"_comp"+str(numComp)+"_"+str(numStep),comp.x.array)
            numComp=numComp+1
    def load(self,filePrefix,numStep):
        numComp=0
        for comp in self.comps:
            #print("state.load "+filePrefix+"_comp"+str(numComp)+"_"+str(numStep))
            if (MT.loadVec(filePrefix+"_comp"+str(numComp)+"_"+str(numStep),comp.x.array)):
                raise mseFileErr("ERREUR loading "+filePrefix+"_comp"+str(numComp)+"_"+str(numStep))
            numComp=numComp+1
    def norm(self):
        res=0
        for comp in self.comps:
            res+=npnorm(comp.x.array)
        return res


