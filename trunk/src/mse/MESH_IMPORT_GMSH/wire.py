import matplotlib.mlab as mlab
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse, Polygon
import mse.MESH_IMPORT_GMSH.edge as Edge
class Wire:
    verbose=0
    colors=['y','r','b','g']
    curColor=0
    def  __init__(self, id,nEdges,label):
        self.id=id
        self.nEdges=nEdges
        self.label=label
        self.pts=[]
        self.edges=[]
        self.signed_edges=[]
        self.reverse=[]

    def add_edge(self,Edge,reverse):
        self.edges.append(int(Edge.id))
        if (not reverse):
            self.signed_edges.append(int(Edge.id))
            self.reverse.append(int(0))
            ptb=Edge.ptb
            self.pts.append(ptb)
            num=1
            while(num<=Edge.nbPt):
                npt=int(Edge.pts[num-1])
                self.pts.append(npt)
                num=num+1
        else:
            self.signed_edges.append(int(-Edge.id))
            self.reverse.append(int(1))
            self.pts.append(Edge.pte)
            num=Edge.nbPt
            while(num>=1):
                npt=int(Edge.pts[num-1])
                self.pts.append(npt)
                num=num-1

    def plot(self,Points, emptyPoly ,drawPoint,withName):
        npts=len(self.pts)
        labelpt=[]
        poly=np.zeros((npts+1,2))
        num=1
        barx=0;
        bary=0;
        while(num<=npts):
            poly[num-1,0]=Points[self.pts[num-1]-1,0]
            poly[num-1,1]=Points[self.pts[num-1]-1,1]
            barx=barx+Points[self.pts[num-1]-1,0]
            bary=bary+Points[self.pts[num-1]-1,1]
            if (drawPoint):
                plt.text(Points[self.pts[num-1]-1,0],Points[self.pts[num-1]-1,1],"  "+str(self.pts[num-1]))
            if (Wire.verbose): print("num point ="+str(self.pts[num-1]))
            num=num+1
        poly[num-1,0]= poly[0,0]
        poly[num-1,1]= poly[0,1]
        barx=barx/npts
        bary=bary/npts
        if (withName):
            plt.text(barx,bary,self.label, fontsize=10)
        if (Wire.verbose): print(str(barx)+" "+str(bary))
        if (emptyPoly):
            #plt.plot(poly[:,1],poly[:,0],'o')
            plt.plot(poly[:,0],poly[:,1])
        else:
            #pl=Polygon(zip(poly[:,0],poly[:,1]), closed=True,color= Wire.colors[Wire.curColor % len(Wire.colors)])
            pl=Polygon(poly, closed=True,color= Wire.colors[Wire.curColor % len(Wire.colors)])
            plt.gca().add_patch(pl)
            Wire.curColor=Wire.curColor+1
            plt.plot(poly[:,0],poly[:,1])
