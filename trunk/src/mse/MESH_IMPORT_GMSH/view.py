from mse.MESH_IMPORT_GMSH.read import geomTopoReader
import mse.MESH_IMPORT_GMSH.edge as Edge
import mse.MESH_IMPORT_GMSH.wire as Wire
import numpy as np
from matplotlib.pyplot import *
from matplotlib import use
use('Qt5Agg')
class viewTopo:
    def __init__(self,aTopo):
        self.topo=aTopo
        self.WireDrawPoint=1
        self.WireEmptyPoly=0
        self.WireWithName=1
        self.Emin=-1
        self.Emax=10000
        self.Wmin=-1
        self.Wmax=1000
        self.xmin=np.amin(aTopo.Points[:,[0]])
        self.xmax=np.amax(aTopo.Points[:,[0]])
        offset=0.05*(self.xmax-self.xmin)
        self.xmin -= offset
        self.xmax += offset
        self.ymin=np.amin(aTopo.Points[:,[1]])
        self.ymax=np.amax(aTopo.Points[:,[1]])
        offset=0.05*(self.ymax-self.ymin)
        self.ymin -= offset
        self.ymax += offset
        print(str(self.xmin)+" "+str(self.ymin))
 
        
    def plotAll(self):
        for i_edges in range(1,len(self.topo.Edges)):
            aE = self.topo.Edges[i_edges]
            if (aE.id >= self.Emin and aE.id <= self.Emax):
                aE.plot(self.topo.Points,self.topo.labelPt,1)
        for aW in self.topo.Wires:
            if (aW.id >=self.Wmin and aW.id <=self.Wmax):
                    aW.plot(self.topo.Points,self.WireEmptyPoly,self.WireDrawPoint,self.WireWithName)
    def plotEdges(self,edgesId):
        for aE in self.topo.Edges:
            for aId in edgesId:
                if (aE.id == aId):
                    aE.plot(self.topo.Points,self.topo.labelPt,1)
    def plotWires(self,wiresId):
        for aW in self.topo.Wires:
            for aId in wiresId:
                if (aW.id == aId):
                    aW.plot(self.topo.Points,self.WireEmptyPoly,self.WireDrawPoint,self.WireWithName)    
    def show(self):
        xlim([self.xmin, self.xmax])
        ylim([self.ymin, self.ymax])
        axis('off')
        savefig(self.topo.name+"_topo.eps")
        show()
