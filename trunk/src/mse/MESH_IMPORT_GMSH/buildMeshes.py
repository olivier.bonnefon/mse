import mse.MESH_IMPORT_GMSH.read

import gmsh
import meshio
import sys
import numpy as np
import os.path
from os import mkdir
import subprocess
from mse.toolsEnv import MSEBuildDir
verbose=0
#############################
## Write one mesh for per domain
#############################
def buildXdmf(aTopo,target_directory, mesh_size,aMeshCallBack=None):
    #############################
    ## Write Mesh for all domains
    #############################
    st_mesh="mesh"
    aTopo.meshDir=target_directory
    MSEBuildDir(target_directory)
    # Create gmsh geometric model
    gmsh.initialize(sys.argv)
    gmsh.model.add("create_mesh")
    # Create Mesh Points
    gmsh_points = [gmsh.model.geo.addPoint(aTopo.Points[i_points][0],aTopo.Points[i_points][1],aTopo.Points[i_points][2], meshSize=mesh_size)
            for i_points in range(len(aTopo.Points))]
    # Create Mesh aTopo.Edges
    gmsh_lines = [-1] # bidon
    for iE in range(1,len(aTopo.Edges)):
        if (aTopo.Edges[iE].nbPt<1):
            gmsh_lines.append(gmsh.model.geo.addLine(gmsh_points[aTopo.Edges[iE].ptb-1], gmsh_points[aTopo.Edges[iE].pte-1]))
        else:
            spline = [gmsh_points[aTopo.Edges[iE].ptb]-1]
            for i_points in range(aTopo.Edges[iE].nbPt):
                spline.append(gmsh_points[aTopo.Edges[iE].pts[i_points]-1])
            spline.append(gmsh_points[aTopo.Edges[iE].pte-1])
            gmsh_lines.append(gmsh.model.geo.addPolyline(spline))
            #gmsh_lines.append(gmsh.model.geo.addLine(spline))
    # Create Mesh aTopo.Wires
    test_edges = np.zeros(len(aTopo.Edges))
    gmsh_loop = []
    gmsh_1D_loops = []
    for iW in range(len(aTopo.Wires)):
        loop = []
        for iE in range(len(aTopo.Wires[iW].edges)):
            if (test_edges[aTopo.Wires[iW].edges[iE]] == 0):
                test_edges[aTopo.Wires[iW].edges[iE]] = 1
            if (aTopo.Wires[iW].reverse[iE]):
                loop.append(-gmsh_lines[aTopo.Wires[iW].edges[iE]])
            else:
                loop.append(gmsh_lines[aTopo.Wires[iW].edges[iE]])
        gmsh_loop.append(gmsh.model.geo.addCurveLoop(loop))
        gmsh_1D_loops.append(loop)
    gmsh_surface = [gmsh.model.geo.addPlaneSurface([gmsh_loop[i_loop]])
            for i_loop in range(len(aTopo.Wires))]
    # Generate 2D Mesh with triangle elements
    gmsh.model.geo.synchronize()
    if (aMeshCallBack):
         gmsh.model.mesh.setSizeCallback(aMeshCallBack)
    gmsh.model.mesh.generate(2)
    # Create Physical Groups for aTopo.Edges
    for iE in range(1,len(aTopo.Edges)):
        if (test_edges[iE]):
            pl = gmsh.model.addPhysicalGroup(1, [gmsh_lines[iE]])
            gmsh.model.setPhysicalName(1, pl, 'E'+str(aTopo.Edges[iE].id))
    # Create Physical Groups for aTopo.Wires
    for i_loop in range(len(aTopo.Wires)):
        ps = gmsh.model.addPhysicalGroup(2, [gmsh_surface[i_loop]])
        gmsh.model.setPhysicalName(2, ps, 'W'+str(aTopo.Wires[i_loop].id))
        if (verbose): print("W"+str(aTopo.Wires[i_loop].id)+"="+str(ps))
    # Write MSH Mesh
    #st = target_directory+st_mesh + '.med'
    st = target_directory+st_mesh + '.msh'
    gmsh.write(st)
    #out = subprocess.run('gmsh -2 ' + st, shell=True)
    gmsh.finalize()
    st_mesh_msh = target_directory+st_mesh + '.msh'
    msh = meshio.read(st_mesh_msh)
    msh.field_data = { key.strip() : value for key, value in msh.field_data.items() } # Remove white space in key
    #list of geom label E1,E2,..,W1,W2
    listKeys= list(msh.field_data.keys())
    for aWire in aTopo.Wires:
        #Building line XDMF
        flag_type_line = 0
        wire_dict={}
        for iE in aWire.edges:
            aKeyEdgesOfW="E"+str(iE)
            wire_dict[aKeyEdgesOfW]=msh.field_data[aKeyEdgesOfW]
            indexEdegInCellData=listKeys.index(aKeyEdgesOfW)
            line_cells_edge = msh.cells[indexEdegInCellData].data
            line_data_l_edge=np.full(msh.cells[indexEdegInCellData].data.shape[0],msh.field_data[aKeyEdgesOfW][0])
            line_mesh_edge = meshio.Mesh(points=msh.points[:, :2],
                                    cells=[("line", line_cells_edge)],
                                    cell_data={"boundaries":[line_data_l_edge]})
            meshio.write(target_directory+"/edge"+str(iE)+".xdmf", line_mesh_edge)
            if flag_type_line == 0:
                flag_type_line=1
                line_cells =line_cells_edge 
                line_data_l=line_data_l_edge
            else:
                line_cells=np.concatenate((line_cells,line_cells_edge))
                line_data_l=np.concatenate((line_data_l,line_data_l_edge))
        line_mesh = meshio.Mesh(points=msh.points[:, :2],
                                    cells=[("line", line_cells)],
                                    cell_data={"boundaries":[line_data_l]})
        meshio.write(target_directory+"/boundary"+str(aWire.id)+".xdmf", line_mesh)
        #End line XDMF
        #Build triangle XDMF
        aKeyOfW=aWire.label # sould be W1, W2 ..
        wire_dict[aKeyOfW]=msh.field_data[aKeyOfW]
        indexTriangleInCellData=listKeys.index(aKeyOfW)
        #print(listKeys)
        #print(indexTriangleInCellData)
        #print(msh.cells)
        #print(msh.cells[indexTriangleInCellData])
        #exit(0)
        triangle_cells =  msh.cells[indexTriangleInCellData].data
        triangle_data_l = np.full(msh.cells[indexTriangleInCellData].data.shape[0],msh.field_data[aKeyOfW][0])
        triangle_mesh = meshio.Mesh(points=msh.points[:, :2],
                                    cells=[("triangle", triangle_cells)],
                                    cell_data={"subdomain":[triangle_data_l]},
                                    field_data=wire_dict)
        meshio.write(target_directory+"/surface"+str(aWire.id)+".xdmf", triangle_mesh)
        #End triangle XDMF
    #build a 1d mesh containing all edges
    edges_dict={}
    #flag_type_line = 1
    line_cells=np.empty((0,2))
    line_data_l=np.array([])
    if (verbose): print("doing meshes with all edges")
    for iE in range(1,len(aTopo.Edges)):
        if (not test_edges[iE]):
            continue
        if (verbose): print("add edge"+str(iE))
        aKeyEdges="E"+str(iE)
        edges_dict[aKeyEdges]=msh.field_data[aKeyEdges]
        indexEdegInCellData=listKeys.index(aKeyEdges)
        #if flag_type_line == 0:
        #    flag_type_line=1
        #    line_cells = msh.cells[indexEdegInCellData].data
        #    line_data_l=np.full(msh.cells[indexEdegInCellData].data.shape[0],msh.field_data[aKeyEdges][0])
        #else:
        line_cells=np.concatenate((line_cells,msh.cells[indexEdegInCellData].data))
        line_data_l=np.concatenate((line_data_l,np.full(msh.cells[indexEdegInCellData].data.shape[0],msh.field_data[aKeyEdges][0])))
    line_mesh = meshio.Mesh(points=msh.points[:, :2],
                            cells=[("line", line_cells)],
                            cell_data={"edges":[line_data_l]},
                            field_data=edges_dict)
    meshio.write(target_directory+"/AllEdges.xdmf", line_mesh)

#############################
## Write 1d mesh
#############################
def build1dXdmf(aTopo,target_directory, mesh_size,aMeshCallBack=None):
    ################
    ## Write 1d Mesh 
    ################
    st_mesh="mesh"
    aTopo.meshDir=target_directory
    MSEBuildDir(target_directory)
    # Create gmsh geometric model
    gmsh.initialize(sys.argv)
    gmsh.model.add("create_1d_mesh")
    # Create Mesh Points
    gmsh_points = [gmsh.model.geo.addPoint(aTopo.Points[i_points][0],aTopo.Points[i_points][1],aTopo.Points[i_points][2], meshSize=mesh_size)
            for i_points in range(len(aTopo.Points))]
    # Create Mesh aTopo.Edges
    gmsh_lines = []
    for iE in range(len(aTopo.Edges)):
        if (aTopo.Edges[iE].nbPt<1):
            gmsh_lines.append(gmsh.model.geo.addLine(gmsh_points[aTopo.Edges[iE].ptb-1], gmsh_points[aTopo.Edges[iE].pte-1]))
        else:
            spline = [gmsh_points[aTopo.Edges[iE].ptb-1]]
            for i_points in range(aTopo.Edges[iE].nbPt):
                spline.append(gmsh_points[aTopo.Edges[iE].pts[i_points]-1])
            spline.append(gmsh_points[aTopo.Edges[iE].pte-1])
            gmsh_lines.append(gmsh.model.geo.addSpline(spline))
            #gmsh_lines.append(gmsh.model.geo.addLine(spline))
    gmsh.model.geo.synchronize()
    gmsh.model.mesh.generate(2)
    # Create Physical Groups for Edges
    for i_edges in range(1,len(aTopo.Edges)):
        pl = gmsh.model.addPhysicalGroup(1, [gmsh_lines[i_edges]])
        gmsh.model.setPhysicalName(1, pl, 'E'+str(aTopo.Edges[i_edges].id))
    # Write MSH Mesh
    st = target_directory+st_mesh + '.msh'
    gmsh.write(st)
    gmsh.finalize()
    st_mesh_msh = target_directory+st_mesh + '.msh'
    msh = meshio.read(st_mesh_msh)
    msh.field_data = { key.strip() : value for key, value in msh.field_data.items() } # Remove white space in key
    #list of geom label E1,E2,..,W1,W2
    listKeys= list(msh.field_data.keys())
    #build a 1d mesh containing all edges
    test_edges = np.zeros(len(aTopo.Edges))
    edges_dict={}
    #flag_type_line = 1
    line_cells=np.empty((0,2))
    line_data_l=np.array([])
    if (verbose): print("doing meshes with all edges")
    for iE in range(1,len(aTopo.Edges)):
        if (verbose): print("add edge"+str(iE))
        aKeyEdges="E"+str(iE)
        edges_dict[aKeyEdges]=msh.field_data[aKeyEdges]
        indexEdegInCellData=listKeys.index(aKeyEdges)
        line_cells=np.concatenate((line_cells,msh.cells[indexEdegInCellData].data))
        line_data_l=np.concatenate((line_data_l,np.full(msh.cells[indexEdegInCellData].data.shape[0],msh.field_data[aKeyEdges][0])))
    line_mesh = meshio.Mesh(points=msh.points[:, :2],
                            cells=[("line", line_cells)],
                            cell_data={"edges":[line_data_l]},
                            field_data=edges_dict)
    meshio.write(target_directory+"/AllEdges.xdmf", line_mesh)
