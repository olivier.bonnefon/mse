import mse.MESH_IMPORT_GMSH.edge as Edge
import mse.MESH_IMPORT_GMSH.wire as Wire
import numpy as np
import subprocess
import os.path
from mse.toolsEnv import MSEPrint
class geomTopoReader:
    verbose=0
    reverseXY=False
    def __init__(self,geo_directory,fileSuffix=""):
        self.fileSuffix=fileSuffix
        self.name=geo_directory.split('/')[-1]
        if (len(self.name)==0):
            self.name=geo_directory.split('/')[-2]
        Edge.Edge.verbose=geomTopoReader.verbose
        Wire.Wire.verbose=geomTopoReader.verbose
        self.geoDirectory=geo_directory
        #READ POINTS
        line = subprocess.check_output(['wc', '-l', geo_directory+"Points"+self.fileSuffix+".txt"])
        aux=line.strip().split()
        self.nbPt=int(aux[0])
        self.Points=np.zeros((self.nbPt,3))
        self.labelPt=[]
        #self.labelPt.append("bidon")
        self.meshDir=""
        #num of point in Points.txt start to 1. so indexPoint=numPoint-1
        with open(geo_directory+"Points"+self.fileSuffix+".txt", "r") as dataFile:
            for line in dataFile:
                aux = line.strip().split()
                indexPt=int(aux[0])-1
                if (indexPt<0):
                    MSEPrint(" index of point in Points.txt must be >0")
                    raise RuntimeError("index point negarif")
                if (geomTopoReader.verbose) : print("Point="+str(numPt))
                if (geomTopoReader.reverseXY):
                    self.Points[indexPt,1]=float(aux[1])
                    self.Points[indexPt,0]=float(aux[2])
                else:
                    self.Points[indexPt,0]=float(aux[1])
                    self.Points[indexPt,1]=float(aux[2])
                self.labelPt.append(aux[3])
        #READ EDGES
        self.Edges=[]
        aE=Edge.Edge(0,0,0,0,"",0)
        self.Edges.append(aE)
        cmpE=1
        edgefile=geo_directory+"Edges"+self.fileSuffix+".txt"
        with open(edgefile, "r") as dataFile:
            for line in dataFile:
                aux = line.strip().split()
                typeEdge=int(aux[0])
                if (geomTopoReader.verbose) : print("Edge="+str(aux))
                numNb=int(aux[1])
                numNe=int(aux[2])
                numPt=int(aux[3])
                label=aux[3+1+numPt]
                numEdge=int(aux[3+1+numPt+1])
                aE=Edge.Edge(numEdge,numNb,numNe,numPt,label,typeEdge)
                #Edges.append(aE)
                num=1
                while (num<=numPt):
                    aE.add_pt(int(aux[3+num]))
                    num=num+1
                cmpE=cmpE+1
                self.Edges.append(aE)
        #READ DOMAINS
        self.Wires=[]
        cmpW=1
        WireDrawPoint=1
        WireEmptyPoly=0
        WireWithName=1
        wireFile=geo_directory+"Wires"+self.fileSuffix+".txt"
        if (os.path.exists(wireFile)):
            with open(wireFile, "r") as dataFile:
                for line in dataFile:
                    aux = line.strip().split()
                    if (geomTopoReader.verbose) : print("Wire="+str(aux))
                    numW=int(aux[0])
                    nbE=int(aux[1])
                    W=Wire.Wire(numW,nbE,"W"+str(numW))
                    num=1
                    if (geomTopoReader.verbose) : print("numW="+str(numW)+"nb Edges="+str(nbE))
                    while(num<=nbE):
                        if (geomTopoReader.verbose) : print("rep "+str(num)+" "+str(nbE))
                        reverse=0
                        numE=int(aux[1+num])
                        if (geomTopoReader.verbose) : print(numE)
                        if (numE<-0.1):
                            reverse=1
                            numE=-numE
                        if (geomTopoReader.verbose) : print("num edge="+str(numE))
                        E=self.Edges[numE]
                        W.add_edge(self.Edges[numE],reverse)
                        num=num+1
                    W.label=aux[num+1]
                    cmpW=cmpW+1
                    self.Wires.append(W)
        else:
           print(wireFile+" doesn t exist")

    def exportMinimal(self):
        #first, get point and edges used
        usedPoints=np.zeros(self.nbPt+1)
        usedEdges=np.zeros(len(self.Edges)+1)
        ie=1
        ip=1
        fedges = open(self.geoDirectory+"Edges"+self.fileSuffix+"_Min.txt","w")
        fpoints = open(self.geoDirectory+"Points"+self.fileSuffix+"_Min.txt","w")
        fwires = open(self.geoDirectory+"Wires"+self.fileSuffix+"_Min.txt","w")
        for aw in self.Wires:
            fwires.write(str(int(aw.id))+" "+str(int(aw.nEdges))+" ")
            for (ae,reverse) in zip(aw.edges,aw.reverse):
                newedge=False
                aEdge=self.Edges[ae]
                if (usedEdges[ae]==0):
                    usedEdges[ae]=ie
                    fedges.write(str(int(aEdge.edtype))+" ")
                    newedge=True
                    ie+=1
                if (usedPoints[aEdge.ptb]==0):
                    usedPoints[aEdge.ptb]=ip
                    newpt=aEdge.ptb
                    fpoints.write(str(int(usedPoints[newpt]))+" "+str(self.Points[newpt-1,0])+" "+str(self.Points[newpt-1,1])+" "+self.labelPt[newpt-1]+"\n")

                    ip+=1
                if (newedge): fedges.write(str(int(usedPoints[aEdge.ptb]))+" ")
                if (usedPoints[aEdge.pte]==0):
                    usedPoints[aEdge.pte]=ip
                    newpt=aEdge.pte
                    fpoints.write(str(int(usedPoints[newpt]))+" "+str(self.Points[newpt-1,0])+" "+str(self.Points[newpt-1,1])+" "+self.labelPt[newpt-1]+"\n")
                    ip+=1
                if (newedge):
                    fedges.write(str(int(usedPoints[aEdge.pte]))+" ")
                    fedges.write(str(len(aEdge.pts))+" ")
                for ap in aEdge.pts:
                    if (usedPoints[ap]==0):
                        usedPoints[ap]=ip
                        newpt=ap
                        fpoints.write(str(int(usedPoints[newpt]))+" "+str(self.Points[newpt-1,0])+" "+str(self.Points[newpt-1,1])+" "+self.labelPt[newpt-1]+"\n")
                        ip+=1
                    if (newedge): fedges.write(str(int(usedPoints[newpt]))+" ")
                if (newedge): fedges.write(aEdge.label+" "+str(int(usedEdges[aEdge.id]))+"\n")
                if (reverse): fwires.write("-")
                fwires.write(str(int(usedEdges[aEdge.id]))+" ")
            fwires.write(aw.label+"\n")
        fpoints.close()
        fedges.close()
        fwires.close()

