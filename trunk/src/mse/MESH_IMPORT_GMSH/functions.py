import numpy as np
import sys
import subprocess
import os.path
import gmsh
import dolfin
import meshio

import MESH_IMPORT_GMSH.edge as Edge
import MESH_IMPORT_GMSH.wire as Wire



def createMeshMSH(self, st_mesh = 'mesh'):

	#####################
	## Read Mesh Topology
	#####################
	showWires=1
	showSave=0

	geo_repertory = self.myGeometry.geoDir
	mesh_size = self.myGeometry.meshSize

	line = subprocess.check_output(['wc', '-l', geo_repertory+"Points.txt"])
	aux=line.strip().split()
	nbPt=int(aux[0])+1
	Points=np.zeros((nbPt,3))
	labelPt=[]
	labelPt.append("bidon")
	with open(geo_repertory+"Points.txt", "r") as dataFile:
			for line in dataFile:
					aux = line.strip().split()
					numPt=int(aux[0])
					print("Point="+str(numPt))
					Points[numPt,0]=float(aux[1])
					Points[numPt,1]=float(aux[2])
					labelPt.append(aux[3])
	
	Edges=[]
	aE=Edge.Edge(0,0,0,0,"",0)
	Edges.append(aE)
	cmpE=1
	if (showSave):
			edgefile=geo_repertory+"EdgesSaved.txt"
	else:
			edgefile=geo_repertory+"Edges.txt"
	with open(edgefile, "r") as dataFile:
			for line in dataFile:
					aux = line.strip().split()
					typeEdge=int(aux[0])
					print("Edge="+str(aux))
					numNb=int(aux[1])
					numNe=int(aux[2])
					numPt=int(aux[3])
					label=aux[3+1+numPt]
					numEdge=int(aux[3+1+numPt+1])
					aE=Edge.Edge(numEdge,numNb,numNe,numPt,label,typeEdge)
					#Edges.append(aE)
					num=1
					while (num<=numPt):
							aE.add_pt(int(aux[3+num]))
							num=num+1
					cmpE=cmpE+1
					Edges.append(aE)
	Wires=[]
	cmpW=1
	WireDrawPoint=1
	WireEmptyPoly=0
	WireWithName=1
	if (showSave):
			wireFile=geo_repertory+"WiresSaved.txt"
	else:
			wireFile=geo_repertory+"Wires.txt"
	if (os.path.exists(wireFile) and showWires):
			with open(wireFile, "r") as dataFile:
					for line in dataFile:
							aux = line.strip().split()
							print("Wire="+str(aux))
							numW=int(aux[0])
							nbE=int(aux[1])
							W=Wire.Wire(numW,nbE,"W"+str(numW))
							num=1
							print("numW="+str(numW)+"nb Edges="+str(nbE))
							while(num<=nbE):
									print("rep "+str(num)+" "+str(nbE))
									reverse=0
									numE=int(aux[1+num])
									print(numE)
									if (numE<-0.1):
											reverse=1
											numE=-numE
									print("num edge="+str(numE))
									E=Edges[numE]
									W.add_edge(Edges[numE],reverse)
									num=num+1
							W.label=aux[num+1]
							cmpW=cmpW+1
							Wires.append(W)
	else:
			print(wireFile+" doesn t exist")

	#############################
	## Write Mesh for all domains
	#############################

	# Create gmsh geometric model
	gmsh.initialize(sys.argv)
	gmsh.model.add("create_mesh")

	# Create Mesh Points
	gmsh_points = [gmsh.model.geo.addPoint(Points[i_points][0],Points[i_points][1],Points[i_points][2], meshSize=mesh_size)
			for i_points in range(len(Points))]

	# Create Mesh Edges
	gmsh_lines = []
	for i_edges in range(len(Edges)):
		if (Edges[i_edges].nbPt<1):
				gmsh_lines.append(gmsh.model.geo.addLine(gmsh_points[Edges[i_edges].ptb], gmsh_points[Edges[i_edges].pte]))
		else:
				spline = [gmsh_points[Edges[i_edges].ptb]]
				for i_points in range(Edges[i_edges].nbPt):
						spline.append(gmsh_points[Edges[i_edges].pts[i_points]])
				spline.append(gmsh_points[Edges[i_edges].pte])
				gmsh_lines.append(gmsh.model.geo.addSpline(spline))

	# Create Mesh Wires
	test_edges = np.zeros(len(Edges))
	gmsh_loop = []
	for i_wires in range(len(Wires)):
		loop = []
		for i_edges in range(len(Wires[i_wires].edges)):
			if (test_edges[Wires[i_wires].edges[i_edges]] == 0):
				test_edges[Wires[i_wires].edges[i_edges]] = 1
			if (Wires[i_wires].reverse[i_edges]):
					loop.append(-gmsh_lines[Wires[i_wires].edges[i_edges]])
			else:
					loop.append(gmsh_lines[Wires[i_wires].edges[i_edges]])
		gmsh_loop.append(gmsh.model.geo.addCurveLoop(loop))

	gmsh_surface = [gmsh.model.geo.addPlaneSurface([gmsh_loop[i_loop]])
			for i_loop in range(len(Wires))]

	# Generate 2D Mesh with triangle elements
	gmsh.model.geo.synchronize()
	gmsh.model.mesh.generate(2)

	# Create Physical Groups for Edges
	for i_edges in range(1,len(Edges)):
		if (test_edges[i_edges]):
			pl = gmsh.model.addPhysicalGroup(1, [gmsh_lines[i_edges]])
			gmsh.model.setPhysicalName(1, pl, 'E'+str(Edges[i_edges].id))

	# Create Physical Groups for Wires
	for i_loop in range(len(Wires)):
		ps = gmsh.model.addPhysicalGroup(2, [gmsh_surface[i_loop]])
		gmsh.model.setPhysicalName(2, ps, 'W'+str(Wires[i_loop].id))

	# Write MSH Mesh
	st = st_mesh + '.med'
	gmsh.write(st)
	out = subprocess.run('gmsh -2 ' + st, shell=True)
	gmsh.finalize()
	st_mesh_msh = st_mesh + '.msh'



def importMeshDolfin(self, st_mesh_msh = 'mesh.msh'):

	msh = meshio.read(st_mesh_msh)

	msh.field_data = { key.strip() : value for key, value in msh.field_data.items() } # Remove white space in key

	for key in msh.cell_data_dict["gmsh:physical"].keys():
		if key == "line":
			line_data = msh.cell_data_dict["gmsh:physical"][key]
		if key == "triangle":
			triangle_data = msh.cell_data_dict["gmsh:physical"][key]

	flag_type_line = 0
	flag_type_triangle = 0
	for cell in msh.cells:
		if cell.type == "line":
			if flag_type_line == 0:
				line_cells = cell.data
				flag_type_line=1
			else:
				line_cells=np.concatenate((line_cells,cell.data))
		if cell.type == "triangle":
			if flag_type_triangle == 0:
				triangle_cells = cell.data
				flag_type_triangle=1
			else:
				triangle_cells=np.concatenate((triangle_cells,cell.data))

	line_mesh = meshio.Mesh(points=msh.points[:, :2],
							cells=[("line", line_cells)],
							cell_data={"boundaries":[line_data]})


	triangle_mesh = meshio.Mesh(points=msh.points[:, :2],
								cells=[("triangle", triangle_cells)],
								cell_data={"subdomain":[triangle_data]},
								field_data=msh.field_data)

	# Write mesh file into xdmf files
	meshio.write("boundary4.xdmf", line_mesh)
	meshio.write("surface4.xdmf", triangle_mesh)

	# Once the XDMF files are created we could read them into dolfin: ::
	self.myDolfinMesh = dolfin.Mesh()

	with dolfin.XDMFFile("surface4.xdmf") as infile:
		infile.read(self.myDolfinMesh)

	mvc_2d = dolfin.MeshValueCollection("size_t", self.myDolfinMesh, self.myDolfinMesh.topology().dim())
	with dolfin.XDMFFile("surface4.xdmf") as infile:
		infile.read(mvc_2d, "subdomain")
	self.myDolfinMeshFunction2D = dolfin.cpp.mesh.MeshFunctionSizet(self.myDolfinMesh, mvc_2d)

	mvc_1d = dolfin.MeshValueCollection("size_t", self.myDolfinMesh, self.myDolfinMesh.topology().dim()-1)
	with dolfin.XDMFFile("boundary4.xdmf") as infile:
		infile.read(mvc_1d, "boundaries")
	self.myDolfinMeshFunction1D = dolfin.cpp.mesh.MeshFunctionSizet(self.myDolfinMesh, mvc_1d)

	# create arrayOfIndex1DGeomToDolfinGeomMarkers and arrayOfIndex2DGeomToDolfinGeomMarkers from msh.field_data
	index1DGeom = 1
	index2DGeom = 1
	self.arrayOfIndex1DGeomToDolfinGeomMarkers = [None]
	self.arrayOfIndex2DGeomToDolfinGeomMarkers = [None]
	for key, value in msh.field_data.items():
		indexGeom = int(key[1:])
		if value[1]==1:
			self.arrayOfIndex1DGeomToDolfinGeomMarkers = self.arrayOfIndex1DGeomToDolfinGeomMarkers + [None]*(indexGeom - index1DGeom) + [value[0]]
			index1DGeom = indexGeom + 1
		elif value[1]==2:
			self.arrayOfIndex2DGeomToDolfinGeomMarkers = self.arrayOfIndex2DGeomToDolfinGeomMarkers + [None]*(indexGeom - index2DGeom) + [value[0]]
			index2DGeom = indexGeom + 1
			





	

