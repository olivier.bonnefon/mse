import MESH_IMPORT_GMSH.read as reader
import MESH_IMPORT_GMSH.buildMeshes as buildMeshes

import study
import dynamicalSystem
import interaction2D1D
import diffusionModel
import system
import simulator
import timeDoOneStep

reader.verbose=0
buildMeshes.verbose=0

aTopo=reader.geomTopoReader("GEOMETRY/SIMPLE2/")
aMeshesDir="./MESHES/SIMPLE2/"
buildMeshes.buildXdmf(aTopo,aMeshesDir,0.1)
aSystem=system.system()
aStudy=study.study(aTopo,aMeshesDir,aSystem)
ds2d1=dynamicalSystem.dynamicalSystem(aStudy,1,2,aMeshesDir+"surface1.xdmf")
ds1d1=dynamicalSystem.dynamicalSystem(aStudy,1,1,aMeshesDir+"boundary1.xdmf")
aSystem.addDS(ds2d1)
aSystem.addDS(ds1d1)
a2D1DInter=interaction2D1D.interaction2D1D(ds2d1,ds1d1)
aSystem.addInteraction(a2D1DInter)
#a2D1DInter._checkMap()

aDiffModel=diffusionModel.diffusionModel(ds2d1,1.0)
#aDiffModel.buildMat()
#mat=aDiffModel.getMat()


aStudy.simulator=simulator.simulator()
aStudy.simulator.timeStep=timeDoOneStep.timeDoOneStep()
