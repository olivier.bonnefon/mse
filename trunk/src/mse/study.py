import mse.MESH_IMPORT_GMSH.read as reader
import mse.MESH_IMPORT_GMSH.buildMeshes as buildMeshes
from mse.system import system

####
# class study gathers objects for a study 
#
#
####
import os
from mse.toolsEnv import computerEnv,MSEPrint,ensureDirName,MSEError
import mse.toolsEnv as mtools
from mse.exception import mseError, mseFileErr

class study:
    def __init__(self,aName,nbParam=0) -> None:
        self.name=aName
        self.nbParam=nbParam
        self.param=[0.0 for _ in range(nbParam)]
        self.paramDolf=[None]*self.nbParam
        self.absoluteStudyDir=os.path.abspath(".")+"/"+ensureDirName(self.name)
        if (not os.path.exists(self.absoluteStudyDir)):
            os.mkdir(self.absoluteStudyDir)
        self.meshesDir=self.absoluteStudyDir+"/MESHES/"
        if (not os.path.exists(self.meshesDir)):
            os.mkdir(self.meshesDir)
        self.exportDir=self.absoluteStudyDir+"/VTK/"
        if (not os.path.exists(self.exportDir)):
            os.mkdir(self.exportDir)
        self.simuDir=self.absoluteStudyDir+"/SIMU/"
        if (not os.path.exists(self.simuDir)):
            os.mkdir(self.simuDir)
        self.covDir=self.absoluteStudyDir+"/COV/"
        if (not os.path.exists(self.covDir)):
            os.mkdir(self.covDir)
        self.adjointDir=self.absoluteStudyDir+"/ADJOINT_SIMU/"
        if (not os.path.exists(self.adjointDir)):
            os.mkdir(self.adjointDir)
        #system
        system(self)
        self.simulator=None
        self.simulatorBackward=None
        self.topo=None
        mtools.aLogFile=open(self.absoluteStudyDir+"logFile.txt","w")
        MSEPrint(" Info, study directory is "+self.absoluteStudyDir)

    def setGeometry(self,geomName,tol=-1,aMeshCallBack=None):
        """
        This function builds the spatial discretisation (meshes) of the study zone

        :param geomName string: the geomzone of the mse env geometry directory
        :param tol double : the number of nodes by lenght unit
        """
        geomName=ensureDirName(geomName)
        #mesh of study zone
        #La geometrie
        self.topo=reader.geomTopoReader(computerEnv.geomDir+geomName)
        #build meshes can be only once
        if (tol>0):
            buildMeshes.buildXdmf(self.topo,self.meshesDir,tol,aMeshCallBack)
    def setSystem(self,aSystem):
        self.system=aSystem
    def setSysAdj(self, sysAdj):
        self.systemAdjoint=sysAdj
    def end(self):
        for ds in self.system.dynamicalSystems:
            ds.end()
        mtools.aLogFile.close()
        mtools.aLogFile=None
    def setParam(self,aParam, index=None):
        """Sets new parameter for the study.

        The parameters corresponding to p1,p2,p3,... in the source terms. The index of param p1 is 0, p2 is 1,... pn is n-1.
        This function updates the list of study parameters with the new aParam parameters in specific index (by default index = [0,1,...,len(aParam)-1) :
        study.Param[index[i]] = aParam[i] 
        Then calls the setParamDolf function for all the dynamicalSystem in the study.

        :param list(float) aParam: list of new parameters to change. The list must be equal length of index. If aParam is longer than index, the rest of the list will be ignored.
        :param list(int)/int index: if index is list, index of parameters to modify, starting at 0. Default=list(range(self.nbParam)). If index[i] > number of parameters, prints an error message, stop updating of study.param and gswitchies to updating param of dynamicalSystem.
        if index is int, create new list index = [index, index+1, ..., index+len(aParam)-1], so that len(index)==len(aParam).
        """
        if (index==None):index=list(range(len(aParam)))
        elif (type(index)==int):index=list(range(index,index+len(aParam)))
        try:
            for count,it in enumerate(index): self.param[it]=aParam[count]
        except IndexError :
            MSEError("Error in study.setParam: index error: "+str(it+1)+" > number of parameter. Number of parameters update: "+str(count))
            #exit(1)
        finally:
            for ds in self.system.dynamicalSystems: ds.setParamDolf()
    def getParam(self):
        return self.param
    def saveState(self):
        """ Can be overload by user """
        self.system.saveState()
    def restoreState(self):
        """Can be overload by user """
        self.system.restoreState()
