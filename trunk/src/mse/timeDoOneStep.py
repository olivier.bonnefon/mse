""" this module drive the time-stepping algorithm, it implements a fix and an adaptive time step"""

from mpi4py import MPI
import petsc4py.PETSc as pet
import mse.study
import mse.system
from mse.linAlgTools import vector
from mse.toolsEnv import MSEPrint, MSEPrint_0,MSEError

class timeDoOneStep:
    myPrint=MSEPrint
    def verbose(aBool):
        if (aBool):
            timeDoOneStep.myPrint=MSEPrint
        else:
            timeDoOneStep.myPrint=MSEPrint_0
    """
    do a fix time step
    :param aStudy study
    :param aTimeScheme timeScheme: do the time step
    """
    def __init__(self,aStudy,aTimeScheme,dt) -> None:
        """The constructor of mother class"""
        self.study=aStudy
        self.dt=dt
        self.aTS=aTimeScheme
        self.aTS.setSystem(aStudy.system)
        #self.solver=PETSc.KSP(MPI.COMM_WORLD).create()
        self.status=0
        self.study.simulator.doOneStep=self
        self.tScheme=None
    def doOneStep(self):
        """by default a fix time step"""
        #update ds covariable
        for ds in self.study.system.dynamicalSystems:
            ds.beforeOneStep(self.study.simulator.curT,
                    self.study.simulator.curT+self.dt)
        self.status=self.aTS.doATimeStep()
        if (self.status):
            raise Exception("timeDoOneStep failed\n")
        return self.dt

class adaptivDoOneStep(timeDoOneStep):
    """ do an adaptive time step
    """
    def __init__(self,aStudy,aTimeScheme, dt,maxMultStep, relativeThreshold,absThrehold) -> None:
        """
        absThrehold is necessary if the state is 0. So a step is accepted if error is smaller than one of the threshold
        """
        super().__init__(aStudy,aTimeScheme,dt)
        self.minDt=dt
        self.curMult=1
        self.maxMultStep=maxMultStep
        self.relativeThreshold=relativeThreshold
        self.absThrehold=absThrehold
        self.curMult=1
        self.solCurMult=vector()
        self.solCurMult.setDim(self.study.system.nbDofs)
        self.saveCurT=0.0
    def init():
        self.dt=self.minDt
        self.curMult=1
    def saveState(self):
        self.study.saveState()
        self.saveCurT=self.study.simulator.curT
    def restoreState(self):
        self.study.restoreState()
        self.study.simulator.curT=self.saveCurT
    def doOneStep(self):
        if (self.maxMultStep==1):
            super().doOneStep()
            timeDoOneStep.myPrint("doing fix time step")
            return self.dt
        if (self.curMult==1):
            self.curMult=2
        self.saveState()
        while (True):
            print("try mult="+str(self.curMult))
            timeDoOneStep.myPrint("try mult="+str(self.curMult))
            self.dt=self.curMult*self.minDt
            testingDt=self.dt
            try:
                super().doOneStep()
            except:
                self.status=1
            self.solCurMult.setValuesFromVec(self.aTS.sol)
            self.dt=0.5*self.dt
            self.restoreState()
            normDiff=0
            if (not self.status):#ie simu not failed
                try:
                    super().doOneStep()
                    self.study.simulator.curT=self.study.simulator.curT+self.dt
                    super().doOneStep()
                    normDiff=self.solCurMult.l2Dist(self.aTS.sol)
                    normState=self.solCurMult.norm2()
                    timeDoOneStep.myPrint("norme diff="+str(normDiff)+" ? "+str(self.relativeThreshold*normState))
                except:
                    self.status=1
            if (self.status or (normDiff>self.relativeThreshold*normState and normDiff > self.absThrehold)):# simu failed or err to big
                    if (self.curMult==2):
                        self.curMult=1
                        self.restoreState()
                        self.dt=self.minDt
                        try:
                            super().doOneStep()
                        except:
                            self.status=1
                            MSEError(" cant solve problem even with minimal value of dt="+str(self.dt))
                        timeDoOneStep.myPrint("get min size of time step = "+str(self.dt))
                        return self.dt
                    else:
                        timeDoOneStep.myPrint("reduce relativeThresholdtime step")
                        self.restoreState()
                        self.curMult=int(self.curMult/2)
            else:#the self.curMult is acceptable
                timeDoOneStep.myPrint("find acceptable time step size ="+str(self.dt)+"="+str(self.curMult)+"*"+str(self.minDt))
                #compute mult factor for next doOneStep
                if ((2*normDiff < self.relativeThreshold*normState or 2*normDiff < self.absThrehold) 
                        and self.maxMultStep> self.curMult):
                    self.curMult=2*self.curMult
                if (self.maxMultStep < self.curMult):
                    timeDoOneStep.myPrint("get max time step")
                return testingDt
        timeDoOneStep.myPrint("bug in musn't happend here")
        assert(0)

class timeDoOneStepBackward(timeDoOneStep):
    myPrint=MSEPrint_0
    """
    Do a fix time step backward

    :param aStudy study
    :param aTimeScheme timeScheme: do the time step

    :return self.dt float: the time step
    """
    def __init__(self,aStudy,aTimeScheme,dt=None) -> None:
        """The constructor of mother class"""
        self.study=aStudy
        self.dt=dt
        self.aTS=aTimeScheme#NOTE: timescheme backward
        self.aTS.setSystem(aStudy.system)
        #self.solver=PETSc.KSP(MPI.COMM_WORLD).create()
        self.status=0
        self.study.simulatorBackward.doOneStep=self
        self.aTS._initExplorTraj()
        self.tScheme=None #FIXME: Need ???
    def doOneStep(self):
        """find the dt and call doAtimeStep() from timeScheme
        we read the file SIMU/time.txt to find the dt
        :return self.dt float: the new dt 
        """
        #read file SIMU/time.txt 
        pathSimu = self.study.simuDir
        fileTime = open(pathSimu+"time.txt",'r')
        tmpStringRead = fileTime.readline()
        #tm1 and tp1 time in file SIMU/time.txt
        #we have tm1 < tp1 AND we are in backward: curT = tp1
        tm1File = float(tmpStringRead.split(" ")[1].strip())
        tmpStringRead = fileTime.readline()
        tp1File = float(tmpStringRead.split(" ")[1].strip())
        time = float(self.study.simulatorBackward.curT)
        while(tp1File - time < -1e-16):
            tm1File = tp1File
            tmpStringRead = fileTime.readline()
            tp1File = float(tmpStringRead.split(" ")[1].strip())
        fileTime.close()
        self.dt = tm1File - tp1File
        self.status=self.aTS.doATimeStep()
        if (self.status):
            raise Exception("timeDoOneStep failed\n")
        return self.dt
