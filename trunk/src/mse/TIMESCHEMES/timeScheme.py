import mse.system
from mse.linAlgTools import matrix,vector,solver
import mse.dynamicalSystem
import mse.interaction
from mse.state import state
from dolfinx.fem.petsc import assemble_vector
from dolfinx.fem import form
from mse.explorTrajectory import explorTrajectory
from mse.toolsEnv import MSEPrint,MSEPrint_0,MSEError
from mse.exception import mseError, mseFileErr

class timeScheme:
    myPrint=MSEPrint_0
    def verbose(aBool):
        if (aBool):
            timeScheme.myPrint=MSEPrint
        else:
            timeScheme.myPrint=MSEPrint_0
    def __init__(self) -> None:
        self.computeMatOnce=0
        self.system=None
        self.M=None
        self.rhs=None
        self.sol=None
        self.solver=solver()
        #only about source term
        #theta = 1 : implicit
        #theta = 0 : explicit
        self.theta=0.5
    def doATimeStep(self):
        """ constructs matrix, rhs and solves for a time step
        """
        raise VirtualException()
    def setSystem(self,aSystem):
        if (aSystem.nbDofs==0):
            raise Exception("In timescheme, system is not initialized")
        self.system=aSystem
#        if (self.M is None):
#            Ndofs=self.system.nbDofs
#            self.M = matrix()
#            self.M.setDim(Ndofs,Ndofs)
        if (self.rhs is None):
            self.rhs=vector()
            self.rhs.setDim(self.system.nbDofs)       
        if (self.sol is None):
            self.sol=vector()
            self.sol.setDim(self.system.nbDofs)
    def initMat(self):
        Ndofs=self.system.nbDofs
        self.M = matrix()
        self.M.setDim(Ndofs,Ndofs)
    def preparTimeStep(self):
        """
        """
        pass
    def solToDof(self):
        #copy sol in utm1 of each ds
        lcurDof=0
        for ds in self.system.dynamicalSystems:
            ds.utm1.vectorToState(self.sol,lcurDof)
            lcurDof=lcurDof+ds.nbDofs

# implicite linéaire : 
# 
#
# F is affine F(Ut) = F(u_k) + F'(u_k) * (Ut-u_k)
# d_t U = Delta U + F(U)
#
# 1/dt(Ut-Utm1) = Delta Ut + F(Ut)
# 1/dt(Ut-Utm1) = Delta Ut + F(u_k) + F'(u_k)(Ut-u_k)
# (1/dt - Delta  - F'(uk))Ut= 1/dt *Utm1 + F(uk)-F'(uk)uk
#
# integral(1/dt u.v + grad(u).grad(v)-F'(uk)*u*v = integral((1/dt) utm1*v + (F(uk)-F'(uk)*v) 
#
#
# theta method:
# 1/dt(Ut-Utm1) = Delta Ut + theta*F(Ut)+ (1-theta) * F(Utm1)
# 1/dt(Ut-Utm1) = Delta Ut + theta*(F(uk) + F'(uk)(Ut-uk))+ (1-theta) * F(Utm1)
# (1/dt - Delta  - thetaF'(uk))Ut= 1/dt *Utm1 + theta*(F(uk)-F'(uk)uk)+(1-theta) F(Utm1)
# 
#
#
#
class implicitLinearTimeScheme(timeScheme):
    def __init__(self) -> None:
        super().__init__()
    def buildMat(self):
        if (self.computeMatOnce and self.M):
            return
        self.initMat()
        invdt=1.0/(self.system.study.simulator.doOneStep.dt)
        for ds in self.system.dynamicalSystems:
            nbDofsInDs=ds.nbDofs
            matGlobal=matrix()
            matGlobal.setDim(nbDofsInDs,nbDofsInDs)
            #1/dt u.v
            for i in range(ds.nbComps):
                #matGlobal.axpy(invdt,ds.massMat)
                matGlobal.assembleMat(ds.massMat,i*ds.nbDofsPerComp,i*ds.nbDofsPerComp)
            matGlobal.scale(invdt)
            for aModel in ds.sourcesTerm:
                aModel.updateLinearisedMat()
                matAux=aModel.getLinearisedMat()
                #-F'(0)*u*v
                if (matAux):
                    matGlobal.axpy(-self.theta,matAux)
            #grad(u).grad(v)
            matDisp=ds.secondOrderTerm.getLinearisedMat()
            matGlobal.axpy(1.0,matDisp)
            self.M.assembleMat(matGlobal,ds.beginDofsIndexInSystem,ds.beginDofsIndexInSystem)
        for interac in self.system.interactions:
            self.M.assembleMat(interac.addM1,interac.dS1.beginDofsIndexInSystem,interac.dS1.beginDofsIndexInSystem)
            self.M.assembleMat(interac.addM12,interac.dS1.beginDofsIndexInSystem,interac.dS2.beginDofsIndexInSystem)
            self.M.assembleMat(interac.addM2,interac.dS2.beginDofsIndexInSystem,interac.dS2.beginDofsIndexInSystem)
            self.M.assembleMat(interac.addM21,interac.dS2.beginDofsIndexInSystem,interac.dS1.beginDofsIndexInSystem)
            
    def buildRhs(self):
        invdt=1.0/(self.system.study.simulator.doOneStep.dt)
        self.rhs.zero()
        for ds in self.system.dynamicalSystems:
            nbDofsInDs=ds.nbDofs
            vecSource=vector()
            vecSource.setDim(nbDofsInDs)
            vecSourcePerComp=vector()
            vecSourcePerComp.setDim(ds.nbDofsPerComp)
            beginIndex=0
            #1/dt *Utm1
            for comp in ds.utm1.comps:
                ds.uBufVec.setValuesFromDolfinFunction(comp)
                ds.massMat.mult(ds.uBufVec,vecSourcePerComp)
                vecSource.assembleVec(vecSourcePerComp,beginIndex)
                beginIndex=beginIndex+ds.nbDofsPerComp
            vecSource.scale(invdt)
            ##vecSource=vector(assemble_vector(form(invdt*ds.utm1*ds.v*ds.dx)))
            #theta*( F(uk)-F(uk)*F'(uk) ) + (1-theta)*F(utm1)
            for aModel in ds.sourcesTerm:
                #print("TS.buildRhs")
                #compute F(uk)-F'(uk)uk
                aModel.buildRhs()
                #theta*F(uk)
                vecSource.axpy(self.theta,aModel.getRhs())
                #(1-theta)*F(utm1)
                vecSource.axpy(1-self.theta,aModel.getFtm1())
            self.rhs.assembleVec(vecSource,ds.beginDofsIndexInSystem)
            
    def solveLinearSystem(self):
        #solve linear system
        self.solver.setMat(self.M)
        self.solver.solveNoNeg(self.rhs,self.sol)
        #print("rhs=",self.rhs.petscVec.array)
        #self.solver.solve(self.rhs,self.sol)
        #print("sol=",self.sol.petscVec.array)
#    def preparTimeStep(self):
#        super().preparTimeStep()
    def doATimeStep(self):
        self.preparTimeStep()
        #copy utm1 in uk
        for ds in self.system.dynamicalSystems:
            ds.uk.setFromState(ds.utm1)
            for aModel in ds.sourcesTerm:
                aModel.updateFtm1()
        self.preparTimeStep()
        self.buildMat()
        self.buildRhs()
        self.solveLinearSystem()
        self.solToDof()
        return 0



#
#
# the adjoint system: -d_t p = \Delta p + F'(a,u_a(t))^* p + vTildeP(u_a(t)) 
# time scheme t -> t-dt , theta method (implicit theta =1)
# the unknwon p(t-dt) is noted p
# the known state p(t) is noted pt (utm1 in implementation) 
# la theta method (theta=1 implicit) pour passer de t -> t-dt, notons l'inconue p(t-dt) = p est:
#            -invdt(p(t)-p) = \Delta p + (1-theta)*d_u F(a,u_a(t))^* p(t) + (1-theta)*vtild(u_a(t)) + theta *d_u F(a,u_a(t-dt))^* p + theta*vtild(u_a(t-dt)) 
#            invdt*p - theta(\Delat D + d_u F(a,u_a(t-dt))^* p = invdt*p(t) + (1-theta)*vtild(u_a(t)) + theta*vtild(u_a(t-dt)) + (1-theta) *d_u F(a,u_a(t))^* p(t)
# For each model, we need to compute the 2 matrices:
#
# F'(a,u_a(t-dt))
# F'(a,u_a(t))
#
# For each obsProcess, we need to compte 2 vectors:
#
# vtilde(u_a(t-dt))
# vtilde(u_a(t))
#
# For each ds:
# 
# uat: u_a(t)
# uatmdt: u_a(t-dt)
#
class implicitLinearBackwardTimeScheme(implicitLinearTimeScheme):
    """
    It makes a negatif time step for adjoint system. The directory simuDirName contains the direct simulation noted u_a(t) for the set of parameter "a". The simuDirName/times.txt will be used for the backward simulation. The matrix of the linear system is built using the linearized system at u_a(t) and u_a(t-dt). The source term is vTildePrim from the obsProcess of the dynamicalSystem. The meaning of ds.utm1 is p(t) and we compute p(t-dt).

    :param simuDirName string: the directory of the direct simulator
    """
    # NOTE: variable:
    # uat = u_a(t) : val of u with param a in time t
    # uatmdt = u_a(t- abs(dt)) : val of u with param a in time t -abs(dt)
    # utm1 = p(t) : val of p(t), solution of the adjoint system in time t : KNOW
    # /!\ after the call of self.solToDof: utm1 = p(t-abs(dt)) : val of p(t-abs(dt)), solution of the adjoint system in time t-abs(dt) : UNKNOW
    def __init__(self,asimuDirName) -> None:
        super().__init__()
        #NOTE: need simuDir ??? = study.simuDir ?
        self.simuDirName = asimuDirName
        self.tmpB=None
        #self.doOneStep=aDoOneStepBackward
        #add necessary menbers in models  and obs process instances
    #call in timeDoOneStepBackward
    def _initExplorTraj(self):
        """ Initialized explortrajectory and create state uat and uatmdt if they don't exist

        function called in timeSchemeBackward
        """
        for ds in self.system.dynamicalSystems:
            if (not hasattr(ds,"uat")):
                ds.uat=state(ds)
                ds.uatmdt=state(ds)
        self.explorSim = explorTrajectory(self.system.study, self.simuDirName)
    def preparTimeStep(self):
        super().preparTimeStep()
        self.explorSim.interpolToTime(self.system.study.simulatorBackward.curT)
        for ds in self.system.dynamicalSystems:
            ds.uat.setFromState(ds.trajState) 
            self.explorSim.interpolToTime(self.system.study.simulatorBackward.curT-abs(self.system.study.simulatorBackward.doOneStep.dt))
            ds.uatmdt.setFromState(ds.trajState)  
    def buildMat(self):
        self.initMat()
        invdt=1.0/(abs(self.system.study.simulatorBackward.doOneStep.dt))#assume doOneStep compute a dt
        for ds in self.system.dynamicalSystems:
            nbDofsInDs=ds.nbDofs
            matGlobal=matrix()
            matGlobal.setDim(nbDofsInDs,nbDofsInDs)
            #1/dt u.v
            for i in range(ds.nbComps):
                #matGlobal.axpy(invdt,ds.massMat)
                matGlobal.assembleMat(ds.massMat,i*ds.nbDofsPerComp,i*ds.nbDofsPerComp)
            matGlobal.scale(invdt)
            for aModel in ds.sourcesTerm:
                #aModel.updateLinearisedMat() built the matrix from the linearized source term at uk
                #here, we need the linearized at u_p(t), so, we set uk with u_p(t), and we set uk with utm1 after the getting the blockMat
                ds.uk.setFromState(ds.uatmdt)#nom à préciser
                aModel.updateLinearisedMat()
                matAux=aModel.getLinearisedMat()
                ds.uk.setFromState(ds.utm1)
                #-F'(ua_tmdt)*u*v
                if (matAux):
                    matGlobal.axpy(-self.theta,matAux)
            #grad(u).grad(v)
            matDisp=ds.secondOrderTerm.getLinearisedMat()
            matGlobal.axpy(1,matDisp)
            self.M.assembleMat(matGlobal,ds.beginDofsIndexInSystem,ds.beginDofsIndexInSystem)
    def buildRhs(self):
        invdt=1.0/(abs(self.system.study.simulatorBackward.doOneStep.dt))
        self.rhs.zero()
        for ds in self.system.dynamicalSystems:
            nbDofsInDs=ds.nbDofs
            vecSource=vector()
            vecSource.setDim(nbDofsInDs)
            vecSourcePerComp=vector()
            vecSourcePerComp.setDim(ds.nbDofsPerComp)
            beginIndex=0
            # M*p(t)*1/dt
            for comp in ds.utm1.comps:
                ds.uBufVec.setValuesFromDolfinFunction(comp)
                ds.massMat.mult(ds.uBufVec,vecSourcePerComp)
                vecSource.assembleVec(vecSourcePerComp,beginIndex)
                beginIndex=beginIndex+ds.nbDofsPerComp
            vecSource.scale(invdt)
            self.rhs.assembleVec(vecSource,ds.beginDofsIndexInSystem)
            # (1-theta) *d_u F(a,u_a(t))^* p(t)
            vecUtm1=vector()
            vecUtm1.setDim(nbDofsInDs)
            for aModel in ds.sourcesTerm:
                vecSource.zero()
                vecUtm1.setValuesFromState(ds.utm1)
                ds.uk.setFromState(ds.uat)#nom à préciser
                aModel.updateLinearisedMat()
                matAux=aModel.getLinearisedMat()
                ds.uk.setFromState(ds.utm1)
                matAux.mult(vecUtm1,vecSource)
                vecSource.scale(1-self.theta)
                self.rhs.assembleVec(vecSource,ds.beginDofsIndexInSystem)
            #(1-theta)*vtild(u_a(t))
            if (not(ds.obsProcess is None)):
                vecSource.zero()
                ds.obsProcess.computeTildevPrim(self.system.study.simulatorBackward.curT)
                tmp_count = round((2-self.system.study.simulatorBackward.curT)/0.05)
                beginIndex=0
                for comp in ds.obsProcess.vtild.comps:
                    ds.uBufVec.setValuesFromDolfinFunction(comp)
                    ds.massMat.mult(ds.uBufVec,vecSourcePerComp)
                    vecSource.assembleVec(vecSourcePerComp,beginIndex)
                    beginIndex=beginIndex+ds.nbDofsPerComp
                vecSource.scale(1-self.theta)
                self.rhs.assembleVec(vecSource,ds.beginDofsIndexInSystem)
                # theta*vtild(u_a(t-dt)) 
                vecSource.zero()
                ds.obsProcess.computeTildevPrim(self.system.study.simulatorBackward.curT-abs(self.system.study.simulatorBackward.doOneStep.dt))
                beginIndex=0
                for comp in ds.obsProcess.vtild.comps:
                    ds.uBufVec.setValuesFromDolfinFunction(comp)
                    ds.massMat.mult(ds.uBufVec,vecSourcePerComp)
                    vecSource.assembleVec(vecSourcePerComp,beginIndex)
                    beginIndex=beginIndex+ds.nbDofsPerComp
                vecSource.scale(self.theta)
                self.rhs.assembleVec(vecSource,ds.beginDofsIndexInSystem)
                vecSource.zero()
            #np.savetxt("TMP_RES/tmpFile_rhs"+str(round((2-self.system.study.simulatorBackward.curT)/0.05))+".txt",self.rhs.petscVec.array)
    def doATimeStep(self):
        for ds in self.system.dynamicalSystems:
            ds.uk.setFromState(ds.utm1)
        self.preparTimeStep()
        self.buildMat()
        self.buildRhs()
        self.solveLinearSystem()
        self.solToDof()
        return 0
    def solveLinearSystem(self):
        #solve linear system
        self.solver.setMat(self.M)
        #self.solver.solveNoNeg(self.rhs,self.sol)
        #print("rhs=",self.rhs.petscVec.array)
        self.solver.solve(self.rhs,self.sol)
        #print("sol=",self.sol.petscVec.array)
#    def preparTimeStep(self):
#        super().preparTimeStep()

class implicitNonLinearTimeScheme(implicitLinearTimeScheme):
    def __init__(self) -> None:
        super().__init__()
        self.criteron=1e-3
        self.maxIt=10
        self.prevSol=None
        self.freqMatUpdate=3
    def setSystem(self,aSystem):
        super().setSystem(aSystem)
        self.prevSol=vector()
        self.prevSol.setDim(self.system.nbDofs)
    def solToUk(self):
        #copy sol in utm1 os each ds
        lcurDof=0
        for ds in self.system.dynamicalSystems:
            ds.uk.vectorToState(self.sol,lcurDof)
            lcurDof=lcurDof+ds.nbDofs
    def doATimeStep(self):
        #copy utm1 in uk
        self.preparTimeStep()
        for ds in self.system.dynamicalSystems:
            ds.uk.setFromState(ds.utm1)
            for aModel in ds.sourcesTerm:
                aModel.updateFtm1()
        residu=self.criteron+1
        newtonIt=0
        while (newtonIt<self.maxIt and residu > self.criteron):
            self.preparTimeStep()
            if (((newtonIt)%self.freqMatUpdate)==0):
                self.buildMat()
            self.buildRhs()
            self.solveLinearSystem()
            if (newtonIt>0):
                residu=self.sol.l2Dist(self.prevSol)
            newtonIt=newtonIt+1
            self.prevSol.setValuesFromVec(self.sol)
            self.solToUk()
            timeScheme.myPrint("newtonIt "+str(newtonIt)+" "+str(residu))
        self.solToDof()
        if (residu>self.criteron):
            timeScheme.myPrint("failed newton residu="+str(residu)+">"+str(self.criteron))
            return 1
        else:
            return 0
