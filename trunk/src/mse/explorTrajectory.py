"""
The explorTrajectory allow to explore the trajectory of the system reading the states in the directory trajDirName.
The member self.states contains the list of state in the same order than system.dynamicalSystems.




"""
from mse.state import state
from mse.system import system
from mse.simulator import simulator
from mse.study import study
#from mse.dynamicalSystem import dynamicalSystem,computeMass
from os.path import isdir
from mse.toolsEnv import MSEPrint, MSEPrint_0

class explorTrajectory:
    """
    :param study aStude: contains the system of dynamical system 
    :param trajDirName: the directory containing the states writting in files
    """
    myPrint=MSEPrint_0
    def verbose(aBool):
        if (aBool):
            explorTrajectory.myPrint=MSEPrint
        else:
            explorTrajectory.myPrint=MSEPrint_0
    def __init__(self,aStudy,trajDirName) -> None:
        self.study=aStudy
        self.trajDirName= trajDirName
        self.fileTime=None
        self.T=0
        assert(isdir(trajDirName))
    def interpolToTime(self,aTime):
        """ Function to interpolate the state in time aTime.

        It update the state ds.trajState
        :param float time: time to interpolate value
        """
        #1: read file time.txt and find both steps wich time is between
        explorTrajectory.myPrint("call with "+str(aTime))
        try:
            explorTrajectory.myPrint("self.trajDirName = " + self.trajDirName)
            f_Time = open(self.trajDirName+"time.txt")
            prevT=0
            curT=0
            curStep=0
            prevStep=0
            #Need to read the first line
            line = f_Time.readline()
            curT=float(line.split()[1])
            curStep=int(line.split()[0])
            while line := f_Time.readline():
                prevT=curT
                prevStep=curStep
                curT=float(line.split()[1])
                explorTrajectory.myPrint("lookng for time "+str(aTime)+" in "+str(prevT)+" "+str(curT))
                curStep=int(line.split()[0])
                #test if aTime between past a current
                if ((curT-aTime>-1e-16) == (curT-prevT>-1e-16)): 
                    explorTrajectory.myPrint("found\n")
                    break
            f_Time.close()
            aCoef=0
            explorTrajectory.myPrint("find "+str(prevT)+" "+str(curT)+" "+str(curStep))
            if (abs(aTime-prevT) < 1e-16):
                for aDs in self.study.system.dynamicalSystems:
                    aDs.trajState.load(self.trajDirName+"ds"+str(aDs.id),prevStep)
            elif(abs(curT-aTime)<1e-16):
                for aDs in self.study.system.dynamicalSystems:
                    aDs.trajState.load(self.trajDirName+"ds"+str(aDs.id),curStep)  
            else:
                aCoef=(aTime-prevT)/(curT-prevT)
                if (aCoef <0 or aCoef > 1):
                    if (aCoef>1):
                        aCoef=1
                    else:
                        aCoef=0
                for aDs in self.study.system.dynamicalSystems:
                    aDs.trajState.load(self.trajDirName+"ds"+str(aDs.id),prevStep)
                    aDs.trajState.scale(1-aCoef)
                    aDs.bufState.load(self.trajDirName+"ds"+str(aDs.id),max(curStep,0))
                    aDs.trajState.axpy(aDs.bufState,aCoef)
                 #resu = aCoef*state(curStep)+(1-aCoef)*state(curStep-1)
            self.T=aTime
        except IOError:
            MSEPrint("IO error interpolTime")
        except Exception as err:
            print(err)
    def rewind(self):
        """ prepar loop in trajectory steps to update ds.utm1

        This function must be called before self.replay
        """
        #read time.txt
        try:
            self.fileTime=open(self.trajDirName+"time.txt","r")
            explorTrajectory.myPrint("opened "+self.trajDirName+"time.txt")
        except Exception as err:
            print(err)
        self.curStep=-1
    def replay(self):
        """ function to update the state ds.utm1 at the next time

        use it in a loop
        """
        #read time.txt
        line=self.fileTime.readline()
        explorTrajectory.myPrint("replay")
        if line:
            self.T=float(line.split()[1])
            self.curStep=self.curStep+1
            for ds in self.study.system.dynamicalSystems:
                ds.load(self.trajDirName,self.curStep)
            return True
        else:
            self.stopReplay()
            return False
    def stopReplay(self):
        """Function call in self.replay to stop the rewind
        """
        #close time.txt
        if (self.fileTime):
            self.fileTime.close()
        self.fileTime=None 
