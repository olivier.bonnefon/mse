""" the module dynamicalSystem allow to apply a model on a geom patch (2d or 1d)"""


from dolfinx.io import XDMFFile
from dolfinx.fem import FunctionSpace,form
from mpi4py import MPI
from ufl import Measure,TrialFunction,TestFunction
from dolfinx.fem import Function, assemble_scalar, Constant
from dolfinx.fem.petsc import assemble_matrix
from mse.linAlgTools import matrix,vector
import numpy as np
from mse.state import state
from petsc4py.PETSc import ScalarType
import os
from mse.toolsEnv import MSEPrint,MSEPrint_0,MSEError
from mse.exception import mseError, mseFileErr

class dynamicalSystem:
    myPrint=MSEPrint_0
    def verbose(aBool):
        if (aBool):
            dynamicalSystem.myPrint=MSEPrint
        else:
            dynamicalSystem.myPrint=MSEPrint_0
    lastId=0
    """The dynamicalSystem class allows applying a partial differential equation (PDE) to a geometry."

    Within these menbers,the class stores the state of the dynamical system, a list of sources terms, the dispersion. Parameters are initialized with parameters of study.


    :param study aStudy: the mse study.
    :param int aGeoDim: 1 for line 2 for field
    :param str aMeshFile: mesh file xdmf
    :param int nbComps: the number of compatiments, or the numbers of species
    """
    def __init__(self,aStudy,aGeoDim,aMeshFile,nbComps=1, obsProc=None) -> None:
        self.id=dynamicalSystem.lastId
        dynamicalSystem.lastId+=1
        """The constructor."""
        self.initialState=[]
        self.initialStateAdjoint=[]
        for i in range(nbComps):
            self.initialState.append("np.exp(-(x[0]**2+x[1]**2))")
            self.initialStateAdjoint.append("0*np.exp(-(x[0]**2+x[1]**2))")
        self.study=aStudy
        self.geoDim=aGeoDim
        self.meshFile=self.study.meshesDir+aMeshFile
        self.obsProcess=obsProc
        try:
            os.mkdir(self.study.covDir+"ds"+str(self.id))
        except:
            pass
        try:
            with XDMFFile(MPI.COMM_WORLD,self.meshFile,"r") as xdmf:
                self.dolfinMesh= xdmf.read_mesh(name="Grid")
        except:
            MSEError(" erreur opening file "+self.meshFile)
            raise mseFileErr(self.meshFile)
        self.dx=Measure('dx',self.dolfinMesh)
        #self.vSpace=FunctionSpace(self.dolfinMesh,("CG",1))
        self.vSpace=FunctionSpace(self.dolfinMesh,("Lagrange",1))# NOTE: new name for vspace GC => Lagrange
        #Creation tableau qui contient param de type dolf.constant
        self.paramD=[None]*self.study.nbParam
        for it in range(self.study.nbParam):
            self.paramD[it]=Constant(self.dolfinMesh, ScalarType(0.0))
        self.setParamDolf()
        #nomber of compartiments
        self.nbComps=nbComps
        self.nbDofsPerComp=self.vSpace.tabulate_dof_coordinates().shape[0]
        self.nbDofs=self.nbDofsPerComp*self.nbComps
        self.u= TrialFunction(self.vSpace)
        self.v =TestFunction(self.vSpace)
        #etat au pas de temps precedent
        self.utm1= state(self)
        self.uBufVec=vector()
        self.uBufVec.buildFromDolfinFunction(self.utm1.comps[0])
        #self.u0= Function(self.vSpace)
        self.uk= state(self)
        self.bufAdjoint = state(self)
        #saveState is used by adapted time step(can be built only if adapted is used)
        self.savState=state(self)
        self.indexInSystem=0
        self.beginDofsIndexInSystem=0
        #state traj and buffer
        self.trajState=state(self)
        self.bufState=state(self)
        #u(1-u)
        self.sourcesTerm=[]
        #\Delta u
        self.secondOrderTerm=None
        #mass matrix
        aux=self.u*self.v*self.dx
        self.massMat=matrix(assemble_matrix(form(aux)))
        self.covariables=[]
        self.study.system._addDS(self)
        #diconary of covariables
        # cov(x,y,t)
        self.dicHeterogenTimeVarCov = dict()
        #cov(x,y)
        self.dicHeterogenCov = dict()
        #cov=cst
        self.dicHomogenCov = dict()
        #cov=v(t)
        self.dicHomogenTimeVarCov = dict()
        self.dicCov= dict()
    def addHomogenTimeVarCov(self,aName,aCov):
        self.dicHomogenTimeVarCov[aName]=aCov
        self.dicCov[aName]=aCov.femCov
    def addHomogenCov(self,aName,aCov):
        self.dicHomogenCov[aName]=aCov
        self.dicCov[aName]=aCov.femCov
    def addHeterogenTimeVarCov(self,aName,aCov):
        self.dicHeterogenTimeVarCov[aName]=aCov
        self.dicCov[aName]=aCov.femCov
    def addHeterogenCov(self,aName,aCov):
        self.dicHeterogenCov[aName]=aCov
        self.dicCov[aName]=aCov.femCov
    #this function is call by the time-step algorithm
    #it can be overloading by user
    def beforeOneStep(self,fromTime,toTime):
        for cov in self.dicHeterogenTimeVarCov.values():
            cov.setValue(0.5*(fromTime+toTime))
    def setParamDolf(self):
        """updating the param with the study param
        """
        for it in range(self.study.nbParam):
            self.paramD[it].value=self.study.param[it]
    def getParam(self):
        return self.paramD
    def checkNbComps(self,aModel):
        """It checks the numbers of compatiments of the model equals self number of compartiments

        :param mse.model aModel: the checked model
        :raise : exception raised if the dimensions are not the same
        """
        if (aModel.nbComps!=self.nbComps):
            MSEError("Erreur aModel.nbComps != self.nbComps")
            raise mseError("aModel.nbComps != self.nbComps")
    def addSourcesTerm(self,aModel):
        """Add a model to the list of source term of the dynamical system

            :param txtModel aModel: the source term model to be added
        """
        self.checkNbComps(aModel)
        self.sourcesTerm.append(aModel)
        #aModel.setDS(self)
    def addSecondOrderTerm(self,aModel):
        """set the second order term of the pde

            :param txtDiffusionModel / diffusionModel aModel: the new diffusion model
        """
        self.checkNbComps(aModel)
        self.secondOrderTerm=aModel
        #aModel.setDS(self)
    def exportStateVtk(self,nStep):
        """Export current state to vtk

            :param int nStep: num export vtk
        """
        self.utm1.exportVtk(nStep)
    def save(self,prefixFile,nSTep):
        """Save the current state

        :param int nStep: part of prefix
        :param str prefixFile: prefix to sava is prefixFile + nStep
        """
        self.utm1.save(prefixFile+"ds"+str(self.id),nSTep)
    def load(self,prefixFile,nSTep):
        """Load in current state
        """
        self.utm1.load(prefixFile+"ds"+str(self.id),nSTep)
    def end(self):
        pass;
    def applyInitialState(self):
        """Define the current state from the initial state conatins in dynamicalSystem menbers

        """
        numComp=0
        def initial_condition(x):
            return eval(self.initialState[numComp])
        for comp in self.utm1.comps:
            comp.interpolate(initial_condition)
            numComp=numComp+1
    def applyInitialStateAdjoint(self):
        """Define the current state from the initial state for adjoint system contains in dynamicalSystem members

        """
        numComp=0
        def initial_condition(x):
            return eval(self.initialStateAdjoint[numComp])
        for comp in self.utm1.comps:
            comp.interpolate(initial_condition)
            numComp=numComp+1 
    def setInitialeAdjointState(self, tabInitialeStateAdjoint):
        for i in range(nbComps):
            self.initialStateAdjoint[i] = tabInitialeStateAdjoint[i]
    def setObsProcess(self, obsProc):
        """Change the obsProcess of the class.

        This function must not be used by user, it is called in the init of the class obsProcess.

        :param obsProcess obsProc: the new obsProcess
        """
        self.obsProcess=obsProc
    def saveState(self):
        """" used by adapted time step """ 
        self.savState.setFromState(self.utm1)
    def restoreState(self):
        """" used by adapted time step """ 
        self.utm1.setFromState(self.savState)
 

def computeMass(ds,mass):
    """It compute the mass of the current state of the dynamicalcSystem

    :param dynamicalSystem ds: 
    :param array mass: array[0 to 0+number of compatiment] will be filled

    """
    numComp=0
    for comp in ds.utm1.comps:
        mass[numComp]=assemble_scalar(form(comp*ds.dx))
        numComp=numComp+1
