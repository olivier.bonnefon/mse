"""modulde mse.linAlgTools contains an programming interface  on algebra tools based on petsc."""



from petsc4py import PETSc
import pymseMatTools.mseMatTools as MT
from mpi4py import MPI
from mse.state import state
from numpy.linalg import norm

class matrix:
    """The matrix classe define the programming interface on a matrix implementation, here petsc.


    :param obj aMat: A petsc mat
    """
    def __init__(self,aMat=None) -> None:
        """The constructor."""
        if (aMat is None):
            self.petscMat = PETSc.Mat()
            self.N=0
            self.M=0
        else:
            self.petscMat=aMat
            aMat.assemble()
            SS=aMat.getSize()
            self.N=SS[0]
            self.M=SS[1]
    
    def setDim(self,aN,aM):
        """It builds an empty sparce matrix.
        
        .. WARNING::

            be carful

        :param int aN: number of row
        :param int aM: number of column 
        """
        self.N=aN
        self.M=aM
        self.petscMat.createAIJ([aN,aM])
        self.petscMat.setUp()
        self.petscMat.assemble()
    def zero(self):
        self.petscMat.zeroEntries()
    def copy(self,aMat):
        self.petscMat.copy(aMat.petscMat)
    #aMat is inserted in self.
    def assembleMat(self,aMat,beginN,beginM):
        """Block addition of aMat starting at row beginN, column beginM

        :param matrix aMat
        :param int beginN
        :param int beginM
        """

        if (aMat is None):
            return
        MT.assembleMat(self.petscMat,aMat.petscMat,beginN,beginM)
        self.petscMat.assemble()
    #self=alpha*aMat+self
    def axpy(self,alpha, aMat, structure=None):
        self.petscMat.axpy(alpha,aMat.petscMat)
    #Y=self*X
    def mult(self,X,Y):
        self.petscMat.mult(X.petscVec,Y.petscVec)
    def scale(self,coef):
        self.petscMat.scale(coef)
    # if Imask and Jmask:
    #    self[iMap[i],iMap[j]] = self[iMap[i],iMap[j]] + alpha*aMat[i,j] ou i,j index de aMat
    # if Imask and not Jmask:
    #    self[iMap[i],j] = self[iMap[i],j] + alpha*aMat[i,j] ou i,j index de aMat
    # if Jmask and not Imask:
    #    self[i,iMap[j]] = self[i,iMap[j]] + alpha*aMat[i,j] ou i,j index de aMat
    
    def addMatMapij(self,alpha,aMat,iMap,jMap,Imask,Jmask):
        MT.addMatMapij(self.petscMat,alpha,aMat.petscMat,iMap,jMap,Imask,Jmask)
    #set one value of the matrix
    def setAij(self,i,j,value):
        self.petscMat.setValue(i,j,value)
        self.petscMat.assemblyBegin()
        self.petscMat.assemblyEnd()

class vector:
    def __init__(self,aVec=None) -> None:
        if (aVec is None):
            self.petscVec = PETSc.Vec()
            self.N=0
        else:
            self.petscVec=aVec
            self.N=aVec.getSize()
    def buildFromDolfinFunction(self,aDolfinFunc):
        self.petscVec = PETSc.Vec()
        self.setDim(aDolfinFunc.x.array.size)
    def setValuesFromState(self,aState):
        curIndexDof=0
        nbDofsPerComp=aState.dS.nbDofsPerComp
        for comp in aState.comps:
            self.petscVec.array[curIndexDof:curIndexDof+nbDofsPerComp]=comp.x.array[:]
            curIndexDof=curIndexDof+nbDofsPerComp
    def setValuesFromDolfinFunction(self,aDolfinFunc):
        self.petscVec.array[:]=aDolfinFunc.x.array[:]
    def setDim(self,aN):
        self.N=aN
        self.petscVec.createSeq(aN)
    def axpy(self,alpha,aVec):
        self.petscVec.axpy(alpha,aVec.petscVec)
    def setValuesFromVec(self,aVec):
        self.petscVec.array[:]=aVec.petscVec.array[:]
    def assembleVec(self,aVec,beginIndex):
        MT.assembleVec(self.petscVec,aVec.petscVec,beginIndex)
    def scale(self,alpha):
        self.petscVec.scale(alpha)
    def l2Dist(self,aOtherVec):
        return norm(self.petscVec.array - aOtherVec.petscVec.array,2)
    def norm2(self):
        return norm(self.petscVec.array,2)
    def zero(self):
        self.petscVec.array.fill(0.0)
    def min(self):
        return self.petscVec.min()
    def max(self):
        return self.petscVec.max()
    def print(self):
        for v in self.petscVec.array:
            print(v)
class solver:
    def __init__(self) -> None:
        self.KSP=PETSc.KSP(MPI.COMM_WORLD).create()
    def setMat(self,aMat):
        self.KSP.setOperators(aMat.petscMat)
        self.KSP.setType(PETSc.KSP.Type.PREONLY)
        self.KSP.getPC().setType(PETSc.PC.Type.LU)
    def solve(self,rhs,sol):
        self.KSP.solve(rhs.petscVec,sol.petscVec)
    def solveNoNeg(self,rhs,sol):
        self.solve(rhs,sol)
        N= sol.petscVec.getSize()
        for i in range(N):
            if (sol.petscVec.array[i]<0):
                sol.petscVec.array[i]=0
