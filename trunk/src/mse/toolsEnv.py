""" 
The module mse.toolsEnv allows to build the initial working directory with the function mse.toolsEnv.initMseEnv()
It is based on environment variable BD_MSE_DIR indicating le location of the datas needs by mse, mainly, it consists in geométrical and model bata base. 

This module checks your env diring python mse import (import mse)
"""
from os import getenv, mkdir,lstat,chmod
from os.path import isdir,dirname,exists
from shutil import copytree
import inspect
import stat
noLog=False
aLogFile=None
def MSEPrint_0(message):
    """
    Mse print function in silence mode

    :param string message: the printing message
    """
    pass
def MSEPrint(message):
    """
    Mse print messages using this function in verbose mode.

    :param string message: the printing message
    """
    if (noLog):
        return
    print("MSEPrint: ",end=": ")
    print(inspect.stack()[1][1],end=": ")
    print(inspect.stack()[1][2],end=": ")
    print(inspect.stack()[1][3],end=": ")
    print(message)
    if (aLogFile):
        aLogFile.write(str(inspect.stack()[1][1])+" "+
                str(inspect.stack()[1][2])+" "+str(inspect.stack()[1][3])+" "+message+"\n")
def MSEError(message):
    print("MSE ERROR: ",end=": ")
    print(inspect.stack()[1][1],end=": ")
    print(inspect.stack()[1][2],end=": ")
    print(inspect.stack()[1][3],end=": ")
    print(message)
    if (aLogFile):
        aLogFile.write("MSE ERROR"+str(inspect.stack()[1][1])+" "+
                str(inspect.stack()[1][2])+" "+str(inspect.stack()[1][3])+" "+message+"\n")
    raise  Exception("Erreur ")

def printNameContext():
    #print(inspect.stack()[0][3],end=" ")
    print(inspect.stack()[1][1],end=": ")
    print(inspect.stack()[1][2],end=": ")
    print(inspect.stack()[1][3],end=": ")


def ensureDirName(aPath):
    if (not aPath is None):
        if (aPath[len(aPath)-1] != '/'):
            aPath=aPath+"/"
        return aPath

def MSEBuildDir(aDir):
    try :
        if not isdir(aDir):
            mkdir(aDir)
    except:
        MSEPrint("cant build :"+aDir)


def _copyBDToMseDir(targetDir):
    installDir=dirname(__file__)
    MSEPrint("copying from "+installDir+" to "+targetDir)
    for aDIR in ["/BD_GEOMETRIES","/BD_MODELS","/DEMOS"]:
        if (exists(targetDir+aDIR)):
            MSEPrint("Directory "+targetDir+aDIR+" not copied because it exists")
        else:
            copytree(installDir+aDIR,targetDir+aDIR)
            chmod(targetDir+aDIR,lstat(targetDir+aDIR).st_mode | stat.S_IWUSR | stat.S_IWGRP)
    if (not exists(targetDir+"/MESHES")):
        mkdir(targetDir+"/MESHES")

def initMseEnv():
    """
    This function helps the user build an mse-environment.
    It consists in copying data base so that can be modified by mse-user.

    """
    #get MSE_DIR
    bdMseDir=ensureDirName(getenv("BD_MSE_DIR"))
    if (bdMseDir is None):
        bdMseDir=input("please enter an absolute path (eg: /pathto/yourUserMSE/)to your working directory for mse (data for mse will be copied inside): ")
        aDir=dirname(bdMseDir)
        if (len(aDir)>0 and exists(aDir)):
            if (not exists(bdMseDir)):
                mkdir(bdMseDir)
                _copyBDToMseDir(bdMseDir)
            MSEPrint("The directory "+bdMseDir+" now contains mse datas. \n Please follow steps: 1) exit python 2) export BD_MSE_DIR="+bdMseDir+" 3) run python.")
        else:
            MSEPrint("The path " +aDir+" of "+bdMseDir+ " doesnt exist. Please build it and retry.")
    else:
        reinit=input("The env var BD_MSE_DIR exists, do you want to copy the mse data inside ? (Y/N) ")
        if (reinit=="Y"):
            _copyBDToMseDir(bdMseDir)
        else:
            MSEPrint("If you want, you can copy data from "+dirname(__file__) + " to "+bdMseDir+" by yourself.") 


class computerEnv:
    #MSE_Dir
    bdMseDir=ensureDirName(getenv("BD_MSE_DIR"))
    if (bdMseDir is None):
        bdMseDir=ensureDirName(dirname(__file__))
        MSEPrint("BD_MSE_DIR is not set. Mse is using default data base of the install directory : "+bdMseDir+" .You can use mse.toolsEnv.initMseEnv() to init your working dirrectory.")
        #MSEPrint("BD_MSE_DIR is not set. You can use mse.toolsEnv.initMseEnv to initialize your mse env,  or set the BD_MSE_DIR (export BD_MSE_DIR=/mypath/userMse/ containing BD_GEOMETRIES ...)")
    #check bdMseDir contains DB_GEOMETRIES
    if (not isdir(bdMseDir+"BD_GEOMETRIES")):
        MSEPrint("BD_MSE_DIR doesnt contains BD_GEOMETRIES dir: "+ bdMseDir+" BD_GEOMETRIES doesnt exists. You can use mse.toolsEnv.initMseEnv() to init your working dirrectory")
    else:
        #geomDir
        geomDir=ensureDirName(getenv("MSE_GEOM_DIR"))
        if (geomDir is None):
            geomDir=bdMseDir+"/BD_GEOMETRIES/"
        #modelDir
        modelsDir=ensureDirName(getenv("MSE_MODELS_DIR"))
        if (modelsDir is None):
            modelsDir=bdMseDir+"/BD_MODELS/"
#todo
#readerVerbose=0
#buildMeshesVerbose=0
