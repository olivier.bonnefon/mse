from mse.study import study
import numpy as np #FIXME

class simulator:
    def __init__(self,t0,t1,aStudy) -> None:
        self.study=aStudy
        aStudy.simulator=self
        self.t0=t0
        self.t1=t1
        self.curT=t0
        self.doOneStep=None
        self.curStep=0
        self.fileTime=None
    def initSimulation(self):
        self.curStep=0
        self.curT=self.t0
        #open file with time step
        self.fileTime=open(self.study.simuDir+"time.txt","w")
        #load initial state
        for ds in self.study.system.dynamicalSystems:
            ds.applyInitialState()
            for mod in ds.sourcesTerm:
                mod.mat=None
            if (ds.secondOrderTerm):
                ds.secondOrderTerm.mat=None
        self.doOneStep.aTS.M=None
        #export vtk initial state and save state
        self.afterStepSimu()
    def endSimu(self):
        #close time file
        self.fileTime.close()
        self.fileTime=None
    def afterStepSimu(self):
        for ds in self.doOneStep.study.system.dynamicalSystems:
            ds.save(self.study.simuDir,self.curStep)
        self.fileTime.write(str(self.curStep)+" "+str(self.curT)+"\n")
    def doSimu(self):
        self.initSimulation()
        #set initial state
        while (self.curT<self.t1):
            self.curT=self.curT+self.doOneStep.doOneStep()
            if (self.doOneStep.status):
                print("simulator.doSimu status="+str(self.doOneStep.status))
                print("stop simu")
                return
            self.curStep=self.curStep+1
            self.afterStepSimu()
            print("\rsimu running: {0:1.3f}".format(100*self.curT/self.t1),"%",end="")
        print("\n")
        self.endSimu()
    def exportSimuToVtk(self):
        #read time.txt
        if (self.fileTime != None):
            print("simulator: ended simulation before build vtk")
            return
        self.fileTime=open(self.study.simuDir+"time.txt","r")
        #open vtk files
        for ds in self.study.system.dynamicalSystems:
            ds.utm1.initFilesToExportVtk(self.study.exportDir+"ds"+str(ds.id))
        self.curStep=0
        for line in self.fileTime:
            for ds in self.doOneStep.study.system.dynamicalSystems:
                ds.load(self.study.simuDir,self.curStep)
                ds.exportStateVtk(self.curStep)
            self.curStep=self.curStep+1
        #close time.txt
        self.fileTime.close()
        self.fileTime=None
        #close vtk files
        for ds in self.study.system.dynamicalSystems:
            ds.utm1.closeFilesToExportVtk()
class simulatorBackward(simulator):
    """
    Sub class of simulator, used for adjoint system.

    :param t0 float: time of the begin of the BACKWARD simulation
    :param t1 float: time of the end of the BACKWARD simulation
    :param aStudy study:

    """
    def __init__(self,t0,t1,aStudy) -> None:
        self.study=aStudy
        aStudy.simulatorBackward=self
        self.t0=t0
        self.t1=t1
        self.curT=float(t0)
        self.doOneStep=None
        self.curStep=0
        self.fileTime=None
    def initSimulation(self):
        """ Function to initialize simulation
        """
        self.curStep=0
        self.curT=float(self.t0)
        #open file with time step
        self.fileTime=open(self.study.adjointDir+"time.txt","w")
        #load initial state
        # change initial state
        for ds in self.study.system.dynamicalSystems:
            ds.applyInitialStateAdjoint()
        #export vtk initial state and save state
        self.afterStepSimu()
    def afterStepSimu(self):
        """ Save the result of simultaion in file

        Function call after initSimulation and each step of simulation.
        This function call the function dS.save
        """
        for ds in self.doOneStep.study.system.dynamicalSystems:
            ds.save(self.study.adjointDir,self.curStep)
        self.fileTime.write(str(self.curStep)+" "+str(self.curT)+"\n")
    def doSimu(self):
        """ Function to run the simulation
        
        This function is called in systemAdjoint.buildAdjointState()
        """
        self.initSimulation()
        #set initial state
        while (self.curT>self.t1):
            self.curT=self.curT+self.doOneStep.doOneStep()
            if (self.doOneStep.status):
                print("simulator.doSimu status="+str(self.doOneStep.status))
                print("stop simu")
                return
            self.curStep=self.curStep+1
            self.afterStepSimu()
            print("\rBackward simu running: {0:1.3f}".format(100+100*(self.t1-self.curT)/(self.t0)),"%",end="")
        print("\n")
        self.endSimu()
    def exportSimuToVtk(self):
        #read time.txt
        if (self.fileTime != None):
            print("simulator: ended simulation before build vtk")
            return
        self.fileTime=open(self.study.adjointDir+"time.txt","r")
        #open vtk files
        for ds in self.study.system.dynamicalSystems:
            ds.utm1.initFilesToExportVtk(self.study.exportDir+"SYSADJ/"+"ds"+str(ds.id))
        self.curStep=0
        for line in self.fileTime:
            for ds in self.doOneStep.study.system.dynamicalSystems:
                ds.load(self.study.adjointDir,self.curStep)
                ds.exportStateVtk(self.curStep)
            self.curStep=self.curStep+1
        #close time.txt
        self.fileTime.close()
        self.fileTime=None
        #close vtk files
        for ds in self.study.system.dynamicalSystems:
            ds.utm1.closeFilesToExportVtk()
