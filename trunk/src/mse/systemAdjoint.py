
class systemAdjoint:
    """An attribute of study for compute adjoint state

    To calculate the gradient of J, we have to solve an adjoint system such as :

    .. math::
        < \\tilde v'(u_\\theta)+\partial _u F(\\theta,u_\\theta)^*P,w >_H=0

    with :math:`\\tilde v'` wich depends of the observation processus

    and :math:`\partial_uF(\\theta,u_\\theta)^*` such that :

    .. math::
        <\partial_uF(\\theta,u_\\theta)\epsilon;w> = <\epsilon;\partial_uF(\\theta,u_\\theta)^* w>

    """
    def __init__(self,aStudy) -> None:
        self.study=aStudy
        self.system=self.study.system
        self.study.setSysAdj(self)
    #NOTE: need ??
    #def addInteraction(self,aI):
    #    self.interactions.append(aI)
    #def saveState(self):
    #    for ds in self.system.dynamicalSystems:
    #        ds.saveState()
    #def restoreState(self):
    #    for ds in self.system.dynamicalSystems:
    #        ds.restoreState()
    #END NOTE
    def buildAdjointState(self):
        """Build adjoint state, It computes the solution of the adjoint system contained in the study and depending in the obsProcess via the obsProcess menber tildev.

        A fisrts implementation consists in building the adjoint system, using dolfinx, for a system using 1 compartiments without interactions.
        A second step is to write if this computation holds for 2 compartiments.
        A third step is to integrate 2D1D interactions
        """
        #call the dosimu systemAdjoint
        self.study.simulatorBackward.doSimu()
    def valJAndGradJ(self, param):
        """ return val of J(param) and gradJ(param)

        The simulation bakward will be run in computeGradV()


        :param param array: parameter to evaluate :math:`J` and :math:`\\nabla J`
        """
        # update param on study
        self.study.setParam(param)
        # compute simu with this param
        self.study.simulator.doSimu()
        # loop for each dS ?
        #NOTE; we supposed only 1 ds
        #NOTE: check what append with num of dS >1
        #NOTE: sum
        for ds in self.system.dynamicalSystems:
            # compute J(param)
            retV = ds.obsProcess.computeV()
            # compute gradJ
            retGradV = ds.obsProcess.computeGradV()
        # return J and GradJ
        return retV, retGradV
