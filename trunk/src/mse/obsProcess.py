from ufl import ds, dx, grad, inner
from dolfinx.fem import Function, assemble_scalar,form, Constant
from ufl import Measure,TrialFunction,TestFunction
import numpy as np
from mpi4py import MPI
from sympy import diff
from mse.MODELS.models import strForDolf
from dolfinx.fem.petsc import assemble_matrix, assemble_vector
from petsc4py import PETSc
from petsc4py.PETSc import ScalarType
from numpy.linalg import norm
from mse.state import state
from mse.explorTrajectory import explorTrajectory
from mse.systemAdjoint import systemAdjoint
from mse.TIMESCHEMES.timeScheme import implicitLinearBackwardTimeScheme
from mse.timeDoOneStep import timeDoOneStepBackward
from mse.simulator import simulatorBackward
from mse.toolsEnv import MSEPrint, MSEPrint_0
from os.path import exists
from os import mkdir
class obsProcess:
    """Virtual class for observation.

    This class contain the definition of main function for an obsprocess.
    """
    def _subFXForm(self,strModel,X):
        """function use for exec of str in Code.

        we transform a string of Model in a string wich contain form and the dX
        """
        #NOTE: move this function ? Used in txtModel, txtDiffModel
        unknownName="abcdefgh"
        for j in range(self.dS.nbComps):
            strModel=strModel.replace("$"+unknownName[j],"self.dS."+X+".comps["+str(j)+"]")
        code="form(("+strModel+")"
        self.myPrint("X ->"+code)
        return code
    def __init__(self, aDS, aDirURef=None)->None:
        self.myPrint=MSEPrint_0
        #self.myPrint=MSEPrint
        self.dS=aDS
        self.dS.setObsProcess(self)
        self.index = self.dS.indexInSystem
        self.bufStateObs=state(self.dS)
        #v and vtild
        self.vi=Function(self.dS.vSpace)
        self.vtild=state(self.dS)
        # Tab containing solution of adjoint system for each dynamicalSystem for each compenant
        # array containing grad of V (J), length nbParam
        self.gradJ=np.zeros(self.dS.study.nbParam)
        #directory containing file with value of uRef
        if (aDirURef==None):
            self.dirURef=self.dS.study.absoluteStudyDir+"/UREF/"
        else:
            self.dirURef=aDirURef
        #explor_u type of explorTrajectory
        self.explor_u = explorTrajectory(self.dS.study, self.dS.study.simuDir)
        #Function we will contain d_a F_i
        self.daF=Function(self.dS.vSpace)
        # float for choice of integral method: 1=trapeze, 0=square
        self.methodIntT=1
        self.sysAdj= systemAdjoint(self.dS.study)
        self.timeSchemeBackward = implicitLinearBackwardTimeScheme(self.dS.study.simuDir)
        t1 = aDS.study.simulator.t1
        t0 = aDS.study.simulator.t0
        self.simuBackward = simulatorBackward(t1,t0,self.dS.study)
        self.tDoStepBackward = timeDoOneStepBackward(self.dS.study, self.timeSchemeBackward)
        self.timeSchemeBackward.theta = 1
        # Tab wich contain the sum of all derivate of ST with respect to param (size of nbParam)
        #this is initiate in computeGradV()
        self.varDerParam = None
    def computeV(self):
        """ Method to compute the observation process for the current study parameters.
        """
        pass
    def computeTildevPrim(self):
        """compute the state tilte v : H ->R; u|->v(u)
        """
        pass
    def computeGradV(self):
        """By default, it builds the adjoint state via systemAdjoint class and build the grad of the obsprocess using :math:`<P.\\partial_aF(a,u_a)e_i>`.

        read file from adjoint simu directory and create gradV.
        """
        ##1
        #Calcul de l'etat adjoint
        # systemAdjoint #
        #NOTE: timeScheme implicit backward
        #call function for solve sysAdjoint
        self.sysAdj.buildAdjointState()
        ##
        ##2
        ## Check that the file 'modelDp.txt' exist, if not, it will be created.
        # for sourceTerm
        for sourceTerm in self.dS.sourcesTerm:
            sourceTerm.enableAdjoint()
        # for 2ndOrderTerm
        self.dS.secondOrderTerm.enableAdjoint()
        ##3
        ## Check if varDerParam is init, else create it
        if self.varDerParam != None:pass
        else:
            self.varDerParam = [None]*(self.dS.study.nbParam)
            # tab of str we wil excecute
            strdFp = [ '' for _ in range(self.dS.study.nbParam)] #tab while contain str of dFp for each comps, =daF
            # loop for each ST
            for sourceTerm in self.dS.sourcesTerm:
                #tmp str, value of '' or '+'
                # We read derivate of ST
                try:
                    fp = open(sourceTerm.txtFile+"Dp"+".txt", "r")
                except IOError:
                    MSEPrint("IOError in obsProcess when reading "+sourceTerm.txtFile+"Dp.txt")
                    return
                for numComp in range(self.dS.nbComps):
                    for numParam in range(self.dS.study.nbParam):
                        # read the line
                        l_linedFp = fp.readline().split('=')[1].strip()
                        #Test if the derivate is !=0
                        if l_linedFp != "0":
                            # replace compment for dolfinix
                            l_linedFp = strForDolf(l_linedFp,sourceTerm.unknownName)
                            # replace param
                            for count,p in enumerate(self.dS.study.param):
                                l_linedFp=l_linedFp.replace('p'+str(count+1),'self.dS.paramD['+str(count)+']')
                            # multiply by adjoint
                            l_linedFp='*'.join((l_linedFp,"self.dS.bufAdjoint.comps["+str(numComp)+"]"))
                            # Test if strFD is empty or not => if had to add '+' or not
                            if (strdFp[numParam] == ''):strdFp[numParam]=l_linedFp
                            else:strdFp[numParam]='+'.join(strdFp[numParam],l_linedFp)
                        else:pass
                fp.close()
            ######
            #NOTE ajout contribution de la diffusion
            ## READ DERIVATIVES PARAM ONLY IF secondOrderTerm.txt exist(else 2ndter is const)
            if hasattr(self.dS.secondOrderTerm,"txtFile"):
                try:
                    fp = open(self.dS.secondOrderTerm.txtFile+"Dp.txt","r")
                except IOError:
                    MSEPrint("IOError in obsProcess when reading "+sourceTerm.txtFile+"Dp.txt")
                    return
                # NOTE: we will save form, if 0, we save NULL or 0
                # loop for each comp
                for numComp in range(self.dS.nbComps):
                    # loop for each param
                    for countP in range(len(self.dS.paramD)):
                        ##READ derivate of SECOND ORDER TERM
                        # NOTE: file is like : dpx1fa=0; dpx2fa=0; dpyfa=....
                        # loop for dim
                        # DxxPi
                        strlineX=fp.readline().split('=')[1].strip()
                        strlineX=strForDolf(strlineX,self.dS.secondOrderTerm.unknownName)
                        # DyyPi
                        strlineY=fp.readline().split('=')[1].strip()
                        strlineY=strForDolf(strlineY,self.dS.secondOrderTerm.unknownName)
                        # index
                        index = countP
                        ## Test if deivate is Null
                        if strlineX == "0":
                            if strlineY =="0":strline=''
                            else:
                            #TODO NOTE: do something if strlineX and strlineY == 0 to not add 0 in form
                                # Only dim x ==0
                                strline ="-grad("+strlineY+"*self.dS.trajState.comps["+str(numComp)+"])[1]*grad(self.dS.bufAdjoint.comps["+str(numComp)+"])[1]"
                        elif strlineY == "0":
                            # Only dim y ==0
                            strline = "-grad("+strlineX+"*self.dS.trajState.comps["+str(numComp)+"])[0]*grad(self.dS.bufAdjoint.comps["+str(numComp)+"])[0]"
                        else:
                            # both != 0
                            strline = "(-grad("+strlineX+"*self.dS.trajState.comps["+str(numComp)+"])[0]*grad(self.dS.bufAdjoint.comps["+str(numComp)+"])[0] - grad("+strlineY+"*self.dS.trajState.comps["+str(numComp)+"])[1]*grad(self.dS.bufAdjoint.comps["+str(numComp)+"])[1])"
                        # Test if strline is not empty
                        if(strline !=''):
                            # Test if strFD is empty or not => if had to add '+' or not
                            if (strdFp[countP] == ''):strdFp[countP]=strline
                            else:strdFp[countP]='+'.join(strdFp[countP],strline)
            ######
            #EXCECUTION
            for count,strline in enumerate(strdFp):
                if strline =='':pass
                else:
                    code = "self.varDerParam ["+str(count)+"]="+self._subFXForm(strline,"trajState")+"*self.dS.dx)"
                    exec(compile(code,'sumstring', 'exec'))
        ##2: create vardp := \partial_aF(a,ua)
        ###V1: with str
                #NOTE: l_strdFp is 2d list, l_strdFp[i][j] return the partial derivate with respect of F_j model of a_i param
        # to use it: dFa_i = "+".join(strdFp[i])pastT
        l_aExplorTrajAdjoint=explorTrajectory(self.dS.study,self.dS.study.adjointDir)
        l_aExplorTrajAdjoint.rewind()
        self.gradJ[:] = 0
        #loop on time
        #tmp_count=0#FIXME
        l_aExplorTrajAdjoint.replay()
        pastT=self.dS.study.simulatorBackward.t0
        pastF=np.zeros(self.dS.study.nbParam)
        curF =np.zeros(self.dS.study.nbParam)
        ### todo: READ SECONDoRDERtER.varfs[??], if != None add in grad
        tmpDPSOTX = 0
        tmpDPSOTY = 0
        while(l_aExplorTrajAdjoint.replay()):
            #adjoint is load in ds.utm1 after call of l_aExplorTrajAdjoint.replay
            # coef si integration rectangle ou trapeze
            coef = self.methodIntT
            #save val of solution of adjoint state
            self.dS.bufAdjoint.setFromState(self.dS.utm1)
            #update curT
            curT=l_aExplorTrajAdjoint.T
            #Update ua
            self.explor_u.interpolToTime(curT)
            l_dtAdj = abs(curT - pastT)
            # loop for each comps
            for count,comp in enumerate(self.dS.bufAdjoint.comps):
                #NOTE: on suppose 1 seul nbComps pour le moment => bug possible plus tard car on a sommer chaque comps
                # gradJ = <P.\partial_aF(a,ua)e_i>.
                # loop for each param
                # comp contain P, u_a is in trajState.comps[]
                #tmp_count+=1#FIXME
                for i in range(self.dS.study.nbParam):
                    # Take derivate of ST with respect of i-th param
                    formAdjoint = self.varDerParam[i]
                    # test if derivate of SOT with respect of param is Null
                    if (formAdjoint  == None):scalarAdjoint=0
                    else:
                        # Assemble
                        scalarAdjoint = assemble_scalar(formAdjoint)
                    curF[i] = scalarAdjoint
            self.gradJ[:]+=l_dtAdj*(coef*pastF[:]+(2-coef)*curF[:])*0.5
            # update of past T and past F
            pastF[:]=curF[:]
            pastT=curT
        return(self.gradJ)
class quadraticDiff(obsProcess):
    """Subclass of obsProcess

    to use if we want a quadratic difference as a observation process

    :param dynamicalStructure aDS:
    :param str aDirURef: an absolute pasth to the directory where observation files are. By default in dir study.absoulteSutyDir+'UREF/'.
    """
    myPrint=MSEPrint_0
    def __init__(self,aDS, aDirURef=None) -> None:
        super().__init__(aDS, aDirURef)
        #explor_uRef type of explorTrajectory
        self.explor_uRef = explorTrajectory(self.dS.study, self.dirURef)
    def computeV(self):
        """ Method to compute the quadratic difference for the current dynamical system parameters.
        Assume simulation has been running.

        .. math::
            \\int_Q sum_k |u_k-u_{ref_k}|^2, k = nbComps.
        return: float V: value of the observation process, the quadratic difference
        """
        V=0.
        current_time = self.dS.study.simulator.t0
        final_time = self.dS.study.simulator.t1
        quadraticDiff.myPrint(" to do : base this time integration on time step in simu, without interpolation")
        count=0
        self.explor_uRef.rewind()
        #exploreTraj while save the result in ds.utm1
        while(self.explor_uRef.replay()):
            ldt=self.dS.study.simulator.doOneStep.dt#NOTE: true ???
            #update time
            current_time = self.explor_uRef.T
            #NOTE: the function explorTime.interpolToTime(t) update ds.trajState,
            #only comps 0
            tmp_uref = np.array(self.dS.trajState.comps[0].x.array[:])
            self.explor_u.interpolToTime(current_time)
            #np.savetxt("TMP_RES/tmpFile_uref"+str(count)+".txt",self.dS.utm1.comps[0].x.array)#FIXME
            #for count, uk in enumerate(self.dS.utm1.comps):
            l_diffUUref = (self.dS.trajState.comps[0].x.array[:]-self.dS.utm1.comps[0].x.array[:])
            self.vi.x.array[:]= l_diffUUref * l_diffUUref
            tmp=assemble_scalar(form(self.vi*self.dS.dx))
            #np.savetxt("TMP_RES/tmpFile_v"+str(count)+".txt",self.vi.x.array)#FIXME
            count+=1
            V+= ldt*tmp
        return(V)
    def computeTildevPrim(self, temps):
        """compute the state :math:`\\tilde v\'` : H ->R; u|-> :math:`\\tilde v\'(u)`

        We suppose: :math:`\\tilde v\' = [0.5*(u_k-u_{ref_k})]`, with k = nbComps

        :param float temps: time of simulation to calculate :math:`\\tilde v\'`
        """
        #NOTE: we supposed only one comps
        #1: call function to interpolate value
        self.explor_uRef.interpolToTime(temps)
        #we copy val of dS.trajState
        self.bufStateObs.setFromState(self.dS.trajState)
        self.explor_u.interpolToTime(temps)
        #2: take value of u, buf is just an address copy
        buf=self.dS.trajState.comps[self.index]
        #update self.vtild
        self.vtild.comps[self.index].x.array[:]=2*(buf.x.array[:]-self.bufStateObs.comps[self.index].x.array[:])
