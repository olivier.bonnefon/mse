from mse.MODELS.models import model, strForDolf, formatCovariables
import mse.dynamicalSystem
from dolfinx.fem import Constant,form
from petsc4py.PETSc import ScalarType
from dolfinx.fem.petsc import assemble_matrix
from ufl import grad,dot
from mse.linAlgTools import matrix
from mse.toolsEnv import computerEnv, MSEPrint, MSEPrint_0,MSEError
from os.path import exists
from sympy import *
from math import exp,log

class diffusionModel(model):
    myPrint=MSEPrint_0
    def verbose(aBool):
        if (aBool):
            model.myPrint=MSEPrint
        else:
            model.myPrint=MSEPrint_0
    def __init__(self,aDs,aDiffCoef=1) -> None:
        super().__init__(aDs)
        aDs.addSecondOrderTerm(self)
        self.diffCoef=Constant(self.dS.dolfinMesh, ScalarType(aDiffCoef))
        self.block=None
        self.blockAdjoint=None
        self.bilinearForm=None
        self._computeBlock()
        self.updateLinearisedMat()
        self.nbDim = 2
        self.varfs=[None for i in range(self.nbComps)]
    def setDiffCoef(self,aDiffCoef):
        """Set a new const coef for the diffusion


        :param float aDiffCoef: new coeffficient for diffusion
        """
        self.diffCoef=Constant(self.dS.dolfinMesh, ScalarType(aDiffCoef))
        self._computeBlock()
        self.updateLinearisedMat()
    def _computeBlock(self):
        aux=dot(grad(self.diffCoef*self.dS.u),grad(self.dS.v))*self.dS.dx
        self.bilinearForm=form(aux)
        #self.myPrint("aux = "+str(aux))
        #self.myPrint("bilineaire form in diff model(cst): "+str(self.bilinearForm))
        self.block=matrix(assemble_matrix(self.bilinearForm))
    def updateBlockMat(self,compi,compj):
        if (compi==compj):
            return self.block
    def _computeBlockAdjoint(self):
        aux=dot(grad(self.dS.u),grad(self.diffCoef*self.dS.v))*self.dS.dx
        self.bilinearForm=form(aux)
        self.blockAdjoint=matrix(assemble_matrix(self.bilinearForm))
    def updateBlockMatAdjoint(self,compi,compj):
        if (compi==compj):
            return self.blockAdjoint
    def enableAdjoint(self):
        pass

class txtDiffusionModel(model):
    """ Class used to add diffusion in model with parameters.
   
    The diffusion model is stored in a txt file.
    NOTE: only avaible for compment in 2d
    The txt file must be this form:
    fax=...
    fay=...
    with 'a' the compement. We are in 2 dim, each compement contain 2 diffusion


    :param dynamicalSystem aDs: class allows applying a PDE to a geometry
    :param str strModel: file contain diffModel.
    """
    unknownName="abcdefgh"
    def _subFXForm(self,strModel,X):
        for j in range(self.nbComps):
            strModel=strModel.replace("$"+txtDiffusionModel.unknownName[j],"self.dS."+X+".comps["+str(j)+"]")
        code="form(("+strModel+")"
        model.myPrint("X ->"+code)
        return code

    def _execStr(self, strEquation, index):
        """Function will take a string wich contain equation, and do execution to transform it in form
        """
        ##replace param
        for tmpCountP,tmpP in enumerate(self.dS.paramD):
            strEquation=strEquation.replace('p'+str(tmpCountP+1),'self.dS.paramD['+str(tmpCountP)+']')
        #replace COVARIABLES
        strEquation=formatCovariables(strEquation,"self.dS.dicCov[\"", "\"]")
        #### BUILD VARF
        code="self.varfs["+str(index)+"]=form("+strEquation+")"
        exec(compile(code, 'sumstring', 'exec'))


    def __init__(self,aDs, aTxtFile) -> None:
        super().__init__(aDs)
        aDs.addSecondOrderTerm(self)
        self.txtFile=aTxtFile
        self.block=None
        self.blockAdjoint=None
        self.bilinearForm=None
        self.nbDim = 2
        #NOTE: change dim
        self.varfs=[None for i in range(self.nbComps)]
        self._computeBlock()
        self.updateLinearisedMat()
    def setDiffCoef(self,aTxtFile):
        """Set a new diffusion model.

        The new model is store in a txt file.

        :param str aTxtFile: the file wich contain the new diffusion model
        """
        self.diffCoef=Constant(self.dS.dolfinMesh, ScalarType(aDiffCoef))
        self._computeBlock()
        self.updateLinearisedMat()
    def _computeBlock(self):
        # we check if file exist
        if (exists(self.txtFile+".txt")):
            self.txtFile=self.txtFile
        else:
            self.txtFile=computerEnv.modelsDir+self.txtFile #self.txtFile form is "affineGrowth"
        model.myPrint("Looking for model  "+self.txtFile)
        try:
            f = open(self.txtFile+".txt", "r")
        except IOError:
            MSEPrint("IOError in txtDiffModel when reading "+self.txtFile+".txt")
            return
        #self.varfsTm1=[None]*self.nbComps
        #for the comp number numComp \in {0,..,self.nbComps-1}:
        strkeys=r"[\s\+\-\*/\=\(\)\^]+"
        for numComps in range(self.nbComps):
            ##READ SECOND ORDER TERM
            strline = ""
            tmpAdd = ""
            # NOTE: dim = 2
            for ind in range(2):
                # Dxx
                strlineX = f.readline().split('=')[1].strip()
                strlineX = strForDolf(strlineX, self.unknownName)
                strlineX = strlineX.replace('^','**')
                ##replace param
                for count_p in range(len(self.dS.paramD)):
                    strlineX=strlineX.replace('p'+str(count_p+1),'self.dS.paramD['+str(count_p)+']')
                    #strlineY=strlineY.replace('p'+str(count_p+1),'self.dS.paramD['+str(count_p)+']')
                #### replace COVARIABLES
                strlineX=formatCovariables(strlineX,"self.dS.dicCov[\"", "\"]")
                #### BUILD VARF USING FK
                strline += tmpAdd+ "grad(self.dS.u*(" + strlineX + "))["+str(ind)+"]*grad(self.dS.v)["+str(ind)+"]*self.dS.dx" #"+grad(self.dS.u*"+strlineY+")[1]*grad(self.dS.v)[1]*self.dS.dx"
                tmpAdd = "+"
            # END LOOP
            model.myPrint(strline)
            index = numComps
            code="self.varfs["+ str(index) +"]="+self._subFXForm(strline,"uk")+")"
            try:
                exec(compile(code, 'sumstring', 'exec'))
            except:
                MSEError("code exec err:"+code)
    def updateBlockMat(self,compi,compj):
        if (compi==compj):
            return matrix(assemble_matrix(self.varfs[compi]))
    def updateBlockMatAdjoint(self,compi,compj):
        if (compi==compj):
            return self.blockAdjoint
    ### surcharge function "getlineraized" to add update
    def getLinearisedMat(self):
        super().updateLinearisedMat()
        return self.mat
    def enableAdjoint(self):
        """Function to check if derivate file to param exist, else create it
        """
        try:
            f = open(self.txtFile+".txt", "r")
        except IOError:
            MSEPrint("IOError in txtDiffModel when reading "+self.txtFile+".txt")
            return
        try:
            fd = open(self.txtFile+"Dp.txt", "r")
        except FileNotFoundError:
            # if didn't exist, we create it
            self.dvFileExist=False
            print("Création fichier "+self.txtFile+"Dp.txt")
            fd = open(self.txtFile+"Dp.txt", "w") #open file in mod write
            # loop for each comp
            for numComps in range(self.dS.nbComps):
             # we read the both dimension
                # X
                strFX=f.readline()
                strFX=strFX.split('=')[1].strip()#.replace("^","**")
                # Y
                strFY=f.readline()
                strFY=strFY.split('=')[1].strip()#.replace("^","**")
                #loop for each param
                for p in range(self.dS.study.nbParam):
                    # X
                    grad1s=diff(strFX,'p'+str(p+1))
                    grad1=str(grad1s).replace("**","^")
                    #write in fic
                    fd.write("dpx"+str(p+1)+"f"+txtDiffusionModel.unknownName[numComps]+"="+grad1+"\n")
                    # Y
                    grad1s=diff(strFY,'p'+str(p+1))
                    grad1=str(grad1s).replace("**","^")
                    #write in fic
                    fd.write("dpy"+str(p+1)+"f"+txtDiffusionModel.unknownName[numComps]+"="+grad1+"\n")
            f.close()
            fd.close()
