echo "ensure you are in mse/trunk directory ?"

TRUNK_MSE=$(pwd)/

rm -rf mseMapTools/build/* mseMapTools/Frontend/build/ 

echo "========================================================"
echo "===== 1) install mseMapTools ==========================="
echo "========================================================"
cd $TRUNK_MSE
mkdir mseMapTools/build
cd mseMapTools/build
cmake .. -DCMAKE_INSTALL_PREFIX=$TRUNK_MSE/TEST_INSTAL/installmseMapTools
make install
export MSEMAPTOOLS_INCLUDE_DIR=$TRUNK_MSE/TEST_INSTAL/installmseMapTools/include/
export MSEMAPTOOLS_LIBRARY_DIR=$TRUNK_MSE/TEST_INSTAL/installmseMapTools/lib/

echo "========================================================"
echo "====== 2) install python interface pymseMapTools ======="
echo "========================================================"
cd $TRUNK_MSE

mkdir mseMapTools/Frontend/build
cd mseMapTools/Frontend/build
cmake .. -DMSE_MAP_TOOLS_INSTALL_PATH=$TRUNK_MSE/TEST_INSTAL/installmseMapTools -DCMAKE_INSTALL_PREFIX=$TRUNK_MSE/TEST_INSTAL/installpymseMapTools
#make VERBOSE=TRUE
make 
export PYTHONPATH=$PYTHONPATH:$TRUNK_MSE/TEST_INSTAL/installpymseMapTools/lib/python3.10/site-packages/

echo "to check this:"
python3 -c "import pymseMapTools.mseMapTools as mp"

cd $TRUNK_MSE


