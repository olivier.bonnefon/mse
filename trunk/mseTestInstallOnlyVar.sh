# 
# This file is useful for developer. Users of the mse library are advised to consult the documentation available at https://mse.biosp.org/ for its usage.
#
# This script allows for :
# 1) to compil all mse components using current sources 
# 2) test the install procedure in view of adjust the guix cook
#
# to use it:
#
# 1 -> set software environnement using guix:
# cd mse/trunck
# guix time-machine -C ../guix/channelsMSE.scm -- shell -C --share=/tmp --preserve='^DISPLAY$' -m ../guix/manifestDolfinxv06.scm  -m ../guix/manifestCompilAllMSEComponents.scm -L ../guix/recettes/
# 2 -> source ./mseTestInstall.sh
#
#

TRUNK_MSE=$(pwd)/
export PYTHONPATH=$PYTHONPATH:$TRUNK_MSE/TEST_INSTAL/installpymseSafranTools/lib/python3.10/site-packages/
export PYTHONPATH=$PYTHONPATH:$TRUNK_MSE/TEST_INSTAL/installpymseMatTools/lib/python3.10/site-packages/
export PYTHONPATH=$PYTHONPATH:$TRUNK_MSE/TEST_INSTAL/installpymseMapTools/lib/python3.10/site-packages/
export PYTHONPATH=$PYTHONPATH:$TRUNK_MSE/TEST_INSTAL/installMse/lib/python3.10/site-packages/

