#import os
#print(os.environ)
#because guix seems not add the path of python pymseMapTools"

try:
    import pymseSafranTools.mseSafranTools as ms
    ms.printInfo("../data/SAFRAN_2003080107_2004080106.nc")
    print("find pymseSafranTools.mseSafranTools")
except: 
    print("CAN NOT find pymseSafranTools.mseSafranTools")
import numpy as np
nRegHours=np.array([0], dtype=np.int32);
ms.getSafranInfo("../data/SAFRAN_2003080107_2004080106.nc",nRegHours);
ms.timeDataToTimeSimulator(0,int(nRegHours[0]),24,0,1.0*nRegHours[0])

tableau = np.fromfile("../data/xyz.bin", dtype=np.float64)
# Lirf le fichier binaire et charger les données dans un tableau numpy
ms.perform("../data/SAFRAN_2003080107_2004080106.nc",ms.SAFRAN_TEMPERATURE,tableau,"T",ms.MSE_CST_NETCDF_TYPE_BRUT,"../data/COV/")
ms.finishSafran()

exit(0)
