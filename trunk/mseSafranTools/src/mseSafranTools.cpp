#include "mseSafranTools.hpp"
#include "rasterIndexSafran.hpp"
#include <stdio.h>
#include <string.h>
#include <netcdf.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <sys/stat.h>
#include "mseMapTools.h"

using namespace std;
//CARACTERISTIQUES DE LA GRILLE SAFRAN a ce jour
#define XMIN 60000.0
#define XMAX 1196000.0
#define DX 8000.0
#define NX 142
//XMIN + NX*DX=XMAX
//nombre de points =NX+1
#define YMIN 1617000.0
#define YMAX 2681000.0
#define DY 8000.0
#define NY 134
#define NB_PT_SAFRAN 9892

//#define DEBUG_NETCDF_MSE
#define NAME_SIZE 2048


#define ERR(e) if ( e!=NC_NOERR){printf("Error: %s\n", nc_strerror(e)); return 2;}
int netcdfTimeInitialized=0;
/*exprimé en heure*/
int netcdfT0data = 0;

/*
 *can difere because of bisextile years
 */
size_t netcdfNbDatesSafran=8760;
int netcdfTFdata =  netcdfNbDatesSafran- 1;
int netcdfTElapse = 24;
/*nb dates that must be export*/
double netcdfT0simulator = 0;
double netcdfTFsimulator = 1;
int netcdfTypeCalcul = 0;
int netcdfIdVar = 0;
char netcdfSimulatorVarName[NAME_SIZE];
char netcdfFile[NAME_SIZE];
char netcdfCovSubDirName[NAME_SIZE];
double netcdfValOffset = 0;
int netcdfEnablePlugin = 0;
char netcdfPluginName[NAME_SIZE];
//int* pRasterIndexSafran=NULL;
int* pInodeToSafranIndex=NULL; 
double *pDatas=NULL;

int __getSafranDatas(double *pData);
int __getSafranDataSize(size_t *sRes);
int readSafranDatas(char* safranFile,int idVarSafran){
	strcpy(netcdfFile,safranFile);
	netcdfIdVar=idVarSafran;
	if (access(safranFile,F_OK)){
		printf("mseSafranTools: readSafranDatas, cant find %s safran file\n",safranFile);
		perror("access");
		return 2;
	}
free(pDatas);
	pDatas=NULL;
	size_t sizeData;
	__getSafranDataSize(&sizeData);
#ifdef DEBUG_NETCDF_MSE
	printf("readSafranDatas: sizeData=%u\n",sizeData);
#endif
	pDatas = (double *) calloc(sizeData, sizeof(double));
	return __getSafranDatas(pDatas);
}
/*
 *unit time of simulator is not necessary the same than safran. 
 * the interval [T0netcdf,Tfnetcdf] is the safran intervel time that must be scaled on the simulator time interval
 * elapseHour time interval in safran hour
 * [T0simulator,TFsimulator] is the simulator time interval
 * safran data are extract at T0simulator+i*timeSimulatorElapse < TFsimulator and for TFsimulator
 * safran data will be write in binary file safran_T_i and a safran_T_time.txt wil be contains the dates:
 * cat  safran_T_time.txt
 * i T0simulator+i*timeSimulatorElapse
 */
int timeDataToTimeSimulator(int T0netcdf,int TFnetcdf,int elapseHour,double T0simulator,double TFsimulator){
	if (elapseHour<=0 || TFnetcdf<=T0netcdf){
		printf("mseSafranTolls, wrong usage because (elapseHour<=0 || TFnetcdf<=T0netcdf)");
		netcdfTimeInitialized=0;
		return 1;
	}
	if (netcdfNbDatesSafran<TFnetcdf){
		printf("mseSafranTolls, netcdfNbDatesSafran<TFnetcdf: %i<%i\n",netcdfNbDatesSafran,TFnetcdf);
		return 1;
	}

	netcdfT0simulator=T0simulator;
	netcdfTFsimulator=TFsimulator;
	
	netcdfT0data=T0netcdf;
	netcdfTFdata= TFnetcdf;
	netcdfTElapse=elapseHour;
	netcdfTimeInitialized=1;
#ifdef DEBUG_NETCDF_MSE
	printf("timeDataToTimeSimulator netcdfT0data= %i  netcdfTFdata=%i  netcdfTElapse=%i \n \t netcdfT0simulator=%e netcdfTFsimulator%e\n",
			netcdfT0data,  netcdfTFdata,  netcdfTElapse, netcdfT0simulator, netcdfTFsimulator);

#endif
	return 0;
}

/*
 * Out print info of content of the netcdf file
 * 
 */
int getSafranInfo (char* safranFile,int * nbRegTime,int nOne)
{	
	//first read the number of dates
	int ncid;
	int retval;
	int idVar=0;
	size_t lenp;
	retval = nc_open(safranFile, NC_NOWRITE, &ncid);
	int nvars;
	retval = nc_inq_varids(ncid, &nvars, NULL);
	ERR(retval);
	int pdimidsp[2];
	retval = nc_inq_vardimid(ncid, idVar, pdimidsp);
	retval = nc_inq_dimlen(ncid, pdimidsp[0], &lenp);
	*nbRegTime=lenp;
	retval = nc_close(ncid);
	printf("getSafranInfo: there are  %i times data \n",lenp/24);
	return retval;
}
int printInfo(char* safranFile)
{
	int ncid, pres_varid, temp_varid;
	int lat_varid, lon_varid;
	int retval;
	/* These program variables hold the latitudes and longitudes. */
	//   float lats[NLAT], lons[NLON];
	/* Loop indexes. */
	int lvl, lat, lon, rec, i = 0;
	int idVar = 0;

	char name[NC_MAX_NAME];
	retval = nc_open(safranFile, NC_NOWRITE, &ncid);
	if (retval) {
		printf("cant read file %s \n", safranFile);
		ERR(retval);
	} else {
		printf("file %s read\n", safranFile);
	}
	int nvars;
	retval = nc_inq_varids(ncid, &nvars, NULL);
	ERR(retval);
	printf("there are %i vars\n", nvars);
	int *pVarsId = (int *) calloc(sizeof(int), nvars);
	retval = nc_inq_varids(ncid, &nvars, pVarsId);
	ERR(retval);
	idVar = 0;
	int dim;
	nc_type aType;
	size_t lenp;
	int ii;
	int pdimidsp[2];
	for (idVar = 0; idVar < nvars; idVar++) {
		retval = nc_inq_varname(ncid, idVar, name);
		ERR(retval);
		retval = nc_inq_varndims(ncid, idVar, &dim);
		ERR(retval);
		retval = nc_inq_vardimid(ncid, idVar, pdimidsp);
		ERR(retval);

		retval = nc_inq_vartype(ncid, idVar, &aType);
		ERR(retval);
		printf("id=%i name=%s dim=%i type=%i ", idVar, name, dim, aType);

		for (ii = 0; ii < dim; ii++) {
			retval = nc_inq_dimlen(ncid, pdimidsp[ii], &lenp);
			ERR(retval);
			retval = nc_inq_dimname(ncid, pdimidsp[ii], name);

			printf("%s=%lu ", name, lenp);
		}
		printf("\n");
	}
	retval = nc_close(ncid);
	ERR(retval);
	size_t nDatas;
	strcpy(netcdfFile,safranFile);
	__getSafranDataSize(&nDatas);
	printf("file data size=%u \n",nDatas);
	return retval;
}

int __getSafranDataSize(size_t *sRes)
{
	int ncid;
	char name[NC_MAX_NAME];
	int retval, dim;
	int pdimidsp[2];
	size_t lenp2, lenp;
	retval = nc_open(netcdfFile, NC_NOWRITE, &ncid);
	if (retval) {
		printf(" __getSafranDataSize: cant read netcdf file %s \n",
			   netcdfFile);
		ERR(retval);
	} else
		printf(" __getSafranDataSize: netcdf file %s readed\n",
			   netcdfFile);
	retval = nc_inq_varname(ncid, netcdfIdVar, name);
	ERR(retval);
#ifdef DEBUG_NETCDF_MSE
	printf(" __getSafranDataSize: netcdf get dim %s id=%i\n", name,
		   netcdfIdVar);
#endif
	retval = nc_inq_varndims(ncid, netcdfIdVar, &dim);
	ERR(retval);
	if (dim != 2) {
		printf("netcdf, variable dim=%d, should be 2.\n", dim);
		return 1;
	}
#ifdef DEBUG_NETCDF_MSE
	printf(" __getSafranDataSize: nc_inq_vardimid\n");
#endif
	retval = nc_inq_vardimid(ncid, netcdfIdVar, pdimidsp);
	ERR(retval);
	lenp = 1;
#ifdef DEBUG_NETCDF_MSE
	printf(" __getSafranDataSize: nc_inq_dimlen\n");
#endif
	retval = nc_inq_dimlen(ncid, pdimidsp[0], &lenp);
	ERR(retval);
	retval = nc_inq_dimlen(ncid, pdimidsp[1], &lenp2);
	ERR(retval);
	lenp = lenp * lenp2;
	*sRes = lenp;
#ifdef DEBUG_NETCDF_MSE
	printf(" __getSafranDataSize: end\n");
#endif
	return 0;
}


int __getSafranDatas(double *pData)
{
	int ncid;
	char name[NC_MAX_NAME];
	int retval, dim;
	int pdimidsp[2];
	size_t lenp2, lenp;
	retval = nc_open(netcdfFile, NC_NOWRITE, &ncid);
	if (retval) {
		printf("__getSafranDatas: cant read netcdf file %s \n",
			   netcdfFile);
		ERR(retval);
		return 1;
	} else
		printf("__getSafranDatas: netcdf file %s readed\n", netcdfFile);
	/*retval =nc_inq_varname(ncid,netcdfIdVar,name) ;ERR(retval);
	   printf("netcdf exporting %s\n",name);
	   retval=nc_inq_varndims(ncid,netcdfIdVar,&dim);ERR(retval);
	   if (dim!=2){
	   printf("netcdf, variable dim=%d, should be 2.\n",dim);
	   return 1;
	   }
	   retval =nc_inq_vardimid    ( ncid, netcdfIdVar,pdimidsp);ERR(retval);
	   lenp=1;

	   retval=nc_inq_dimlen   (       ncid,       pdimidsp[0],        &lenp );ERR(retval);
	   retval=nc_inq_dimlen   (       ncid,       pdimidsp[2],        &lenp2 );ERR(retval);
	   lenp=lenp*lenp2; */

	printf("netcdf reading data\n");
	retval = nc_get_var_double(ncid, netcdfIdVar, pData);
	ERR(retval);
	printf("netcdf read data done\n");

	retval = nc_close(ncid);
	ERR(retval);
	return 0;
}

/*precalcul, export  lon lat from safran in view of projection user*/
/*pour convertir WGS4 en lambertIIe carto centre:
  installer gdal-bin
cat /opt/build/MSE/Tutorial/NETCDF/LonlatSafran.txt | gdaltransform -s_srs EPSG:4326 -t_srs EPSG:27572 | awk '{print $1" "$2}'> epsg27572Safran.txt  */
int netcdfExportLongLat(char * fileNetcdfName)
{
	int ncid, retval;
	size_t lenp;
	int idVar, dim;
	int pdimidsp[2];
	retval = nc_open(fileNetcdfName, NC_NOWRITE, &ncid);
	if (retval) {
		printf("cant read netcdf file %s \n", netcdfFile);
		ERR(retval);
		return 0;
	} else
		printf("netcdf file %s read\n", netcdfFile);
	idVar = 1;
	retval = nc_inq_varndims(ncid, idVar, &dim);
	ERR(retval);
	retval = nc_inq_vardimid(ncid, idVar, pdimidsp);
	ERR(retval);
	int ii = 0;
	retval = nc_inq_dimlen(ncid, pdimidsp[ii], &lenp);
	ERR(retval);
	double *pLat = (double *) calloc(lenp, sizeof(double));
	retval = nc_get_var_double(ncid, idVar, pLat);
	ERR(retval);
	double *pLon = (double *) calloc(lenp, sizeof(double));
	retval = nc_get_var_double(ncid, idVar + 1, pLon);
	ERR(retval);
	FILE *pLatLon = fopen("LonlatSafran.txt", "w");
	if (!pLatLon) {
		printf("cant read LonlatSafran.txt\n");
		return 1;
	}
	for (int ii = 0; ii < lenp; ii++) {
		fprintf(pLatLon, "%15e %15e\n", *(pLon + ii), *(pLat + ii));
	}
	fclose(pLatLon);
	retval = nc_close(ncid);
	ERR(retval);
	free(pLat);
	free(pLon);
	return 0;
}

/*read safran epsg27572Safran.txt, to build the raster of safran index
 write raster line major begining top,left.
 index de ligne : (YMAX-y)/DY
 index de colonne : (x-XMIN)/DX
The output file contains the index of the safran cell corresponding to the raster cell. The goal is to complet the raster safran.*/
int __buildRasterIndex(int *pRasterIndex_);
int netcdfPrecalRasterIndex()
{
	printf("_netcdfPrecalRasterIndex for precalcul only\n");
	int npts = (NX + 1) * (NY + 1);
	int *pRasterIndex_ = (int *) malloc(npts * sizeof(int));
	__buildRasterIndex(pRasterIndex_);
	free(pRasterIndex_);
	printf("_netcdfPrecalRasterIndex done, ready to import meshes\n");
	return 0;
}

int __buildRasterIndex(int *pRasterIndex_)
{
	FILE *fSafran, *fRasterIndex;
	int i, j, k;
	int nnx = NX + 1;
	int nny = NY + 1;
	int npts = nnx * nny;

	int *pRasterEnd = pRasterIndex_ + npts;
	int *pIaux = pRasterIndex_;
	double x, y;
	for (i = 0; i < npts; i++) {
		*pIaux++ = -1;
	}
	fSafran = fopen("../data/epsg27572Safran.txt", "r");
	if (!fSafran) {
		printf
			("__buildRasterIndex: cant open epsg27572Safran.txt file \n");
		return 1;
	}

	for (k = 0; k < NB_PT_SAFRAN; k++) {
		fscanf(fSafran, "%lf%lf", &x, &y);

		i = round((x - XMIN) / ((1 + 1e-3) * DX));
		j = round((YMAX - y) / ((1 + 1e-3) * DY));
		printf("%e %e %i %i %i\n", x, y, i, j, k);
		pRasterIndex_[i + j * nnx] = k;
	}
	fclose(fSafran);

	//on bouche les bord
	if (1) {
		int *pBeginMinus1;
		pIaux = pRasterIndex_;
		if (*pIaux == -1)
			pBeginMinus1 = pIaux;
		else {
			while (*pIaux != -1 && pIaux != pRasterEnd)
				pIaux++;
			pBeginMinus1 = pIaux;
		}
		while (pIaux != pRasterEnd) {
			while (*pIaux == -1 && pIaux != pRasterEnd)
				pIaux++;
			if (pIaux == pRasterEnd) {
				int v = *(pBeginMinus1 - 1);
				while (pBeginMinus1 < pRasterEnd)
					*pBeginMinus1++ = v;
				break;
			}
			while (pBeginMinus1 < pIaux)
				*pBeginMinus1++ = *pIaux;

			while (*pIaux != -1 && pIaux != pRasterEnd)
				pIaux++;
			if (pIaux != pRasterEnd)
				pBeginMinus1 = pIaux;
		}
	}
	fRasterIndex = fopen("../data/rasterIndexSafran.txt", "w");
	if (!fRasterIndex) {
		printf("cant open w rasterIndexSafran.txt \n");
		return 1;
	}
	pIaux = pRasterIndex_;
	for (i = 0; i < npts; i++)
		fprintf(fRasterIndex, "%i\n", *pIaux++);
	fclose(fRasterIndex);
	return 0;
}

/*out : pRasterIndex make the link beteween the index in the complete raster to the incomplet safran raster*/
int __readRasterIndexSafran(int *pRasterIndex_, int nbpts)
{
	char FILENAME[NAME_SIZE];
	sprintf(FILENAME, "../data/rasterIndexSafran.txt");
	FILE *fRasterIndex = fopen(FILENAME, "r");
	if (fRasterIndex == 0) {
		printf("cant find %s \n", FILENAME);
		return 1;
	}
	for (int i = 0; i < nbpts; i++)
		fscanf(fRasterIndex, "%i", pRasterIndex_++);
	fclose(fRasterIndex);
	return 0;
}

/*id=4 name=Tair dim=2 type=6 time=8760 Number_of_points=9892. Tair[indext,indexx]
 last dimension varying fastes: on peut donc se placer au debut des données pour t=t1: =pDatas+date*NB_PT_SAFRAN.*/
int finishSafran(){
	//free(pRasterIndexSafran); pRasterIndexSafran=NULL;
	free(pInodeToSafranIndex); pInodeToSafranIndex=NULL;
	free(pDatas);pDatas=NULL;
	return 0;
}
/*
 * msake the conversion of XY epsg coordinates (EPSG:27572) to indeces in srafran grid
 * in: double* pXYZ coordinates (note that Z is zero, because 2D geometry)
 * in: int nNodes numbers of Nodes, ie points*3 double in pXYZ
 *
 *
 */
int precomputeSafranIndexFromCoordinates(double *pXYZ,int nNodes)
{
	if (!pInodeToSafranIndex)
		pInodeToSafranIndex=(int *) malloc((nNodes) * sizeof(double));
	int *pAux = pInodeToSafranIndex;
	int nbptsRaster = (NX + 1) * (NY + 1);
//	if (!pRasterIndexSafran)
//		pRasterIndexSafran = (int *) malloc(nbptsRaster * sizeof(int));
//	printf("getNodesMeshIndexInSafranRaster %e %e %e\n",pXYZ[0],pXYZ[1],pXYZ[2]);
#ifdef DEBUG_NETCDF_MSE_PRE
	printf("getNodesMeshIndexInSafranRaster __readRasterIndexSafran\n");
#endif
//	if (__readRasterIndexSafran(pRasterIndexSafran, nbptsRaster))
//		return 1;
	printf("getNodesMeshIndexInSafranRaster %e %e %e\n",pXYZ[0],pXYZ[1],pXYZ[2]);
	for ( int i = 0; i < nNodes; i++) {
		double bufT;
		double x = pXYZ[3*i];
		double y = pXYZ[3*i+1];
#ifdef DEBUG_NETCDF_MSE_PRE
		printf("i= %i x=%e,y=%e",i,x,y);
#endif
		int icol = round((x - XMIN) / ((1 + 1e-3) * DX));
		int irow = round((YMAX - y) / ((1 + 1e-3) * DY));
		int index = icol + (irow) * (NX + 1);
		if (index >= nbptsRaster) {
			printf ("WARNING i=%i in getNodesIndexInSafranRaster , spatial(x=%e,y=%e) index out of raster=%i <%i\n",i,
				 x,y,index, nbptsRaster);
			index = nbptsRaster - 1;
		}
#ifdef DEBUG_NETCDF_MSE_PRE
		printf("index safran %i \n",index);
#endif
		*pAux++ = pRasterIndex[index];
	}
	return 0;
}

int __netcdfExportAveraged(int fromDateNetcdf,int toDateNetcdf, int* pInodeToSafranIndex,
					   int nNodes, double *pValMesh)
{
	//initialize mesh values
	for (long int i = 0; i < nNodes; i++) {
		pValMesh[i] = 0;
	}
	//add in mesh values
	for (int netdate=fromDateNetcdf; netdate<=toDateNetcdf;netdate++){
		double *pAux = pDatas + netdate * NB_PT_SAFRAN;
		int *pIndexNodesInSafranLocal = pInodeToSafranIndex;
		double *pValMeshLocal=pValMesh;
		for (long int i = 0; i < nNodes; i++) {
			double buf = pAux[*pIndexNodesInSafranLocal] + netcdfValOffset;
			*pValMeshLocal += buf;
			pValMeshLocal++;
			pIndexNodesInSafranLocal++;
		}
	}
	//averaged mesh value
	for ( int i = 0; i < nNodes; i++) {
		*pValMesh++ /= (toDateNetcdf-fromDateNetcdf+1);
	}
	return 0;
}
/*compute pValMesh(x,y)=  max_{t\in beginHourlySafran endHourlySafran }(dataSafran(t,x,y))
 */
int __netcdfExportMax(int fromDateNetcdf,int toDateNetcdf,int * pInodeToSafranIndex,
					   int nNodes, double *pValMesh)
{
	//add in mesh values
	for (int netdate=fromDateNetcdf; netdate<=toDateNetcdf;netdate++){
		double *pAux = pDatas + netdate * NB_PT_SAFRAN;
		int *pIndexNodesInSafranLocal = pInodeToSafranIndex;
		double *pValMeshLocal=pValMesh;
		for (long int i = 0; i < nNodes; i++) {
			double buf = pAux[*pIndexNodesInSafranLocal] + netcdfValOffset;
			if (netdate == fromDateNetcdf  || buf > *pValMeshLocal)
				*pValMeshLocal = buf;
			pValMeshLocal++;
			pIndexNodesInSafranLocal++;
		}
	}
	return 0;
}

/*//il faut maintetnant ecrire le champs correspondant aux valeur pAux + indexMesh et le sauvegarder au format binaire compatible FF indexe par le temp et le nom de la variable
 */
int __netcdfExportBrut(int fromDate, int toDatenDate, int *pInodeToSafranIndex, int nNodes, double *pValMesh)
{
	double *pAux = pDatas + fromDate * NB_PT_SAFRAN;
	for (long int i = 0; i < nNodes; i++) {
		*pValMesh++ = pAux[*pInodeToSafranIndex++] + netcdfValOffset;
	}
	return 0;

}


#include <dlfcn.h>
typedef void (*performPlugin)(double *, int);
performPlugin aperformPlugin;
int __netcdfExportBody(double *XY,int nXY)
{
	//first read the number of dates
	int ncid;
	int retval;
	int idVar=0;
	size_t lenp;
	retval = nc_open(netcdfFile, NC_NOWRITE, &ncid);
	int nvars;
	retval = nc_inq_varids(ncid, &nvars, NULL);
	ERR(retval);
	int pdimidsp[2];
	if (!pDatas){
		printf("Nothing done, read safran data first\n");
		return 1;
	}

	retval = nc_inq_vardimid(ncid, idVar, pdimidsp);
	retval = nc_inq_dimlen(ncid, pdimidsp[0], &lenp);
	netcdfNbDatesSafran=lenp;
	//netcdfTFdata =  netcdfNbDatesSafran- 1;
	retval = nc_close(ncid);
	printf("__netcdfExportBody: read %i times data ie %i days\n",netcdfNbDatesSafran,lenp/24);
	int nnx = NX + 1;
	int nbptsRaster = (NX + 1) * (NY + 1);
	char fileName[NAME_SIZE];
#ifdef DEBUG_NETCDF_MSE
	printf("__netcdfExportBody: __readRasterIndexSafran\n");
#endif

	long int nNodes = nXY;
	precomputeSafranIndexFromCoordinates(XY,nXY);
#ifdef DEBUG_NETCDF_MSE
	printf("__netcdfExportBody: getNodesMeshIndexInSafranRaster done\n");
#endif
	double *pValMesh = (double *) malloc(nNodes * sizeof(double));
#ifdef DEBUG_NETCDF_MSE
	printf("__netcdfExportBody: readXY done\n");
#endif
	/*Assume pX,pY is EPSG:21572 */
	int fromDateNetcdf=netcdfT0data;
	int toDateNetcdf=netcdfT0data+netcdfTElapse;
	int numDate=0;
	
	sprintf(fileName,"%s/time%s.txt",netcdfCovSubDirName,netcdfSimulatorVarName); 
	FILE* fichier = fopen(fileName,"r");
	if (fichier){
	 	char buffer[4*NAME_SIZE];
	    	while (fgets(buffer, 4*NAME_SIZE, fichier) != NULL) {
		    sscanf(buffer,"%i",&numDate);
	    	}
	    	fclose(fichier);
	}
	if (numDate){
		printf("WARNING: CAT DATA SAFRAN  mseSafran find numDate=%i\n",numDate);
		numDate++;
	}
	while(toDateNetcdf<=netcdfTFdata){
		switch (netcdfTypeCalcul) {
		case MSE_CST_NETCDF_TYPE_BRUT:
			//build binary file containing the field value of the time cov
			__netcdfExportBrut(fromDateNetcdf,toDateNetcdf, pInodeToSafranIndex,
					   nNodes, pValMesh);
			break;
		case MSE_CST_NETCDF_TYPE_AVERAGED:
			//build binary file containing the field value of the time cov
			//because we compute the average on the time interval numDate-1,numDate.
			__netcdfExportAveraged(fromDateNetcdf,toDateNetcdf, pInodeToSafranIndex,
					   nNodes, pValMesh);
			break;
		case MSE_CST_NETCDF_TYPE_MAX:
			//build binary file containing the field value of the time cov
			//because we compute the average on the time interval numDate-1,numDate.
			__netcdfExportMax(fromDateNetcdf,toDateNetcdf, pInodeToSafranIndex,
					   nNodes, pValMesh);
			break;
		default:
			printf("__netcdfExportBody %i not implemented\n",
				   netcdfTypeCalcul);
		}
		if (netcdfEnablePlugin) {
			char *plugin_name;
			void *plugin;
			plugin = dlopen(netcdfPluginName, RTLD_NOW);
			if (!plugin) {
				printf("Cannot load %s: %s\n", netcdfPluginName,
					   dlerror());
			} else {
				printf("load %s done\n", netcdfPluginName);

				aperformPlugin = (performPlugin) dlsym(plugin, "perform");
				char *result = dlerror();
				if (result) {
					printf("Cannot find query in %s: %s\n", "perform",
						   result);
				}
				printf("find plugin %s \n", "perform");
				(*aperformPlugin) (pValMesh, nNodes);
			}

		}
		saveMyTimeVect( netcdfCovSubDirName, 
		   netcdfSimulatorVarName, pValMesh, nNodes,numDate,
		   netcdfT0simulator+(netcdfTFsimulator-netcdfT0simulator)*(((double) (fromDateNetcdf-netcdfT0data))/((double)(netcdfTFdata-netcdfT0data))));
#ifdef DEBUG_NETCDF_MSE
		printf("netcdfT0simulator =%e netcdfTFsimulator=%e \n  netcdfT0data=%i netcdfTFdata=%i \n tsinc = %e\n",
				netcdfT0simulator,netcdfTFsimulator,
				netcdfT0data,netcdfTFdata,
				(netcdfTFsimulator-netcdfT0simulator)*(((double) (fromDateNetcdf-netcdfT0data))/((double)(netcdfTFdata-netcdfT0data))));

#endif
 		fromDateNetcdf=toDateNetcdf;
		toDateNetcdf=toDateNetcdf+netcdfTElapse;
		numDate++;
	}
	free(pValMesh);
	return 0;
}




int perform(double *XYZ,int nXYZ,char* lMseCovName,int interpolType,char* mseCovDir){
	if (!netcdfTimeInitialized){
		printf("mseSafranTools: perform can not be run because time maps is not initialised, please call timeDataToTimeSimulator\n");
		return 1;
	}
	if (access(mseCovDir, X_OK)){
		if (mkdir(mseCovDir,0755) == -1) {
			printf("mseSafranTools: perform, cant find and make %s dir\n",mseCovDir);
			return 3;
		}
	}
	netcdfTypeCalcul=interpolType;
	strcpy(netcdfSimulatorVarName,lMseCovName);
	strcpy(netcdfCovSubDirName,mseCovDir);
	__netcdfExportBody(XYZ,nXYZ/3);
	return 0;
}

