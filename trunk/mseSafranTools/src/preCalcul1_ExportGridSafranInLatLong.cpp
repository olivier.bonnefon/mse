#include "stdio.h"
#include "stdlib.h"
#include "mseSafranTools.hpp"

int main(){
	netcdfExportLongLat("../data/SAFRAN_2003080107_2004080106.nc");
	printf("nest step: run cat ../data/LonlatSafran.txt | gdaltransform -s_srs EPSG:4326 -t_srs EPSG:27572 | awk '{print $1\" \"$2}'> epsg27572Safran.txt\n");
	return 0;
}
