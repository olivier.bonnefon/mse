#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "mseSafranTools.hpp"
/*Number of nodes 8228*/
#define NDOUBLES 3*8228
int main(){
	double *pXYZ=(double*)malloc(NDOUBLES*sizeof(double));
	FILE *pF=fopen("../data/xyz.bin","rb");
	/*number of double inside file*/
	fseek(pF,0,SEEK_END);
	long file_size=ftell(pF);
	rewind(pF);
	size_t num_elem=file_size/sizeof(double);

	printf("there are %u doubles, should be %u\n",num_elem,NDOUBLES);
	if (pF){
		fread(pXYZ,sizeof(double),num_elem,pF);
		printf("File read \n %e %e %e\n",*pXYZ,*(pXYZ+1),*(pXYZ+2));
	}else{
		printf("cant read file\n");
	}
	printf("compute indeces in safran grid\n");
	int nRegHours;
	readSafranDatas("../data/SAFRAN_2003080107_2004080106.nc",4);
	getSafranInfo("../data/SAFRAN_2003080107_2004080106.nc",&nRegHours,1);
	timeDataToTimeSimulator(0,nRegHours,24,0,nRegHours/24);
	perform(pXYZ,num_elem,"T",0,"../data/COV/");
	finishSafran();
	free(pXYZ);
	return 0;
}
