#ifndef MSESAFRANMSE_H
#define MSESAFRANMSE_H
enum SAFRAN_MSE_INTERPOL_TYPE{
MSE_CST_NETCDF_TYPE_BRUT=0,
MSE_CST_NETCDF_TYPE_AVERAGED=1,
MSE_CST_NETCDF_TYPE_MAX=2
};
enum SAFRAN_ID{
SAFRAN_TEMPERATURE=4
};
#ifdef __cplusplus
extern "C" {
#endif
/*
 * getSafranInfo:
 * in : safranFile
 * out : number of time registred (1 per hour) 
 * in: nOne because of python interface
 *
 */
int getSafranInfo(char* safranFile, int* nbRegTime,int nOne);
/*
 *timeDataToTimeSimulator:
 * in [T0netcdf, TFnetcdf]: interval hours used to export safran data, must be included in [0, nbRegTime]
 * in elapseHour : period for export data safran
 * in [T0simulator,TFsimulator]: corresponding simulator time 
 *
 * this function must be call bfore perform
 */
int timeDataToTimeSimulator(int T0netcdf,int TFnetcdf,int elapseHour,double T0simulator,double TFsimulator);
/*
 * used by mse
 * open safran file and read datas
 * - safranFile is the safran-netcdf file path
 * - idVarSafran: 4 for T.
*/
int readSafranDatas(char* safranFile,int idVarSafran);
/*
 * used by mse
 * export binary safran at given coordinates
 * - XY array of coordinates
 * - mseCovName suffix for files outputs
 * - interpoleType a SAFRAN_MSE_INTERPOL_TYPE
 * - mseCovDir directory to write outputs files
 *
 */
int perform(double *XYZ,int nXYZ,char* mseCovName,int interpolType,char* mseCovDir);
int finishSafran();
/*Folowing finction are usefull for precompute :
 * - global, it constsis on building the grid safran in epsg:27572
 * - per mesh, coordinates on safran grid, 
 *
 *not directly used in mse
 *
 */
int printInfo(char* safranFileName);
int netcdfExportLongLat(char * fileNetcdfName);
int netcdfPrecalRasterIndex();
/*
 * msake the conversion of XY epsg coordinates (EPSG:27572) to indeces in srafran grid
 * in: double* XYZ coordinates(note that Z is zero, because 2D geometry)
 * in: int nXYZ numbers of doubles, ie points*3
 *
 * memory is store inside library
 */
int precomputeSafranIndexFromCoordinates(double *XYZ,int nXYZ);
#ifdef __cplusplus
}
#endif
#endif
