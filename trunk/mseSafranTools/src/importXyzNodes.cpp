#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "mseSafranTools.hpp"
/*Number of nodes 8228*/
#define NBNODES 8228
int main(){
	double *pXYZ=(double*)malloc(NBNODES*3*sizeof(double));
	FILE *pF=fopen("../data/xyz.bin","rb");
	/*number of double inside file*/
	fseek(pF,0,SEEK_END);
	long file_size=ftell(pF);
	rewind(pF);
	size_t num_elem=file_size/sizeof(double);

	printf("there are %u doubles, should be %u\n",num_elem,3*NBNODES);
	if (pF){
		fread(pXYZ,sizeof(double),num_elem,pF);
		printf("File read \n %e %e %e\n",*pXYZ,*(pXYZ+1),*(pXYZ+2));
	}else{
		printf("cant read file\n");
	}
	printf("compute indeces in safran grid\n");
	precomputeSafranIndexFromCoordinates(pXYZ,NBNODES);
	finishSafran();
	free(pXYZ);
	return 0;
}
