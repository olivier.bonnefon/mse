# - Try to find numpy
# Once done this will define
#
#  Numpy_FOUND        - system has numpy
#  Numpy_INCLUDE_DIRS  - include directories for numpy
#  Numpy_VERSION      - version of numpy
#  Numpy_VERSION_MAJOR - first number in Numpy_VERSION
#  Numpy_VERSION_MINOR - second number in Numpy_VERSION

# Based on FindNumPy.cmake

#=============================================================================
# Copyright (C) 2013 Lawrence Mitchell
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#=============================================================================

message(STATUS "Checking for package 'Numpy'")

if(Numpy_INCLUDE_DIRS)
  # In cache already
  set(Numpy_FIND_QUIETLY TRUE)
endif(Numpy_INCLUDE_DIRS)


execute_process(
  COMMAND ${Python3_EXECUTABLE} -c "import numpy; print(numpy.get_include())"
  OUTPUT_VARIABLE Numpy_INCLUDE_DIRS
  RESULT_VARIABLE Numpy_NOT_FOUND
  OUTPUT_STRIP_TRAILING_WHITESPACE
  )

if(Numpy_INCLUDE_DIRS)
  set(Numpy_FOUND TRUE)
  set(Numpy_INCLUDE_DIRS ${Numpy_INCLUDE_DIRS} CACHE STRING "numpy include path")
else(Numpy_INCLUDE_DIRS)
  message(STATUS "haha= ${Numpy_INCLUDE_DIRS} ${Numpy_NOT_FOUND}")
  set(Numpy_FOUND FALSE)
endif(Numpy_INCLUDE_DIRS)

if(Numpy_FOUND)
  execute_process(
    COMMAND ${Python3_EXECUTABLE} -c "import numpy; print(numpy.__version__)"
    OUTPUT_VARIABLE Numpy_VERSION
    RESULT_VARIABLE Numpy_NOT_FOUND
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
  string(REPLACE "." ";" Numpy_VERSION_LIST ${Numpy_VERSION})
  list(GET Numpy_VERSION_LIST 0 Numpy_VERSION_MAJOR)
  list(GET Numpy_VERSION_LIST 1 Numpy_VERSION_MINOR)
  if(NOT Numpy_FIND_QUIETLY)
    message(STATUS "numpy version ${Numpy_VERSION} found")
  endif(NOT Numpy_FIND_QUIETLY)
else(Numpy_FOUND)
  if(Numpy_FIND_REQUIRED)
    message(FATAL_ERROR "numpy missing")
  endif(Numpy_FIND_REQUIRED)
endif(Numpy_FOUND)

mark_as_advanced(Numpy_INCLUDE_DIRS, Numpy_VERSION, Numpy_VERSION_MAJOR, Numpy_VERSION_MINOR)
#
#if (Numpy_FIND_VERSION)
#  # Check if version found is >= required version
#  if (NOT "${Numpy_VERSION}" VERSION_LESS "${Numpy_FIND_VERSION}")
#    set(Numpy_VERSION_OK TRUE)
#  endif()
#else()
#  # No specific version requested
#  set(Numpy_VERSION_OK TRUE)
#endif()
#mark_as_advanced(Numpy_VERSION_OK)
#
## Standard package handling
#include(FindPackageHandleStandardArgs)
#find_package_handle_standard_args(PETSc4py
#  "PETSc4py could not be found. Be sure to set PYTHONPATH appropriately."
#  Numpy_INCLUDE_DIRS Numpy_VERSION Numpy_VERSION_OK)
#
