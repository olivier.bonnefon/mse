""" Example of wrapping a C function that takes C double arrays as input using
    the Numpy declarations from Cython """

# cimport the Cython declarations for numpy
cimport numpy as np

# if you want to use the Numpy-C-API from Cython
# (not strictly necessary for this example, but good practice)
np.import_array()

# cdefine the signature of our c function
cdef extern from "buildKMatc.h":
    void _computeK(double * in_array2D, int size2d, double * out_array2D)

cdef extern from "buildKMatc.h":
    void _computeKU(double * K, int size, double * U,double * KU)

# create the wrapper code, with numpy type annotations
def buildK(np.ndarray[double, ndim=2, mode="c"] in2d_array not None,
            np.ndarray[double, ndim=2, mode="c"] out2d_array not None):
    _computeK(<double*> np.PyArray_DATA(in2d_array),
                in2d_array.shape[0],
                <double*> np.PyArray_DATA(out2d_array))

# create the wrapper code, with numpy type annotations
def KU(np.ndarray[double, ndim=2, mode="c"] K not None,
            np.ndarray[double, ndim=1, mode="c"] U not None,
            np.ndarray[double, ndim=1, mode="c"] KU not None,):
    _computeKU(<double*> np.PyArray_DATA(K),
                K.shape[0],
                <double*> np.PyArray_DATA(U),
                <double*> np.PyArray_DATA(KU))