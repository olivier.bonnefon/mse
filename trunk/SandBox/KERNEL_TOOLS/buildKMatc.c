#include "stdio.h"
#include "math.h"

#define DEBUG
void _computeK(double * in_array2D, int size2d, double * out_array2D){
    int i2d,j2d;
#ifdef DEBUG
    printf("call buildKMatc, size=%i\n",size2d);
#endif
    for (i2d=0;i2d<size2d;i2d++){
        double xi=in_array2D[3*i2d];
        double yi=in_array2D[3*i2d+1];
        //printf("call buildMapc, i2d=%i %e %e \n",i2d,xi,yi);
        for (j2d=0;j2d<size2d;j2d++){
            double dx=in_array2D[3*j2d]-xi;
            double dy=in_array2D[3*j2d+1]-yi;
            //printf("call buildMapc, j2d=%i %e %e \n",j2d,dx,dy);
            out_array2D[i2d+size2d*j2d]=1/pow(1+sqrt(dx*dx+dy*dy)/5000,3);
        }
    }
}

void _computeKU(double * K, int size, double * U,double * KU){
    int i,j;
    for (i=0;i<size;i++){
        *KU=0.0;
        for (j=0;j<size;j++){
            *KU+=(*K++)*U[j];
        }
        KU++;
    }
}