from dolfinx.fem import FunctionSpace,Function
from dolfinx.fem import form,assemble_scalar,Constant,form,Expression
from dolfinx import io
from dolfinx.mesh import create_mesh
from dolfinx.io import VTKFile,XDMFFile
from mpi4py import MPI
import ufl
import numpy as np
import math
from ufl import grad,dot
from petsc4py.PETSc import ScalarType
from dolfinx.fem.petsc import assemble_matrix, assemble_vector, apply_lifting, create_vector, set_bc


from petsc4py import PETSc

meshFile="./MESHES/SIMPLE2/AllEdges.xdmf"
with XDMFFile(MPI.COMM_WORLD,meshFile,"r") as xdmf:
        mesh = xdmf.read_mesh(name="Grid")

V = FunctionSpace(mesh, ("P", 1))
print(V.tabulate_dof_coordinates(), mesh.geometry.dim)
print(mesh.topology.index_map(mesh.topology.dim).size_local)

dx=ufl.Measure('dx',mesh)
cst=form(1.0*dx)
assemble_scalar(cst)


u = ufl.TrialFunction(V)
v = ufl.TestFunction(V)
u_n = Function(V)
f_ = Function(V)

def f_fct(x):
    nElem=x.shape[1]
    values = np.zeros((1, nElem))
    for i in range(nElem):
        if (x[1,i]<0.21 and x[0,i]<0.21):
            values[0][i]=0.2
    return values

f_.interpolate(f_fct)




x = ufl.SpatialCoordinate(mesh)
p=ufl.exp(-5.0*(x[0]**2 + x[1]**2))
expr = Expression(p, V.element.interpolation_points)
u_n.interpolate(expr)
u_n.x.set(0)
mass=assemble_scalar(form(u_n*dx))
print("mass ="+str(mass))
dt=0.1
t=0.0
T=50
alpha_=Constant(mesh, ScalarType(1/dt))
D1d_=Constant(mesh,ScalarType(0.1))
a=alpha_*u*v*dx + D1d_*dot(grad(u),grad(v))*dx
L=alpha_*u_n*v*dx + f_*v*dx-f_*u_n*v*dx
aa=form(a)
A=assemble_matrix(aa)
LL=form(L)
b = assemble_vector(LL)
A.assemble()
A.view()


    
assemble_vector(b, LL)
u_ = Function(V)

vtkf = VTKFile(mesh.comm, "diff1D.pvd", "w")
vtkf.write_function(u_n, t)
solver = PETSc.KSP().create(mesh.comm)
solver.setOperators(A)
solver.setType(PETSc.KSP.Type.PREONLY)
solver.getPC().setType(PETSc.PC.Type.LU)

while(t<T):
    with b.localForm() as loc_b:
        loc_b.set(0)
    assemble_vector(b, LL)
    solver.solve(b, u_.vector)
    u_n.x.array[:] = u_.x.array[:]
    t=t+dt
    vtkf.write_function(u_n, t)
    mass=assemble_scalar(form(u_*dx))
    print("mass ="+str(mass))
        
vtkf.close()