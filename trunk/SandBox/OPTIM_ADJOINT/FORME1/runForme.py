import kppForme as model
import numpy as np
import os

os.system("rm SAVEUp1/* SAVE/* RESULTS/* REF/*")
model.pref=0.4
model.buildRef()
message=""
for pRaduis in [0.2,0.3,0.5,0.6]:
    da=0.1
    res1=model.computeJForMIN(pRaduis)
    gAdjoint=model.dadjoint(pRaduis)
    message=message+str(pRaduis)+" gAdjoint="+str(gAdjoint)+"\n"
    for cmp in range(3):
        res2=model.computeJForMIN(pRaduis+da)
        message=message+str(da)+" dif finie d_aV("+str(pRaduis)+")="+str((res2-res1)/da)+"\n"
        da=da*0.5
        print(message)

print(message)
 
'''
résultat  de python3 runForme.py
J=0.1298163662055798
0.2 gAdjoint=[-1.30093494]
0.1 dif finie d_aV(0.2)=-1.3523635440423278
0.05 dif finie d_aV(0.2)=-1.4452480898198106
0.025 dif finie d_aV(0.2)=-1.4046735714042802
0.3 gAdjoint=[-1.11732323]
0.1 dif finie d_aV(0.3)=-0.5626447588411551
0.05 dif finie d_aV(0.3)=-0.8556156247354396
0.025 dif finie d_aV(0.3)=-1.0407052075857013
0.5 gAdjoint=[0.68514962]
0.1 dif finie d_aV(0.5)=0.7282243900807748
0.05 dif finie d_aV(0.5)=0.7051399057590069
0.025 dif finie d_aV(0.5)=0.6874444572909935
0.6 gAdjoint=[0.69304759]
0.1 dif finie d_aV(0.6)=0.5199059259483214
0.05 dif finie d_aV(0.6)=0.621636551866517
0.025 dif finie d_aV(0.6)=0.6566636998626024

YES

'''
