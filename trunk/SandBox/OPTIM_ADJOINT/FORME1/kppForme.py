from toolsEnv import computerEnv,MSEPrint
import MESH_IMPORT_GMSH.read as reader
import MESH_IMPORT_GMSH.buildMeshes as buildMeshes
from dolfinx.io import XDMFFile,VTKFile
from ufl import Measure,TrialFunction,TestFunction, grad, inner,SpatialCoordinate
from dolfinx import mesh
from dolfinx.fem import Function,FunctionSpace, assemble_scalar,form
from mpi4py import MPI
import numpy as np
import gmsh
import meshio
import sys
import numpy as np
import os.path
import subprocess
import MAP_TOOLS.buildMapc as mc
from petsc4py import PETSc
from dolfinx.fem.petsc import assemble_matrix, assemble_vector
from numpy.linalg import norm

def save_step(u,REP,SUFFIX,step):
   np.save(REP+'/state'+SUFFIX+str(step),u.x.array)

def read_step(u,REP,SUFFIX,step):
    u.x.array[:] = np.load(REP+'/state'+SUFFIX+str(step)+'.npy')



T0=0
TF=10
dt=0.5
nTi=round(TF/dt)

d2D=0.05
#rectangular geometry
xmin=0.0
ymin=0.0
xmax=1.5
ymax=1.5
#center
xcenter=0.5*(xmin+xmax)
ycenter=0.5*(ymin+ymax)




msh = mesh.create_rectangle(comm=MPI.COMM_WORLD,
                            points=((xmin, ymin), (xmax, ymax)), n=(100, 100),
                            cell_type=mesh.CellType.triangle)

vSpace = FunctionSpace(msh, ("CG", 1))
        
nbDofs=vSpace.tabulate_dof_coordinates().shape[0]
dx=Measure('dx',msh)
XYZ=vSpace.tabulate_dof_coordinates().copy()
u = TrialFunction(vSpace)
v = Function(vSpace)
vprim = Function(vSpace)
utm1 = Function(vSpace)
uk = Function(vSpace)
w = TestFunction(vSpace)
x = SpatialCoordinate(msh)
sol=PETSc.Vec().createSeq(nbDofs)
prevSol=PETSc.Vec().createSeq(nbDofs)
prevSol.array.fill(0.0)
uref=Function(vSpace)
buf=Function(vSpace)

pa = TrialFunction(vSpace)
p_tp1=Function(vSpace)
u_a=Function(vSpace)

criteronNR=1e-3
maxIt=10


import dolfinx.geometry as dg
tree= dg.BoundingBoxTree(msh,2)

#radius=0.2
nCirclePoints=12*2
points=np.zeros(nCirclePoints*3).reshape((nCirclePoints,3))
mesh_size=0.02

cells=[]
def computeCellsForDist(lradius):
   global cells,points
   cells=[]
   angle=0
   angleStep=2*np.pi/(1.0*nCirclePoints)
   #add points for the circle
   for i in range(nCirclePoints):
      points[i][0]=xcenter+lradius*np.cos(angle)
      points[i][1]=ycenter+lradius*np.sin(angle)
      angle+=angleStep
      x0=[points[i][0],points[i][1],0.0]
      cell_candidates =dg.compute_collisions(tree, x0)
      colliding_cells = dg.compute_colliding_cells(msh,cell_candidates,x0)
      cells.append(colliding_cells[0])


class solver:
    def __init__(self) -> None:
        self.KSP=PETSc.KSP(MPI.COMM_WORLD).create()
    def setMat(self,aMat):
        self.KSP.setOperators(aMat)
        self.KSP.setType(PETSc.KSP.Type.PREONLY)
        self.KSP.getPC().setType(PETSc.PC.Type.LU)
    def solve(self,rhs,sol):
        self.KSP.solve(rhs,sol)
    def solveNoNeg(self,rhs,sol):
        self.solve(rhs,sol)
        sol.array[sol.array < 0.] = 0.

aSolver=solver()

def doANonLinStep(a,L,sol):
    error=criteronNR+1
    numIt=0
    uk.x.array[:]=utm1.x.array[:]
    while(error>criteronNR and numIt<maxIt):
        M=assemble_matrix(form(a))
        M.assemble()
        rhs=assemble_vector(form(L))
        aSolver.setMat(M)
        aSolver.solveNoNeg(rhs,sol)
        error=norm(sol.array - prevSol.array)
        prevSol.array[:]=sol.array[:]
        numIt+=1
        print("doANonLinStep "+str(numIt)+" "+str(error))
        uk.x.array[:]=sol.array[:]
    if (error > 10*criteronNR):
        print("Newton failed "+str(error))




def computeJ():
    print("begin", sys._getframe().f_code.co_name)
    J=0.0
    step=0
    current_time=T0;
    final_time=TF;
    while (current_time < final_time):
        current_time = current_time + dt
        step=step+1
        if (step==nTi):
            read_step(buf,"SAVE","u",step)
            read_step(uref,"REF","u",step)
            computev(buf,uref,step)
            J=J+dt*assemble_scalar(form(v*dx))
            print("J="+str(J))
    return J



def computeJForMIN(p1):
    f=open("trace.txt","a")
    f.write(str((pref - p1)*(pref - p1)))
    f.write(" ")
    f.close()
    computeCellsForDist(p1)
    simul(p1)
    os.system("cp SAVE/*u* SAVEUp")
    return computeJ()

def computeJac(p1):
    g=dadjoint(p1)
    f=open("trace.txt","a")
    f.write(str(g.dot(g)))
    f.write("\n")
    f.close()
    return g

def buildRMask(lradius,x):
    if (np.square(x[0]-xcenter)+np.square(x[1]-ycenter) < lradius*lradius):
        return 1;
    else:
        return 0

vbuildRMask=np.vectorize(buildRMask,signature="(),(d)->()")


x = None
ic="np.exp(-16.0*(np.square(x[0]-0.5*xmax)+np.square(x[1]-0.5*ymax)))"
def initial_condition(x):
    return eval(ic)

def simul(p):
    global lastStep,R
    print("simul ",p)
    R=Function(vSpace)
    R.x.array[:]=vbuildRMask(p,XYZ)
    A = u*w*dx+ dt*d2D*inner(grad(u),grad(w))*dx- dt*R*(1-2*uk)*u*w*dx
    L = utm1*w*dx + dt*R*uk*uk*w*dx
    step=0
    current_time=T0;
    final_time=TF;
    utm1.interpolate(initial_condition)
    vtkfile_u = VTKFile(msh.comm,'RESULTS/solution_u.pvd',"w")
    vtkfile_u.write_function(utm1, step)
    save_step(utm1,"SAVE","u",step)
    while (current_time < final_time):
        doANonLinStep(A,L,sol)
        utm1.x.array[:]=sol.array[:]
        current_time = current_time + dt
        step=step+1
        vtkfile_u.write_function(utm1, step)
        save_step(utm1,"SAVE","u",step)
    lastStep=step
    vtkfile_u.close()


def solveAdjoint(FA,LA,solAdjoint):
    M=assemble_matrix(form(FA))
    M.assemble()
    rhs=assemble_vector(form(LA))
    aSolver.setMat(M)
    aSolver.solve(rhs,solAdjoint)

def computev(buf,uref,step):
    if (step==nTi):
       v.x.array[:]=0.5*np.square(buf.x.array[:]-uref.x.array[:])/dt
    else:
       v.x.array.fill(0.0)

def computevprim(buf,uref,step):
    if (step==nTi):
        vprim.x.array[:]=(buf.x.array[:]-uref.x.array[:])/dt
    else:
        vprim.x.array.fill(0.0)

def dadjoint(p):
    print("begin", sys._getframe().f_code.co_name)
    g=0
    FA=pa*w*dx +  dt*d2D*inner(grad(pa),grad(w))*dx-dt*R*pa*w*dx +2*dt*u_a*R*pa*w*dx
    LA= dt*vprim*w*dx + p_tp1*w*dx
    #LA= p_tp1*w*dx
#    LA= dt*uMask*(up1-uref)*w*dx + p_tp1*w*dx
    step=nTi
    current_time=TF;
    final_time=T0;
    p_tp1.x.array.fill(0.0)
    read_step(u_a,"SAVEUp","u",step)
    read_step(uref,"REF","u",step)
    computevprim(u_a,uref,step)
    dJ=0
    while (current_time > 0):
        solveAdjoint(FA,LA,sol)
        p_tp1.x.array[:]=sol.array[:]
        current_time=current_time-dt
        step=step-1
        bufPtp1=0.0
        bufUa=0.0
        bufG=0.0
        for i in range(nCirclePoints):
           x0=[points[i][0],points[i][1],0.0]
           bufPtp1=p_tp1.eval(x0,cells[i])
           bufUa=u_a.eval(x0,cells[i])
           bufG=bufG+bufUa*(1-bufUa)*bufPtp1
        g=g+dt*bufG*(2.0*np.pi*p/nCirclePoints)
        #for i in range(nbDofs1DPerComp):
        #   p1d.x.array[i]=p_tp1.x.array[index1DTo2D[i]]
        #   buf1d.x.array[i]=u_a.x.array[index1DTo2D[i]]
        #g=g+dt*assemble_scalar(form(buf1d*(1-buf1d)*p1d*dx1d))
        read_step(u_a,"SAVEUp","u",step)
        read_step(uref,"REF","u",step)
        computevprim(u_a,uref,step)
    return g


def buildRef():
    print("buildRef: we are building the ref (ie data) state for p=",pref)
    simul(pref)
    os.system("cp SAVE/* REF/")
    os.system("cp RESULTS/* REF/")

