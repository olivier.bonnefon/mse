Cas à 1 paramètre: un rayon.
 On s'intéresse au système (\ref{modReac100}) où $a$ est un réel représentant un rayon du disque, $D_a$, dans lequel $R$ prend la valeur $1.0$, et en dehors duquel $R$ prend la valeur $0.$. La fonction objectif est la distance quadratique à $\uaref$. 
