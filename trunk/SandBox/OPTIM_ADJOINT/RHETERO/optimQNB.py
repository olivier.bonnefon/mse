
import kppRHpartiel as model
import numpy as np
import os
import qnb

print("First, we del outputs")
os.system("rm SAVEUp1/* SAVE/* RESULTS/* REF/*")

for i in range(model.N):
    for j in range(model.M):
        model.pref[i*model.M+j]=5*(0.1+i/model.N+j/model.N)-0.1


model.buildRef(model.pref)
f=open("trace.txt","w")
f.close()

dim=model.N*model.M
x0=np.zeros(model.N*model.M)
x0.fill(5.0)
dxmin=np.zeros(model.N*model.M)
for i in range(model.N):
    for j in range(model.M):
        dxmin[i*model.M+j]=2.0

precision=1e-6
lb=np.zeros(model.N*model.M)
ub=np.zeros(model.N*model.M)
for i in range(model.N):
    for j in range(model.M):
        lb[i*model.M+j]=0.01
        ub[i*model.M+j]=11.0
        x0[i*model.M+j]=1.0


f=model.computeJForMIN(x0)
g=model.computeJac(x0)
dxmin=precision*(ub-lb)
noptim=0
df1=f
mode =1
imp=3
io=(noptim)*1000+10
niter=5000
nsim=3*niter
reverse=1
noptim=0
wzf=np.zeros(model.N*model.M*(model.N*model.M+9))
wif=np.zeros(2*model.N*model.M+1,dtype=np.int32)
epsabs_ori=1e-18
epsabs=np.array([epsabs_ori])
modenp=np.array([1],dtype=int)
nit=0
print("nit ",nit,"x ",x0,"f ",f,"g ",g)
qnb.n2qn1(x0,f,g,dxmin,df1,epsabs,imp,io,modenp,niter,nsim,lb,ub,wif,wzf,reverse,dim)
print("mode n2qn1: ",modenp[0])
fx0=open("traceX0.txt","w")
fx0.close()
def printX0():
    fx0=open("traceX0.txt","a")
    for i in range(model.N):
        for j in range(model.M):
            fx0.write(str(x0[i*model.M+j])+" "+str(model.pref[i*model.M+j])+" "+str(abs((model.pref[i*model.M+j]-x0[i*model.M+j])/x0[i*model.M+j]))+"\n")
    fx0.write("\n")
    fx0.close()

while(modenp[0]>7):
    if (nit%10 == 0):
        printX0()
    nit=nit+1
    f=model.computeJForMIN(x0)
    g=model.computeJac(x0)
    print("nit ",nit,"x ",x0,"f ",f,"g ",g)
    epsabs=np.array([epsabs_ori])
    nsim=3*niter
    qnb.n2qn1(x0,f,g,dxmin,df1,epsabs,imp,io,modenp,niter,nsim,lb,ub,wif,wzf,reverse,dim)
    io=io+1
    print("mode n2qn1: ",modenp[0])

printX0()
