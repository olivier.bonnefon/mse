## Internet Link
#  https://bitbucket.org/fenics-project/dolfin/src/39253449ee544fe04eb8bf387eab115e094d325e/python/demo/undocumented/block-assembly-2D2D/demo_block-assembly-2D2D-nonlinear.py?at=cecile%2Fmixed-dimensional


import ufl
from dolfinx import fem, io, mesh, plot
from dolfinx.fem.petsc import LinearProblem
from ufl import ds, dx, grad, inner
from dolfinx.fem import Function, assemble_scalar,form
from ufl import Measure,TrialFunction,TestFunction
from dolfinx.io import VTKFile
import numpy as np
import os
import sys
from mpi4py import MPI
from dolfinx.fem.petsc import assemble_matrix, assemble_vector
from petsc4py import PETSc
from numpy.linalg import norm


# Create meshes
xmax= 10.0
ymax= 10.0

msh = mesh.create_rectangle(comm=MPI.COMM_WORLD,
                            points=((0.0, 0.0), (xmax, ymax)), n=(24, 24),
                            cell_type=mesh.CellType.triangle)
V1 = fem.FunctionSpace(msh, ("Lagrange", 1))

u = TrialFunction(V1)
#wp = Function(V1)
u_a = Function(V1)
v = Function(V1)
vprim = Function(V1)
u_aeps = Function(V1)
utm1 = Function(V1)
uk = Function(V1)
w = TestFunction(V1)
uref=Function(V1)
buf=Function(V1)
buf1=Function(V1)
buf2=Function(V1)
buf3=Function(V1)
nDofs=V1.tabulate_dof_coordinates().shape[0]
XYZ=V1.tabulate_dof_coordinates().copy()
xref=np.array([0.0,0.0,0.0])
pa = TrialFunction(V1)
p_tp1 = Function (V1)




#N row
#M col
#R[0,N-1][0,M-1]
N=10
M=10
p1=np.zeros(N*M)

# visible if (i*M+j)%forMask == 0
forMask = 10



def buildRMaskij(i,j,x):
    if (x[0] >= (i)*xmax/N and ( x[0]<((i+1)*xmax/N) or (i == (N-1) and abs(x[0] - xmax)< 1e-12 ) ) and
        x[1] >= (j)*ymax/M and (x[1]<((j+1)*ymax/M) or (j == (M-1) and abs(x[1] - ymax) < 1e-12))):
        return 1
    else:
        return 0

vbuildRMaskij=np.vectorize(buildRMaskij,signature="(),(),(d)->()")

RS=[0]* N*M
for i in range(N):
    for j in range(M):
        RS[i*M+j]=Function(V1)
        RS[i*M+j].x.array[:]=vbuildRMaskij(i,j,XYZ)

#plot mask:
vtkfile_u =VTKFile(msh.comm,'IMAGES/maskRij.pvd',"w")
for i in range(N):
    for j in range(M):
        vtkfile_u.write_function(RS[i*M+j], j+i*M)

vtkfile_u.close()

def buildMaskCenter(x):
    if (x[0] < 7 and x[0]>3 and x[1] < 7 and x[1]>3):
        return 1
    else:
        return 0

vbuildMaskCenter=np.vectorize(buildMaskCenter,signature="(d)->()")

maskObs = Function(V1)
maskObs.x.array[:]=vbuildMaskCenter(XYZ)

vtkfile_u =VTKFile(msh.comm,'IMAGES/maskObs.pvd',"w")
vtkfile_u.write_function(maskObs, 0)
vtkfile_u.close()


for i in range(N):
    for j in range(M):
        p1[i*M+j]=0.1+i/N+j/M


x = ufl.SpatialCoordinate(msh)
ic="np.exp(-((x[0]-0.5*xmax)*(x[0]-0.5*xmax)+(x[1]-0.5*ymax)*(x[1]-0.5*ymax)))"
def initial_condition(x):
    return eval(ic)

def save_step(u,REP,SUFFIX,step):
   np.save(REP+'/state'+SUFFIX+str(step),u.x.array)

def read_step(u,REP,SUFFIX,step):
    u.x.array[:] = np.load(REP+'/state'+SUFFIX+str(step)+'.npy')

T0=0
TF=1
dt=0.05
d2D=10.0
nTi=round(TF/dt)
J=0.03

with open("../tools.py", mode="r", encoding="utf-8") as tools:
    code = tools.read()

exec(code)
pref=np.zeros(N*M)
# must be run to register in SAVE the current state,
#
def simul(p):
    print("simul ",p)
    A = u*w*dx + dt*d2D*inner(grad(u),grad(w))*dx
    L = utm1*w*dx
    for i in range(N):
        for j in range(M):
            if (p[i*M+j] > 1e-12):
                A = A - dt*p[i*M+j]*RS[i*M+j]*(1-2*uk)*u*w*dx
                L = L + dt*p[i*M+j]*RS[i*M+j]*uk*uk*w*dx
    step=0
    current_time=T0;
    final_time=TF;
    utm1.interpolate(initial_condition)
    vtkfile_u = VTKFile(msh.comm,'RESULTS/solution_u.pvd',"w")
    vtkfile_u.write_function(utm1, step)
    save_step(utm1,"SAVE","u",step)
    while (current_time < final_time):
        doANonLinStep(A,L,sol)
        utm1.x.array[:]=sol.array[:]
        current_time = current_time + dt
        step=step+1
        vtkfile_u.write_function(utm1, step)
        save_step(utm1,"SAVE","u",step)
    vtkfile_u.close()

#build ref

def buildRef(p1):
    print("buildRef: we are building the ref (ie data) state for p=",p1)
    simul(p1)
    os.system("cp SAVE/* REF/")
    os.system("cp RESULTS/* REF/")



# assume u_a contain current the simu with param valp
def computevprim(buf,step):
    vprim.x.array[:]=maskObs.x.array[:]*(buf.x.array[:]-uref.x.array[:])

def computev(buf,uref,step):
    v.x.array[:]=0.5*maskObs.x.array[:]*(buf.x.array[:]-uref.x.array[:])*(buf.x.array[:]-uref.x.array[:])


def dadjoint(p):
    print("begin", sys._getframe().f_code.co_name)
    g=np.zeros(N*M)
    FA=pa*w*dx +  dt*d2D*inner(grad(pa),grad(w))*dx
    for i in range(N):
        for j in range(M):
            if (p[i*M+j] > 1e-12):
                FA = FA -dt*p[i*M+j]*RS[i*M+j]*pa*w*dx +2*dt*u_a*p[i*M+j]*RS[i*M+j]*pa*w*dx
    LA= dt*vprim*w*dx + p_tp1*w*dx
    #LA= p_tp1*w*dx
#    LA= dt*uMask*(up1-uref)*w*dx + p_tp1*w*dx
    step=nTi
    current_time=TF;
    final_time=T0;
    p_tp1.x.array.fill(0.0)
    read_step(u_a,"SAVEUp","u",step)
    read_step(uref,"REF","u",step)
    computevprim(u_a,step)
    dJ=0
    while (current_time > 0):
        solveAdjoint(FA,LA,sol)
        p_tp1.x.array[:]=sol.array[:]
        current_time=current_time-dt
        step=step-1
        for i in range(N):
            for j in range(M):
                if (p[i*M+j] > 1e-12):
                    g[i*M+j]=g[i*M+j]+dt*assemble_scalar(form(RS[i*M+j]*u_a*(1-u_a)*p_tp1*dx))
        read_step(u_a,"SAVEUp","u",step)
        read_step(uref,"REF","u",step)
        computevprim(u_a,step)
    return g



#computeJForMIN(p1)
#print(dadjoint(p1))
