#!/bin/bash
#SBATCH -J OPT_DOL1 #job name
#SBATCH -o output.out #le fichier de sortie d écran
#SBATCH -e error.out  # le fichier de sortie d erreur
#SBATCH --ntasks=5   #nombre de
#SBATCH -w pcbiom99 
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/apps/pkg/boost_1_61/lib/
source /apps/pkg/dolfin/share/dolfin/dolfin.conf


mpirun -np 1 python3.7 optimQNB.py

