## Internet Link
#  https://bitbucket.org/fenics-project/dolfin/src/39253449ee544fe04eb8bf387eab115e094d325e/python/demo/undocumented/block-assembly-2D2D/demo_block-assembly-2D2D-nonlinear.py?at=cecile%2Fmixed-dimensional


from dolfin import *
import numpy as np
import os

set_log_active(0)
def newton_solver_parameters():
    return{"nonlinear_solver": "newton","newton_solver": {"linear_solver": "gmres"}}

def linear_solver_parameters():
    return{"linear_solver": "gmres"}


# Create meshes
nPerL = 8
xmax= 10.0
ymax= 5.0
nx = int(nPerL*xmax)
ny = int(nPerL*ymax)
pt0 = Point([0.0,0.0])
pt1 = Point([xmax,ymax])
mesh = RectangleMesh(pt0, pt1, nx, ny, 'right')
V = FunctionSpace(mesh, "CG", 1)
#N row
#M col
#R[0,N-1][0,M-1]
N=7
M=7
p1=np.zeros(N*M)
pref=np.zeros(N*M)
class RHij(UserExpression):
    def __init__(self,i,j,**kwargs):
        super().__init__(**kwargs)
        self.i=i
        self.j=j
    
    def eval(self, value, x):
        value[0]=0
        if (x[0] >= (self.i)*xmax/N and ( x[0]<((self.i+1)*xmax/N) or (self.i == (N-1) and abs(x[0] - xmax)< 1e-12 ) ) and
            x[1] >= (self.j)*ymax/M and (x[1]<((self.j+1)*ymax/M) or (self.j == (M-1) and abs(x[1] - ymax) < 1e-12))):
            value[0]=1
    
    def value_shape(self):
        return ()

vall = Function(V)
RS=[0]* N*M
for i in range(N):
    for j in range(M):
        RS[i*M+j]=interpolate(RHij(i=i,j=j),V)
        vall.assign(vall+RS[i*M+j])

for i in range(N):
    for j in range(M):
        p1[i*M+j]=0.1+i/N+j/M

vtkfile_k = File('R.pvd')
#vtkfile_k<<R00
#vtkfile_k<<R01
#vtkfile_k<<R10
#vtkfile_k<<R11
vtkfile_k<<vall

class myU0(UserExpression):
    def eval(self,value,x):
        value[0]= exp(-((x[0]-0.5*xmax)*(x[0]-0.5*xmax)+(x[1]-0.5*ymax)*(x[1]-0.5*ymax)))
    def value_shape(self):
        return ()


u0 = interpolate(myU0( degree=2), V)

u = Function(V)
wp1 = Function(V)
up1 = Function(V)
up1eps = Function(V)
u_old = Function(V)
v = TestFunction(V)
uref=Function(V)
buf=Function(V)
buf1=Function(V)
buf2=Function(V)
buf3=Function(V)
sol = Function(V)

def save_step(u,REP,SUFFIX,step):
   uvals = u.vector().get_local()
   np.save(REP+'/state'+SUFFIX+str(step),uvals)

def read_step(u,REP,SUFFIX,step):
    uvals = u.vector().get_local()
    uvals = np.load(REP+'/state'+SUFFIX+str(step)+'.npy')
    u.vector().set_local(uvals)

T0=0
TF=1
dt=0.05
d2D=10.0
nTi=round(TF/dt)
J=0.0

# must be run to register in SAVE the current state,
#
def simul(p):
    print("simul ",p)
    F = u*v*dx - u_old*v*dx + dt*d2D*inner(grad(u),grad(v))*dx 
    for i in range(N):
        for j in range(M):
            if (p[i*M+j] > 1e-12):
                F = F - dt*p[i*M+j]*RS[i*M+j]*u*(1-u)*v*dx
    step=0
    current_time=T0;
    final_time=TF;
    u_old.assign(u0)
    vtkfile_u = File('RESULTS/solution_u.pvd',"compressed")
    vtkfile_u << (u_old, step)
    save_step(u_old,"SAVE","u",step)
    while (current_time < final_time):
        solve(F == 0, u, solver_parameters=newton_solver_parameters())
        u_old.assign(u)
        uvals = u_old.vector().get_local()     # temporary copy of function value arrays
        uvals[uvals < 0.] = 0.               # numpy syntax for overwriting negative values
        u_old.vector().set_local(uvals)        # overwrite function values with corrected array
        current_time = current_time + dt
        step=step+1
        vtkfile_u << (u_old, step)
        save_step(u_old,"SAVE","u",step)

#build ref

def buildRef(p1):
    print("buildRef: we are building the ref (ie data) state for p=",p1)
    simul(p1)
    os.system("cp SAVE/* REF/")
    os.system("cp RESULTS/* REF/")

# comput J using the current sate saved in SAVE and the ref in REF
def computeJ():
    J=0.0
    step=0
    current_time=T0;
    final_time=TF;
    while (current_time < final_time):
        current_time = current_time + dt
        step=step+1
        read_step(buf,"SAVE","u",step)
        read_step(uref,"REF","u",step)
        J=J+dt*0.5*assemble((buf-uref)*(buf-uref)*dx)
    return J



pa = TrialFunction(V)
p_tp1 = Function (V)

def dadjoint(p):
    g=np.zeros(N*M)
    FA=pa*v*dx +  dt*d2D*inner(grad(pa),grad(v))*dx
    for i in range(N):
        for j in range(M):
            if (p[i*M+j] > 1e-12):
                FA = FA + -dt*p[i*M+j]*RS[i*M+j]*pa*v*dx +2*dt*up1*p[i*M+j]*RS[i*M+j]*pa*v*dx
    LA= dt*(up1-uref)*v*dx + p_tp1*v*dx
    step=nTi
    current_time=TF;
    final_time=T0;
    p_tp1.assign(Expression("0.0",degree=1))
    read_step(up1,"SAVEUp","u",step)
    read_step(uref,"REF","u",step)
    dJ=0
    while (current_time > 0):
        solve(FA == LA, sol,solver_parameters={"linear_solver": "gmres"})
        p_tp1.assign(sol)
        current_time=current_time-dt
        step=step-1
        for i in range(N):
            for j in range(M):
                if (p[i*M+j] > 1e-12):
                    g[i*M+j]=g[i*M+j]+dt*assemble(RS[i*M+j]*up1*(1-up1)*p_tp1*dx)
        read_step(up1,"SAVEUp","u",step)
        read_step(uref,"REF","u",step)
    return g



#no J check d_p1 U

print("First, we del outputs")
os.system("rm SAVEUp1/* SAVE/* RESULTS/* REF/*")
for i in range(N):
    for j in range(M):
        pref[i*M+j]=0.5+i/N+j/N
print("First, we del outputs")
os.system("rm SAVEUp/* SAVE/* RESULTS/* REF/*;mkdir SAVEUp")

buildRef(pref)
f=open("trace.txt","w")
f.close()
def computeJForMIN(p1):
    f=open("trace.txt","a")
    f.write(str((pref - p1).dot(pref - p1)))
    f.write(" ")
    f.close()
    simul(p1)
    os.system("cp SAVE/*u* SAVEUp")
    return computeJ()

def computeJac(p1):
    g=dadjoint(p1)
    f=open("trace.txt","a")
    f.write(str(g.dot(g)))
    f.write("\n")
    f.close()
    return g

#computeJForMIN(p1)
#print(dadjoint(p1))
