
import kppC as model
import numpy as np
import os
import qnb

print("First, we del outputs")
os.system("rm SAVEUp1/* SAVE/* RESULTS/* REF/*")


model.refFromPicture=1
model.withCmotion=0
model.initMem()
model.buildRef()

f=open("trace.txt","w")
f.close()

x0=np.zeros(model.dim)
x0.fill(.5)

lmaxArange=2*np.pi
lincArange=lmaxArange/model.nptInWireR
laux=np.arange(0,lmaxArange-lincArange+1e-9,lincArange)
#x0[0:model.nptInWireR]=0.3+(0.1+0.15*np.sin(0.5*laux))
dxmin=np.zeros(model.dim)
dxmin.fill(.01)
precision=1e-4
lb=np.zeros(model.dim)
ub=np.zeros(model.dim)
for i in range(model.dim):
    lb[i]=0.05
    ub[i]=0.7
    model.cx=1.176482
    model.cy=0.751260
if (model.withCmotion):
    lb[model.dim-2]=0.71
    ub[model.dim-2]=1.29
    lb[model.dim-1]=0.71
    ub[model.dim-1]=1.29
    x0[model.dim-2]=0.9
    x0[model.dim-1]=0.9

f=model.computeJForMIN(x0)
g=model.computeJac(x0)
dxmin=precision*(ub-lb)
noptim=0
df1=f
mode =1
imp=3
io=(noptim)*1000+10
niter=5000
nsim=3*niter
reverse=1
noptim=0
wzf=np.zeros(model.dim*(model.dim+9))
wif=np.zeros(2*model.dim+1,dtype=np.int32)
epsabs_ori=1e-18
epsabs=np.array([epsabs_ori])
modenp=np.array([1],dtype=int)
nit=0
fx0=open("traceX0.txt","w")
fx0.close()
def printX0():
    fx0=open("traceX0.txt","a")
    if (model.refFromPicture==1):
        for i in range(model.dim):
            fx0.write(str(x0[i])+" "+str(g[i])+"\n")
    else:
        for i in range(model.dim):
            fx0.write(str(x0[i])+" "+str(model.pref[i])+" "+str(abs((model.pref[i]-x0[i])/x0[i]))+"\n")
    fx0.write(str(f)+"\n")
    fx0.close()

printX0()
print("nit ",nit,"x ",x0,"f ",f,"g ",g)
qnb.n2qn1(x0,f,g,dxmin,df1,epsabs,imp,io,modenp,niter,nsim,lb,ub,wif,wzf,reverse,model.dim)
print("mode n2qn1: ",modenp[0])



while(modenp[0]>7):
    #if (nit%10 == 0):
    nit=nit+1
    f=model.computeJForMIN(x0)
    g=model.computeJac(x0)
    print("nit ",nit,"x ",x0,"f ",f,"g ",g)
    printX0()
    epsabs=np.array([epsabs_ori])
    nsim=3*niter
    qnb.n2qn1(x0,f,g,dxmin,df1,epsabs,imp,io,modenp,niter,nsim,lb,ub,wif,wzf,reverse,model.dim)
    io=io+1
    print("mode n2qn1: ",modenp[0])

printX0()
