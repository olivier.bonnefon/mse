import kppC as model
import matplotlib.pyplot as plt
import numpy as np

model.withCmotion=1
model.refFromPicture=1
f=open("traceX0.txt","r")
Lines = f.readlines()
dim=model.nptInWireR
p1=np.zeros(dim)
p1Ref=np.zeros(dim)
xLine=np.zeros(dim+1)
yLine=np.zeros(dim+1)
xLineRef=np.zeros(dim+1)
yLineRef=np.zeros(dim+1)
curLine=0
if (model.refFromPicture==0):
    for i in range(model.nptInWireR):
        p1Ref[i]=float(Lines[curLine].split()[1])
        curLine=curLine+1
    model.onlyPreComputePointsPos(p1Ref)
    for i in range(model.nptInWireR):
        xLineRef[i]=model.points[i][0]
        yLineRef[i]=model.points[i][1]
    xLineRef[dim]=xLineRef[0]
    yLineRef[dim]=yLineRef[0]



curLine=0
def readPoints():
    global curLine,p1,cx,cy
    for i in range(model.nptInWireR):
        p1[i]=float(Lines[curLine].split()[0])
        curLine=curLine+1
    if (model.withCmotion==1):
        model.cx=float(Lines[curLine].split()[0])
        curLine=curLine+1
        model.cy=float(Lines[curLine].split()[0])
        curLine=curLine+1
    curLine=curLine+1
    print(p1)
    model.onlyPreComputePointsPos(p1)
    for i in range(model.nptInWireR):
        xLine[i]=model.points[i][0]
        yLine[i]=model.points[i][1]
    xLine[dim]=xLine[0]
    yLine[dim]=yLine[0]

numIt=0
npcx=np.zeros(1)

npcy=np.zeros(1)
npcy[0]=model.cy
while (curLine+10<len(Lines)):
    readPoints()
    plt.figure(figsize=(8, 8))
    if (model.refFromPicture==0):
        plt.plot(xLineRef,yLineRef)
    plt.plot(xLine, yLine)
    npcx[0]=model.cx
    npcy[0]=model.cy
    plt.plot(npcx,npcy,'o')
    plt.xlim([0, 2])
    plt.ylim([0, 2])
    if (numIt<10):
        plt.savefig("toto0"+str(numIt)+".tif")
    else:
        plt.savefig("toto"+str(numIt)+".tif")
    numIt+=1
    plt.close()
