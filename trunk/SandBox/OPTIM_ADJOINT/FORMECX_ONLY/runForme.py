import kppForme as model
import numpy as np
import os

os.system("rm SAVEUp1/* SAVE/* RESULTS/* REF/*")

model.buildRef()
message=""
p1=np.zeros(model.nptInWireR)
p1[:]=model.pref[:]
paux=np.zeros(model.nptInWireR)

message=""
da=0.05
p1=np.zeros(model.nptInWireR)
p1=model.pref+0.2*np.sin(-model.aux)
res1=model.computeJForMIN(p1)
gAdjoint1=model.dadjoint()
for i in range(model.nptInWireR):
    paux[:]=p1[:]
    paux[i]=paux[i]+da
    res2=model.computeJForMIN(paux)
    gAdjoint2=model.dadjoint()
    message=message+str(i)+" "+str(0.5*(gAdjoint1[i]+gAdjoint2[i]))+" "+str((res2-res1)/da)+"\n"
    print(message)

print(message)
 
'''

résultat  de python3 runForme.py
J=0.1298163662055798
0.2 gAdjoint=[-1.30093494]
0.1 dif finie d_aV(0.2)=-1.3523635440423278
0.05 dif finie d_aV(0.2)=-1.4452480898198106
0.025 dif finie d_aV(0.2)=-1.4046735714042802
0.3 gAdjoint=[-1.11732323]
0.1 dif finie d_aV(0.3)=-0.5626447588411551
0.05 dif finie d_aV(0.3)=-0.8556156247354396
0.025 dif finie d_aV(0.3)=-1.0407052075857013
0.5 gAdjoint=[0.68514962]
0.1 dif finie d_aV(0.5)=0.7282243900807748
0.05 dif finie d_aV(0.5)=0.7051399057590069
0.025 dif finie d_aV(0.5)=0.6874444572909935
0.6 gAdjoint=[0.69304759]
0.1 dif finie d_aV(0.6)=0.5199059259483214
0.05 dif finie d_aV(0.6)=0.621636551866517
0.025 dif finie d_aV(0.6)=0.6566636998626024

YES

'''
