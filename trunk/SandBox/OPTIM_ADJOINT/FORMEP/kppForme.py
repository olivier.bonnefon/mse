from toolsEnv import computerEnv,MSEPrint
import MESH_IMPORT_GMSH.read as reader
import MESH_IMPORT_GMSH.buildMeshes as buildMeshes
from dolfinx.io import XDMFFile,VTKFile
from ufl import Measure,TrialFunction,TestFunction, grad, inner,SpatialCoordinate
from dolfinx import mesh
from dolfinx.fem import Function,FunctionSpace, assemble_scalar,form
from mpi4py import MPI
import numpy as np
import gmsh
import meshio
import sys
import numpy as np
import os.path
import subprocess
import MAP_TOOLS.buildMapc as mc
from petsc4py import PETSc
from dolfinx.fem.petsc import assemble_matrix, assemble_vector
from numpy.linalg import norm

def save_step(u,REP,SUFFIX,step):
   np.save(REP+'/state'+SUFFIX+str(step),u.x.array)

def read_step(u,REP,SUFFIX,step):
    u.x.array[:] = np.load(REP+'/state'+SUFFIX+str(step)+'.npy')



T0=0
TF=10
dt=0.5
nTi=round(TF/dt)

d2D=0.005
#mortalite additionnelle partout
mu=0.1
#rectangular geometry
xmin=0.0
ymin=0.0
xmax=2
ymax=2
#center
xcenter=0.5*(xmin+xmax)
ycenter=0.5*(ymin+ymax)

amax=0.9
amin=0.05



msh = mesh.create_rectangle(comm=MPI.COMM_WORLD,
                            points=((xmin, ymin), (xmax, ymax)), n=(150, 150),
                            cell_type=mesh.CellType.triangle)

vSpace = FunctionSpace(msh, ("CG", 1))
        
nbDofs=vSpace.tabulate_dof_coordinates().shape[0]
dx=Measure('dx',msh)
XYZ=vSpace.tabulate_dof_coordinates().copy()
u = TrialFunction(vSpace)
v = Function(vSpace)
vprim = Function(vSpace)
utm1 = Function(vSpace)
uk = Function(vSpace)
w = TestFunction(vSpace)
x = SpatialCoordinate(msh)
sol=PETSc.Vec().createSeq(nbDofs)
prevSol=PETSc.Vec().createSeq(nbDofs)
prevSol.array.fill(0.0)
uref=Function(vSpace)
buf=Function(vSpace)
R=Function(vSpace)
Rdmup1=Function(vSpace)

pa = TrialFunction(vSpace)
p_tp1=Function(vSpace)
u_a=Function(vSpace)

criteronNR=1e-3
maxIt=10



import dolfinx.geometry as dg
tree= dg.BoundingBoxTree(msh,2)

#nptInWireR est la taille de p
nptInWireR=24*2
refFromPicture=1
pref=np.zeros(nptInWireR)
pref.fill(0.5)
maxArange=12*np.pi
incArange=maxArange/nptInWireR
aux=np.arange(0,maxArange-incArange+1e-9,incArange)
pref=0.5+0.2*np.sin(aux)
points=np.zeros(nptInWireR*3).reshape((nptInWireR,3))
mesh_size=0.02
angleStep=2*np.pi/(1.0*nptInWireR)


BufV=np.zeros(6*3).reshape((6,3))
def buildRMask(x):
   for i in range(nptInWireR):
      inext=(i+1)%nptInWireR
      BufV[0][0]=points[i][0]-xcenter
      BufV[0][1]=points[i][1]-ycenter
      BufV[1][0]=x[0]-xcenter
      BufV[1][1]=x[1]-ycenter
      BufV[2][0]=points[inext][0]-xcenter
      BufV[2][1]=points[inext][1]-ycenter
      Z1=vectZ(BufV[0],BufV[1])
      Z2=vectZ(BufV[1],BufV[2])
      Z3=vectZ(BufV[0],BufV[2])
      #vecteur normal a MiMinext 
      BufV[3][0]=points[inext][1]-points[i][1]
      BufV[3][1]=-points[inext][0]+points[i][0]
      BufV[4][0]=xcenter-points[i][0]
      BufV[4][1]=ycenter-points[i][1]
      BufV[5][0]=x[0]-points[i][0]
      BufV[5][1]=x[1]-points[i][1]
      #on verifie que center et x sont du meme cote
      #cas aligne
      if (Z1==0):
         if (np.dot(BufV[0],BufV[1]) >= 0 and np.linalg.norm(BufV[1])<np.linalg.norm(BufV[0])):
             return 1
      if (Z2==0):
         if (np.dot(BufV[2],BufV[1]) >= 0 and np.linalg.norm( BufV[1])<np.linalg.norm(BufV[2])):
             return 1
      if ((BufV[3][0]*BufV[4][0]+BufV[3][1]*BufV[4][1])*(BufV[3][0]*BufV[5][0]+BufV[3][1]*BufV[5][1])>0):
         if ( (Z1*Z2>0 and Z2*Z3>0) ):
            return 1;
   return 0


vbuildRMask=np.vectorize(buildRMask,signature="(d)->()")

def preComputePointsPos(p):
   global R
   angle=0
   for i in range(nptInWireR):
      print(str(p[i]))
      points[i][0]=xcenter+p[i]*np.cos(angle)
      points[i][1]=ycenter+p[i]*np.sin(angle)
      angle+=angleStep
   R.x.array[:]=vbuildRMask(XYZ)
   vtkfile_R = VTKFile(msh.comm,'RESULTS/R.pvd',"w")
   vtkfile_R.write_function(R, 0)
   vtkfile_R.close()

def onlyPreComputePointsPos(p):
   global R
   angle=0
   for i in range(nptInWireR):
      print(str(p[i]))
      points[i][0]=xcenter+p[i]*np.cos(angle)
      points[i][1]=ycenter+p[i]*np.sin(angle)
      angle+=angleStep

   
cells=[]
#nPointsBySeg sert à discrétiser le segment M_i, M_i+1.
nPointsBySeg=4
def preComputeCellsForDist():
   global cells,points,pointsSeg
   cells=[]
   angle=0
   #add points for the circle
   for i in range(nptInWireR):
      inext=(i+1)%nptInWireR
      cellsSeg=[]
      for k in range(nPointsBySeg):
         x0=[((1.0*(nPointsBySeg-1-k))*points[i][0]+(1.0*k)*points[inext][0])/(1.0*(nPointsBySeg-1)),
             ((1.0*(nPointsBySeg-1-k))*points[i][1]+(1.0*k)*points[inext][1])/(1.0*(nPointsBySeg-1)),
             0.0]
         cell_candidates =dg.compute_collisions(tree, x0)
         colliding_cells = dg.compute_colliding_cells(msh,cell_candidates,x0)
         cellsSeg.append(colliding_cells[0])
      cells.append(cellsSeg)

class solver:
    def __init__(self) -> None:
        self.KSP=PETSc.KSP(MPI.COMM_WORLD).create()
    def setMat(self,aMat):
        self.KSP.setOperators(aMat)
        self.KSP.setType(PETSc.KSP.Type.PREONLY)
        self.KSP.getPC().setType(PETSc.PC.Type.LU)
    def solve(self,rhs,sol):
        self.KSP.solve(rhs,sol)
    def solveNoNeg(self,rhs,sol):
        self.solve(rhs,sol)
        sol.array[sol.array < 0.] = 0.

aSolver=solver()

def doANonLinStep(a,L,sol):
    error=criteronNR+1
    numIt=0
    uk.x.array[:]=utm1.x.array[:]
    while(error>criteronNR and numIt<maxIt):
        M=assemble_matrix(form(a))
        M.assemble()
        rhs=assemble_vector(form(L))
        aSolver.setMat(M)
        aSolver.solveNoNeg(rhs,sol)
        error=norm(sol.array - prevSol.array)
        prevSol.array[:]=sol.array[:]
        numIt+=1
        print("doANonLinStep "+str(numIt)+" "+str(error))
        uk.x.array[:]=sol.array[:]
    if (error > 10*criteronNR):
        print("Newton failed "+str(error))




def computeJ():
    print("begin", sys._getframe().f_code.co_name)
    J=0.0
    step=0
    current_time=T0;
    final_time=TF;
    while (current_time < final_time):
        current_time = current_time + dt
        step=step+1
        if (step==nTi):
            read_step(buf,"SAVE","u",step)
            read_step(uref,"REF","u",step)
            computev(buf,uref,step)
            J=J+dt*assemble_scalar(form(v*dx))
            print("J="+str(J))
    return J



def computeJForMIN(p1):
    f=open("trace.txt","a")
    if (refFromPicture==0):
       f.write(str(np.dot((pref - p1),(pref - p1))))
       f.write(" ")
       f.close()
    preComputePointsPos(p1)
    preComputeCellsForDist()
    simul(p1)
    os.system("cp SAVE/*u* SAVEUp")
    return computeJ()

def computeJac(p1):
    g=dadjoint()
    f=open("trace.txt","a")
    f.write(str(g.dot(g)))
    f.write("\n")
    f.close()
    return g

def vectZ(V1,V2):
   res=V1[0]*V2[1]-V1[1]*V2[0]
   if (res>1e-9):
      return 1
   if (res<-1e-9):
      return -1
   return 0




x = None
ic="np.exp(-16.0*(np.square(x[0]-0.5*xmax)+np.square(x[1]-0.5*ymax)))"
def initial_condition(x):
    return eval(ic)

def simul(p):
    global lastStep,R
    print("simul ",p)
    A = u*w*dx+ dt*d2D*inner(grad(u),grad(w))*dx- dt*R*(1-2*uk)*u*w*dx +dt*mu*u*w*dx
    L = utm1*w*dx + dt*R*uk*uk*w*dx
    step=0
    current_time=T0;
    final_time=TF;
    utm1.interpolate(initial_condition)
    vtkfile_u = VTKFile(msh.comm,'RESULTS/solution_u.pvd',"w")
    vtkfile_u.write_function(utm1, step)
    save_step(utm1,"SAVE","u",step)
    while (current_time < final_time):
        doANonLinStep(A,L,sol)
        utm1.x.array[:]=sol.array[:]
        current_time = current_time + dt
        step=step+1
        vtkfile_u.write_function(utm1, step)
        save_step(utm1,"SAVE","u",step)
    lastStep=step
    vtkfile_u.close()


def solveAdjoint(FA,LA,solAdjoint):
    M=assemble_matrix(form(FA))
    M.assemble()
    rhs=assemble_vector(form(LA))
    aSolver.setMat(M)
    aSolver.solve(rhs,solAdjoint)

def computev(buf,uref,step):
    if (step==nTi):
       v.x.array[:]=0.5*np.square(buf.x.array[:]-uref.x.array[:])/dt
    else:
       v.x.array.fill(0.0)

def computevprim(buf,uref,step):
    if (step==nTi):
        vprim.x.array[:]=(buf.x.array[:]-uref.x.array[:])/dt
    else:
        vprim.x.array.fill(0.0)

def dadjoint():
    print("begin", sys._getframe().f_code.co_name)
    g=np.zeros(nptInWireR)
    FA=pa*w*dx +  dt*d2D*inner(grad(pa),grad(w))*dx-dt*R*pa*w*dx +2*dt*u_a*R*pa*w*dx + mu*pa*w*dx
    LA= dt*vprim*w*dx + p_tp1*w*dx
    #LA= p_tp1*w*dx
#    LA= dt*uMask*(up1-uref)*w*dx + p_tp1*w*dx
    step=nTi
    current_time=TF;
    final_time=T0;
    p_tp1.x.array.fill(0.0)
    read_step(u_a,"SAVEUp","u",step)
    read_step(uref,"REF","u",step)
    computevprim(u_a,uref,step)
    dJ=0
    while (current_time > 0):
        solveAdjoint(FA,LA,sol)
        p_tp1.x.array[:]=sol.array[:]
        current_time=current_time-dt
        step=step-1
        bufPtp1=0.0
        bufUa=0.0
        bufG=0.0
        for i in range(nptInWireR):
           inext=(i+1)%nptInWireR
           iprev=(i-1)%nptInWireR
           Mi=[points[i][0],points[i][1]]
           Miprev=[points[iprev][0],points[iprev][1]]
           Minext=[points[inext][0],points[inext][1]]
           #contribution de M_i a M_inext
           
           #ponderation de Mi a Mi_next
           cellSeg=cells[i]
           dseg=np.sqrt(np.square(Mi[0]-Minext[0])+np.square(Mi[1]-Minext[1]))/nPointsBySeg
           bufG=0.0
           for k, pond in enumerate(np.arange(1.0,-1e-4,-1.0/(nPointsBySeg-1))):
              if (pond==0.0):
                 pass
              x0=[((1.0*(nPointsBySeg-1-k))*points[i][0]+(1.0*k)*points[inext][0])/(1.0*(nPointsBySeg-1)),
                  ((1.0*(nPointsBySeg-1-k))*points[i][1]+(1.0*k)*points[inext][1])/(1.0*(nPointsBySeg-1)),
                  0.0]
              bufPtp1=p_tp1.eval(x0,cellSeg[k])
              bufUa=u_a.eval(x0,cellSeg[k])
              bufG=bufG+pond*bufUa*(1-bufUa)*bufPtp1
           g[i]=g[i]+dt*bufG*dseg
           #ponderation de Miprev a Mi
           cellSeg=cells[iprev]
           dseg=np.sqrt(np.square(Mi[0]-Miprev[0])+np.square(Mi[1]-Miprev[1]))/nPointsBySeg
           bufG=0.0
           for k, pond in enumerate(np.arange(0.0,1+1e-4,1.0/(nPointsBySeg-1))):
              if (pond==0.0):
                 pass
              x0=[((1.0*(nPointsBySeg-1-k))*points[iprev][0]+(1.0*k)*points[i][0])/(1.0*(nPointsBySeg-1)),
                  ((1.0*(nPointsBySeg-1-k))*points[iprev][1]+(1.0*k)*points[i][1])/(1.0*(nPointsBySeg-1)),
                  0.0]
              bufPtp1=p_tp1.eval(x0,cellSeg[k])
              bufUa=u_a.eval(x0,cellSeg[k])
              bufG=bufG+pond*bufUa*(1-bufUa)*bufPtp1
           g[i]=g[i]+dt*bufG*dseg
        #for i in range(nbDofs1DPerComp):
        #   p1d.x.array[i]=p_tp1.x.array[index1DTo2D[i]]
        #   buf1d.x.array[i]=u_a.x.array[index1DTo2D[i]]
        #g=g+dt*assemble_scalar(form(buf1d*(1-buf1d)*p1d*dx1d))
        read_step(u_a,"SAVEUp","u",step)
        read_step(uref,"REF","u",step)
        computevprim(u_a,uref,step)
    return g



def buildRefFromPicture():
   global R
   from PIL import Image
   im = Image.open("home.png")
   imarray = np.array(im)
   ndx=imarray.shape[0]
   ndy=imarray.shape[1]
   sizeHome=0.5*(xmax-xmin)
   rasterDx=sizeHome/ndx
   rasterDy=sizeHome/ndy
   def buildRMaskPicture(x):
      xindex=int((x[0]-(xcenter-0.5*sizeHome))/rasterDx)
      yindex=int((x[1]-(ycenter-0.5*sizeHome))/rasterDy)
      if (xindex < 0 or xindex > (ndx-1) or yindex < 0 or yindex > (ndy-1)):
         return 0
      return 1-imarray[xindex,yindex]
   vbuildRMask=np.vectorize(buildRMaskPicture,signature="(d)->()")
   R.x.array[:]=vbuildRMask(XYZ)
   vtkfile_R = VTKFile(msh.comm,'RESULTS/R.pvd',"w")
   vtkfile_R.write_function(R, 0)
   vtkfile_R.close()
   simul("ref")
   os.system("cp SAVE/* REF/")
   os.system("cp RESULTS/* REF/")

def buildRef():
   if (refFromPicture):
      buildRefFromPicture()
   else:
      print("buildRef: we are building the ref (ie data) state for p=",pref)
      preComputePointsPos(pref)
      preComputeCellsForDist()
      simul(pref)
      os.system("cp SAVE/* REF/")
      os.system("cp RESULTS/* REF/")

