import kppForme as model
import matplotlib.pyplot as plt
import numpy as np

f=open("traceX0.txt","r")
Lines = f.readlines()
dim=model.nptInWireR
p1=np.zeros(dim)
p1Ref=np.zeros(dim)
xLine=np.zeros(dim+1)
yLine=np.zeros(dim+1)
xLineRef=np.zeros(dim+1)
yLineRef=np.zeros(dim+1)
curLine=0

for i in range(dim):
    p1Ref[i]=float(Lines[curLine].split()[1])
    curLine=curLine+1

model.onlyPreComputePointsPos(p1Ref)
for i in range(dim):
    xLineRef[i]=model.points[i][0]
    yLineRef[i]=model.points[i][1]

xLineRef[dim]=xLineRef[0]
yLineRef[dim]=yLineRef[0]



curLine=0
def readPoints():
    global curLine,p1
    for i in range(dim):
        p1[i]=float(Lines[curLine].split()[0])
        curLine=curLine+1
    curLine=curLine+1
    print(p1)
    model.onlyPreComputePointsPos(p1)
    for i in range(dim):
        xLine[i]=model.points[i][0]
        yLine[i]=model.points[i][1]
    xLine[dim]=xLine[0]
    yLine[dim]=yLine[0]

numIt=0
while (curLine+10<len(Lines)):
    readPoints()
    plt.figure(figsize=(8, 8))
    plt.plot(xLineRef,yLineRef)
    plt.plot(xLine, yLine)
    plt.xlim([0, 2])
    plt.ylim([0, 2])
    if (numIt<10):
        plt.savefig("toto0"+str(numIt)+".tif")
    else:
        plt.savefig("toto"+str(numIt)+".tif")
    numIt+=1
