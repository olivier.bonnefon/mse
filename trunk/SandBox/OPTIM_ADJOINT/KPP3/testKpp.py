from kpp3x import *
from os.path import exists
from os import mkdir

if (not exists("REF")):
    mkdir("REF")
if (not exists("SAVE")):
    mkdir("SAVE")
if (not exists("RESULTS")):
    mkdir("RESULTS")

pref=np.array([5,1.0,.5])
buildRef(pref)
x0=np.array([5.5,1.2,.6])
simul(x0)
Jx0=computeJ()
nablaJ=dadjoint(x0)
eps=np.array([0.1,.05,.05])
computedJdf(x0,eps,Jx0)
print(nablaJ)
print(dJdf)
