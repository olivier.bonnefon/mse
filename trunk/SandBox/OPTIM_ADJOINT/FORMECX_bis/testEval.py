import ufl
from dolfinx import fem, io, mesh, plot
from dolfinx.fem.petsc import LinearProblem
from ufl import ds, dx, grad, inner
from dolfinx.fem import Function, assemble_scalar,form
from ufl import Measure,TrialFunction,TestFunction
from dolfinx.io import VTKFile
import numpy as np
import os
import sys
from mpi4py import MPI
from dolfinx.fem.petsc import assemble_matrix, assemble_vector
from petsc4py import PETSc
from numpy.linalg import norm


# Create meshes
xmax= 10.0
ymax= 10.0
xmin=0
ymin=0
xcenter=0.5*(xmin+xmax)
ycenter=0.5*(ymin+ymax)
radius=0.3

xpt=xcenter+radius*np.cos(np.pi/3.0)
ypt=ycenter+radius*np.sin(np.pi/3.0)

msh = mesh.create_rectangle(comm=MPI.COMM_WORLD,
                            points=((xmin, ymin), (xmax, ymax)), n=(400, 400),
                            cell_type=mesh.CellType.triangle)
V1 = fem.FunctionSpace(msh, ("Lagrange", 1))

u = Function(V1)


x0=[xcenter+radius*np.cos(np.pi/3.0),ycenter+radius*np.sin(np.pi/3.0),0.0]
import dolfinx.geometry as dg
tree= dg.BoundingBoxTree(msh,2)
cell_candidates =dg.compute_collisions(tree, x0)
colliding_cells = dg.compute_colliding_cells(msh,cell_candidates,x0)
aCollinding=[colliding_cells[0]]
val=u.eval(x0,aCollinding)
print(val)
txtFormula="np.sqrt(("+str(xcenter)+"-x[0])**2+("+str(ycenter)+"-x[1])**2)"
def initial_condition(x):
    return eval(txtFormula)

u.interpolate(initial_condition)
val=u.eval(x0,aCollinding)
print(val)
