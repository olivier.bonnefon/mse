from toolsEnv import computerEnv,MSEPrint
import MESH_IMPORT_GMSH.read as reader
import MESH_IMPORT_GMSH.buildMeshes as buildMeshes
from dolfinx.io import XDMFFile,VTKFile
from ufl import Measure,TrialFunction,TestFunction, grad, inner,SpatialCoordinate
from dolfinx import mesh
from dolfinx.fem import Function,FunctionSpace, assemble_scalar,form
from mpi4py import MPI
import numpy as np
import gmsh
import meshio
import sys
import numpy as np
import os.path
import subprocess
import MAP_TOOLS.buildMapc as mc
from petsc4py import PETSc
from dolfinx.fem.petsc import assemble_matrix, assemble_vector
from numpy.linalg import norm

def save_step(u,REP,SUFFIX,step):
   np.save(REP+'/state'+SUFFIX+str(step),u.x.array)

def read_step(u,REP,SUFFIX,step):
    u.x.array[:] = np.load(REP+'/state'+SUFFIX+str(step)+'.npy')

#
# ici, le parametrage porte sur la donnees initiale par une recherche de coordonnees
#

T0=0
TF=10
dt=0.5
nTi=round(TF/dt)

d2D=0.005
#mortalite additionnelle partout
mu=0.0
#rectangular geometry
xmin=0.0
ymin=0.0
xmax=2
ymax=2
#center
xcenter=0.5*(xmin+xmax)
ycenter=0.5*(ymin+ymax)

amax=0.9
amin=0.05

R=1.0
nmesh=100
msh = mesh.create_rectangle(comm=MPI.COMM_WORLD,
                            points=((xmin, ymin), (xmax, ymax)), n=(nmesh, nmesh),
                            cell_type=mesh.CellType.triangle)

vSpace = FunctionSpace(msh, ("CG", 1))
        
nbDofs=vSpace.tabulate_dof_coordinates().shape[0]
dx=Measure('dx',msh)
XYZ=vSpace.tabulate_dof_coordinates().copy()
u = TrialFunction(vSpace)
v = Function(vSpace)
vprim = Function(vSpace)
utm1 = Function(vSpace)
uk = Function(vSpace)
w = TestFunction(vSpace)
x = SpatialCoordinate(msh)
sol=PETSc.Vec().createSeq(nbDofs)
prevSol=PETSc.Vec().createSeq(nbDofs)
prevSol.array.fill(0.0)
uref=Function(vSpace)
buf=Function(vSpace)

pa = TrialFunction(vSpace)
p_tp1=Function(vSpace)
u_a=Function(vSpace)

criteronNR=1e-3
maxIt=10




pref=np.zeros(2)
pref[0]=0.1
pref[1]=0.2



class solver:
    def __init__(self) -> None:
        self.KSP=PETSc.KSP(MPI.COMM_WORLD).create()
    def setMat(self,aMat):
        self.KSP.setOperators(aMat)
        self.KSP.setType(PETSc.KSP.Type.PREONLY)
        self.KSP.getPC().setType(PETSc.PC.Type.LU)
    def solve(self,rhs,sol):
        self.KSP.solve(rhs,sol)
    def solveNoNeg(self,rhs,sol):
        self.solve(rhs,sol)
        sol.array[sol.array < 0.] = 0.

aSolver=solver()

def doANonLinStep(a,L,sol):
    error=criteronNR+1
    numIt=0
    uk.x.array[:]=utm1.x.array[:]
    while(error>criteronNR and numIt<maxIt):
        M=assemble_matrix(form(a))
        M.assemble()
        rhs=assemble_vector(form(L))
        aSolver.setMat(M)
        aSolver.solveNoNeg(rhs,sol)
        error=norm(sol.array - prevSol.array)
        prevSol.array[:]=sol.array[:]
        numIt+=1
        print("doANonLinStep "+str(numIt)+" "+str(error))
        uk.x.array[:]=sol.array[:]
    if (error > 10*criteronNR):
        print("Newton failed "+str(error))




def computeJ():
    print("begin", sys._getframe().f_code.co_name)
    J=0.0
    step=0
    current_time=T0;
    final_time=TF;
    while (current_time < final_time):
        current_time = current_time + dt
        step=step+1
        if (step==nTi):
            read_step(buf,"SAVE","u",step)
            read_step(uref,"REF","u",step)
            computev(buf,uref,step)
            J=J+dt*assemble_scalar(form(v*dx))
            print("J="+str(J))
    return J



def computeJForMIN(p1):
    f=open("trace.txt","a")
    simul(p1)
    os.system("cp SAVE/*u* SAVEUp")
    return computeJ()

def computeJac(p1):
    g=dadjoint()
    f=open("trace.txt","a")
    f.write(str(g.dot(g)))
    f.write("\n")
    f.close()
    return g



XIC=0.0
YIC=0.0
x = None
ic="np.exp(-16.0*(np.square(x[0]-XIC)+np.square(x[1]-YIC)))"
def initial_condition(x):
    return eval(ic)

def simul(p):
    global lastStep,R,XIC,YIC
    print("simul ",p)
    XIC=p[0]
    YIC=p[1]
    A = u*w*dx+ dt*d2D*inner(grad(u),grad(w))*dx- dt*R*(1-2*uk)*u*w*dx +dt*mu*u*w*dx
    L = utm1*w*dx + dt*R*uk*uk*w*dx
    step=0
    current_time=T0;
    final_time=TF;
    utm1.interpolate(initial_condition)
    vtkfile_u = VTKFile(msh.comm,'RESULTS/solution_u.pvd',"w")
    vtkfile_u.write_function(utm1, step)
    save_step(utm1,"SAVE","u",step)
    while (current_time < final_time):
        doANonLinStep(A,L,sol)
        utm1.x.array[:]=sol.array[:]
        current_time = current_time + dt
        step=step+1
        vtkfile_u.write_function(utm1, step)
        save_step(utm1,"SAVE","u",step)
    lastStep=step
    vtkfile_u.close()


def solveAdjoint(FA,LA,solAdjoint):
    M=assemble_matrix(form(FA))
    M.assemble()
    rhs=assemble_vector(form(LA))
    aSolver.setMat(M)
    aSolver.solve(rhs,solAdjoint)

def computev(buf,uref,step):
    if (step==nTi):
       v.x.array[:]=0.5*np.square(buf.x.array[:]-uref.x.array[:])/dt
    else:
       v.x.array.fill(0.0)

def computevprim(buf,uref,step):
    if (step==nTi):
        vprim.x.array[:]=(buf.x.array[:]-uref.x.array[:])/dt
    else:
        vprim.x.array.fill(0.0)

def dadjoint():
    print("begin", sys._getframe().f_code.co_name)
    g=np.zeros(2)
    FA=pa*w*dx +  dt*d2D*inner(grad(pa),grad(w))*dx-dt*R*pa*w*dx +2*dt*u_a*R*pa*w*dx + mu*pa*w*dx
    LA= dt*vprim*w*dx + p_tp1*w*dx
    #LA= p_tp1*w*dx
#    LA= dt*uMask*(up1-uref)*w*dx + p_tp1*w*dx
    step=nTi
    current_time=TF;
    final_time=T0;
    p_tp1.x.array.fill(0.0)
    read_step(u_a,"SAVEUp","u",step)
    read_step(uref,"REF","u",step)
    computevprim(u_a,uref,step)
    dJ=0
    while (current_time > 0):
        solveAdjoint(FA,LA,sol)
        p_tp1.x.array[:]=sol.array[:]
        current_time=current_time-dt
        step=step-1
        bufPtp1=0.0
        bufUa=0.0
        bufG=0.0
        if (current_time<1e-9):
           dic="2*16.0*(x[0]-XIC)*np.exp(-16.0*(np.square(x[0]-XIC)+np.square(x[1]-YIC)))"
           def d_initial_condition(x):
              return eval(dic)
           utm1.interpolate(d_initial_condition)
           g[0]=assemble_scalar(form(utm1*p_tp1*dx))
           dic="2*16.0*(x[1]-YIC)*np.exp(-16.0*(np.square(x[0]-XIC)+np.square(x[1]-YIC)))"
           def d_initial_condition(x):
              return eval(dic)
           utm1.interpolate(d_initial_condition)
           g[1]=assemble_scalar(form(utm1*p_tp1*dx))
        read_step(u_a,"SAVEUp","u",step)
        read_step(uref,"REF","u",step)
        computevprim(u_a,uref,step)
    return g



def buildRef():
   simul(pref)
   os.system("cp SAVE/* REF/")
   os.system("cp RESULTS/* REF/")

