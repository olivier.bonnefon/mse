import matplotlib.pyplot as plt
import numpy as np
file1=open("traceX0.txt","r")
lines=file1.readlines()
nIt=len(lines)
xit=np.zeros(nIt)
yit=np.zeros(nIt)
xRef=np.zeros(1)
yRef=np.zeros(1)

xRef[0]=0.3
yRef[0]=0.7
#plt.xlim([0, 2])
#plt.ylim([0, 2])
plt.plot(xRef,yRef,'o')

for i in range(nIt):
    xit[i]=float(lines[i].split()[0])
    yit[i]=float(lines[i].split()[1])

plt.plot(xit,yit)
plt.show()
