
import ufl
from dolfinx import fem, io, mesh, plot
from dolfinx.fem.petsc import LinearProblem
from ufl import ds, dx, grad, inner
from dolfinx.fem import Function, assemble_scalar,form
from ufl import Measure,TrialFunction,TestFunction
from dolfinx.io import VTKFile
import numpy as np
import os
import sys
from mpi4py import MPI
from dolfinx.fem.petsc import assemble_matrix, assemble_vector
from petsc4py import PETSc
from numpy.linalg import norm
from dolfinx.geometry import BoundingBoxTree,compute_collisions,compute_colliding_cells
class solver:
    def __init__(self) -> None:
        self.KSP=PETSc.KSP(MPI.COMM_WORLD).create()
    def setMat(self,aMat):
        self.KSP.setOperators(aMat)
        self.KSP.setType(PETSc.KSP.Type.PREONLY)
        self.KSP.getPC().setType(PETSc.PC.Type.LU)
    def solve(self,rhs,sol):
        self.KSP.solve(rhs,sol)
    def solveNoNeg(self,rhs,sol):
        self.solve(rhs,sol)
        sol.array[sol.array < 0.] = 0.

aSolver=solver()
# Create meshes
xmin=0.0
ymin=0.0
xmax=2.0
ymax=1.0
msh = mesh.create_rectangle(comm=MPI.COMM_WORLD,
                            points=((xmin, ymin), (xmax, ymax)), n=(32, 16),
                            cell_type=mesh.CellType.triangle)
V1 = fem.FunctionSpace(msh, ("Lagrange", 1))

x = ufl.SpatialCoordinate(msh)
ic="np.exp(-3*(x[0]**2+x[1]**2))"
def initial_condition(x):
    return eval(ic)


def defineR(radius2,coord):
    dist2=pow(coord[0]-(xmax+xmin)*0.5),2)+pow(coord[1] -(ymax+ymin)*0.5),2)
    if (dist2>radius2):
        return 0
    else:
        return 1

vdefineR=np.vectorize(defineR,signature="(),(d)->()")
R.x.array[:]=vdefineR(p1,XYZ)

u = TrialFunction(V1)
#wp = Function(V1)
u_a = Function(V1)
v = Function(V1)
vprim = Function(V1)
u_aeps = Function(V1)
utm1 = Function(V1)
uk = Function(V1)
w = TestFunction(V1)
uref=Function(V1)
buf=Function(V1)
buf1=Function(V1)
buf2=Function(V1)
buf3=Function(V1)
R=Function(V1)
nDofs=V1.tabulate_dof_coordinates().shape[0]
XYZ=V1.tabulate_dof_coordinates().copy()

xref=np.array([0.0,0.0,0.0])
def dist2(x):
    aux=x-xref
    return (np.dot(aux,aux))

vdist2 = np.vectorize(dist2,signature="(d)->()")


def getNearestDofs(aX,aY):
    xref[0]=aX
    xref[1]=aY
    return np.argmin(vdist2(XYZ))
    


def save_step(u,REP,SUFFIX,step):
   np.save(REP+'/state'+SUFFIX+str(step),u.x.array)

def read_step(u,REP,SUFFIX,step):
    u.x.array[:] = np.load(REP+'/state'+SUFFIX+str(step)+'.npy')

T0=0
TF=1
dt=0.025
#p=np.array([10.0,2.0,1.0])
nTi=round(TF/dt)
J=0.0


# f(u)=p2*u(1-u/p3)
# f(u=uk+e)=p2*u(1-(uk+e)/p3)
#             =p2*u(1-uk/p3)+p2*u(-e/p3)
#             =p2*u(1-uk/p3)-p2*uk(u-uk)/p3 +O(e*e)
#             =p2*u(1-uk/p3)-p2*uk*u/p3 + p2*uk*uk/p3 +O(e*e)
#             =u*(p2-uk*(2*p2/p3)) + p2*uk*uk/p3 + O(e*e)
#
#on alors:
#f'(u)=p2(1-u/p3)-p2*u/p3
#     =p2 -2p2u/p3
#f(uk+e)=f(uk)+e*(p2-2p2*uk/p3)
#          =u(p2-2p2*uk/p3) +p2*uk(1-uk/p3)-uk(p2-2p2*uk/p3)
#          =u(p2-2p2*uk/p3) +p2*uk*uk/p3 
#
# a(u,v)=p2*(1-2uk/p3)*u*v
# L(v) = (p2/p3)*uk*uk*v

#a=u*w*dx + dt*p1*inner(grad(u),grad(w))*dx - dt*p2*(1-2*uk/p3)*u*w*dx
#L=dt*(p2/p3)*uk*uk*w*dx+utm1*w*dx



with open("../tools.py", mode="r", encoding="utf-8") as tools:
    code = tools.read()

exec(code)
# must be run to register in SAVE the current state,
#
#import defPi
D2d=10.0
def simul(valp):
    print("begin", sys._getframe().f_code.co_name)
    p1=valp[0]
    print("p1="+str(p1))
    a=u*w*dx + dt*D2d*inner(grad(u),grad(w))*dx - dt*p1*(1-2*uk)*u*w*dx
    L=dt*p1*uk*uk*w*dx+utm1*w*dx
    step=0
    current_time=T0;
    final_time=TF;
    utm1.interpolate(initial_condition)
    vtkfile_u =VTKFile(msh.comm,'RESULTS/solution_u.pvd',"w")
    vtkfile_u.write_function(utm1, step)
    save_step(utm1,"SAVE","u",step)
    while (current_time < final_time):
        doANonLinStep(a,L,sol)
        utm1.x.array[:]=sol.array[:]
        current_time = current_time + dt
        step=step+1
        vtkfile_u.write_function(utm1, step)
        save_step(utm1,"SAVE","u",step)
    vtkfile_u.close()
#build ref
def buildRef(pref):
    print("begin", sys._getframe().f_code.co_name," : we are building the ref (ie data) state for p=",pref)
    simul(pref)
    os.system("cp SAVE/* REF/;rm -f SAVE/*")
    os.system("cp RESULTS/* REF/;rm -f RESULTS/*")

#def de la fonction v



def computev(buf,uref,step):
    if (step==nTi):
        print("computevANYWHERE_ENDTIME end time")
        v.x.array[:]=0.5*(buf.x.array[:]-uref.x.array[:])*(buf.x.array[:]-uref.x.array[:])/dt
    else:
        v.x.array.fill(0.0)
        
def computevprim(buf,step):
    if (step==nTi):
        vprim.x.array[:]=(buf.x.array[:]-uref.x.array[:])/dt
    else:
        vprim.x.array.fill(0.)


# comput J using the current sate saved in SAVE and the ref in REF

dJdf=[]
def computedJdf(valp,eps,Jref):
    global dJdf
    for nump in range(0, valp.size):
        paux=np.copy(valp)
        paux[nump]=paux[nump]+eps[nump]
        simul(paux)
        Jeps=computeJ()
        dJdf.append((Jeps-Jref)/eps[nump])




def computeIntCircle(u,radius):
    bbtree = BoundingBoxTree(msh, 2)
    nPoints=10
    points = np.zeros((3, nPoints))
    cell_candidates = compute_collisions(bbtree, points.T)
    colliding_cells = compute_colliding_cells(msh, cell_candidates, points.T)
    points_on_proc=[]
    cells=[]
    lcmp=0
    for pt in points:
        pt[0]=radius*cos(2*lcmp*np.pi/nPoints)
        pt[1]=radius*sin(2*lcmp*np.pi/nPoints)
    for i, point in enumerate(points.T):
     if len(colliding_cells.links(i))>0:
         points_on_proc.append(point)
         cells.append(colliding_cells.links(i)[0])
    points_on_proc = np.array(points_on_proc, dtype=np.float64)
    return np.average(u.eval(points_on_proc, cells))*2*np.pi*radius
    
    
# assume u_a contain current the simu with param valp
#

def dadjoint(valp):
    print("begin", sys._getframe().f_code.co_name)
    global dJda
    p1=valp[0]
    FA=pa*w*dx +  dt*D2d*inner(grad(pa),grad(w))*dx - dt*p1*pa*w*dx +2*p2*dt*u_a*pa*w*dx
    LA= dt*vprim*w*dx + p_tp1*w*dx
    step=nTi
    current_time=TF;
    final_time=T0;
    p_tp1.x.array.fill(0.0)
#    save_step(p_tp1,"SAVE","p",step)
#    simul(p1)
#    vtkfile_u = File('RESULTS/solution_pp1.pvd',"compressed")
#    vtkfile_u <<(p_tp1,step)
    read_step(u_a,"SAVE","u",step)
    read_step(uref,"REF","u",step)
    computevprim(u_a,step)
    dJ1=0
    while (current_time > 0):
        solveAdjoint(FA,LA,sol)
        p_tp1.x.array[:]=sol.array[:]
        current_time=current_time-dt
        step=step-1
        #dJ1=dJ1+dt*assemble_scalar(form(p_tp1*dx))
        dJ1=dJ1+dt*computeIntCircle(p_tp1,p1)
        read_step(u_a,"SAVE","u",step)
        read_step(uref,"REF","u",step)
        computevprim(u_a,step)
    return np.array([dJ1])
 #       save_step(p_tp1,"SAVE","p",step)
 

