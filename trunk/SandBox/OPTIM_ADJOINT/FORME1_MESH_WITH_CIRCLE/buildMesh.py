from toolsEnv import computerEnv,MSEPrint
import MESH_IMPORT_GMSH.read as reader
import MESH_IMPORT_GMSH.buildMeshes as buildMeshes
from dolfinx.io import XDMFFile,VTKFile

from dolfinx.fem import Function,FunctionSpace
from mpi4py import MPI
import numpy as np
import gmsh
import meshio
import sys
import numpy as np
import os.path
import subprocess
reader.verbose=0
buildMeshes.verbose=0
#rectangular geometry
xmin=0.0
ymin=0.0
xmax=2.0
ymax=1.0
#center
xcenter=0.5*(xmin+xmax)
ycenter=0.5*(ymin+ymax)
radius=0.2
nCirclePoints=12*2
mesh_size=0.05
with open("_buildMesh.py") as f:
    exec(f.read())
