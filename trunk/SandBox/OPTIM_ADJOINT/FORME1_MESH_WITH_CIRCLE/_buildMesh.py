nPoints=4+nCirclePoints
#x,y,z coordinates
points=np.zeros(nPoints*3).reshape((nPoints,3))
#rectangle
points[0][0]=xmin;points[0][1]=ymin
points[1][0]=xmax;points[1][1]=ymin
points[2][0]=xmax;points[2][1]=ymax
points[3][0]=xmin;points[3][1]=ymax
curPt=4
angle=0
angleStep=2*np.pi/(1.0*nCirclePoints)
#add points for the circle
for i in range(nCirclePoints):
    points[curPt][0]=xcenter+radius*np.cos(angle)
    points[curPt][1]=ycenter+radius*np.sin(angle)
    curPt+=1
    angle+=angleStep

gmsh.initialize(sys.argv)
gmsh.model.add("create_mesh")
#add points in gmsh
gmsh_points = [gmsh.model.geo.addPoint(point[0],point[1],point[2], meshSize=mesh_size) for point in points]
gmsh_lines = []
polyLine = []
#add rectangle in gmsh
for i in range(3):
    gmsh_lines.append(gmsh.model.geo.addLine(i+1,i+2))

gmsh_lines.append(gmsh.model.geo.addLine(4,1))

#add circle line in gmsh
gmsh_lines_C1=[]
for i in range(nCirclePoints-1):
    gmsh_lines_C1.append(gmsh.model.geo.addLine(i+5,i+6))

gmsh_lines_C1.append(gmsh.model.geo.addLine(4+nCirclePoints,5))

gmsh_loop = []
loop = list(range(5,5+nCirclePoints))
gmsh_loop.append(gmsh.model.geo.addCurveLoop(loop))
gmsh_surface = [gmsh.model.geo.addPlaneSurface([gmsh_loop[0]])]



loop = list(range(1,5))
gmsh_loop.append(gmsh.model.geo.addCurveLoop(loop))
gmsh_surface.append(gmsh.model.geo.addPlaneSurface([gmsh_loop[1],gmsh_loop[0]]))

gmsh.model.geo.synchronize()
gmsh.model.mesh.generate(2)
gmsh.model.addPhysicalGroup(1, gmsh_lines, -1, "Cout")
ph=gmsh.model.addPhysicalGroup(1, gmsh_lines_C1, -1, "Cin")
ps = gmsh.model.addPhysicalGroup(2, gmsh_surface, -1, "W1")
st = "toto.med"
gmsh.write(st)

out = subprocess.run('gmsh -2 ' + st, shell=True)

#out = subprocess.run('/usr/bin/gmsh -2 ' + st, shell=True)
gmsh.finalize()

st_mesh_msh = "toto.msh"



msh = meshio.read(st_mesh_msh)
listKeys= list(msh.field_data.keys())
aKeyEdges="Cin"

wire_dict={}
msh.field_data[aKeyEdges]
indexEdegInCellData=listKeys.index(aKeyEdges)
wire_dict[aKeyEdges]=msh.field_data[aKeyEdges]
cmpelem=0
for indexCin in gmsh_lines_C1:
    indexCin=indexCin-1
    if cmpelem == 0:
        line_cells = msh.cells[indexCin].data
        line_data_l=np.full(msh.cells[indexCin].data.shape[0],msh.field_data[aKeyEdges][0])
    else:
        line_cells = np.concatenate((line_cells,msh.cells[indexCin].data))
    cmpelem+=msh.cells[indexCin].data.shape[0]

line_data_l=np.full(cmpelem,msh.field_data[aKeyEdges][0])
line_mesh = meshio.Mesh(points=msh.points[:, :2],
                                    cells=[("line", line_cells)],
                                    cell_data={"boundaries":[line_data_l]})
meshio.write("Cin.xdmf", line_mesh)

aKeyOfW="W1"
wire_dict[aKeyOfW]=msh.field_data[aKeyOfW]
triangle_cells =  msh.cells[28].data
triangle_cells= np.concatenate((triangle_cells,msh.cells[29].data))
triangle_data_l=np.full(triangle_cells.shape[0],msh.field_data[aKeyOfW][0])

triangle_mesh = meshio.Mesh(points=msh.points[:, :2],
                            cells=[("triangle", triangle_cells)],
                            cell_data={"subdomain":[triangle_data_l]},
                            field_data=wire_dict)
meshio.write("surface.xdmf", triangle_mesh)

