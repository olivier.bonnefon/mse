


class solver:
    def __init__(self) -> None:
        self.KSP=PETSc.KSP(MPI.COMM_WORLD).create()
    def setMat(self,aMat):
        self.KSP.setOperators(aMat)
        self.KSP.setType(PETSc.KSP.Type.PREONLY)
        self.KSP.getPC().setType(PETSc.PC.Type.LU)
    def solve(self,rhs,sol):
        self.KSP.solve(rhs,sol)
    def solveNoNeg(self,rhs,sol):
        self.solve(rhs,sol)
        sol.array[sol.array < 0.] = 0.

aSolver=solver()

pref=None
sol=PETSc.Vec().createSeq(nDofs)
print(sol)
prevSol=PETSc.Vec().createSeq(nDofs)
prevSol.array.fill(0.0)
criteronNR=1e-3
maxIt=10
def doANonLinStep(a,L,sol):
    error=criteronNR+1
    numIt=0
    uk.x.array[:]=utm1.x.array[:]
    while(error>criteronNR and numIt<maxIt):
        M=assemble_matrix(form(a))
        M.assemble()
        rhs=assemble_vector(form(L))
        aSolver.setMat(M)
        aSolver.solveNoNeg(rhs,sol)
        error=norm(sol.array - prevSol.array)
        prevSol.array[:]=sol.array[:]
        numIt+=1
        print("doANonLinStep "+str(numIt)+" "+str(error))
        uk.x.array[:]=sol.array[:]
    if (error > 10*criteronNR):
        print("Newton failed "+str(error))


def computeJ():
    print("begin", sys._getframe().f_code.co_name)
    J=0.0
    step=0
    current_time=T0;
    final_time=TF;
    while (current_time < final_time):
        current_time = current_time + dt
        step=step+1
        read_step(buf,"SAVE","u",step)
        read_step(uref,"REF","u",step)
        computev(buf,uref,step)
        J=J+dt*assemble_scalar(form(v*dx))
        print("J="+str(J))
    return J

def solveAdjoint(FA,LA,solAdjoint):
    M=assemble_matrix(form(FA))
    M.assemble()
    rhs=assemble_vector(form(LA))
    aSolver.setMat(M)
    aSolver.solve(rhs,solAdjoint)


def computeJForMIN(p1):
    f=open("trace.txt","a")
    f.write(str((pref - p1).dot(pref - p1)))
    f.write(" ")
    f.close()
    simul(p1)
    os.system("cp SAVE/*u* SAVEUp")
    return computeJ()

def computeJac(p1):
    g=dadjoint(p1)
    f=open("trace.txt","a")
    f.write(str(g.dot(g)))
    f.write("\n")
    f.close()
    return g
