import MESH_IMPORT_GMSH.read as reader
import MESH_IMPORT_GMSH.buildMeshes as buildMeshes

import study
import dynamicalSystem
import interaction2D1D
import system
import simulator
from dolfinx.io import XDMFFile,VTKFile
from ufl import Measure,TrialFunction,TestFunction
from petsc4py.PETSc import ScalarType
from dolfinx.fem import form,assemble_scalar,Constant,form,Expression
from dolfinx.fem.petsc import assemble_matrix, assemble_vector, apply_lifting, create_vector, set_bc
from dolfinx.fem import Function

import numpy as np
import math
from petsc4py import PETSc
import time

reader.verbose=0
buildMeshes.verbose=0

aTopo=reader.geomTopoReader("../epi_nl/trunk/build_mesh_ebola/shapeToMse/GEODATA/GLS/")
aMeshesDir="./MESHES/GLS/"
#buildMeshes.buildXdmf(aTopo,aMeshesDir,7e3)
aSystem=system.system()
aStudy=study.study(aTopo,aMeshesDir,aSystem)
ds2d1=dynamicalSystem.dynamicalSystem(aStudy,1,2,aMeshesDir+"surface1.xdmf")
#ds1d1=dynamicalSystem.dynamicalSystem(aStudy,1,1,aMeshesDir+"boundary1.xdmf")
mesh2d=ds2d1.dolfinMesh
vtkf = VTKFile(mesh2d.comm, "SIMU/mesh2d.pvd", "w")
vtkf.write(mesh2d)
vtkf.close()
N=ds2d1.nbDofs
S=TrialFunction(ds2d1.vSpace)
I=TrialFunction(ds2d1.vSpace)
u=TrialFunction(ds2d1.vSpace)
v=TestFunction(ds2d1.vSpace)

Stm1=Function(ds2d1.vSpace)
Itm1=Function(ds2d1.vSpace)
IConvol=Function(ds2d1.vSpace)
resS = Function(ds2d1.vSpace)
FBuf=Function(ds2d1.vSpace)

def f_fct1(x):
    nElem=x.shape[1]
    values = np.zeros((1, nElem))
    for i in range(nElem):
            values[0][i]=1
    return values

def f_fct0(x):
    nElem=x.shape[1]
    values = np.zeros((1, nElem))
    return values

def f_fctI0(x):
    nElem=x.shape[1]
    print(nElem)
    values = np.zeros((1, nElem))
    for i in range(nElem):
        if (math.fabs(x[0,i]-382644) + math.fabs(x[1,i]-953296)<25000):
            values[0][i]=0.001
    return values



vtkf = VTKFile(mesh2d.comm, "SIMU/SI0.pvd", "w")
vtkf.write_function(Stm1,0)
vtkf.write_function(Itm1,1)
vtkf.close()


dx=ds2d1.dx
dt=0.01
theta=0.5
t=0
T=35
lambda0=1e-4
alpha=Constant(mesh2d, ScalarType(1/dt))
alphaTheta=Constant(mesh2d, ScalarType(1/dt+1-theta))
thetaConvol=Constant(mesh2d, ScalarType(0.5))
mI=alphaTheta*u*v*dx
mS=alpha*u*v*dx
mSForm=form(mS)
#mMass=u*v*dx
#mMassForm=form(mMass)
matS=assemble_matrix(mSForm)
matS.assemble()
#matS.view()


solver = PETSc.KSP().create(mesh2d.comm)
solver.setOperators(matS)
solver.setType(PETSc.KSP.Type.PREONLY)
solver.getPC().setType(PETSc.PC.Type.LU)


import torch
usePytorche=1
useDouble=0
matK=torch.randn(N, N).type(torch.FloatTensor)
matK=torch.randn(N, N).type(torch.DoubleTensor)

dofsCoordinates=ds2d1.vSpace.tabulate_dof_coordinates()

def JKernel(i,j):
    xi=dofsCoordinates[i,0]
    yi=dofsCoordinates[i,1]
    dx=dofsCoordinates[j,0]-xi
    dy=dofsCoordinates[j,1]-yi
    return 1/math.pow(1+(math.sqrt(dx*dx+dy*dy)/5000),3)

from KERNEL_TOOLS.buildKMatc import buildK,KU

if(1):
    buildK(dofsCoordinates,matK.numpy())
    '''
    for i in range(0,N):
        print(i)
        for j in range(0,N):
            matK[i,j]=JKernel(i,j)
    '''
    torch.save(matK, 'matDK5000.pt')
    torch.save(matK.type(torch.FloatTensor), 'matFK5000.pt')

if (not useDouble):
    matK=torch.load('matFK5000.pt')
    if (not usePytorche):
        matK=torch.load('matFK5000.pt').type(torch.DoubleTensor)
else:
    matK=torch.load('matDK5000.pt')
    if (usePytorche):
        print("can t use double with pytorch")
        exit(0)

vtkf = VTKFile(mesh2d.comm, "SIMU/K.pvd", "w")
cmp=0
for i in {10,100,1000,2000,10000,1500,17,3}:
    FBuf.x.array[:]=matK[:,i]
    vtkf.write_function(FBuf,cmp)
    cmp=cmp+1

vtkf.close()


def pyTorchToDolfinx(aV_,aV):
    for i in range(N):
        aV.x.array[i]=aV_[i]



nBuildConvol=0
timeConvol=0.0

def buildConvolFuncCython(dolfinxFunc,ConvoledDolfinxFunc):
    global nBuildConvol,timeConvol
    #contruire Mass*df
    L=dolfinxFunc*v*dx
    LL=form(L)
    MassDf= assemble_vector(LL)
    #produit KMassDf
    t1=time.time()
    KU(matK.numpy(),MassDf.array,ConvoledDolfinxFunc.x.array)
    timeConvol=timeConvol+(time.time()-t1)
    nBuildConvol=nBuildConvol+1

def buildConvolFuncPytorch(dolfinxFunc,ConvoledDolfinxFunc):
    global nBuildConvol,timeConvol
    df_=torch.from_numpy(dolfinxFunc.x.array)
    #contruire Mass*df
    L=dolfinxFunc*v*dx
    LL=form(L)
    MassDf= assemble_vector(LL)
    MassDf_=torch.from_numpy(MassDf.array).type(torch.FloatTensor)
    t1=time.time()
    ConvolDf_=torch.matmul(matK,MassDf_)
    timeConvol=timeConvol+(time.time()-t1)
    nBuildConvol=nBuildConvol+1
    pyTorchToDolfinx(ConvolDf_,ConvoledDolfinxFunc)

def nonNegs(VV):
    for i in range(N):
        if (VV.x.array[i]<0):
            VV.x.array[i]=0

def nonPoss(VV):
    for i in range(N):
        if (VV.x.array[i]>0):
            VV.x.array[i]=0

def nonSomNeg(VV,WW):
    for i in range(N):
        if (WW.x.array[i]+lambda0*VV.x.array[i]<0):
            VV.x.array[i]=-WW.x.array[i]/lambda0

Itm1.interpolate(f_fctI0)
Stm1.interpolate(f_fct1)
vtkf = VTKFile(mesh2d.comm, "SIMU/Stm1.pvd","w")
vtki = VTKFile(mesh2d.comm, "SIMU/Itm1.pvd","w")
vtkic = VTKFile(mesh2d.comm, "SIMU/ConvolItm1.pvd","w")
vtkf.write_function(Stm1,0.0)
vtki.write_function(Itm1,0.0)

for nStep in range(32):
    FBuf =Function(ds2d1.vSpace)
    print(nStep)
    if (usePytorche):
        buildConvolFuncPytorch(Itm1,IConvol)
    else:
        buildConvolFuncCython(Itm1,IConvol)
    vtkic.write_function(IConvol,(nStep+1)*dt)
    #-1.0*((1-thetaConvol)*Sk+thetaConvol*Stm1)*Convolk*v
    #VBuf=Mass*Stm1
    rhsL=dt*((thetaConvol-1.0)*Stm1-thetaConvol*Stm1)*IConvol*v*dx
    rhsLL=form(rhsL)
    rhsS=assemble_vector(rhsLL)
    solver.solve(rhsS,resS.vector)
    nonPoss(resS)
    nonSomNeg(resS,Stm1)
    Stm1.x.array[:] = Stm1.x.array[:] + lambda0*resS.x.array[:]
    #nonNegs(Stm1)
    Itm1.x.array[:] = Itm1.x.array[:] - lambda0*resS.x.array[:]
    
    vtkf.write_function(Stm1,(nStep+1)*dt)
    vtki.write_function(Itm1,(nStep+1)*dt)

nameIfile="SaveI"+str(nStep)+"_pytorch"+str(usePytorche)+"_useDouble"+str(useDouble)+".npy"
fileI= open(nameIfile,'wb')
np.save(fileI,Itm1.x.array)
fileI.close()

vtkf.close()
vtki.close()
vtkic.close()

print("temps moyen de calcul de convolution="+str(timeConvol/nBuildConvol)+" FF do 0.3s")

compartSol=1
if (compartSol):
    useDouble=1
    usePytorche=0
    nameIfile="SaveI"+str(nStep)+"_pytorch"+str(usePytorche)+"_useDouble"+str(useDouble)+".npy"
    FBuf.x.array[:]=np.load(nameIfile)[:]
    useDouble=0
    nameIfile="SaveI"+str(nStep)+"_pytorch"+str(usePytorche)+"_useDouble"+str(useDouble)+".npy"
    Itm1.x.array[:]=np.load(nameIfile)[:]
    Stm1.x.array[:]=FBuf.x.array[:]-Itm1.x.array[:]
    vtkf= VTKFile(mesh2d.comm, "SIMU/diffDFPytorche.pvd","w")
    vtkf.write_function(Stm1,0.0)
    usePytorche=1
    nameIfile="SaveI"+str(nStep)+"_pytorch"+str(usePytorche)+"_useDouble"+str(useDouble)+".npy"
    Itm1.x.array[:]=np.load(nameIfile)[:]
    Stm1.x.array[:]=FBuf.x.array[:]-Itm1.x.array[:]
    vtkf.write_function(Stm1,1.0)
    vtkf.close()  