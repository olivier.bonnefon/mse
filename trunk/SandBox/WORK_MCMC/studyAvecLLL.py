from MSE.study import study
import numpy as np
nParams=3
aStudy=study(nParams)

#aStudy.computeLogLikelyhood(np.array([0.1,0.2,1,1])) 

from MSE.METROPOLIS import metro
binf=np.array([0.5,0.02,1/3])
bsup=np.array([5,1,1])
initGuess=np.array([2.25,0.1,2/3])
aMetro=metro(binf,bsup,initGuess,200000,aStudy)

aMetro.runMetro(1)
