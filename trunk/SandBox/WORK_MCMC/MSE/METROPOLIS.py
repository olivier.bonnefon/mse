import numpy as np
import MSE.study as study

class metro:
    def __init__(self,inf,sup,initGuess,cptmax,aStudy) -> None:
        #link with MSE
        self.study=aStudy
        # priors (parameter bounds)
        self.initGuess=initGuess
        self.binf=inf
        self.bsup=sup
        self.nbp=len(self.binf)
        self.cptmax=cptmax
        self.cpt=0
        self.nom="toto"
        # proposals (standard deviation)
        self.s_p = (self.bsup - self.binf) / 1e3
        # Generate a random seed using SeedSequence
        entropy = np.random.SeedSequence()
        #seed = entropy.entropy_as_int()
        # Set the random seed for both rand and randn
        np.random.seed(int(entropy.entropy)%2**32 )
        # initial guess
        self.theta0=self.binf + np.random.rand(1, self.nbp) * (self.bsup - self.binf)
        self.LL = np.zeros(self.cptmax)
        self.ratio = np.zeros(self.cptmax)
        self.theta=np.zeros((self.cptmax,self.nbp))
        self.acc=0
    def runMetro(self, restart=0):
        # initial guess
        #self.theta[0] = self.binf + np.random.rand(1, self.nbp) * (self.bsup - self.binf)
        if (restart==0):
            self.theta[0] =self.initGuess
            self.LL[self.cpt] = self.study.computeLL(self.theta[0])
        else:
            curLL=np.load(self.nom+"LL.npy")
            curTheta=np.load(self.nom+"Theta.npy")
            for ll in curLL:
                if (ll==0):
                    break
                else:
                    self.LL[self.cpt]=curLL[self.cpt]
                    self.theta[self.cpt]=curTheta[self.cpt]
                    self.cpt=self.cpt+1
            self.cpt=self.cpt-1
            if (self.cpt==0):
                self.theta[0] =self.initGuess
                self.LL[self.cpt] = self.study.computeLL(self.theta[0])
        while (self.cpt<(self.cptmax-1)):
            # proposals
            thetab=self.proposal(self.theta[self.cpt])
            # compute the likelihood
            LLb = self.study.computeLL(thetab)
            # accept or reject
            deltav = LLb - self.LL[self.cpt]
            self.cpt = self.cpt + 1
            w = np.random.rand(1)
            if np.log(w) < deltav:  # accept
                self.acc=self.acc+1
                self.theta[self.cpt] = thetab
                self.LL[self.cpt] = LLb
            else:
                self.theta[self.cpt, :] = self.theta[self.cpt - 1]
                self.LL[self.cpt] = self.LL[self.cpt - 1]
            self.ratio[self.cpt] = self.acc / self.cpt  # accept rate
            if self.cpt % 10 == 0:
                np.save(self.nom+"LL",self.LL)
                np.save(self.nom+"Theta",self.theta)
        np.save(self.nom+"LL",self.LL)
        np.save(self.nom+"Theta",self.theta)
    def proposal(self,X):
        Thetab = X + self.s_p * np.random.randn (self.nbp)
        while self.priorF(Thetab) == 0:  # check the prior
            Thetab = X + self.s_p * np.random.randn(self.nbp)
        return Thetab
    def priorF(self,X):
        return np.prod(X <= self.bsup) * np.prod(X >= self.binf)
    
