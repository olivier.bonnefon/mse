
import numpy as np

# for // computation
ns = input('Enter simulation number: ')
nom = f'MCMC_chain_{ns}'

# priors (parameter bounds)
np = len(binf)
cptmax = int(1e6)  # maximum number of iterations
binf = binf
bsup = bsup
priorF = lambda X: np.prod(X <= bsup) * np.prod(X >= binf)

# proposals (standard deviation)
s_p = (bsup - binf) / 1e3

# initial guess
Theta0 = binf + np.random.rand(1, np) * (bsup - binf)
while priorF(Theta) == 0:  # check the prior
    Theta0 = binf + np.random.rand(1, np) * (bsup - binf)

# initial log-likelihood
cpt = 0
LL = np.zeros(cptmax)
ratio = np.zeros(cptmax)
LL[cpt] = computell(Theta0)
Theta[0, :] = Theta0

# main loop: Metropolis-Hasting algo
stop = 0
acc = 0  # accept rate

cptratio = 1
while stop == 0:
    # proposals
    Thetab = Theta[cpt, :] + s_p * np.random.randn(1, np)
    while priorF(Thetab) == 0:  # check the prior
        Thetab = Theta[cpt, :] + s_p * np.random.randn(1, np)
    # compute the likelihood
    LLb = computell(dx, Thetab, tobs, Nobs, Mobs, X, counts, fdist)
    # accept or reject
    deltav = LLb - LL[cpt]
    cpt = cpt + 1
    w = np.random.rand(1)
    if np.log(w) < deltav:  # accept
        acc = acc + 1
        Theta[cpt, :] = Thetab
        LL[cpt] = LLb
    else:  # reject
        Theta[cpt, :] = Theta[cpt - 1, :]
        LL[cpt] = LL[cpt - 1]
    if cpt % 1000 == 1:
        np.save(nom)
    if cpt > cptmax:
        stop = 1
    ratio[cpt] = acc / cpt  # accept rate


-- 
Cet e-mail a été vérifié par le logiciel antivirus d'Avast.
www.avast.com
