from math import exp
import CUBA500500.likelihood as ll
import numpy as np
class study:
    def __init__(self,nParams) -> None:
        self.datas=np.load("../simuHomoEds.npy")

    def computeLL(self,theta):
        return ll.likelihood(theta,self.datas)

