import numpy as np
import sys
import subprocess
import os.path
import gmsh
import meshio

import MESH_IMPORT_GMSH.edge as Edge
import MESH_IMPORT_GMSH.wire as Wire

st_mesh = 'mesh'
showWires=1
showSave=0
geo_repertory = "./GEOMETRY/SIMPLE2/"
target_directory="./MESHES/SIMPLE2/"
mesh_size = 0.1
line = subprocess.check_output(['wc', '-l', geo_repertory+"Points.txt"])
aux=line.strip().split()
nbPt=int(aux[0])+1
Points=np.zeros((nbPt,3))
labelPt=[]
labelPt.append("bidon")
with open(geo_repertory+"Points.txt", "r") as dataFile:
    for line in dataFile:
        aux = line.strip().split()
        numPt=int(aux[0])
        print("Point="+str(numPt))
        Points[numPt,0]=float(aux[1])
        Points[numPt,1]=float(aux[2])
        labelPt.append(aux[3])

Edges=[]

aE=Edge.Edge(0,0,0,0,"",0)
Edges.append(aE)
cmpE=1
if (showSave):
    edgefile=geo_repertory+"EdgesSaved.txt"
else:
    edgefile=geo_repertory+"Edges.txt"

with open(edgefile, "r") as dataFile:
    for line in dataFile:
        aux = line.strip().split()
        typeEdge=int(aux[0])
        print("Edge="+str(aux))
        numNb=int(aux[1])
        numNe=int(aux[2])
        numPt=int(aux[3])
        label=aux[3+1+numPt]
        numEdge=int(aux[3+1+numPt+1])
        aE=Edge.Edge(numEdge,numNb,numNe,numPt,label,typeEdge)
        #Edges.append(aE)
        num=1
        while (num<=numPt):
            aE.add_pt(int(aux[3+num]))
            num=num+1
        cmpE=cmpE+1
        Edges.append(aE)

print("cmpE="+str(cmpE))
Wires=[]
cmpW=1
WireDrawPoint=1
WireEmptyPoly=0
WireWithName=1
if (showSave):
    wireFile=geo_repertory+"WiresSaved.txt"
else:
    wireFile=geo_repertory+"Wires.txt"

print("len(Edges)="+str(len(Edges)))
if (os.path.exists(wireFile) and showWires):
    with open(wireFile, "r") as dataFile:
        for line in dataFile:
            aux = line.strip().split()
            print("Wire="+str(aux))
            numW=int(aux[0])
            nbE=int(aux[1])
            W=Wire.Wire(numW,nbE,"W"+str(numW))
            num=1
            print("numW="+str(numW)+"nb Edges="+str(nbE))
            while(num<=nbE):
                print("rep "+str(num)+" "+str(nbE))
                reverse=0
                numE=int(aux[1+num])
                print(numE)
                if (numE<-0.1):
                    reverse=1
                    numE=-numE
                print("num edge="+str(numE))
                E=Edges[numE]
                W.add_edge(Edges[numE],reverse)
                num=num+1
            W.label=aux[num+1]
            cmpW=cmpW+1
            Wires.append(W)
else:
    print(wireFile+" doesn t exist")


#############################
## Write Mesh for all domains
#############################

# Create gmsh geometric model
gmsh.initialize(sys.argv)
gmsh.model.add("create_mesh")

# Create Mesh Points
gmsh_points = [gmsh.model.geo.addPoint(Points[i_points][0],Points[i_points][1],Points[i_points][2], meshSize=mesh_size)
           for i_points in range(len(Points))]

# Create Mesh Edges
gmsh_lines = []
for i_edges in range(len(Edges)):
    if (Edges[i_edges].nbPt<1):
        gmsh_lines.append(gmsh.model.geo.addLine(gmsh_points[Edges[i_edges].ptb], gmsh_points[Edges[i_edges].pte]))
    else:
        spline = [gmsh_points[Edges[i_edges].ptb]]
        for i_points in range(Edges[i_edges].nbPt):
            spline.append(gmsh_points[Edges[i_edges].pts[i_points]])
        spline.append(gmsh_points[Edges[i_edges].pte])
        gmsh_lines.append(gmsh.model.geo.addSpline(spline))

# Create Mesh Wires
test_edges = np.zeros(len(Edges))
gmsh_loop = []
gmsh_1D_loops = []
for i_wires in range(len(Wires)):
    loop = []
    for i_edges in range(len(Wires[i_wires].edges)):
        if (test_edges[Wires[i_wires].edges[i_edges]] == 0):
            test_edges[Wires[i_wires].edges[i_edges]] = 1
        if (Wires[i_wires].reverse[i_edges]):
            loop.append(-gmsh_lines[Wires[i_wires].edges[i_edges]])
        else:
            loop.append(gmsh_lines[Wires[i_wires].edges[i_edges]])
    gmsh_loop.append(gmsh.model.geo.addCurveLoop(loop))
    gmsh_1D_loops.append(loop)

gmsh_surface = [gmsh.model.geo.addPlaneSurface([gmsh_loop[i_loop]])
        for i_loop in range(len(Wires))]

# Generate 2D Mesh with triangle elements
gmsh.model.geo.synchronize()
gmsh.model.mesh.generate(2)
# Create Physical Groups for Edges
for i_edges in range(1,len(Edges)):
    if (test_edges[i_edges]):
        pl = gmsh.model.addPhysicalGroup(1, [gmsh_lines[i_edges]])
        gmsh.model.setPhysicalName(1, pl, 'E'+str(Edges[i_edges].id))

# Create Physical Groups for Wires
for i_loop in range(len(Wires)):
    ps = gmsh.model.addPhysicalGroup(2, [gmsh_surface[i_loop]])
    gmsh.model.setPhysicalName(2, ps, 'W'+str(Wires[i_loop].id))
    print("W"+str(Wires[i_loop].id)+"="+str(ps))

#ne fonctionne pas, car un element ne peut avoir qu'une seule marque
#ps1d = gmsh.model.addPhysicalGroup(1, gmsh_1D_loops[0])
#gmsh.model.setPhysicalName(1, ps1d, 'L0')
#ps1d = gmsh.model.addPhysicalGroup(1, gmsh_1D_loops[1])
#gmsh.model.setPhysicalName(1, ps1d, 'L1')
    #gmsh.model.setPhysicalName(1, ps1d, 'L'+str(Wires[i_loop].id))

# Write MSH Mesh
st = target_directory+st_mesh + '.med'
gmsh.write(st)
out = subprocess.run('gmsh -2 ' + st, shell=True)
gmsh.finalize()
st_mesh_msh = target_directory+st_mesh + '.msh'



#def importMeshDolfin(self, st_mesh_msh = 'mesh.msh'):

msh = meshio.read(st_mesh_msh)

msh.field_data = { key.strip() : value for key, value in msh.field_data.items() } # Remove white space in key

#list of geom label E1,E2,..,W1,W2
listKeys= list(msh.field_data.keys())
for aWire in Wires:
    #Building line XDMF
    flag_type_line = 0
    wire_dict={}
    for iE in aWire.edges:
        aKeyEdgesOfW="E"+str(iE)
        wire_dict[aKeyEdgesOfW]=msh.field_data[aKeyEdgesOfW]
        indexEdegInCellData=listKeys.index(aKeyEdgesOfW)
        if flag_type_line == 0:
            flag_type_line=1
            line_cells = msh.cells[indexEdegInCellData].data
            line_data_l=np.full(msh.cells[indexEdegInCellData].data.shape[0],msh.field_data[aKeyEdgesOfW][0])
        else:
            line_cells=np.concatenate((line_cells,msh.cells[indexEdegInCellData].data))
            line_data_l=np.concatenate((line_data_l,np.full(msh.cells[indexEdegInCellData].data.shape[0],msh.field_data[aKeyEdgesOfW][0])))
        line_mesh = meshio.Mesh(points=msh.points[:, :2],
                                cells=[("line", line_cells)],
                                cell_data={"boundaries":[line_data_l]})
        meshio.write(target_directory+"/boundary"+str(aWire.id)+".xdmf", line_mesh)
    #End line XDMF
    #Build triangle XDMF
    aKeyOfW=aWire.label # sould be W1, W2 ..
    wire_dict[aKeyOfW]=msh.field_data[aKeyOfW]
    indexTriangleInCellData=listKeys.index(aKeyOfW)
    triangle_cells =  msh.cells[indexTriangleInCellData].data
    triangle_data_l = np.full(msh.cells[indexTriangleInCellData].data.shape[0],msh.field_data[aKeyOfW][0])
    triangle_mesh = meshio.Mesh(points=msh.points[:, :2],
                                cells=[("triangle", triangle_cells)],
                                cell_data={"subdomain":[triangle_data_l]},
                                field_data=wire_dict)
    meshio.write(target_directory+"/surface"+str(aWire.id)+".xdmf", triangle_mesh)
    #End triangle XDMF

