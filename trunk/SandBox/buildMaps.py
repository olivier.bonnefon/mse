from dolfinx.io import XDMFFile,VTKFile
from dolfinx.fem import FunctionSpace
import matplotlib

import numpy as np
import sys
import subprocess
import os.path
import gmsh
import meshio

import MESH_IMPORT_GMSH.edge as Edge
import MESH_IMPORT_GMSH.wire as Wire
from mpi4py import MPI

fileName2d="./MESHES/SIMPLE2/surface1.xdmf"
fileName1d="./MESHES/SIMPLE2/boundary1.xdmf"

with XDMFFile(MPI.COMM_WORLD,fileName2d,"r") as xdmf:
        mesh2d = xdmf.read_mesh(name="Grid")

vtkf = VTKFile(mesh2d.comm, "mesh2d.pvd", "w")
vtkf.write(mesh2d)
vtkf.close()

with XDMFFile(MPI.COMM_WORLD,fileName1d,"r") as xdmf:
        mesh1d = xdmf.read_mesh(name="Grid")

vtkf = VTKFile(mesh1d.comm, "mesh1d.pvd", "w")
vtkf.write(mesh1d)
vtkf.close()

V1D=FunctionSpace(mesh1d,("CG",1))
V2D=FunctionSpace(mesh2d,("CG",1))

x1d=V1D.tabulate_dof_coordinates()
x2d=V2D.tabulate_dof_coordinates()

v1d2d=np.zeros(x1d.shape[0],dtype=np.int32)

import MAP_TOOLS.buildMapc as mc
mc.doMapc(x1d,x2d,v1d2d,1e-9)


#
# revoir l'export géométrique pour construire un mesh par domaine
# 
# sur un mesh exporter un fichier linesW0.h5 
# construire le mesh 2D 
# construire le mesh 1D à partir de linesW0.h5
# a partir de V2DW0, récupérer les coordonnées des dofs : xW0=V2DW0.tabulate_dof_coordinates()
# construire les maps entre les infos linesW0.h5 et xW0
#
# remarque: on ne peut pas construire les maps sans dolfinx, car dolfinx ré-index les noeuds.
##
import h5py
f1=h5py.File("lines.h5",'r')
f1.keys()
a_group_key = list(f.keys())[0]
coord= f1[a_group_key][()]
a_group_key = list(f1.keys())[1]
edges = f1[a_group_key][()]
a_group_key = list(f1.keys())[2]
mark = f1[a_group_key][()]
coord[edges[0][0]]
##

ds = ufl.Measure('ds', subdomain_data=ft, domain=myDolfinMesh)


cst=fem.form(1.0*ds)
fem.assemble_scalar(cst)

cst1=fem.form(1.0*ds(1))
fem.assemble_scalar(cst1)

cst2=fem.form(1.0*ds(2))
fem.assemble_scalar(cst2)

cst3=fem.form(1.0*ds(3))
fem.assemble_scalar(cst3)

cst4=fem.form(1.0*ds(4))
fem.assemble_scalar(cst4)

cst5=fem.form(1.0*ds(5))
fem.assemble_scalar(cst5)

cst6=fem.form(1.0*ds(6))
fem.assemble_scalar(cst6)

cst7=fem.form(1.0*ds(7))
fem.assemble_scalar(cst7)

DS = ufl.Measure('dS', subdomain_data=ft, domain=myDolfinMesh)


cst=fem.form(1.0*DS)
fem.assemble_scalar(cst)

cst1=fem.form(1.0*DS(1))
fem.assemble_scalar(cst1)

cst2=fem.form(1.0*DS(2))
fem.assemble_scalar(cst2)

cst3=fem.form(1.0*DS(3))
fem.assemble_scalar(cst3)

cst4=fem.form(1.0*DS(4))
fem.assemble_scalar(cst4)

cst5=fem.form(1.0*DS(5))
fem.assemble_scalar(cst5)

cst6=fem.form(1.0*DS(6))
fem.assemble_scalar(cst6)

cst7=fem.form(1.0*DS(7))
fem.assemble_scalar(cst7)


myDs=ds+DS(2)
cst=fem.form(1.0*myDs)
fem.assemble_scalar(cst)
