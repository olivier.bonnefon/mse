import kpp3x as model
import numpy as np
import os
import qnb

model.setCASE_OBS(model.SOMEWHERE_ENDTIME)

model.pref=np.array([0.1,1.5,.5])
model.buildRef(model.pref)

dim=3
x0=np.array([2,0.1,.1])
f=model.computeJForMIN(x0)
g=model.computeJac(x0)
dxmin=np.array([5.0,2.0,2.0])
precision=1e-10
lb=np.array([0.01,0.01,0.01])
ub=np.array([12.0,5.0,5.0])

dxmin=precision*(ub-lb)
noptim=0
df1=f
mode =1
imp=3
io=(noptim)*1000+10
niter=500
nsim=3*niter
reverse=1
noptim=0
wzf=np.zeros(1024)
wif=np.zeros(1024,dtype=np.int32)
epsabs_ori=1e-18
epsabs=np.array([epsabs_ori])
modenp=np.array([1],dtype=int)
nit=0
print("nit ",nit,"x ",x0,"f ",f,"g ",g)
qnb.n2qn1(x0,f,g,dxmin,df1,epsabs,imp,io,modenp,niter,nsim,lb,ub,wif,wzf,reverse,dim)
print("mode n2qn1: ",modenp[0])
while(modenp[0]>7):
    nit=nit+1
    f=model.computeJForMIN(x0)
    g=model.computeJac(x0)
    print("nit ",nit,"x ",x0,"f ",f,"g ",g)
    epsabs=np.array([epsabs_ori])
    nsim=3*niter
    qnb.n2qn1(x0,f,g,dxmin,df1,epsabs,imp,io,modenp,niter,nsim,lb,ub,wif,wzf,reverse,dim)
    #io=io+1
    print("mode n2qn1: ",modenp[0])
    
