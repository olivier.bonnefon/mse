from toolsEnv import computerEnv,MSEPrint
import MESH_IMPORT_GMSH.read as reader
import MESH_IMPORT_GMSH.buildMeshes as buildMeshes
from dolfinx.io import XDMFFile,VTKFile
from ufl import Measure
from dolfinx.fem import Function,FunctionSpace,assemble_scalar,form
from mpi4py import MPI
import numpy as np
import gmsh
import meshio
import sys
import numpy as np
import os.path
import subprocess
import MAP_TOOLS.buildMapc as mc

meshFile2D="/home/olivierb/nonlocal_2d1d/trunk/MESHES/SQUARE/surface1.xdmf"
meshFile1D="/home/olivierb/nonlocal_2d1d/trunk/MESHES/SQUARE/boundary1.xdmf"
meshFile2D="surface.xdmf"
meshFile1D="Cin.xdmf"
dolfinMesh2D=None

dolfinMesh1D=None

with XDMFFile(MPI.COMM_WORLD,meshFile2D,"r") as xdmf:
    dolfinMesh2D= xdmf.read_mesh(name="Grid")

with XDMFFile(MPI.COMM_WORLD,meshFile1D,"r") as xdmf:
    dolfinMesh1D= xdmf.read_mesh(name="Grid")

vSpace2D=FunctionSpace(dolfinMesh2D,("CG",1))
nbDofs2DPerComp=vSpace2D.tabulate_dof_coordinates().shape[0]

vSpace1D=FunctionSpace(dolfinMesh1D,("CG",1))
nbDofs1DPerComp=vSpace1D.tabulate_dof_coordinates().shape[0]

index1DTo2D=np.zeros((nbDofs1DPerComp,),dtype=np.int32)
mc.doMapc(vSpace1D.tabulate_dof_coordinates(),vSpace2D.tabulate_dof_coordinates(),index1DTo2D,1e-9)

dx=Measure('dx',dolfinMesh1D)

f2D=Function(vSpace2D)
f1D=Function(vSpace1D)

txtFormula="np.sqrt((1-x[0])**2+(0.5-x[1])**2)"
def initial_condition(x):
    return eval(txtFormula)

f2D.interpolate(initial_condition)

for i in range(nbDofs1DPerComp):
    f1D.x.array[i]=f2D.x.array[index1DTo2D[i]]

mass=assemble_scalar(form(f1D*dx))
print(mass)
