from toolsEnv import computerEnv,MSEPrint
import MESH_IMPORT_GMSH.read as reader
import MESH_IMPORT_GMSH.buildMeshes as buildMeshes
from dolfinx.io import XDMFFile,VTKFile
from ufl import Measure,TrialFunction,TestFunction, grad, inner,SpatialCoordinate

from dolfinx.fem import Function,FunctionSpace, assemble_scalar,form
from mpi4py import MPI
import numpy as np
import gmsh
import meshio
import sys
import numpy as np
import os.path
import subprocess
import MAP_TOOLS.buildMapc as mc
from petsc4py import PETSc
from dolfinx.fem.petsc import assemble_matrix, assemble_vector
from numpy.linalg import norm

def save_step(u,REP,SUFFIX,step):
   np.save(REP+'/state'+SUFFIX+str(step),u.x.array)

def read_step(u,REP,SUFFIX,step):
    u.x.array[:] = np.load(REP+'/state'+SUFFIX+str(step)+'.npy')



T0=0
TF=10
dt=0.5
nTi=round(TF/dt)

d2D=0.05
#rectangular geometry
xmin=0.0
ymin=0.0
xmax=1.5
ymax=1.5
#center
xcenter=0.5*(xmin+xmax)
ycenter=0.5*(ymin+ymax)
#radius=0.2
nCirclePoints=12*2
mesh_size=0.04

dolfinMesh2D=None
dolfinMesh1D=None
vSpace2D=None
nbDofs2DPerComp=0
nDofs=0
vSpace1D=None
nbDofs1DPerComp=0
index1DTo2D=None
dx1d=None
dx2d=None
meshFile2D="surface.xdmf"
meshFile1D="Cin.xdmf"
pref=0.
p1=0.1
pa=None
XYZ=None
u=None
v=None
vprim=None
utm1=None
uk=None
w=None
buf=None
uref=None
prevSol=None
sol=None
aSolver=None
p1d=None
buf1d=None
p_tp1=None
criteronNR=1e-3
maxIt=10
R=None
u_a=None

class solver:
    def __init__(self) -> None:
        self.KSP=PETSc.KSP(MPI.COMM_WORLD).create()
    def setMat(self,aMat):
        self.KSP.setOperators(aMat)
        self.KSP.setType(PETSc.KSP.Type.PREONLY)
        self.KSP.getPC().setType(PETSc.PC.Type.LU)
    def solve(self,rhs,sol):
        self.KSP.solve(rhs,sol)
    def solveNoNeg(self,rhs,sol):
        self.solve(rhs,sol)
        sol.array[sol.array < 0.] = 0.


def doANonLinStep(a,L,sol):
    error=criteronNR+1
    numIt=0
    uk.x.array[:]=utm1.x.array[:]
    while(error>criteronNR and numIt<maxIt):
        M=assemble_matrix(form(a))
        M.assemble()
        rhs=assemble_vector(form(L))
        aSolver.setMat(M)
        aSolver.solveNoNeg(rhs,sol)
        error=norm(sol.array - prevSol.array)
        prevSol.array[:]=sol.array[:]
        numIt+=1
        print("doANonLinStep "+str(numIt)+" "+str(error))
        uk.x.array[:]=sol.array[:]
    if (error > 10*criteronNR):
        print("Newton failed "+str(error))




def computeJ():
    print("begin", sys._getframe().f_code.co_name)
    J=0.0
    step=0
    current_time=T0;
    final_time=TF;
    while (current_time < final_time):
        current_time = current_time + dt
        step=step+1
        if (step==nTi):
            read_step(buf,"SAVE","u",step)
            read_step(uref,"REF","u",step)
            computev(buf,uref,step)
            J=J+dt*assemble_scalar(form(v*dx2d))
            print("J="+str(J))
    return J



def computeJForMIN(p1):
    f=open("trace.txt","a")
    f.write(str((pref - p1)*(pref - p1)))
    f.write(" ")
    f.close()
    buildMesh(p1)
    buildRef()
    simul(p1)
    os.system("cp SAVE/*u* SAVEUp")
    return computeJ()

def computeJac(p1):
    g=dadjoint(p1)
    f=open("trace.txt","a")
    f.write(str(g.dot(g)))
    f.write("\n")
    f.close()
    return g

def buildRMask(lradius,x):
    if (np.square(x[0]-xcenter)+np.square(x[1]-ycenter) < lradius*lradius):
        return 1;
    else:
        return 0

vbuildRMask=np.vectorize(buildRMask,signature="(),(d)->()")


x = None
ic="np.exp(-16.0*(np.square(x[0]-0.5*xmax)+np.square(x[1]-0.5*ymax)))"
def initial_condition(x):
    return eval(ic)

def simul(p):
    global lastStep,R
    print("simul ",p)
    R=Function(vSpace2D)
    R.x.array[:]=vbuildRMask(p,XYZ)
    A = u*w*dx2d+ dt*d2D*inner(grad(u),grad(w))*dx2d- dt*R*(1-2*uk)*u*w*dx2d
    L = utm1*w*dx2d + dt*R*uk*uk*w*dx2d
    step=0
    current_time=T0;
    final_time=TF;
    utm1.interpolate(initial_condition)
    vtkfile_u = VTKFile(dolfinMesh2D.comm,'RESULTS/solution_u.pvd',"w")
    vtkfile_u.write_function(utm1, step)
    save_step(utm1,"SAVE","u",step)
    while (current_time < final_time):
        doANonLinStep(A,L,sol)
        utm1.x.array[:]=sol.array[:]
        current_time = current_time + dt
        step=step+1
        vtkfile_u.write_function(utm1, step)
        save_step(utm1,"SAVE","u",step)
    lastStep=step
    vtkfile_u.close()


def solveAdjoint(FA,LA,solAdjoint):
    M=assemble_matrix(form(FA))
    M.assemble()
    rhs=assemble_vector(form(LA))
    aSolver.setMat(M)
    aSolver.solve(rhs,solAdjoint)

def computev(buf,uref,step):
    if (step==nTi):
       v.x.array[:]=0.5*np.square(buf.x.array[:]-uref.x.array[:])/dt
    else:
       v.x.array.fill(0.0)

def computevprim(buf,uref,step):
    if (step==nTi):
        vprim.x.array[:]=(buf.x.array[:]-uref.x.array[:])/dt
    else:
        vprim.x.array.fill(0.0)

def dadjoint(p):
    print("begin", sys._getframe().f_code.co_name)
    g=0
    FA=pa*w*dx2d +  dt*d2D*inner(grad(pa),grad(w))*dx2d-dt*R*pa*w*dx2d +2*dt*u_a*R*pa*w*dx2d
    LA= dt*vprim*w*dx2d + p_tp1*w*dx2d
    #LA= p_tp1*w*dx
#    LA= dt*uMask*(up1-uref)*w*dx + p_tp1*w*dx
    step=nTi
    current_time=TF;
    final_time=T0;
    p_tp1.x.array.fill(0.0)
    read_step(u_a,"SAVEUp","u",step)
    read_step(uref,"REF","u",step)
    computevprim(u_a,uref,step)
    dJ=0
    while (current_time > 0):
        solveAdjoint(FA,LA,sol)
        p_tp1.x.array[:]=sol.array[:]
        current_time=current_time-dt
        step=step-1
        for i in range(nbDofs1DPerComp):
           p1d.x.array[i]=p_tp1.x.array[index1DTo2D[i]]
           buf1d.x.array[i]=u_a.x.array[index1DTo2D[i]]
        g=g+dt*assemble_scalar(form(buf1d*(1-buf1d)*p1d*dx1d))
        read_step(u_a,"SAVEUp","u",step)
        read_step(uref,"REF","u",step)
        computevprim(u_a,uref,step)
    return g


def buildRef():
    print("buildRef: we are building the ref (ie data) state for p=",pref)
    simul(pref)
    os.system("cp SAVE/* REF/")
    os.system("cp RESULTS/* REF/")



def buildMesh(radius):
    print("buildMesh with radius "+str(radius))
    global dolfinMesh2D,dolfinMesh1D,vSpace2D,nbDofs2DPerComp,vSpace1D,nbDofs1DPerComp,index1DTo2D,dx1d,dx2d,XYZ,u,utm1,uk,w,v,vprim,x,nDofs,doANonLinStep,sol,pref,prevSol,criteronNR,maxIt,aSolver,buf,uref,aSolver,pa,p1d,buf1d,p_tp1,u_a
    with open("_buildMesh.py") as f:
        exec(f.read())
    with XDMFFile(MPI.COMM_WORLD,meshFile2D,"r") as xdmf:
        dolfinMesh2D= xdmf.read_mesh(name="Grid")
    with XDMFFile(MPI.COMM_WORLD,meshFile1D,"r") as xdmf:
        dolfinMesh1D= xdmf.read_mesh(name="Grid")
    vSpace2D=FunctionSpace(dolfinMesh2D,("CG",1))
    nbDofs2DPerComp=vSpace2D.tabulate_dof_coordinates().shape[0]
    vSpace1D=FunctionSpace(dolfinMesh1D,("CG",1))
    nbDofs1DPerComp=vSpace1D.tabulate_dof_coordinates().shape[0]
    index1DTo2D=np.zeros((nbDofs1DPerComp,),dtype=np.int32)
    mc.doMapc(vSpace1D.tabulate_dof_coordinates(),vSpace2D.tabulate_dof_coordinates(),index1DTo2D,1e-9)
    dx1d=Measure('dx',dolfinMesh1D)
    dx2d=Measure('dx',dolfinMesh2D)
    XYZ=vSpace2D.tabulate_dof_coordinates().copy()
    u = TrialFunction(vSpace2D)
    v = Function(vSpace2D)
    vprim = Function(vSpace2D)
    utm1 = Function(vSpace2D)
    uk = Function(vSpace2D)
    w = TestFunction(vSpace2D)
    x = SpatialCoordinate(dolfinMesh2D)
    sol=PETSc.Vec().createSeq(nbDofs2DPerComp)
    prevSol=PETSc.Vec().createSeq(nbDofs2DPerComp)
    prevSol.array.fill(0.0)
    uref=Function(vSpace2D)
    buf=Function(vSpace2D)
    aSolver=solver()
    pa = TrialFunction(vSpace2D)
    p_tp1=Function(vSpace2D)
    p1d=Function(vSpace1D)
    buf1d=Function(vSpace1D)
    u_a=Function(vSpace2D)
