import kppForme as model
import numpy as np
import os

os.system("rm SAVEUp1/* SAVE/* RESULTS/* REF/*")
model.pref=0.4

message=""
for pRaduis in [0.3,0.2,0.5,0.6]:
    da=0.1
    res1=model.computeJForMIN(pRaduis)
    gAdjoint=model.dadjoint(pRaduis)
    message=message+str(pRaduis)+" gAdjoint="+str(gAdjoint)+"\n"
    for cmp in range(3):
        res2=model.computeJForMIN(0.2+da)
        message=message+str(da)+" dif finie d_aV("+str(pRaduis)+")="+str((res2-res1)/da)+"\n"
        da=da*0.5
        print(message)
print(message)
