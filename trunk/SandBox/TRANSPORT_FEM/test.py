from dolfinx.io import XDMFFile
from dolfinx.fem import FunctionSpace,form
from mpi4py import MPI
from ufl import Measure,TrialFunction,TestFunction
from dolfinx.fem import Function, assemble_scalar, Constant
from dolfinx.fem.petsc import assemble_matrix
import numpy as np
from mse.state import state
from petsc4py.PETSc import ScalarType
import os
from ufl import grad,dot
from dolfinx.fem.petsc import assemble_matrix, assemble_vector

import ufl
from petsc4py import PETSc
from numpy.linalg import norm
import sys
from dolfinx.io import VTKFile


import pymseMapTools.mseMapTools as mc
import pymseMatTools.mseMatTools as MT


with XDMFFile(MPI.COMM_WORLD,"./MESHES/surface1.xdmf","r") as xdmf:
    dolfinMesh= xdmf.read_mesh(name="Grid")

with XDMFFile(MPI.COMM_WORLD,"./MESHES/edge4.xdmf","r") as xdmf:
    dolfinBorderMesh= xdmf.read_mesh(name="Grid")

vSpace=FunctionSpace(dolfinMesh,("Lagrange",1))# NOTE: new name for vspace GC => Lagrange
nDofs=vSpace.tabulate_dof_coordinates().shape[0]
vSpaceBorder=FunctionSpace(dolfinBorderMesh,("Lagrange",1))
nDofsBorder=vSpaceBorder.tabulate_dof_coordinates().shape[0]

index1DTo2D=np.zeros((nDofsBorder,),dtype=np.int32)
mc.buildMapc(vSpaceBorder.tabulate_dof_coordinates().reshape((3*nDofsBorder)),vSpace.tabulate_dof_coordinates().reshape((3*nDofs)),index1DTo2D,1e-9)
dx1d=Measure('dx',dolfinBorderMesh)
u1d= TrialFunction(vSpaceBorder)
v1d =TestFunction(vSpaceBorder)
a1d=1000*u1d*v1d*dx1d
M1d=assemble_matrix(form(a1d))
M1d.assemble()
 

dx=Measure('dx',dolfinMesh)
u= TrialFunction(vSpace)
v =TestFunction(vSpace)
w =TestFunction(vSpace)
utm1= Function(vSpace)
u0= Function(vSpace)


class solver:
    def __init__(self) -> None:
        self.KSP=PETSc.KSP(MPI.COMM_WORLD).create()
    def setMat(self,aMat):
        self.KSP.setOperators(aMat)
        self.KSP.setType(PETSc.KSP.Type.PREONLY)
        self.KSP.getPC().setType(PETSc.PC.Type.LU)
    def solve(self,rhs,sol):
        self.KSP.solve(rhs,sol)
    def solveNoNeg(self,rhs,sol):
        self.solve(rhs,sol)
        sol.array[sol.array < 0.] = 0.

aSolver=solver()


x = ufl.SpatialCoordinate(dolfinMesh)
ic="np.exp(-3*(x[0]**2+x[1]**2))"
def initial_condition(x):
    return eval(ic)

def defNRIC(u):
    lim=1.00011
    for i in range(nDofs):
        if(abs(vSpace.tabulate_dof_coordinates()[i][0])<lim and abs(vSpace.tabulate_dof_coordinates()[i][1])<lim):
            u.x.array[i]=1
        else:
            u.x.array[i]=0


T0=0
TF=5
dt=0.005
nTi=round(TF/dt)


#
# d_t u= -V\d_x u dans [0,1]
# u(0,t)=0
# u(x,0)=u0(x)=exp(-(x-0)^2/s)
#
# ut-utm1 = -dt*V(theta* d_x ut +(1-theta)d_x utm1))
# ut +dt*theta*V*d_x*ut =utm1-(1-theta)dt*V*d_x utm1
# la formulation faible:
#
#trouver ut tq pour tout w:
# int_ (ut*w+dt*theta*V*d_x*u_t*w) =int_ (utm1+(1-theta)dt*V*d_x utm1)*w 
#
theta=.5
V=1
#for numerical diffusion
K=0*0.5*dt*V*V
D=0
a=u*w*dx + dt*theta*V*grad(u)[0] *w*dx-theta*dt*(K-D)*dot(grad(u),grad(w))*dx 
L=utm1*w*dx -(1-theta)*dt*V*grad(utm1)[0]*w*dx 

sol=PETSc.Vec().createSeq(nDofs)
def doALinStep(a,L,sol):
    M=assemble_matrix(form(a))
    M.assemble() 
    if (D<1e-9):
        MT.addMatMapij(M,1,M1d,index1DTo2D,index1DTo2D,1,1)
    rhs=assemble_vector(form(L))
    aSolver.setMat(M)
    aSolver.solveNoNeg(rhs,sol)
    #aSolver.solve(rhs,sol)


def simul(a,L):
    print("begin", sys._getframe().f_code.co_name)
    step=0
    current_time=T0;
    final_time=TF
    utm1.interpolate(initial_condition)
    #defNRIC(utm1)
    vtkfile_u =VTKFile(dolfinMesh.comm,'RESULTS/solution_u.pvd',"w")
    vtkfile_u.write_function(utm1, step)
    while (current_time < final_time):
        doALinStep(a,L,sol)
        utm1.x.array[:]=sol.array[:]
        current_time = current_time + dt
        step=step+1
        vtkfile_u.write_function(utm1, step)
    vtkfile_u.close()

simul(a,L)
