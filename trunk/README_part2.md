# MSE Python project
## Sofware for simulation and estimation of reaction-diffusion PDE in the INRAE context

![MSE Logo](https://forgemia.inra.fr/uploads/-/system/project/avatar/389/mse.png "MSE image")   

[The source for this project is here.][src]

----

TO USE MSE
----------

The simplest method is to use guix to create an MSE environment based on an archived version of MSE. To do this, simply run the following command:

.. code-block:: bash

	cd mse/trunk
	guix time-machine -C ../guix/myCurrentChannels.scm -- shell -C -m ../guix/manifestDolfinxv06.scm -m ../guix/manifestOnlyMsev2.scm -L ../guix/recettes/

To install guix and get a brief overview, look at this link "https://gitlab.inria.fr/fuentes/guix4n00bs". 


BUILDING AND INSTALL all components of MSE from sources using guix
------------------------------------------------------------------
For developers, it can be useful to build MSE from sources. Again, the simplest is to use guix. The first step is to get sources and launch the environment in the mse/trunk directory:   

.. code-block:: bash

	guix time-machine -C ../guix/myCurrentChannels.scm -- shell -C --share=/tmp --preserve='^DISPLAY$' -m ../guix/manifestCompilAllMseComponents.scm -m ../guix/manifestDolfinxv06.scm  

The guix option  --share=/tmp --preserve='^DISPLAY$' are useful for graphic export (for debug with ddd) 

Within this environment, Mse installation needs 5 compilations steps (4 cmake + 1 setup.py) resume in following commands. The copy-paste works, thanks guix.

.. code-block:: python

    echo "ensure you are in mse/trunk directory ?" 
    
    TRUNK_MSE=$(pwd)/   
    mkdir TEST_INSTAL  
    rm -Rf TEST_INSTAL/*  
    
    echo "remove previuos builds"
    rm -rf mseMatTools/build/* mseMatTools/Frontend/build/* mseMapTools/build/* mseMapTools/Frontend/build/* 

    echo "1) install mseMatTools :"
    
    cd $TRUNK_MSE  
    mkdir mseMatTools/build  
    cd mseMatTools/build  
    cmake .. -DCMAKE_INSTALL_PREFIX=$TRUNK_MSE/TEST_INSTAL/installmseMatTools  
    make install  
    
    echo "and compil with option -DCMAKE_BUILD_TYPE=Debug"  
    
    echo "2) install python interface pymseMatTools"  
    
    cd $TRUNK_MSE  
    mkdir mseMatTools/Frontend/build  
    cd mseMatTools/Frontend/build  
    cmake .. -DMSE_MAT_TOOLS_INSTALL_PATH=$TRUNK_MSE/TEST_INSTAL/installmseMatTools -DCMAKE_INSTALL_PREFIX=$TRUNK_MSE/TEST_INSTAL/installpymseMatTools  
    make  
    export PYTHONPATH=$PYTHONPATH:$TRUNK_MSE/TEST_INSTAL/installpymseMatTools/lib/python3.10/site-packages/   
    echo "to check this:"  
    python3 -c "import pymseMatTools.mseMatTools as mt"  
    
    echo "3) install mseMapTools " 
    cd $TRUNK_MSE  
    mkdir mseMapTools/build  
    cd mseMapTools/build  
    cmake .. -DCMAKE_INSTALL_PREFIX=$TRUNK_MSE/TEST_INSTAL/installmseMapTools  
    make install  
    echo "4) install python interface pymseMapTools"  
    cd $TRUNK_MSE  
    
    mkdir mseMapTools/Frontend/build  
    cd mseMapTools/Frontend/build  
    cmake .. -DMSE_MAP_TOOLS_INSTALL_PATH=$TRUNK_MSE/TEST_INSTAL/installmseMapTools -DCMAKE_INSTALL_PREFIX=$TRUNK_MSE/TEST_INSTAL/installpymseMapTools  
    make  
    export PYTHONPATH=$PYTHONPATH:$TRUNK_MSE/TEST_INSTAL/installpymseMapTools/lib/python3.10/site-packages/   
    
    echo "to check this:"  
    python3 -c "import pymseMapTools.mseMapTools as mp"  
    
    echo "5) python install:"  
    
    cd $TRUNK_MSE  
    echo "clean local python build :  " 
    rm -rf dist build src/MSE.egg-info/ $TRUNK_MSE/TEST_INSTAL/installMse/ 
    echo "- run the python installation"  
    
    python3 setup.py bdist_wheel  
    python3 -m pip install --find-links=dist/ MSE --prefix=$TRUNK_MSE/TEST_INSTAL/installMse/  
    ls $TRUNK_MSE/TEST_INSTAL//lib/python3.10/site-packages/mse/  
    echo "The path to local install must be add to python path:"  
    export PYTHONPATH=$PYTHONPATH:$TRUNK_MSE/TEST_INSTAL/installMse/lib/python3.10/site-packages/
    echo "To ckeck that mse is available:"  
    python3 -c "import mse"  


BUILDING AND INSTALL component by component of MSE (mseMatTools, pymseMatTools, mseMapTools, pymseMapTools) using guix  
----------------------------------------------------------------------------------------------------------------------
* mseMatTools:

cd mse/trunk  

using the guix environnement "guix time-machine -C ../guix/myCurrentChannels.scm -- shell -C -m ../guix/manifestCompilmseMatTools.scm"   
guix env must be launch in mse/trunk to be sure than all project files will be accessible  


To get the path for the petcs include :  
cd mseMatTools/  
mkdir build  
cd build  
python3 -c "import petsc4py; print(petsc4py.__path__)"  
cmake -DBUILD_SHARED_LIBS=ON -DPETSC_INCLUDE_DIRS=/gnu/store/3qmg1gjxix59pgm6n1qdaqw46ghrxdsc-profile/lib/python3.10/site-packages/petsc4py/include/ -DCMAKE_INSTALL_PREFIX=./installmseMatTools ..  
make  
make install  

* mseMatTools and pymseMatTools  

cd mseMatTools  

using the guix environnement "guix time-machine -C ../../guix/myCurrentChannels.scm -- shell -C -m ../../guix/manifestCompilmseMatTools.scm -m ../../guix/manifestCompilpymseMatTools.scm  

-) build and install mseMatTools as describe above  
-) build the python interface:  
cd Frontend; mkdir build; cd build  
cmake -DBUILD_SHARED_LIBS=ON -DCMAKE_LIBRARY_PATH=$GUIX_ENVIRONMENT/lib/   -DCMAKE_INCLUDE_PATH=$GUIX_ENVIRONMENT/include   -DMSE_MAT_TOOLS_INSTALL_PATH=xxxx/mse/trunk/mseMatTools/build/installmseMatTools/   -DCMAKE_INSTALL_PREFIX=./installpymseMatTools ..  
make   
make install  
The path to local install must be add to python path:  
export PYTHONPATH=xxxxx/mse/trunk/mseMatTools/Frontend/build/installpymseMatTools/lib/python3.10/site-packages/  
cd /tmp  
python3  
>>> import pymseMatTools  
>>> pymseMatTools.__path__  

cd xxxxx/mse/trunk/mseMatTools/Frontend/  
python3 test_MseMatTools.py  

* mseMapTools:  

cd mse/trunk  

using the guix environnement "guix time-machine -C ../guix/myCurrentChannels.scm -- shell -C -m ../guix/manifestCompilmseMapTools.scm"   
guix env must be launch in mse/trunk to be sure than all project files will be accessible  


To get the path for the petcs include :  
cd mseMapTools/  
mkdir build  
cd build  
cmake -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=./installmseMapTools ..  
make  
make install  

* mseMatTools and pymseMapTools

cd mse/trunk  

using the guix environnement "guix time-machine -C ../guix/myCurrentChannels.scm -- shell -C -m ../guix/manifestCompilmseMapTools.scm -m ../guix/manifestCompilpymseMapTools.scm"  
guix env must be launch in mse/trunk to be sure than all project files will be accessible  

-) build and install mseMapTools has desribed above  
-) build the python interface  

cd FrontEnd;  mkdir build; cd build  

cmake -DBUILD_SHARED_LIBS=ON -DCMAKE_LIBRARY_PATH=$GUIX_ENVIRONMENT/lib/
        -DCMAKE_INCLUDE_PATH=$GUIX_ENVIRONMENT/include
        -DMSE_MAP_TOOLS_INSTALL_PATH=xxxx/mse/trunk/mseMapTools/build/installmseMapTools/
        -DCMAKE_INSTALL_PREFIX=./installpymseMapTools ..  

make  
make install  

export PYTHONPATH=xxxxx/mse/trunk/mseMapTools/Frontend/build/installpymseMapTools/lib/python3.10/site-packages/  

cd /tmp  
python3  
>>> import pymseMapTools  

ONLY INSTALL AND TEST OF THE MSE PYTHON CODE:
---------------------------------------------
* First do in guix env :

cd mse/trunk  
guix time-machine -C ../guix/myCurrentChannels.scm -- shell -C -m ../guix/manifestToCompilMSE.scm  

* After :  

To compile, do the step 5) above.  


doc generation
--------------


Documention is built using sphinx. The documention building needs a working install of mse.

.. code-block:: bash

    cd docs
    make clean;make html

The generated doc is in result is in docs/build



[src]: https://forgemia.inra.fr/olivier.bonnefon/mse/


