#test with 2 dynamical system 1d and interactionEdgesConnect
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem, computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme, implicitNonLinearTimeScheme
from mse.INTERACTIONS.interaction import interactionEdgesConnect
from mse.toolsEnv import computerEnv, MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import mkdir
from os.path import isdir


nbComps = 2
studName = "TEST_MULTI_EDGE_CONNECT"
nbParam = 0
# a study
aStudy = study(studName, nbParam)
# the study zone
aStudy.setGeometry("PAYSAGE20", 0.4)

# a system
aSystem = system(aStudy)
# dynamical system
# First DS
for i in range (1,len(aStudy.topo.Edges)):
    aDs=dynamicalSystem(aStudy, 1, "edge"+str(i)+".xdmf",nbComps)
    diffusionModel(aDs).setDiffCoef(3.)
    for nc in range(nbComps):
        aDs.initialState[nc] = "0.0*"+aDs.initialState[nc]
aDs=aStudy.system.dynamicalSystems[0]
for nc in range(nbComps):
    aDs.initialState[nc] = "1.0+"+aDs.initialState[nc]
 
for i in range(len(aStudy.topo.Edges)-1-1):
    for j in range(i+1,len(aStudy.topo.Edges)-1):
        interactionEdgesConnect(aStudy,aStudy.system.dynamicalSystems[i],aStudy.system.dynamicalSystems[j])

for inter in aStudy.system.interactions :
    inter.mulCoef(100.0,100.0)

aTS = implicitLinearTimeScheme()
# simu in t \in [0,1]
simulator(0, 3, aStudy)
# based on fix time step 0.05
aTStep = timeDoOneStep(aStudy, aTS, 1.0)
# run the simulation
aStudy.simulator.doSimu()


# for visu, export simulation steps to vtk
aStudy.simulator.exportSimuToVtk()

# loop on simu to compute mass
import numpy as np
#it tests if :
# 1) global population on ds1d1 and ds1d2 is constant
# 2) ds1d1 population pass to ds1d2 
def test_interactionEdgesConnect():
    mass = np.zeros((nbComps), dtype=np.double)
    aExplorTraj = explorTrajectory(aStudy, aStudy.simuDir)
    aExplorTraj.rewind()
    prevTotalMass=np.zeros((nbComps), dtype=np.double)
    prevTotalMass[:]=-1
    while(aExplorTraj.replay()):
        TotalMass = np.zeros((nbComps), dtype=np.double)
        print("step "+str(aExplorTraj.curStep)+" : ", end="")
        for ds in aStudy.system.dynamicalSystems:
            computeMass(ds, mass)
            assert(mass[0]>-1e-9)
            print(mass, end=",")
            TotalMass = TotalMass+mass
        print(TotalMass, end=", ")
        if (abs(prevTotalMass[0]+1)>0.5):
            for m1,m2 in zip(prevTotalMass,TotalMass):
                assert (abs(m1-m2)<0.0001*m2)
                assert (abs(m2)>0)
        prevTotalMass[:]=TotalMass[:]
        print("")

