from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory

from os import  mkdir
from os.path import isdir

nbComps=1
WITH_AFFINE_GROWTH=1
testNLTS=0


#une etude
studName="TEST_FONCTIONAL1"

aStudy=study(studName)
aStudy.setGeometry("SQUARE",1.)

#un systeme
aSystem=system(aStudy)
#un systeme dynamique
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
#ds2d1.initialState[0]="0.2+0.0*"+ds2d1.initialState[0]
ds2d1.initialState[0]="0.0*"+ds2d1.initialState[0]
#

#la diffusion
aDiffModel=diffusionModel(ds2d1)
#ds2d1.addSecondOrderTerm(aDiffModel)
aDiffModel.setDiffCoef(1.0)
if (WITH_AFFINE_GROWTH):
    #affineGrowth(ds2d1,R,K)
    m=txtModel(ds2d1,"affineGrowth")
    m.computeMatOnce=1
    m.computeRhsOnce=1

aTS=None
#schema en temps implicite lineaire
if (testNLTS):
    aTS=implicitNonLinearTimeScheme()
else:
    aTS=implicitLinearTimeScheme()
    aTS.computeMatOnce=1

simulator(0,2,aStudy)
aTStep=timeDoOneStep(aStudy,aTS,0.5)

aStudy.simulator.doSimu()


#for visu, export simulation steps to vtk
aStudy.simulator.exportSimuToVtk()

#loop on simu to compute mass
import numpy as np
mass=np.zeros((ds2d1.nbComps),dtype=np.double)
aExplorTraj=explorTrajectory(aStudy,aStudy.simuDir)
aExplorTraj.rewind()
while(aExplorTraj.replay()):
    computeMass(ds2d1,mass)
    print("mass "+str(aExplorTraj.curStep)+" = ",end="")
    for m in mass:
        print(m,end=", ")
    print("")
aStudy.end()

def test_massEnd():
    assert (abs(mass[0]-43.52)<1e-1)
