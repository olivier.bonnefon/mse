from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.state import state
from mse.explorTrajectory import explorTrajectory
from os import  mkdir, rename, remove
from os.path import isdir


nbComps=1
WITH_AFFINE_GROWTH=1
testNLTS=0


#une etude
studName="TEST_EXPLOR_TRAJECTORY"

aStudy=study(studName)
aStudy.setGeometry("SQUARE",1.)

#un systeme
aSystem=system(aStudy)
#un systeme dynamique
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
#ds2d1.initialState[0]="0.2+0.0*"+ds2d1.initialState[0]
ds2d1.initialState[0]="0.0*"+ds2d1.initialState[0]
#

#la diffusion
aDiffModel=diffusionModel(ds2d1)
#ds2d1.addSecondOrderTerm(aDiffModel)
aDiffModel.setDiffCoef(1.0)
if (WITH_AFFINE_GROWTH):
    #affineGrowth(ds2d1,R,K)
    m=txtModel(ds2d1,"affineGrowth")
    m.computeMatOnce=1
    m.computeRhsOnce=1

aTS=None
#schema en temps implicite lineaire
if (testNLTS):
    aTS=implicitNonLinearTimeScheme()
else:
    aTS=implicitLinearTimeScheme()
    aTS.computeMatOnce=1

simulator(0,2,aStudy)
aTStep=timeDoOneStep(aStudy,aTS,0.5)

aStudy.simulator.doSimu()
def test_explorTraj():
    aExplorTrajectory=explorTrajectory(aStudy,aStudy.simuDir)
    aExplorTrajectory.interpolToTime(0.5)
    assert(abs(ds2d1.trajState.comps[0].x.array[0]-0.3999999999999999)<0.001)
    aExplorTrajectory.interpolToTime(1.6)
    assert(abs(ds2d1.trajState.comps[0].x.array[0]-0.8012799999999999)<0.001)
    aExplorTrajectory.interpolToTime(1.5)
    aExplorTrajectory.interpolToTime(1.75)
    aExplorTrajectory.interpolToTime(2)
def test_explorTrajBackward():
    #test if file time is backward
    # create time backward
    path=aStudy.simuDir
    fnewtime = open(path+"tmptime.txt",'w')
    ftime = open(path+"time.txt")
    time = ftime.readlines()
    backTime = time[::-1]
    #create backward file
    for line in backTime: fnewtime.write(line) 
    ftime.close()
    fnewtime.close()
    #rename file time
    rename(path+"time.txt", path+"tmp2time.txt")
    rename(path+"tmptime.txt", path+"time.txt")
    print("path = ",path+"time+txt")
    #test
    aExplorTrajectory=explorTrajectory(aStudy,aStudy.simuDir)
    aExplorTrajectory.interpolToTime(0.5)
    assert(abs(ds2d1.trajState.comps[0].x.array[0]-0.3999999999999999)<0.001)
    aExplorTrajectory.interpolToTime(1.6)
    assert(abs(ds2d1.trajState.comps[0].x.array[0]-0.8012799999999999)<0.001)
    aExplorTrajectory.interpolToTime(1.5)
    aExplorTrajectory.interpolToTime(1.75)
    aExplorTrajectory.interpolToTime(2)
    #file return/find word to describe
    #remove(path+"time.txt")
    #rename(path+"time.txt", path+"tmptime.txt")
    #rename(path+"tmp2time.txt", path+"time.txt")
