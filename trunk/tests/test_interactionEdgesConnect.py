#test with 2 dynamical system 1d and interactionEdgesConnect
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem, computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import (implicitLinearTimeScheme,
                                        implicitNonLinearTimeScheme)
from mse.INTERACTIONS.interaction import interactionEdgesConnect
from mse.toolsEnv import computerEnv, MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import mkdir
from os.path import isdir


nbComps = 2
studName = "TEST_EDGE_CONNECT"
nbParam = 0
# a study
aStudy = study(studName, nbParam)
# the study zone
aStudy.setGeometry("SQUARE", 0.4)

# a system
aSystem = system(aStudy)
# dynamical system
# First DS
ds1d1 = dynamicalSystem(aStudy, 1, "edge1.xdmf", nbComps=nbComps)
ds1d2 = dynamicalSystem(aStudy, 1, "edge2.xdmf", nbComps=nbComps)
ds1d3 = dynamicalSystem(aStudy, 1, "edge3.xdmf", nbComps=nbComps)
ds1d4 = dynamicalSystem(aStudy, 1, "edge4.xdmf", nbComps=nbComps)
for i in range(nbComps):
    ds1d1.initialState[i] = "1.0+"+ds1d2.initialState[i]
    ds1d2.initialState[i] = "0.0*"+ds1d2.initialState[i]
    ds1d3.initialState[i] = "0.0*"+ds1d2.initialState[i]
    ds1d4.initialState[i] = "0.0*"+ds1d2.initialState[i]
#

# add diffusion motion
diffusionModel(ds1d1).setDiffCoef(3.)
diffusionModel(ds1d2).setDiffCoef(1.)
diffusionModel(ds1d3).setDiffCoef(1.5)
diffusionModel(ds1d4).setDiffCoef(1.)

aInter = interactionEdgesConnect(aStudy, ds1d1, ds1d2)
aInter.mulCoef(100.0,100.0)
aInter = interactionEdgesConnect(aStudy, ds1d2, ds1d3)
aInter.mulCoef(100.0,100.0)
aInter = interactionEdgesConnect(aStudy, ds1d3, ds1d4)
aInter.mulCoef(100.0,100.0)
aInter = interactionEdgesConnect(aStudy, ds1d4, ds1d1)
aInter.mulCoef(100.0,100.0)





aTS = implicitLinearTimeScheme()
# simu in t \in [0,1]
simulator(0, 2, aStudy)
# based on fix time step 0.05
aTStep = timeDoOneStep(aStudy, aTS, 0.5)
# run the simulation
aStudy.simulator.doSimu()


# for visu, export simulation steps to vtk
aStudy.simulator.exportSimuToVtk()

# loop on simu to compute mass
import numpy as np
#it tests if :
# 1) global population on ds1d1 and ds1d2 is constant
# 2) ds1d1 population pass to ds1d2 
def test_interactionEdgesConnect():
    mass = np.zeros((nbComps), dtype=np.double)
    aExplorTraj = explorTrajectory(aStudy, aStudy.simuDir)
    aExplorTraj.rewind()
    prevTotalMass=np.zeros((nbComps), dtype=np.double)
    prevTotalMass[:]=-1
    while(aExplorTraj.replay()):
        TotalMass = np.zeros((nbComps), dtype=np.double)
        print("step "+str(aExplorTraj.curStep)+" : ", end="")
        for ds in aStudy.system.dynamicalSystems:
            computeMass(ds, mass)
            assert(mass[0]>-1e-9)
            print(mass, end=",")
            TotalMass = TotalMass+mass
        print(TotalMass, end=", ")
        if (abs(prevTotalMass[0]+1)>0.5):
            for m1,m2 in zip(prevTotalMass,TotalMass):
                assert (abs(m1-m2)<0.0001*m2)
                assert (abs(m2)>0)
        prevTotalMass[:]=TotalMass[:]
        print("")

