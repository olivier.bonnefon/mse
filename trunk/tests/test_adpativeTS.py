from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep,adaptivDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from mse.state import state
import os
from os import  mkdir 
from os.path import isdir

#une etude
studName="TEST_ADPATIV_STEP"

aStudy=study(studName)
aStudy.setGeometry("SQUARE",1.)

#un systeme
nbComps=1
aSystem=system(aStudy)
#un systeme dynamique
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
#ds2d1.initialState[0]="0.2+0.0*"+ds2d1.initialState[0]
ds2d1.initialState[0]="0.0*"+ds2d1.initialState[0]
#

#la diffusion
aDiffModel=diffusionModel(ds2d1)
#ds2d1.addSecondOrderTerm(aDiffModel)
aDiffModel.setDiffCoef(1.0)
#affineGrowth(ds2d1,R,K)
m=txtModel(ds2d1,"affineGrowth")
#m.computeMatOnce=1
#m.computeRhsOnce=1

aTS=None
#schema en temps implicite lineaire
aTS=implicitLinearTimeScheme()
#aTS.computeMatOnce=1

simulator(0,2,aStudy)
timeStep=0.5
aTStep=timeDoOneStep(aStudy,aTS,timeStep)
#timeDoOneStep.verbose(True)
#explorTrajectory.verbose(True)
timeDoOneStep.verbose(0)
explorTrajectory.verbose(0)
aStudy.simulator.doSimu()
aStudy.simulator.exportSimuToVtk()
os.system("cp -r "+aStudy.simuDir+" "+aStudy.absoluteStudyDir+"/SIMU_FIX_STEP")
os.system("cp -r "+aStudy.exportDir+" "+aStudy.absoluteStudyDir+"/VTK_FIX_STEP")
maxMult=1
absErr=1
absRel=0.001
aTStep=adaptivDoOneStep(aStudy,aTS,timeStep,maxMult,absRel,absErr)

aStudy.simulator.doSimu()
aStudy.simulator.exportSimuToVtk()
os.system("rm -rf "+aStudy.absoluteStudyDir+"/SIMU_ADAPT1/; cp -r "+aStudy.simuDir+" "+aStudy.absoluteStudyDir+"/SIMU_ADAPT1/")

aTStep=adaptivDoOneStep(aStudy,aTS,0.01,8,absRel,absErr)
aStudy.simulator.doSimu()
aStudy.simulator.exportSimuToVtk()
os.system("rm -rf "+aStudy.absoluteStudyDir+"/SIMU_ADAPT2/;cp -r "+aStudy.simuDir+" "+aStudy.absoluteStudyDir+"/SIMU_ADAPT2/")


def test_adapt1():
    aExpTrajFix=explorTrajectory(aStudy,aStudy.absoluteStudyDir+"/SIMU_FIX_STEP/") 
    aExpTrajAdap=explorTrajectory(aStudy,aStudy.absoluteStudyDir+"/SIMU_ADAPT1/") 
    aExpTrajFix.rewind()
    aExpTrajAdap.rewind()
    aBufState=state(ds2d1)
    #NOTE: replay save in ds.utm1, interpoltime save in trajstate
    while (aExpTrajFix.replay()):
        aBufState.setFromState(ds2d1.utm1)
        aExpTrajAdap.replay()
        aBufState.axpy(ds2d1.utm1,-1)
        assert(aBufState.norm()<1e-5)

def test_dapt2():
    aExpTrajFix=explorTrajectory(aStudy,aStudy.absoluteStudyDir+"/SIMU_FIX_STEP/") 
    aExpTrajAdap=explorTrajectory(aStudy,aStudy.absoluteStudyDir+"/SIMU_ADAPT2/") 
    aExpTrajFix.rewind()
    aBufState=state(ds2d1)
    while (aExpTrajFix.replay()):
        aBufState.setFromState(ds2d1.utm1)
        aExpTrajAdap.interpolToTime(aExpTrajFix.T)
        aBufState.axpy(ds2d1.trajState,-1)
        assert(aBufState.norm()<1e-1)
