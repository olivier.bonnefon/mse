###
# Test sur les classes interation$D$D
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.INTERACTIONS.interaction import interaction2D1D, interaction1D1D
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import  mkdir
from os.path import isdir


nbComps=1
WITH_INT_1D1D=1
testNLTS=1
studName="TEST_INTERACTION"
nbParam=0
#a study
aStudy=study(studName, nbParam)
# the study zone
aStudy.setGeometry("SIMPLE2",0.4)

# a system
aSystem=system(aStudy)
# dynamical  system 
# First DS
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
# Second DS
ds2d2=dynamicalSystem(aStudy,2,"surface2.xdmf",nbComps=nbComps)
# edge
ds1d1=dynamicalSystem(aStudy,1,"edge2.xdmf",nbComps=nbComps)
ds1d2=dynamicalSystem(aStudy,1,"edge2.xdmf",nbComps=nbComps)
y0=-0.45
coefInit = 1
ds2d1.initialState[0]="np.exp(-"+str(coefInit)+"*(x[0]*0.0+(x[1]-"+str(y0)+")**2))"
ds2d2.initialState[0]="0.0*"+ds2d2.initialState[0]
ds1d1.initialState[0]="0.0*"+ds1d1.initialState[0]
ds1d2.initialState[0]="0.0*"+ds1d2.initialState[0]
if (nbComps>1):
    ds2d1.initialState[1]="0+0.0*"+ds2d1.initialState[0]
    ds2d2.initialState[1]="0.0+0.0*"+ds2d1.initialState[0]
    ds1d1.initialState[1]="0.0+0.0*"+ds2d1.initialState[0]
    ds1d2.initialState[1]="0.0+0.0*"+ds2d1.initialState[0]
#

#add diffusion motion
diffCoefOmega = 0.2
diffusionModel(ds2d1).setDiffCoef(diffCoefOmega)
diffusionModel(ds2d2).setDiffCoef(1.0)
diffusionModel(ds1d1).setDiffCoef(3.)
diffusionModel(ds1d2).setDiffCoef(1.)

aInter2D1D1=interaction2D1D(aStudy,ds2d1,ds1d1)
aInter2D1D2=interaction2D1D(aStudy,ds2d2,ds1d2)

aInter1D1D=interaction1D1D(aStudy, ds1d1,ds1d2)




aTS=None
# select the time algorithm
if (testNLTS):
    aTS=implicitNonLinearTimeScheme()
else:
    aTS=implicitLinearTimeScheme()
#simu in t \in [0,1]
simulator(0,1,aStudy)
#based on fix time step 0.05
aTStep=timeDoOneStep(aStudy,aTS,0.05)
#run the simulation
aStudy.simulator.doSimu()


#for visu, export simulation steps to vtk
aStudy.simulator.exportSimuToVtk()

#loop on simu to compute mass
import numpy as np
aExplorTraj=explorTrajectory(aStudy,aStudy.simuDir)
aExplorTraj.rewind()
###############################################################################

def test_interaction1D1DMember():
    assert(aInter1D1D.study == aStudy)
    assert(aInter1D1D.dS1==ds1d1)
    assert(aInter1D1D.dS2==ds1d2)




def test_totalMass():
    ### Test if the mass is conserved
    #
    TotalMass=np.zeros((1),dtype=np.double)
    pastTotalMass=np.zeros((1),dtype=np.double)
    if(aExplorTraj.replay()):
        for ds in aStudy.system.dynamicalSystems:
            mass=np.zeros((ds.nbComps),dtype=np.double)
            computeMass(ds,mass)
            TotalMass=TotalMass+mass
    while(aExplorTraj.replay()):
        pastTotalMass[:]=TotalMass[:]
        TotalMass[:]=0.
        for ds in aStudy.system.dynamicalSystems:
            computeMass(ds,mass)
            TotalMass=TotalMass+mass
        assert(abs(TotalMass-pastTotalMass)<1e-4)
aStudy.end()
