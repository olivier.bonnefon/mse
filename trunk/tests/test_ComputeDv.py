from mse.toolsEnv import computerEnv,MSEPrint, MSEPrint_0
from sympy import symbols, sympify, nan, Abs, log, zoo, oo 
import pytest

"""
    Routines for pytest that test whether the partial derivatives file  "model+Dv.txt" matches the source terms file "model.txt"
exit(0) ----> clean exit, no issues
exit(1) ----> issues with creation of files
exit(2) ----> issues with the values/expressions of partial derivatives
"""
myPrint=MSEPrint_0
#myPrint=MSEPrint
def tDer(function, dF, numVar, valForVar):
    """
    Function which test if the partial derivate is good.

    Use the limit of numerical differentiation and the function limit of sympy. The limit is adapted for machine epsilon = 10**-16, the machine epsilon varies between 10**-3 and 10**-8.

    :param function str: string of the function. Variable can only be "a,b,c,d,e,f".
    :param dF str: string of the partial derivate to test. The variable of this partial derivate will be specified in the parameter "numvar".
    :param numVar=0 int: a number which give the variable of the partial derivate: variable = var[numVar] with var= ['a','b','c','d','e','f']. Start to 0, max=5.
"""
    print("-------------------------------------------------\n    Funct = "+function+"\n    Dv = "+dF+"\n    var = "+"abcdefgh"[numVar]+"\n-------------------------------------------------")
    eps=1e-15
    dvExacte=False
    hf=[1./(2**n) for n in range(3,8)]
    lhf = [log(y) for y in hf]
    nbRun=len(hf)
    tupleSymbols = symbols("a b c d e f g h")
    tupleReplace=tuple(zip(tupleSymbols,valForVar))
    #On passe de str à expression sympy
    exprF = sympify(function)
    exprDf = sympify(dF)
   #on defini la fonction f(var) et le reel df(var)
    exprF = exprF.subs(k for k in tupleReplace if k!=tupleReplace[numVar])
    valDf = exprDf.subs(tupleReplace)
    valFVar = exprF.subs(tupleReplace)
    print("Value of the derivate= "+str(valDf))
    tabTA=[None]*nbRun
    print("----1/h----------Rate of change--------------f(var)-----------------f(var+h)------------ f(var+h)-f(var)---------- Abs(Rate - Derivate)")
    for i in range(len(hf)):
        valFvarHi = exprF.subs(tupleSymbols[numVar],valForVar[numVar]+hf[i])
        diffFini=(valFvarHi-valFVar)
        valTauxAcc=1/hf[i]*diffFini
        diffTADV = valTauxAcc - valDf
        tabTA[i] = diffTADV
        if(diffTADV==zoo or diffTADV==oo or diffTADV==-oo or diffTADV == nan):
            print("error in tDer: the calculation of numerical derivates = NAN")
            return(False)
        elif(-eps<(diffTADV)<eps):print("Valeur de la dérivée exacte");dvExacte=True #cas où on a taux accroissement et derivee de sympy egaux
        print("%5.2e" % hf[i]+"%20.16g" % valTauxAcc + "%30.16g" %valFVar+ "%25.16g" %valFvarHi+ "%25.16g" %diffFini+ "%25.16g" %Abs(diffTADV))
    # Interpolation of log of diffTADV
    lValTabTA = [log(Abs(y)) if y!=0 else 0 for y in tabTA]
    interp=[(lValTabTA[j+1] -lValTabTA[j])/(lhf[j+1]-lhf[j]) for j in range(len(hf)-1)]
    valInterp=sum(interp)/len(interp)
    print("pente moyenne du loglog = ",valInterp)
    print("Pente sur chaque point = ",interp)
    print(valInterp)
    print(dvExacte)
    return (valInterp > 0.95 or dvExacte)
   

#@pytest.mark.parametrize("modelFile, nbComps, parameter, valVar",[(computerEnv.modelsDir+"affineGrowth",1, [], [2]), (computerEnv.modelsDir+"affineGrowthRK", 1, [1,2], [2]), (computerEnv.modelsDir+"logisticGrowthParam", 1, [1,2], [2]), (computerEnv.modelsDir+"logisticGrowth", 1, [1,2], [2]), (computerEnv.modelsDir+"logistic", 1, [1,2], [2]), (computerEnv.modelsDir+"preyPred", 2, [1,2], [2,3])])
@pytest.mark.parametrize("modelFile, nbComps, parameter, valVar",[(computerEnv.modelsDir+"affineGrowth",1, [], [2]), (computerEnv.modelsDir+"logisticGrowth", 1, [1,2], [2]), (computerEnv.modelsDir+"preyPred", 2, [1,2], [2,3])])


def test_DVFile(modelFile, nbComps, parameter, valVar):
    """
    Function to test DV file. If the file is wrong or it does not exist return false

    Call the function "tDer", and test the partial derivate for all the variable defined, numVar = 0,...,nbComps-1
    Warning ! if you have parameter in your file, it will be "p1,p2,..."
    Warning ! the name of variables in your file should be 'a','b',...,'g','h'

    :param str modelFile: name of the file model. This file MUST BE type of '.txt', BUT the string "modelFile" must not contain '.txt' at the end.
    :param int nbComps: number of variables, by default =1.
    :param list[float] parameter: value of the parameter, it will replace p1 by parma[0], p2 by parameter[1], ... pn by parameter[n-1]. if not parametereter ignore it.
    :param valVar list[float] valVar: value of the variables. by default valVar=[9.,11.,13.,14.,12.,18.].
    """
    ret=True
    #ouverture fichier F et df
    try:
        f = open(modelFile+".txt", "r")
    except IOError:
        myPrint("IOError in txtModel when reading "+modelFile+".txt")
        exit(1)
    try:
        fd = open(modelFile+"Dv.txt", "r")
    except IOError:
        myPrint("IOError in txtModel when reading "+modelFile+"Dv.txt")
        exit(1)   
    print("\n\n         ------ File term source: "+modelFile+"         ------ \n\n")
    for i in range(nbComps):
        #lecture fichier source term
        strline=f.readline().split('=')[1].strip().replace('^','**')
        print("\nFunction = "+strline)
        #remplacement des "p1,..." par des reels
        for count,p in enumerate(parameter):strline=strline.replace('p'+str(count+1),str(p))
        for j in range(nbComps):
            strlineDv=fd.readline().split('=')[1].replace(' ','')#.replace('^','**')
            print("\nPartial derivate of "+str(j)+" var = "+strlineDv)
            #remplacement des "p1,..." par des reels
            for count,p in enumerate(parameter):strlineDv=strlineDv.replace('p'+str(count+1),str(p))
            if((tDer(strline,strlineDv, j, valVar))==False):
                print("Bad expresion of df, please change/remove file \"Dv\"  and recreate Model")
                ret==False
    f.close()
    fd.close()
    assert ret 


