from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory

import numpy as np
import pytest
from os import  mkdir
from os.path import isdir

"""
exit(0) ---> clean exit without errors
exit(1) ---> issues with the result
exit(2) ---> issues with update of parameters
"""



#un modele
nameModele="logisticGrowthParam"
sourceModele = computerEnv.modelsDir+nameModele+".txt"
nbComps=1
nbparam=2
eps=1e-6
eps2=1e-2

    #une etude
studName="TEST_PARAM2"
aStudy=study(studName,nbparam)
aStudy.setGeometry("SQUARE",0.4) 
    #un systeme
aSystem=system(aStudy)
    #un systeme dynamique
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
     
    #la diffusion
aDiffModel=diffusionModel(ds2d1)
    #ds2d1.addSecondOrderTerm(aDiffModel)
aDiffModel.setDiffCoef(1.0)
    

m2=txtModel(ds2d1,nameModele)
    #m2.computeMatOnce=1
    #m2.computeRhsOnce=0
    
aTS=None
#schema en temps implicite lineaire
aTS=implicitLinearTimeScheme()
    
simulator(0,1,aStudy)
aTStep=timeDoOneStep(aStudy,aTS,0.1)
mass=np.zeros((ds2d1.nbComps),dtype=np.double)

rTab=[1.81455919, 5.18167237, 2.20271945]
rTab=[1.802307664882926, 5.6452487665530, 2.20271945]

@pytest.mark.parametrize("result, index, param, trueParam",[(rTab[0], None, [1.,1.], [1.,1.]), (rTab[1], None, [2.,4.],[2.,4.]), (rTab[1], [5,1], [2., 3.],[2.,4.]), (rTab[2], [0,5], [1.,2.],[1.,4.]), (rTab[0], [1], [1.], [1.,1.])])
def test_Param2(result, index, param, trueParam):
    """ Test to know if paramters are update after call of study.setParam() and if the calculation value is correct.

    """

    r=True
    try:
        print(param)
        aStudy.setParam(param,index)
    except Exception:
        # We check if we get an error when index out of range
        print("index = ", index)
        if ((isinstance(index, int) and index > 0) or
        (isinstance(index, list) and any(val > 0 for val in index))):
            assert True
            return
        else:
            assert (false)
            return
    if((trueParam[0]-eps<=ds2d1.paramD[0].value<=trueParam[0]+eps or trueParam[1]-eps<=ds2d1.paramD[1].value<=trueParam[1]+eps)==False) :
        print("Bad update of param in dynamicalsystem")
        r=False 
    aStudy.simulator.doSimu()
    aExplorTraj=explorTrajectory(aStudy,aStudy.simuDir)
    aExplorTraj.rewind()
    while(aExplorTraj.replay()):
        computeMass(ds2d1,mass)# -> Warning in this line, but not find how to catvh it. (number of warnings = number of step of time)
    if((-eps2<=mass[0]-result<=eps2)==False):r=False
    print("final mass = "+str(mass[0])+" should be "+str(result))
    assert r

