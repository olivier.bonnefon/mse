import pytest

from mse.obsProcess import obsProcess, quadraticDiff
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.systemAdjoint import systemAdjoint
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.toolsEnv import computerEnv,MSEPrint
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from dolfinx.fem import Function, assemble_scalar, Constant, form

import os
from os import  mkdir
from os.path import isdir
import shutil
import numpy as np


#######################
#        FIXME        #
# Problem in file time .txt
# t0 = 0, t1 =1, dt =0.1  => in file .txt: tfinal = 1.09999999.. !!
#  TODO: use an epsilon ?

#eps: epsilon/zero machine, use for test like float:a!=0. => a>eps or a<-eps
#NOTE: we have 'only' 8 digit for sol
eps=1e-7



WITH_AFFINE_GROWTH=1
nbparam=2
#une etude
studName="TEST_CLASS_QUADDIFF"
aStudy=study(studName, nbparam)
aStudy.setGeometry("SQUARE",0.4)
#un systeme
aSystem=system(aStudy) 
print("aStudy create")
#un systeme adjoint
aSysAdj = systemAdjoint(aStudy) 

print("ds2d1 create")
nbComps=1
#un systeme dynamique
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
#la diffusion
aDiffModel=diffusionModel(ds2d1)
#ds2d1.addSecondOrderTerm(aDiffModel)
aDiffModel.setDiffCoef(1.0)


print("aTS create")
testNLTS=0
aTS=None
#schema en temps implicite lineaire
aTS=implicitLinearTimeScheme()
t0 = 0.
t1 = 1.
timeStep = 0.1
totStep = int((t1-t0)*1/timeStep)
simulator(t0 ,t1, aStudy)
aTStep=timeDoOneStep(aStudy,aTS,timeStep)



m=txtModel(ds2d1,"logisticGrowthParam")
#m.computeMatOnce=1
#m.computeRhsOnce=1
print("txtmodel create")

aStudy.setParam([1.,1.])
aStudy.simulator.doSimu()
path=aStudy.absoluteStudyDir+"/UREF/"
if(os.path.exists(path)):
    shutil.rmtree(path)
shutil.copytree(aStudy.simuDir, path)
quaDiff=quadraticDiff(ds2d1)
aStudy.setParam([5.,4.])
aStudy.simulator.doSimu()
#NOTE: use class ??


def test_quadraticDiff():
    """test creation of class quadraticDiff

    first two assert check whether study and obsProcess are attributes of them
    the third assert check whether file directory of u and uref are different.
    fourth assert check if QuadraticDiff is subclass of obsProcess
    last assert check value of mass
    """
    rTab=[1.8023076648829266, 40.886318645056726]
    mass=np.zeros((ds2d1.nbComps),dtype=np.double)
    mass2=np.zeros((ds2d1.nbComps),dtype=np.double)
    assert quaDiff.dS == ds2d1
    assert ds2d1.obsProcess == quaDiff
    assert ( quaDiff.dirURef != aStudy.simuDir) 
    #test if Qd subClass of OP
    assert issubclass(quadraticDiff, obsProcess)
    #test if compute mass different/val of compute mass
    #we check only last time
    with open(aStudy.simuDir+"time.txt","r") as file:
        finalTime = float(file.readlines()[-1].split(" ")[1])# only use if time.txt is not too big
    quaDiff.explor_u.interpolToTime(finalTime)
    # compute mass for u
    computeMass(ds2d1,mass)
    # compute mass for u_ref
    quaDiff.explor_uRef.interpolToTime(finalTime)
    for numComp, comp in enumerate(ds2d1.trajState.comps):
        mass2[numComp]=assemble_scalar(form(comp*ds2d1.dx))
    assert (mass[0]!=mass2[0])
    print(mass[0])
    print(mass2[0])
    assert(abs(mass[0]-rTab[1]) < eps)
    assert(abs(mass2[0]-rTab[0]) < eps)
 


vtildprim_sol=[
        [3.68245892e-07, 7.23484762e-07, 5.24171485e-07, 3.69415057e-07, 1.24869947e-06, 2.48543099e-06, 5.18244260e-07, 4.32095012e-06, 9.00150401e-06, 2.98588183e-07],
        [0.00022815, 0.00037543, 0.000293, 0.00022694, 0.00057279, 0.00099006, 0.00029141, 0.00153962, 0.00274254, 0.00019199],
        [0.00586405, 0.00837634, 0.00696564, 0.00580249, 0.01149654, 0.01759991, 0.00691562, 0.02489064, 0.03911545,0.00508041],
        [0.33886224, 0.39908717, 0.36428966, 0.33409954, 0.46603639, 0.58655744, 0.35928975, 0.70988442, 0.9160064, 0.30702597]
        ]
vtildprim_sol=[
        [3.63121668e-06, 6.04451091e-06, 4.69281178e-06, 3.61406415e-06, 9.32668625e-06, 1.63946627e-05, 4.66270375e-06, 2.59123372e-05, 4.74174833e-05, 3.05093945e-06],
        [0.00115058, 0.00167641, 0.00138122, 0.00113943, 0.00234215, 0.00367281, 0.00137136, 0.00530797, 0.00861663, 0.00099369],
        [0.02361604, 0.03061891, 0.02665244, 0.02331474, 0.03892794, 0.0545738, 0.02637563, 0.07217628, 0.10445944, 0.02091478],
        [1.0917266,  1.19499791, 1.13312404, 1.07774103 ,1.30360977, 1.49778759, 1.11655161, 1.68109609, 1.97308786, 1.01402075]
]


@pytest.mark.parametrize("time, sol_vtp",[(0.01, vtildprim_sol[0] ),(0.25, vtildprim_sol[1] ), (0.5, vtildprim_sol[2]), (0.98, vtildprim_sol[3])])
def test_FnctQDGetTildev(time, sol_vtp):
    """test function computeTildevPrim from class quadraticDiff

    test if vtild[i] is greater than 0, ie: greather than an epsilon.
    """
    quaDiff.computeTildevPrim(time)
    print(quaDiff.vtild.comps[0].x.array[0:10])
    absoluteDiff = np.absolute(quaDiff.vtild.comps[0].x.array[0:10]-sol_vtp)
    assert (np.all(np.less_equal(absoluteDiff,np.ones(10)*eps)))

def test_FnctQuadComputeV():
    """test function computeV from class quadraticDiff
    """
    v_sol =23.230832311982 
    tmp = quaDiff.computeV()
    print(tmp)
    assert (abs(tmp-v_sol)< eps)
