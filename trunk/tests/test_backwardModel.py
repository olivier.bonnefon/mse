from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.systemAdjoint import systemAdjoint 
from mse.obsProcess import quadraticDiff
from mse.simulator import simulator,simulatorBackward
from mse.timeDoOneStep import timeDoOneStep,timeDoOneStepBackward
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme,implicitLinearBackwardTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import  mkdir
from os.path import isdir, exists
import numpy as np
import shutil
import pytest


# Test the resolution of the system adjoint, using backward simulator, timeScheme backward and timeDoOnestepBackward
#
# First we will do a simulation, second we will create the adjoint system and third do simulation to resolve the adjoint system
#  the model will be a logistic growth with param
#
#On se retrouve dans un probleme equivalent de ColabMarc avec (D,R,K) = (1,p1,p2) et sans le taux de mortalité

#La geometrie
nbComps=1
WITH_AFFINE_GROWTH=1
testNLTS=1


#une etude
studName="TEST_BACKWARDSIMU"

aStudy=study(studName, nbParam=2)
aStudy.setGeometry("SQUARE",0.4)

#un systeme
aSystem=system(aStudy)
#un systeme dynamique
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
#ds2d1.initialState[0]="0.0+0.0*"+ds2d1.initialState[0]
ds2d1.initialStateAdjoint[0]="0.0+0.0*"+ds2d1.initialStateAdjoint[0]

#la diffusion
aDiffModel=diffusionModel(ds2d1)
#ds2d1.addSecondOrderTerm(aDiffModel)
D = 5.
aDiffModel.setDiffCoef(D)
#if (WITH_AFFINE_GROWTH):
    #affineGrowth(ds2d1,R,K)
m=txtModel(ds2d1,"logisticGrowthParam")
#    m.computeMatOnce=1
#    m.computeRhsOnce=1
aTS=None
#schema en temps implicite lineaire
#if (testNLTS):
aTS=implicitNonLinearTimeScheme()
#else:
#    aTS=implicitLinearTimeScheme()
#    aTS.computeMatOnce=1
aTS.theta=1
aTS.maxIt=5
#simu in t \in [0,1]
t0=0.
t1=2.
simulator(t0,t1,aStudy)
#based on fix time step 0.05
aTStep=timeDoOneStep(aStudy,aTS,0.05)


### Adjoint system ###
# we do first simu with param => will be the ref
# then we do an other simu with other param => it will be the u_a
#
# REF #
aStudy.setParam([1,0.5])
aStudy.simulator.doSimu()
path=aStudy.absoluteStudyDir+"/UREF/"
if(exists(path)):
    shutil.rmtree(path)
shutil.copytree(aStudy.simuDir, path)
#quadraticDiff
quaDiff=quadraticDiff(ds2d1)

#for visu, export simulation steps to vtk
#aStudy.simulator.exportSimuToVtk()


# We test with different epsilon and parameters.
print("Param a_ref =",aStudy.getParam)
@pytest.mark.parametrize("defaultParam",[([0.5,0.1]),([1.5,1.]),([1.2,.6])])
def test_GradWithFiniteDifference(defaultParam):
    """
    test the approximation of GradV mse with finite Difference.
    
    we used center difference: f'(x) = [f(x+eps/2) - f(x-eps/2)] / eps
    the test is do with param far from ref, aprroximate to ref.
    """
    # SIMU #
    #D = 5.5
    #aDiffModel.setDiffCoef(D)
    print("\n*************\n\tParam a_sim =",defaultParam)
    aStudy.setParam(defaultParam)
    aStudy.simulator.doSimu()
    #
    VIn = quaDiff.computeV()
    quaDiff.computeGradV()
    GJ = quaDiff.gradJ
    print("gradV = :",GJ)
    param= [None]*aStudy.nbParam
    gradDif = [None]*aStudy.nbParam
    tabEpsilon = [5e-3, 5e-4]
    for eps in tabEpsilon:
        print("** eps for diff finie = ",eps)
        invEps = 1/eps
        for i in range(aStudy.nbParam):
            #define param
            param= [defaultParam[k] + (k==i)*eps for k in range(aStudy.nbParam)]
            aStudy.setParam(param)
            #Simu
            aStudy.simulator.doSimu()
            VForeward = quaDiff.computeV()
            ##define param
            #param= [defaultParam[k] - (k==i)*eps*0.5 for k in range(aStudy.nbParam)]
            #aStudy.setParam(param)
            ##Simu
            #aStudy.simulator.doSimu()
            ##val of V
            gradDif[i] = (VForeward - VIn)*invEps
        print("gradV of finite element = ",gradDif)
        print("gradV of MSE = ", GJ)
        print("dir grad FE: ",gradDif[1]*1./gradDif[0])
        print("dir grad MSE: ",GJ[1]*1./GJ[0])
        #On compare la direction du gradient et non chaque composant du gradient 1 à 1
        normGJ_Adj = ((GJ[0])**2 + (GJ[1])**2)**0.5
        normGJ_Dif =((gradDif[0])**2 + (gradDif[1])**2)**0.5
        print("norm adj = ",normGJ_Adj)
        print("norm dif = ",normGJ_Dif)
        dir_Adj = GJ[1]*(1./GJ[0])if (gradDif[0]!=0 and GJ[0] !=0) else GJ[0]*(1./GJ[1])
        dir_Dif = gradDif[1]*(1./gradDif[0])if (gradDif[0]!=0 and GJ[0] !=0) else gradDif[0]*(1./gradDif[1])
        errDir =  abs(dir_Adj - dir_Dif)*(1/dir_Dif)
        errNorm = abs(normGJ_Adj-normGJ_Dif)*(1./normGJ_Dif)
        print(f"Erreur direction: {errDir}%")
        print(f"Erreur norm2: {errNorm}%")
        assert(errNorm < 1.5*1e-1)
        assert(errDir < 4.*1e-1)
    #for i in range(aStudy.nbParam):
    #    tmpscal = gradDif[i] if gradDif[i] !=0 else 1
    #    assert(abs((gradDif[i] - GJ[i])/tmpscal)< 5*1e-2)
