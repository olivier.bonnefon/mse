import os

from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.systemAdjoint import systemAdjoint
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
import numpy as np
import pytest
from os import  mkdir
from os.path import isdir

"""
exit(0) ---> clean exit without errors
exit(1) ---> issues with the result
exit(2) ---> issues with update of parameters
"""



#un modele
nbComps=1
    
    #une etude
studName="TEST_STATE"
aStudy=study(studName)
aStudy.setGeometry("SQUARE",0.4)

from mse.state import state
    #un systeme
aSystem=system(aStudy)
    #un systeme dynamique
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
#un systeme adjoint
aSysAdj = systemAdjoint(aStudy) 
    

def test_state():
    aState1=state(ds2d1)
    aState2=state(ds2d1)
    for comp in aState1.comps:
        comp.x.array[:]=1

    for comp in aState1.comps:
        for a in comp.x.array:
            assert (abs(a-1)<1e-16)

    aState1.scale(5.0)
    for comp in aState1.comps:
        for a in comp.x.array:
            assert (abs(a-5)<1e-16)

    for comp in aState2.comps:
        comp.x.array[:]=7

    aState1.axpy(aState2,2.0)

    for comp in aState1.comps:
        for a in comp.x.array:
            assert (abs(a-19)<1e-16)

