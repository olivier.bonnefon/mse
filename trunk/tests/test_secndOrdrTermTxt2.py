from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel, txtDiffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.systemAdjoint import systemAdjoint 
from mse.obsProcess import quadraticDiff
from mse.simulator import simulator,simulatorBackward
from mse.timeDoOneStep import timeDoOneStep,timeDoOneStepBackward
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme,implicitLinearBackwardTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory

import numpy as np
import pytest
from os import  mkdir
from os.path import isdir, exists
import shutil



### NOTE: without sourceTerm ? only 2nd order term read in file ?
## test with constant sourceTerm + other.
### we had obsprocess to compute jacobian
# we will do 2 simu, one ref and one simu, and change only diffusion order term.
# if same result: problem. NOTE: don't check mass !! same both


#un modele
nameModele="logisticGrowthParam"
nameSendOrdTerm="diff2dIsotrop"
nbComps=1
WITH_AFFINE_GROWTH=1
nbparam=3
eps=1e-6
eps2=1e-2

    #une etude
studName="TEST_SNDORDTERM2"
aStudy=study(studName,nbparam)
aStudy.setGeometry("SQUARE",0.4) 
    #un systeme
aSystem=system(aStudy)
    #un systeme dynamique
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
     
#add diffusion motion
# param = [R, K, D]]
aStudy.setParam([1.,.5,5.])
aDiffModel=txtDiffusionModel(ds2d1, nameSendOrdTerm)
m=txtModel(ds2d1,nameModele)

aTS=None
# select the time algorithm
aTS=implicitNonLinearTimeScheme()
aTS.theta=1
aTS.maxIt=5
#simu in t \in [0,2]
simulator(0,2.,aStudy)
#based on fix time step 0.05
aTStep=timeDoOneStep(aStudy,aTS,0.05)
#run the simulation
aStudy.simulator.doSimu()

print("param Ref= ",aStudy.param)


### Adjoint system ###
# we do first simu with param => will be the ref
# then we do an other simu with other param => it will be the u_a
#
# REF #
aStudy.simulator.doSimu()
path=aStudy.absoluteStudyDir+"/UREF/"
if(exists(path)):
    shutil.rmtree(path)
shutil.copytree(aStudy.simuDir, path)
#quadraticDiff
quaDiff=quadraticDiff(ds2d1)

#for visu, export simulation steps to vtk
#aStudy.simulator.exportSimuToVtk()


###compare value
#TODO: move this test in test_secndOrdTerm.py ???
#aStudy.setParam([1.,.5,5.5])
#aStudy.simulator.doSimu()
#def test_valueUrefSimu():
#    exploreTrajRef=explorTrajectory(aStudy,quaDiff.dirURef)
#    exploreTrajSim=explorTrajectory(aStudy,aStudy.simuDir)
#    # Rewind: prepare simu
#    exploreTrajRef.rewind()
#    exploreTrajSim.rewind()
#    # Replay, we will replay n times and check if values are different
#    tabRef=np.zeros(len(ds2d1.utm1.comps[0].x.array))
#    for i in range(5):
#        exploreTrajRef.replay()
#        #copie profonde
#        tabRef[:]=ds2d1.utm1.comps[0].x.array[:]
#        exploreTrajSim.replay()
#    # Compare: we will compare both value and check if they are different
#    #partial copy
#    tabSim=ds2d1.utm1.comps[0].x.array
#    assert(tabSim[5] != tabRef[5])

# SIMU #
aStudy.setParam([1.2,.6,5.5])
aStudy.simulator.doSimu()
### Now we will compute the gradV like in test_backward, and compare with kpp3

#for visu, export simulation steps to vtk
aStudy.simulator.exportSimuToVtk()
VIn = quaDiff.computeV()
print("param Simu= ",aStudy.param)
quaDiff.computeGradV()
print("h")
GJ = quaDiff.gradJ
print("gradV = :",GJ)
def test_valueUrefSimu():
    # We comparethe result with the result of KPP3
    gradKPP3 = [0.2953702967205263, 0.11021221334867048, 0.0013629142893941196]
    normKPP3 =np.sqrt( 0.0013629142893941196**2 + 0.11021221334867048**2 + 0.2953702967205263**2 )
    normDiff = 0
    for i in range(len(gradKPP3)):
        #NOTE we add first the smallest number => 3-i in loop
        print(i)
        normDiff+= (gradKPP3[2-i] - GJ[2-i])**2
    errRelativ = np.sqrt(normDiff)
    print("errRelativ = ",errRelativ)
    assert (errRelativ < 0.05 )

#doPlot=False
#paramPath=[]
#globalCount=0
##une etude avec diffusion constante
#studName="TEST_SNDORDTERM2_CST"
#
#aStudy=study(studName, nbParam=2)
#aStudy.setGeometry("SQUARE",0.4)
#
##un systeme
#aSystem=system(aStudy)
##un systeme dynamique
#ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
##ds2d1.initialState[0]="0.0+0.0*"+ds2d1.initialState[0]
#ds2d1.initialStateAdjoint[0]="0.0+0.0*"+ds2d1.initialStateAdjoint[0]
#
##la diffusion
#aDiffModel=diffusionModel(ds2d1)
##ds2d1.addSecondOrderTerm(aDiffModel)
#D = 5.
#aDiffModel.setDiffCoef(D)
##if (WITH_AFFINE_GROWTH):
#    #affineGrowth(ds2d1,R,K)
#m=txtModel(ds2d1,"logisticGrowthParam")
#aTS=None
##schema en temps implicite lineaire
#aTS=implicitNonLinearTimeScheme()
#aTS.theta=1
#aTS.maxIt=5
##simu in t \in [0,2]
#t0=0.
#t1=2.
#simulator(t0,t1,aStudy)
##based on fix time step 0.05
#aTStep=timeDoOneStep(aStudy,aTS,0.05)
#
#
#### Adjoint system ###
## we do first simu with param => will be the ref
## then we do an other simu with other param => it will be the u_a
##
## REF #
#aStudy.setParam([1,0.5])
#aStudy.simulator.doSimu()
#path=aStudy.absoluteStudyDir+"/UREF/"
#if(exists(path)):
#    shutil.rmtree(path)
#shutil.copytree(aStudy.simuDir, path)
##quadraticDiff
#quaDiff=quadraticDiff(ds2d1)
#
#print("********\nParam a_sim =",[1.2,0.6])
#aStudy.setParam([1.2,0.6])
#aStudy.simulator.doSimu()
##
#VIn = quaDiff.computeV()
#quaDiff.computeGradV()
#GJ = quaDiff.gradJ
#print("gradV = :",GJ)
##def aCallableOpt(intermediate_result: OptimizeResult):
##    """a callable use in intermediate odo a kdf  ozf zkf sofnqqk oajn 
##    """
##    print("In iteration: \n",intermediate_result)
##    paramPath.append(intermediate_result.x.copy())
##
##def findParamOpt(defaultParam):
##    """ call function to find param opt
##    """
##    global globalCount
##    # SIMU #
##    print("********\nParam a_sim =",defaultParam)
##    aStudy.setParam(defaultParam)
##    aStudy.simulator.doSimu()
##    #we call function to find paramOpt
##    optParam = minimize(quaDiff.sysAdj.valJAndGradJ, defaultParam, method="L-BFGS-B", bounds=[(0.1,3)], jac=True, tol=None, callback=aCallableOpt, options={"maxiter":7})
##    print (optParam)
##    tmpParamPath = paramPath[-1-optParam.nit:]
##    tmpParamPath.insert(0,np.array(defaultParam))
##    tmpParamPath = np.array(tmpParamPath)
##    print("path of param: ",tmpParamPath)
##    #fig to show
##    if (doPlot):
##        plt.figure()
##        plt.plot(tmpParamPath[:,0],tmpParamPath[:,1],'b+-')
##        plt.plot([1.],[1.],'r+')
##        plt.xlabel("Parameter p1")
##        plt.ylabel("Parameter p2")
##        plt.axis((0.5,1.5,0.5,1.5))
##        globalCount+=1
##        plt.title("parameter for test "+str(globalCount))
##        plt.savefig(studName+"/pathParameter"+str(globalCount)+".png")
##### list of param to test
##paramInit = [([0.9,1]),([1.2,1.]),([1,0.54]),([1,1.3]),([1.4,1.3]),([0.64,0.73])]
##### call of function to find paramOPt
##findParamOpt(paramInit[0])
##
##loop on simu to compute mass
##import numpy as np
##mass=np.zeros((ds2d1.nbComps),dtype=np.double)
##aExplorTraj=explorTrajectory(aStudy,aStudy.simuDir)
##aExplorTraj.rewind()
##while(aExplorTraj.replay()):
##    computeMass(ds2d1,mass)
##    print("mass "+str(aExplorTraj.curStep)+" = ",end="")
##    for m in mass:
##        print(m,end=", ")
##    print("")
##aStudy.end()
