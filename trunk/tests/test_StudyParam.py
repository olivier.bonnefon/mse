import os
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory

import numpy as np
import pytest
import warnings
from os import  mkdir
from os.path import isdir


class TestClass:
    nbComps=1
    WITH_AFFINE_GROWTH=1
    nbparam=1
    eps=1e-6
    eps2=1e-2
    #une etude
    studName="TEST_PARAM"
    aStudy=study(studName,nbparam)
    aStudy.setGeometry("SQUARE",0.4)
    #un modele
    nameModele="modeleTest"
    sourceModele = aStudy.absoluteStudyDir+nameModele+".txt"
    print("sourceModele = "+sourceModele+"\nfa=p1-a")
    if os.path.isfile(aStudy.absoluteStudyDir+nameModele+"Dv.txt"):
        #print("suppression fichier "+computerEnv.modelsDir+nameModele+"Dv.txt")
        os.remove(aStudy.absoluteStudyDir+nameModele+"Dv.txt")
    if os.path.isfile(sourceModele):
        #print("suppression fichier "+sourceModele)
        os.remove(sourceModele)
    f=open(sourceModele,"w")
    f.write("fa = p1-a")
    f.close()
    #un systeme
    aSystem=system(aStudy)
    #un systeme dynamique
    ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
    #ds2d1.initialState[0]="0.2+0.0*"+ds2d1.initialState[0]
    #la diffusion
    aDiffModel=diffusionModel(ds2d1)
    #ds2d1.addSecondOrderTerm(aDiffModel)
    aDiffModel.setDiffCoef(1.0)
    if (WITH_AFFINE_GROWTH):
        #affineGrowth(ds2d1,R,K)
        m2=txtModel(ds2d1,aStudy.absoluteStudyDir+nameModele)
    #schema en temps implicite lineaire
    aTS=implicitLinearTimeScheme()
    aTS.computeMatOnce=1
    simulator(0,1,aStudy)
    aTStep=timeDoOneStep(aStudy,aTS,0.1)
    mass=np.zeros((ds2d1.nbComps),dtype=np.double)



class TestSubClass(TestClass):
    rTab=[33.63342408,67.0051424]
    @pytest.mark.parametrize("result, index, param",
                        [(rTab[0], None, [1.]),
                        (rTab[1], None, [2.]),
                        (rTab[1], 1, [2.]),
                        (rTab[1], [1, 1], [2., 3.]),
                        (rTab[0], [0,1], [1.,2.])])
    def test_GoodParam(self, result, index, param):
        """ Test to know if paramters are update after call of study.setParam() and if the calculation value is correct.

        """
        r=True
        try:
            self.aStudy.setParam(param,index)
        except Exception:
            # We check if we get an error when index out of range
            if ((isinstance(index, int) and index > 0) or
            (isinstance(index, list) and any(val > 0 for val in index))):
                assert True
                return
            else:
                assert (False)
                return
        if((param[0]-self.eps<=self.ds2d1.paramD[0].value<=param[0]+self.eps)==False) :
            print("Bad update of param in dynamicalsystem")
            assert False
            return
        print("Hello 2!")
        self.aStudy.simulator.doSimu()
        #for visu, export simulation steps to vtk
        aExplorTraj=explorTrajectory(self.aStudy,self.aStudy.simuDir)
        aExplorTraj.rewind()
        while(aExplorTraj.replay()):
            computeMass(self.ds2d1,self.mass)
            #NOTE -> Warning in this line, but not find how to catvh it. (number of warnings = number of step of time)
            # ONLY IF we call "computeMass" in test_goodParam
        r=(-self.eps2<=self.mass[0]-result<=self.eps2)
        print("final mass = ",self.mass[0])
        assert True

