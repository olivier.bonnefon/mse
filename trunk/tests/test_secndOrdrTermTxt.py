from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel, txtDiffusionModel
from mse.MODELS.models import txtModel, model
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory

import numpy as np
import pytest
from os import  mkdir
from os.path import isfile



### NOTE: without sourceTerm ? only 2nd order term read in file ?
## test with constant sourceTerm + other.
# TODO: unit test

#un modele
nameModele="affineGrowth"
sourceModele = computerEnv.modelsDir+nameModele+".txt"
nbComps=1
WITH_AFFINE_GROWTH=1
nbparam=2
eps=1e-6
eps2=1e-2
testNLTS=0

    #une etude
studName="TEST_SNDORDTERM"
aStudy=study(studName,nbparam)
aStudy.setGeometry("SQUARE",0.4) 
    #un systeme
aSystem=system(aStudy)
    #un systeme dynamique
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
     
aStudy.setParam([2.,1.])
    
#add diffusion model txt

def test_creationTxtDiffModel():
    # aDiffModel type
    nameSendOrdTerm="sOTConst"
    aDiffModel=txtDiffusionModel(ds2d1, nameSendOrdTerm)
    assert issubclass(txtDiffusionModel, model)

def test_depedence():
    nameSendOrdTerm="sOTConst"
    aDiffModel=txtDiffusionModel(ds2d1, nameSendOrdTerm)
    assert(ds2d1.secondOrderTerm == aDiffModel)
    assert(aDiffModel.dS == ds2d1)
    assert(aDiffModel.txtFile == computerEnv.modelsDir+nameSendOrdTerm)

def test_valueVarfs():
    nameSendOrdTerm="sOTConst"
    aDiffModel=txtDiffusionModel(ds2d1, nameSendOrdTerm)
    print("varfs = ",aDiffModel.varfs)
    assert(aDiffModel.varfs[0]!=None)

def test_enableAdjoint():
    """Test if enableAdjoint() create file
    """
    nameSendOrdTerm="sOTConst"
    aDiffModel=txtDiffusionModel(ds2d1, nameSendOrdTerm)
    aDiffModel.enableAdjoint()
    assert(isfile(computerEnv.modelsDir+nameSendOrdTerm+"Dp.txt"))
