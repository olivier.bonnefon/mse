import pytest

from mse.obsProcess import obsProcess, quadraticDiff
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.toolsEnv import computerEnv,MSEPrint
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from dolfinx.fem import Function, assemble_scalar, Constant, form

import os
from os import  mkdir
from os.path import isdir
import shutil
import numpy as np


#######################
#        FIXME        #
# Problem in file time .txt
# t0 = 0, t1 =1, dt =0.1  => in file .txt: tfinal = 1.09999999.. !!

#eps: epsilon/zero machine, use for test like float:a!=0. => a>eps or a<-eps
eps=1e-8

p1=1.
p2=1.




WITH_AFFINE_GROWTH=1
nbparam=2
#une etude
studName="TEST_CLASS_QUADDIFF2"
aStudy=study(studName, nbparam)
aStudy.setGeometry("SQUARE",0.4)
#un systeme
aSystem=system(aStudy) 
print("aStudy create")


print("ds2d1 create")
nbComps=1
#un systeme dynamique
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
#la diffusion
aDiffModel=diffusionModel(ds2d1)
aDiffModel.setDiffCoef(1.0)


print("aTS create")
testNLTS=0
aTS=None
#schema en temps implicite lineaire
aTS=implicitLinearTimeScheme()
aTS.computeMatOnce=1
t0 = 0.
t1 = 0.999
timeStep = 0.1
simulator(t0 ,t1, aStudy)
aTStep=timeDoOneStep(aStudy,aTS,timeStep)



m=txtModel(ds2d1,"logisticGrowthParam")
#m.computeMatOnce=1
#m.computeRhsOnce=1
print("txtmodel create")
aStudy.setParam([1.,1.])

aStudy.simulator.doSimu()
def test_sameUandUref():
    """
    Test val of V and vtildprime with u == uref
    """
    quaDiff=quadraticDiff(ds2d1, aStudy.simuDir)
    #test V=0
    assert(quaDiff.computeV() <= eps)
    timeToTest = [0.,0.4,0.65,0.9]
    for t in timeToTest:
        quaDiff.computeTildevPrim(t)
        #test vtilprim(t) =0
        assert(np.all(np.less_equal(quaDiff.vtild.comps[0].x.array, np.ones(len(quaDiff.vtild.comps[0].x.array))*eps)))

def test_easyCaseUrefUsim():
    """
    Test with uref == 0 and u(x,t) == p2 ==1
    """
    #Ref:
    ds2d1.initialState[0]="0.0*"+ds2d1.initialState[0]
    aStudy.simulator.doSimu()
    path=aStudy.absoluteStudyDir+"/UREF/"
    if(os.path.exists(path)):
        shutil.rmtree(path)
    shutil.copytree(aStudy.simuDir, path)
    quaDiff=quadraticDiff(ds2d1, path)
    #Simu:
    ds2d1.initialState[0]="1.0 + 0.0*"+ds2d1.initialState[0]
    aStudy.simulator.doSimu()
    print("\nquadiff V = ",quaDiff.computeV())
    assert(abs(quaDiff.computeV()-55.)<eps)
    timeToTest = [0.,0.4,0.65,0.9]
    for t in timeToTest:
        quaDiff.computeTildevPrim(t)
        print("vtp of "+str(t)+" = ", quaDiff.vtild.comps[0].x.array[0:4])
        absvsol=np.abs(quaDiff.vtild.comps[0].x.array[:]-np.ones(len(quaDiff.vtild.comps[0].x.array))*2.0)
        assert(np.all(np.less_equal(absvsol, np.ones(len(quaDiff.vtild.comps[0].x.array))*eps)))

