import mse.obsProcess
from mse.toolsEnv import computerEnv,MSEPrint
import pytest

from mse.obsProcess import obsProcess, quadraticDiff
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.systemAdjoint import systemAdjoint
from mse.simulator import simulator,simulatorBackward
from mse.timeDoOneStep import timeDoOneStep,timeDoOneStepBackward
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme,implicitLinearBackwardTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.state import state
from mse.explorTrajectory import explorTrajectory
from os import  mkdir, rename, remove
import os
import shutil
from os.path import isdir

nbComps=1
WITH_AFFINE_GROWTH=1
testNLTS=0

nbParam = 2
#une etude
studName="TEST_OBS_PROCESS"

aStudy=study(studName, nbParam)
aStudy.setGeometry("SQUARE",1.)

#un systeme
aSystem=system(aStudy)
#NOTE: create system in the init of study ??? + same for sysAdj ???
#un systeme dynamique
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
#ds2d1.initialState[0]="0.2+0.0*"+ds2d1.initialState[0]
ds2d1.initialState[0]="0.0*"+ds2d1.initialState[0]
#un systeme adjoint
aSysAdj = systemAdjoint(aStudy)

#la diffusion
aDiffModel=diffusionModel(ds2d1)
#ds2d1.addSecondOrderTerm(aDiffModel)
aDiffModel.setDiffCoef(1.0)
if (WITH_AFFINE_GROWTH):
    #affineGrowth(ds2d1,R,K)
    m=txtModel(ds2d1,"logisticGrowthParam")
    m.computeMatOnce=1
    m.computeRhsOnce=1

aTS=None
#schema en temps implicite lineaire
if (testNLTS):
    aTS=implicitNonLinearTimeScheme()
else:
    aTS=implicitLinearTimeScheme()
    aTS.computeMatOnce=1

t0 = 0.
t1 = 4.
timeStep = 0.5
simulator(t0 ,t1, aStudy)
aTStep=timeDoOneStep(aStudy,aTS,timeStep)

obsP=obsProcess(ds2d1)
aTimeSchemeBackward = implicitLinearBackwardTimeScheme(aStudy.simuDir)
aSimuBackward = simulatorBackward(t1,t0,aStudy)
aTDoStepBackward = timeDoOneStepBackward(aStudy, aTimeSchemeBackward, 0.5)
#create directory with AdjointState
aStudy.setParam([2.,1.])
aStudy.simulator.doSimu()
path=aStudy.adjointDir
if(os.path.exists(path)):
    shutil.rmtree(path)
shutil.copytree(aStudy.simuDir, path)
#we change /ADJOINT_SIMU/time.txt to match with a adjoint simu (backward)
# create time backward
fnewtime = open(path+"tmptime.txt",'w')
ftime = open(path+"time.txt")
time = ftime.readlines()
backTime = time[::-1]
#create backward file
for line in backTime: fnewtime.write(line) 
ftime.close()
fnewtime.close()
#rename file time
rename(path+"tmptime.txt", path+"time.txt")

aStudy.setParam([2.,4.])
aStudy.simulator.doSimu()

def test_ClassObsProcess_Ds():
    """test of class obsProcess and dynamicalSystem
    """
    assert (obsP.dS == ds2d1)
    assert(ds2d1.obsProcess == obsP)
def test_ClassObsProcess_Dir():
    """test if directory is correct in class obsProcess
    """
    assert (obsP.dirURef == aStudy.absoluteStudyDir+"/UREF/")
