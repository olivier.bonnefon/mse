# Test the resolution of the system adjoint, using backward simulator, timeScheme backward and timeDoOnestepBackward
#
# First we will do a simulation, second we will create the adjoint system and third do simulation to resolve the adjoint system
#  the model will be a logistic growth with param
#from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel, txtDiffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.systemAdjoint import systemAdjoint 
from mse.obsProcess import quadraticDiff
from mse.simulator import simulator,simulatorBackward
from mse.timeDoOneStep import timeDoOneStep,timeDoOneStepBackward
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme,implicitLinearBackwardTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import  mkdir
from os.path import isdir, exists
import numpy as np
import matplotlib.pyplot as plt
import shutil
import qnb



#On se retrouve dans un probleme equivalent de ColabMarc avec (D,R,K) = (p3,p1,p2) et sans le taux de mortalité
"""
# This study show how to estimate parameter for KPP-Fisher equation. The model is like:
#   \partial_t u(t,x,y) =  p1*u(t,x,y) * (1-u(t,x,y)/p2) + \\nabla p1*u(t,x,y)
#       (x,y) in \Omega (\Omega in R^2), t in [T_begin, T_end], and pi parameter to define (in R)
#   u(0,x,y) = u_0(x,y)
#   \partial_n u(t,x,y) = 0  \\forall t and for (x,y) in boudary
# In this example, we do a simulation to create the reference
"""



# ****** Can be change by user
# Boolean, if we need to create a reference
needRef = True
# Number of compement
nbComps=1
# Number of parameter
nbParam=2
# Dimension
dim=2
# Name gives to file result
studName="STUDY11"
# Name of directory's Mesh
#NOTE: to choose your own mesh, create a directory with your files in  the directory "BD_GEOMETRY"
myDirMesh = "SQUARE"
# Time for simulation, t \in [t0,t1]
t0=0.
t1=2.
# TimeStep for simulation
stepT = 0.05
# Initiale State,
#The initiale state must be write in a str, with x[0] for x coordinate and x[1] for y coordinate; with python notation.
#If you need specific function, use numpy (the import is done, numpy as np)
#By default ds2d1.initialStateAdjoint[0] = "np.exp(-(x[0]**2+x[1]**2))"
#For example: "x[0]*12.5 + np.exp(-x[1]**2)"
#NOTE: Even if your initiale state is homogenous in space, you must have to write x[0] and x[1]
#for example: (x[0]+x[1]) * 0 + cst
#myInitState = "0.0+1.0*np.exp(-(x[0]**2+x[1]**2))"
# model (without the diffusion term)
# check Readme for more detail about file model
myModel = "logisticGrowthRParam"
# Diffusion Model
nameSendOrdTerm="diff2dIsotropP1"

# ****** End of user changes

# Creation of the study
aStudy=study(studName, nbParam)

# Define the mesh
aStudy.setGeometry(myDirMesh,0.4)
#aStudy.setGeometry(myDirMesh,0.2)#NOTE: same as KPP3 ?

# Dynamical system
ds2d1=dynamicalSystem(aStudy,dim,"surface1.xdmf",nbComps=nbComps)

paramRef = [5., 1.]
aStudy.setParam(paramRef)
# Diffusion
aDiffModel=txtDiffusionModel(ds2d1, nameSendOrdTerm)
# Model
m=txtModel(ds2d1,myModel)
aTS=None
#schema en temps implicite lineaire
aTS=implicitNonLinearTimeScheme()
aTS.theta=1
aTS.maxIt=5
simulator(t0,t1,aStudy)
#based on fix time step 0.05
aTStep=timeDoOneStep(aStudy,aTS,stepT)


### Adjoint system ###
# we do first simu with param => will be the ref
# then we do another simu with other param => it will be the u_a
#
# REF #
if (needRef):
    #FIXME: can we change the time step ? dt=0.1 ?
    #aTStep.dt=0.1
    print("Build ref for example with param = ",paramRef)
    aStudy.simulator.doSimu()
    path=aStudy.absoluteStudyDir+"/UREF/"
    if(exists(path)):
        shutil.rmtree(path)
    shutil.copytree(aStudy.simuDir, path)
    #aTStep.dt=stepT
#quadraticDiff
quaDiff=quadraticDiff(ds2d1)

### Function for optimizer
def computeJForMIN(p1):
    p = p1.tolist()
    aStudy.setParam(p)
    aStudy.simulator.doSimu()
    return quaDiff.computeV()

def computeJac(p1):
    return quaDiff.computeGradV()




##### Call to opmtimizer
dim=2
x0=np.array([10., 5.])
f=computeJForMIN(x0)
g=computeJac(x0)
dxmin=np.array([1.0,1.0])
precision=1e-4
lb=np.array([0.01,0.1])
ub=np.array([12.0,5.0])

dxmin=precision*(ub-lb)
noptim=0
df1=f
mode =1
imp=3
io=(noptim)*1000+10
niter=100
nsim=3*niter
reverse=1
noptim=0
wzf=np.zeros(1024)
wif=np.zeros(1024,dtype=np.int32)
epsabs_ori=1e-18
epsabs=np.array([epsabs_ori])
modenp=np.array([1],dtype=int)
nit=0
print("nit ",nit,"x ",x0,"f ",f,"g ",g)
qnb.n2qn1(x0,f,g,dxmin,df1,epsabs,imp,io,modenp,niter,nsim,lb,ub,wif,wzf,reverse,dim)
print("mode n2qn1: ",modenp[0])
while(modenp[0]>7):
    nit=nit+1
    f=computeJForMIN(x0)
    g=computeJac(x0)
    print("nit ",nit,"x ",x0,"f ",f,"g ",g)
    epsabs=np.array([epsabs_ori])
    nsim=3*niter
    qnb.n2qn1(x0,f,g,dxmin,df1,epsabs,imp,io,modenp,niter,nsim,lb,ub,wif,wzf,reverse,dim)
    #io=io+1
    print("mode n2qn1: ",modenp[0])
    


