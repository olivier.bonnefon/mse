#demo showing who import safran data 
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import  mkdir
from os.path import isdir
import os
from mse.MESH_IMPORT_GMSH.view import viewTopo
import numpy as np
nbComps=1
studName="STUDY_AEDES"
D2D_AEDES=1500
D1D_AEDES=1500
#a study
aStudy=study(studName)
# the study zone
aStudy.setGeometry("FRANCE_ROUTES")

ds2ds=[]
for w in aStudy.topo.Wires:
    aDs=dynamicalSystem(aStudy,2,"surface"+str(w.id)+".xdmf",nbComps=nbComps)
    diffusionModel(aDs).setDiffCoef(D2D_AEDES)
    txtModel(aDs,"logistic")
    aDs.initialState[0] = "0.0*"+aDs.initialState[0]
    ds2ds.append(aDs)



from dolfinx.io import VTKFile

from mse.COVARIABLE.covariable import covariableFromCovFile,covariable

import pymseMapTools.mseMapTools as mp

for aDs in ds2ds:
        tableau = np.copy(aDs.vSpace.tabulate_dof_coordinates()).reshape((3*aDs.nbDofsPerComp))
        if (not isdir(aStudy.covDir+"/ds"+str(aDs.id)+"/P/")):
            mkdir(aStudy.covDir+"/ds"+str(aDs.id)+"/P/")
        mp.computePhotoPeriod(tableau,"P",aStudy.covDir+"/ds"+str(aDs.id)+"/P/",10.0)

aVtkFile=VTKFile(ds2ds[0].dolfinMesh.comm, aStudy.exportDir+"P.pvd","w")
for aDs in ds2ds:
    Pcov= covariableFromCovFile( "P",aDs,"P")
    for time in range(365):
        Pcov.setValue(time)
        aVtkFile.write_function(Pcov.femCov,time)

aVtkFile.close()

for aDs in ds2ds:
        tableau = np.copy(aDs.vSpace.tabulate_dof_coordinates()).reshape((3*aDs.nbDofsPerComp))
        if (not isdir(aStudy.covDir+"/ds"+str(aDs.id)+"/D/")):
            mkdir(aStudy.covDir+"/ds"+str(aDs.id)+"/D/")
        mp.computePhotoPeriod(tableau,"D",aStudy.covDir+"/ds"+str(aDs.id)+"/D/",-10.0)

aVtkFile=VTKFile(ds2ds[0].dolfinMesh.comm, aStudy.exportDir+"D.pvd","w")
for aDs in ds2ds:
    Pcov= covariableFromCovFile( "D",aDs,"D")
    for time in range(365):
        Pcov.setValue(time)
        aVtkFile.write_function(Pcov.femCov,time)

aVtkFile.close()


aStudy.end()


