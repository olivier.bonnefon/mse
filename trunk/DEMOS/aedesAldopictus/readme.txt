Export des donnees de temperatures par annees jours apres jours, 

1) python3 studySafran.py

- creer les maillages
- extrait les température moyenne journalière (dans le repertoire COV/dsi/2003) dans fichiers safran
- calcul le GDD
- visualiser avec paraview STUDY_AEDES/VTK/GDDT.pvd

les programmes fonctionnent mais doivent etre ajuster pour les besoin du simulateur

2) python3 studyPhotoPeriod.py

- se base sur le maillage precedent
- calcul les photoperiodes seuillees


visualisation: paraview STUDY_AEDES/VTK/P.pvd
les programmes fonctionnent mais doivent etre ajuster pour les besoin du simulateur

3) appel du simulateur

- se base sur le maillage precedent
 en cours de dev
