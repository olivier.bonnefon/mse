import mse.MESH_IMPORT_GMSH.read as reader
from mse.MESH_IMPORT_GMSH.view import viewTopo
from mse.toolsEnv import computerEnv,MSEPrint

aTopo=reader.geomTopoReader(computerEnv.geomDir+"FRANCE_ROUTES/")
aViewer=viewTopo(aTopo)
for e in aTopo.Edges:
    if (e.id==0):
        pass
    if (e.edtype==1):
        e.plot(aTopo.Points,aTopo.labelPt,1)
aViewer.show()


