#demo showing who import safran data 
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import  mkdir
from os.path import isdir
import os
from mse.MESH_IMPORT_GMSH.view import viewTopo
import numpy as np
nbComps=1
studName="STUDY_AEDES"
D2D_AEDES=1500
D1D_AEDES=1500
#a study
aStudy=study(studName)
# the study zone
aStudy.setGeometry("FRANCE_ROUTES",10000)

ds2ds=[]
for w in aStudy.topo.Wires:
    aDs=dynamicalSystem(aStudy,2,"surface"+str(w.id)+".xdmf",nbComps=nbComps)
    diffusionModel(aDs).setDiffCoef(D2D_AEDES)
    txtModel(aDs,"logistic")
    aDs.initialState[0] = "0.0*"+aDs.initialState[0]
    ds2ds.append(aDs)



from dolfinx.io import VTKFile

import pymseSafranTools.mseSafranTools as ms
doSafranExport=True
for year in (2003,2004):
    if (not doSafranExport):
        break
    #doing export from from 01 august at 7 oclock year to 01 august at 6 oclock year+1
    print("doing export from from 01 august at 7 oclock "+str(year)+" to 01 august at 6 oclock "+str(year+1)) 
    #
    # this demo needs a safran file:
    #
    pathToSafranFile="../../mseSafranTools/data/SAFRAN_"+str(year)+"080107_"+str(year+1)+"080106.nc"
    if (year!=2003):#because only one safran file (remove it for reel usage)
        pathToSafranFile="../../mseSafranTools/data/SAFRAN_2003080107_2004080106.nc"
    #data from 01 august at 7 oclock year to 01 august at 6 oclock year+1
    #number of hour between 
    # number of day in year : 31 +30+31+30+31=153 days (153x24= 3672 h) interval 0-3672
    # number of day in year+1 : 365-153 days x24=5088 h interval 3672-8760
    if ( not os.path.exists(pathToSafranFile)):
        print("can not find the safran file: "+pathToSafranFile)
        exit(1)
    nRegHours=np.array([0], dtype=np.int32)
    ms.getSafranInfo(pathToSafranFile,nRegHours)
    ms.readSafranDatas(pathToSafranFile,ms.SAFRAN_TEMPERATURE)
    #one export end 01-08-2003 tu 31-12-2003
    ms.timeDataToTimeSimulator(0,3672,24,365-153,365)
    for aDs in ds2ds:
        tableau = np.copy(aDs.vSpace.tabulate_dof_coordinates()).reshape((3*aDs.nbDofsPerComp))
        # Lirf le fichier binaire et charger les données dans un tableau numpy
        if (not isdir(aStudy.covDir+"/ds"+str(aDs.id))):
            mkdir(aStudy.covDir+"/ds"+str(aDs.id))
        if (not isdir(aStudy.covDir+"/ds"+str(aDs.id)+"/"+str(year))):
            mkdir(aStudy.covDir+"/ds"+str(aDs.id)+"/"+str(year))
        ms.perform(tableau,"T",ms.MSE_CST_NETCDF_TYPE_AVERAGED,aStudy.covDir+"/ds"+str(aDs.id)+"/"+str(year)+"/")
    ms.timeDataToTimeSimulator(3672,8760,24,0,365-153)
    for aDs in ds2ds:
        tableau = np.copy(aDs.vSpace.tabulate_dof_coordinates()).reshape((3*aDs.nbDofsPerComp))
        # Lirf le fichier binaire et charger les données dans un tableau numpy
        if (not isdir(aStudy.covDir+"/ds"+str(aDs.id))):
            mkdir(aStudy.covDir+"/ds"+str(aDs.id))
        if (not isdir(aStudy.covDir+"/ds"+str(aDs.id)+"/"+str(year+1))):
            mkdir(aStudy.covDir+"/ds"+str(aDs.id)+"/"+str(year+1))
        ms.perform(tableau,"T",ms.MSE_CST_NETCDF_TYPE_AVERAGED,aStudy.covDir+"/ds"+str(aDs.id)+"/"+str(year+1)+"/")
    ms.finishSafran()

import pymseMapTools.mseMapTools as mp
for aDs in ds2ds:
    mp.computeGDD(aStudy.covDir+"/ds"+str(aDs.id)+"/2004/","GDD",
              aStudy.covDir+"/ds"+str(aDs.id)+"/2004/","T",
              273.16+10.0,293.0)

#for visualisation
from mse.COVARIABLE.covariable import covariableFromCovFile,covariable
#covariable.verbose(True)
aVtkFile=VTKFile(ds2ds[0].dolfinMesh.comm, aStudy.exportDir+"GDDT.pvd","w")
for aDs in ds2ds:
    Tcov= covariableFromCovFile( "T",aDs,"2004")
    GDDcov= covariableFromCovFile( "GDDT",aDs,"2004")
    #for it,time in enumerate(range(270)[-1.0,1,5,10,100.0,270,276,277,278,300,364.0,400.0]):
    for time in range(120,270):
        GDDcov.setValue(time)
        Tcov.setValue(time)
        if (not isdir(aStudy.exportDir+"/Tds"+str(aDs.id))):
            mkdir(aStudy.exportDir+"/Tds"+str(aDs.id))
        if (not isdir(aStudy.exportDir+"/Tds"+str(aDs.id)+"/2004/")):
            mkdir(aStudy.exportDir+"/Tds"+str(aDs.id)+"/2004/")
        Tcov.exportVtk(aStudy.exportDir+"/Tds"+str(aDs.id)+"/2004/T"+str(time))
        aVtkFile.write_function(GDDcov.femCov,time)
        #GDDcov.exportVtk(aStudy.exportDir+"/Tds"+str(aDs.id)+"/2004/GDDT"+str(time))
aVtkFile.close()

aStudy.end()


