# simulation 2D, with linear system
# test linear and non-linear time stepping
#
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep,adaptivDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme,timeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import  mkdir
from os.path import isdir
from mse.INTERACTIONS.interaction import interaction2D1D,interactionEdgesConnect,interaction
interaction.verbose(True)
timeDoOneStep.verbose(True)
timeScheme.verbose(True)
nbComps=1
studName="STUDY_AEDES"
D2D_AEDES=1500
D1D_AEDES=1500
#a study
aStudy=study(studName)
# the study zone, do not rebuid mesh
aStudy.setGeometry("FRANCE_ROUTES")

ds2ds=[]
for w in aStudy.topo.Wires:
    aDs=dynamicalSystem(aStudy,2,"surface"+str(w.id)+".xdmf",nbComps=nbComps)
    diffusionModel(aDs).setDiffCoef(D2D_AEDES)
    txtModel(aDs,"logistic")
    aDs.initialState[0] = "0.0*"+aDs.initialState[0]
    ds2ds.append(aDs)

dsA8E=None
ds1ds=[]
for e in aStudy.topo.Edges:
    if (e.id==0):
        pass
    if (e.edtype==1):
        aDs=dynamicalSystem(aStudy,1,"edge"+str(e.id)+".xdmf",nbComps=nbComps)
        diffusionModel(aDs).setDiffCoef(D1D_AEDES)
        ds1ds.append(aDs)
        for w,ads2d in zip(aStudy.topo.Wires,ds2ds):
            for ae in w.edges:
                if (ae==e.id):
                    interaction2D1D(aStudy,ads2d,aDs)
        if (e.label=="A8E"):
            dsA8E=aDs
            dsA8E.initialState[0] = "1.0+"+dsA8E.initialState[0]
if (dsA8E==None):
    print("cant find init DS")
    exit()

aTS=implicitNonLinearTimeScheme()
#simu in t \in [0,1]
simulator(0,25,aStudy)
#based on fix time step 0.05
aTStep=adaptivDoOneStep(aStudy,aTS,0.001,1024,1e-2,1e-2)
#run the simulation
aStudy.simulator.doSimu()


#for visu, export simulation steps to vtk
aStudy.simulator.exportSimuToVtk()

aStudy.end()
