#demo to build covariable from a raster.

from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
import pymseMatTools.mseMatTools as MT
from mse.COVARIABLE.covariable import covariableFromRaster

from os import  mkdir
from os.path import isdir

nbComps=1
WITH_AFFINE_GROWTH=1
testNLTS=0


#une etude
studName="STUDY1_DIFFUSION"
if (WITH_AFFINE_GROWTH):
    studName=studName+"_AND_AFFINE_GROWTH"

#a study
aStudy=study(studName)
# the study zone
aStudy.setGeometry("SQUARE",0.4)




#un systeme
aSystem=system(aStudy)
#un systeme dynamique
ds2d1=dynamicalSystem(aStudy,1,2,aMeshesDir+"surface1.xdmf",nbComps=nbComps)

rasterFile="/home/usermse/RASTER/R3Raster"
acov=covariableFromRaster(ds2d1,"T")
acov.fillFromRaster(rasterFile)
acov.exportVtk("testCovRaster")
