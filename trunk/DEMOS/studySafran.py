#demo showing who import safran data 
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import  mkdir
from os.path import isdir
import os
from mse.MESH_IMPORT_GMSH.view import viewTopo
import numpy as np

nbComps=1
testNLTS=0
studName="STUDY_SAFRAN_"
viewTopo=1
GEONAME="FRANCE"
tol=10000
#a study
aStudy=study(studName+GEONAME)
# build mesh
if (os.path.exists(aStudy.meshesDir+"/surface1.xdmf")):
    aStudy.setGeometry(GEONAME)
else:
    aStudy.setGeometry(GEONAME,tol)

#a dynamical system
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)


import pymseSafranTools.mseSafranTools as ms
#
# this demo needs a safran file:
#
pathToSafranFile="../mseSafranTools/data/SAFRAN_2003080107_2004080106.nc"

if ( not os.path.exists(pathToSafranFile)):
    print("can not find the safran file: "+pathToSafranFile)
    exit(1)

nRegHours=np.array([0], dtype=np.int32);
ms.getSafranInfo(pathToSafranFile,nRegHours);
#one export per day
ms.timeDataToTimeSimulator(0,int(nRegHours[0]),24,0,1.0*nRegHours[0]/24)


tableau = np.copy(ds2d1.vSpace.tabulate_dof_coordinates()).reshape((3*ds2d1.nbDofsPerComp))
# Lirf le fichier binaire et charger les données dans un tableau numpy
ms.perform(pathToSafranFile,ms.SAFRAN_TEMPERATURE,tableau,"T",ms.MSE_CST_NETCDF_TYPE_BRUT,aStudy.covDir+"/ds"+str(ds2d1.id)+"/")
ms.finishSafran()


from mse.COVARIABLE.covariable import covariableFromCovFile

Tcov= covariableFromCovFile( "T",ds2d1)
for time in (-1.0,0.0,10.0,50.0,200.0,400.0):
    Tcov.setValue(time)
    Tcov.exportVtk(aStudy.exportDir+"/T"+str(time))

aStudy.end()


