#exemple with 4 dynamical systems
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import  mkdir
from os.path import isdir


nbComps=1
WITH_AFFINE_GROWTH=0
testNLTS=0
studName="STUDY12_DIFFUSION"
if (WITH_AFFINE_GROWTH):
    studName=studName+"_AND_AFFINE_GROWTH"

#a study
aStudy=study(studName)
# the study zone
#aStudy.setGeometry("SIMPLE2",0.4)


# a system
aSystem=system(aStudy)
#a dynamical  system 
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
ds2d2=dynamicalSystem(aStudy,2,"surface2.xdmf",nbComps=nbComps)
ds1d1=dynamicalSystem(aStudy,1,"boundary1.xdmf",nbComps=nbComps)
ds1d2=dynamicalSystem(aStudy,1,"boundary2.xdmf",nbComps=nbComps)
for ie in range(1,8):
    diffusionModel(dynamicalSystem(aStudy,1,"edge"+str(ie)+".xdmf",nbComps=nbComps))
#ds2d1.initialState[0]="0.2+0.0*"+ds2d1.initialState[0]
#ds2d1.initialState[0]="0.2+0.0*"+ds2d1.initialState[0]
if (nbComps>1):
    ds2d1.initialState[1]="0.5*"+ds2d1.initialState[0]
    ds2d2.initialState[1]="0.25*"+ds2d2.initialState[0]
#

#add diffusion motion
diffusionModel(ds2d1).setDiffCoef(1.0)
diffusionModel(ds2d2).setDiffCoef(1.0)
diffusionModel(ds1d1).setDiffCoef(1.0)
diffusionModel(ds1d2).setDiffCoef(1.0)
if (WITH_AFFINE_GROWTH):
    m=txtModel(ds2d1,"affineGrowth")
    m.computeMatOnce=1
    m.computeRhsOnce=1

aTS=None
# select the time algorithm
if (testNLTS):
    aTS=implicitNonLinearTimeScheme()
else:
    aTS=implicitLinearTimeScheme()
#simu in t \in [0,1]
simulator(0,1,aStudy)
#based on fix time step 0.05
aTStep=timeDoOneStep(aStudy,aTS,0.05)
#run the simulation
aStudy.simulator.doSimu()


#for visu, export simulation steps to vtk
aStudy.simulator.exportSimuToVtk()

#loop on simu to compute mass
import numpy as np
for ds in aStudy.system.dynamicalSystems:
    mass=np.zeros((ds.nbComps),dtype=np.double)
    aExplorTraj=explorTrajectory(aStudy,aStudy.simuDir)
    aExplorTraj.rewind()
    while(aExplorTraj.replay()):
        computeMass(ds,mass)
        print("mass "+str(aExplorTraj.curStep)+" = ",end="")
        for m in mass:
            print(m,end=", ")
        print("")
aStudy.end()
