
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme
from mse.INTERACTIONS.interaction import interaction2D1D
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory

from os import  mkdir
from os.path import isdir


WITH_AFFINE_GROWTH=1
nbComps = 2
TEST_2D1D=1
TEST_1D2D=2
CAS_TEST=TEST_2D1D

#une etude
studName="STUDY3BIS_DIFFUSION"
if (WITH_AFFINE_GROWTH):
    studName="STUDY3BIS_DIFFUSION_AND_AFFINE_GROWTH"

#a study
aStudy=study(studName)
# the study zone
aStudy.setGeometry("SQUARE",0.4)



#un systeme
aSystem=system(aStudy)
#un systeme dynamique
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf", nbComps = nbComps)
ds1d1=dynamicalSystem(aStudy,1,"boundary1.xdmf", nbComps = nbComps)
if (CAS_TEST == TEST_2D1D):
    ds1d1.initialState[0]="0.0*x[0]"
    ds2d1.initialState[0]="np.exp(-(x[0]**2+x[1]**2))"
    if(nbComps == 2):
        ds1d1.initialState[1]="0.0*x[0]"
        ds2d1.initialState[0]="np.exp(-(x[0]**2+x[1]**2))"
else:
    ds1d1.initialState[0]="np.exp(-(x[0]**2+x[1]**2))"
    ds2d1.initialState[0]="0.0*x[0]"
    if(nbComps == 2):
        ds1d1.initialState[0]="np.exp(-(x[0]**2+x[1]**2))"
        ds2d1.initialState[0]="0.0*x[0]"
#la diffusion
aDiffModel2d=diffusionModel(ds2d1)
aDiffModel1d=diffusionModel(ds1d1)
#ds2d1.addSecondOrderTerm(aDiffModel)
aDiffModel2d.setDiffCoef(1.0)
aDiffModel1d.setDiffCoef(1.0)
if (WITH_AFFINE_GROWTH):
    aAffineGrowth=txtModel(ds2d1,"preyPred")

aInter2D1D=interaction2D1D(aStudy,ds2d1,ds1d1)

simulator(0,10,aStudy)
#schema en temps implicite lineaire
aTS=implicitLinearTimeScheme()
aTStep=timeDoOneStep(aStudy,aTS,0.05)

aStudy.simulator.doSimu()
#for visu, export simulation steps to vtk
aStudy.simulator.exportSimuToVtk()
#loop on simu to compute mass
import numpy as np
mass1d=np.zeros((ds2d1.nbComps),dtype=np.double)
mass2d=np.zeros((ds2d1.nbComps),dtype=np.double)

aExplorTraj=explorTrajectory(aStudy,aStudy.simuDir)
aExplorTraj.rewind()
while(aExplorTraj.replay()):
    computeMass(ds1d1,mass1d)
    computeMass(ds2d1,mass2d)
    print("mass "+str(aExplorTraj.curStep)+" (1d 2d sum ) = ("+str(mass1d[0])+" "+str(mass2d[0])+" "+str(mass1d[0]+mass2d[0])+")")

aStudy.end()

