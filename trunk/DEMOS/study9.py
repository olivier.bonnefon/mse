#demo with 1D only

from mse.study import study

from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.INTERACTIONS.interaction import interaction2D1D
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint,ensureDirName
from mse.explorTrajectory import explorTrajectory
from dolfinx.io import VTKFile
import mse.MESH_IMPORT_GMSH.read as reader
import mse.MESH_IMPORT_GMSH.buildMeshes as buildMeshes


from os import  mkdir
from os.path import isdir
buildMeshes.verbose=1
WITH_KPP=1
class study1d(study):
    def setGeometry(self,geomName,tol):
        """
        This function builds the spatial discretisation (meshes) of the study zone

        :param geomName string: the geomzone of the mse env geometry directory
        :param tol double : the number of nodes by lenght unit
        """
        geomName=ensureDirName(geomName)
        #mesh of study zone
        #La geometrie
        aTopo=reader.geomTopoReader(computerEnv.geomDir+geomName)
        #build meshes can be only once
        buildMeshes.build1dXdmf(aTopo,self.meshesDir,tol)

class myDs(dynamicalSystem):
    def applyInitialState(self):
        numComp=0
        XYZ=self.vSpace.tabulate_dof_coordinates()
        for comp in self.utm1.comps:
            for nDof in range(XYZ.shape[0]):
                if (XYZ[nDof][1]<0.8):
                    comp.x.array[nDof]=1
                else:
                    comp.x.array[nDof]=0
 

#une etude
studName="STUDY9"

#a study
aStudy=study1d(studName)
# define the study zone and meshe it.
aStudy.setGeometry("TREE",0.4)

#un systeme
aSystem=system(aStudy)
#un systeme dynamique
#ds1d1=dynamicalSystem(aStudy,1,"AllEdges.xdmf")
ds1d1=myDs(aStudy,1,"AllEdges.xdmf")
vtkf = VTKFile(ds1d1.dolfinMesh.comm, "STUDY9/MESHES/mesh.pvd", "w")
vtkf.write(ds1d1.dolfinMesh)
vtkf.close()
#initial state
ds1d1.initialState[0]="if (x[1]<1) : np.exp(-((x[1])**2)) else: 0"

#la diffusion
aDiffModel1d=diffusionModel(ds1d1)
#ds2d1.addSecondOrderTerm(aDiffModel)
aDiffModel1d.setDiffCoef(0.1)

TF=10
#KPP
if (WITH_KPP):
    m=txtModel(ds1d1,"logisticGrowth")
#schema en temps implicite lineaire
aTS=implicitLinearTimeScheme()

simulator(0,TF,aStudy)
aTStep=timeDoOneStep(aStudy,aTS,1e-3*TF)

aStudy.simulator.doSimu()
#for visu, export simulation steps to vtk
aStudy.simulator.exportSimuToVtk()

#loop on simu to compute mass
import numpy as np
mass1d=np.zeros((ds1d1.nbComps),dtype=np.double)
aExplorTraj=explorTrajectory(aStudy,aStudy.simuDir)
aExplorTraj.rewind()
while(aExplorTraj.replay()):
    computeMass(ds1d1,mass1d)
    print("mass 1d"+str(aExplorTraj.curStep)+" = "+str(mass1d[0]))
aStudy.end()
