# study with 2 compatiments
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitNonLinearTimeScheme,timeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory

from os import  mkdir
from os.path import isdir


nbComps=2

#une etude
studName="STUDY5_PREYPRED"
#a study
aStudy=study(studName)
# the study zone
aStudy.setGeometry("SQUARE",0.4)

#un systeme
aSystem=system(aStudy)
#un systeme dynamique
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
ds2d1.initialState[0]="0.2+0.0*"+ds2d1.initialState[0]
if (nbComps>1):
    ds2d1.initialState[1]="0.5*"+ds2d1.initialState[0]

#la diffusion
aDiffModel=diffusionModel(ds2d1)
#ds2d1.addSecondOrderTerm(aDiffModel)
aDiffModel.setDiffCoef(1.0)
m=txtModel(ds2d1,"preyPred")

#schema en temps implicite non lineaire

aTS=implicitNonLinearTimeScheme()
simulator(0,10,aStudy)
aTStep=timeDoOneStep(aStudy,aTS,0.05)
timeScheme.verboseMode=0
aStudy.simulator.doSimu()
nSteps=aStudy.simulator.curStep+1

#for visu, export simulation steps to vtk
aStudy.simulator.exportSimuToVtk()


#loop on simu to compute mass
import numpy as np
memMass=np.zeros((ds2d1.nbComps)*nSteps,dtype=np.double).reshape((ds2d1.nbComps,nSteps))
mass=np.zeros((ds2d1.nbComps),dtype=np.double)

cmp=0
aExplorTraj=explorTrajectory(aStudy,aStudy.simuDir)
aExplorTraj.rewind()
while(aExplorTraj.replay()):
    computeMass(ds2d1,mass)
    memMass[0][cmp]=mass[0]
    memMass[1][cmp]=mass[1]
    print("mass "+str(aExplorTraj.curStep)+" = ", end=" ")
    for m in mass:
        print(m,end=", ")
    print("")
    cmp+=1

aStudy.end()

import matplotlib.pyplot as plt
f = plt.figure()
plt.plot(memMass[0],memMass[1])
plt.show()

f.savefig(aStudy.absoluteStudyDir+"preyPred.pdf", bbox_inches='tight')
