#study with 2 dynamical system 2D and 1D without interactions
#



from mse.study import study

from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.INTERACTIONS.interaction import interaction2D1D
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory

from os import  mkdir
from os.path import isdir

WITH_AFFINE_GROWTH=1


#une etude
studName="STUDY2_DIFFUSION"
if (WITH_AFFINE_GROWTH):
    studName="STUDY2_DIFFUSION_AND_AFFINE_GROWTH"


#a study
aStudy=study(studName)
# the study zone
aStudy.setGeometry("SQUARE",0.4)



#un systeme dynamique
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf")
ds1d1=dynamicalSystem(aStudy,1,"boundary1.xdmf")
#la diffusion
aDiffModel2d=diffusionModel(ds2d1)
aDiffModel1d=diffusionModel(ds1d1)
#ds2d1.addSecondOrderTerm(aDiffModel)
aDiffModel2d.setDiffCoef(1.0)
aDiffModel1d.setDiffCoef(1.0)
if (WITH_AFFINE_GROWTH):
    aAffineGrowth=txtModel(ds2d1,"affineGrowth")
    aAffineGrowth1d=txtModel(ds2d1,"affineGrowth")

#schema en temps implicite lineaire
aTS=implicitLinearTimeScheme()

simulator(0,10,aStudy)
aTStep=timeDoOneStep(aStudy,aTS,0.05)

aStudy.simulator.doSimu()
#for visu, export simulation steps to vtk
aStudy.simulator.exportSimuToVtk()

#loop on simu to compute mass
import numpy as np
mass2d=np.zeros((ds2d1.nbComps),dtype=np.double)
mass1d=np.zeros((ds1d1.nbComps),dtype=np.double)
aExplorTraj=explorTrajectory(aStudy,aStudy.simuDir)
aExplorTraj.rewind()
while(aExplorTraj.replay()):
    computeMass(ds2d1,mass2d)
    computeMass(ds1d1,mass1d)
    print("mass 2d 1d"+str(aExplorTraj.curStep)+" = "+str(mass2d[0])+" "+str(mass1d[0]))
aStudy.end()
