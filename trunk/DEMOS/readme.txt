study1.py : diffusion et  terme source linéaire.
study2.py : diffusion sur domaine 2D et 1D non connectés.
study3.py : diffusion sur deux domaines 2D 1D  connectés.
study4.py : test non linear time scheme using one compartiment
study5.py : test non linear time scheme using two compartiments
study6.py : test covariable sur un modèle linéaire textuel
study7.py : test raster import in covariable
study10.py: find param opt of KPP with sympy
study11.py: find param opt of KPP with m2qn1
