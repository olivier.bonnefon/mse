from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel, txtDiffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import  mkdir
from os.path import isdir
import os
from mse.MESH_IMPORT_GMSH.view import viewTopo
import numpy as np
from dolfinx.io import VTKFile
from dolfinx.fem import form
from mse.state import state
from mse.linAlgTools import vector
from ufl import grad,dot
import math 
import toolsMRR 

toolsMRR.buildMRRStudy(toolsMRR.model_heterogen_10000)



########################
# FIRST ckeck d_pi U
########################

params=np.array([66*66*0.5,76*76*0.5,0.23,.16])
#params=np.array([50*50*0.5,15*15*0.5,0.05,6])
#
print(params)
toolsMRR.aStudy.setParam(aParam=params)
toolsMRR.simulator.doSimu()
dt=toolsMRR.simulator.doOneStep.dt
toolsMRR.simulator.exportSimuToVtk()
cumDie=0
aExplorTraj=explorTrajectory(toolsMRR.aStudy,toolsMRR.aStudy.simuDir)
aExplorTraj.rewind()
mass=[0]
while(aExplorTraj.replay()):
    computeMass(toolsMRR.ds2d,mass)
    cumDie+=mass[0]*(1.0-np.exp(-params[2]*dt))

toolsMRR.aCapture.computeCaptured()

print("mass died alive captured total(shoul be 1) \n",cumDie,mass[0],
      np.sum(toolsMRR.aCapture.comptages) ,mass[0]+cumDie+np.sum(toolsMRR.aCapture.comptages))

