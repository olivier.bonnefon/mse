import numpy as np
from matplotlib.patches import Polygon
from scipy.spatial.distance import cdist


import math
#TRAPS DESCRPTIONS

radius2=10.0*10.0
Xtraps=np.array([-141.42, -138.08, -184.78, 50, 57.4   , -38.27, 19.13   , 0    , 76.54  , -393.92, -153.07, -95.67, -57.4   , 400    , 282.84, 212.13  , 168.68 , 100    , -35.36, -282.84, -95.67])
Ytraps=np.array([141.42 , 57.4   ,  -76.54, 0.,  138.58,  92.39,  -46.19 , -100., -184.78,  -99.46, -369.55, -230.97, -138.58,    0.  , -282.84, -212.13,  107.46,    0.  ,   35.36,  282.84,  230.97])


# Définition des polygones
landscapes = {
    "poly1": np.array([[-250, -69], [-137, -212], [-142, -212], [-170, -384], [-116, -507], [43, -509],
                  [42, -336], [119, -334], [243, -213], [151, -110], [145, 151], [183, 169], [160, 227], [-163, 178]]),
    "poly2": np.array([[-453, -105], [-337, -80], [-301, -140], [-308, -192], [-388, -295], [-417, -156], [-457, -147]]),
    "poly3": np.array([[463, 11], [392, 24], [376, 143], [246, 136], [251, 7], [427, -64], [481, -65]]),
    "poly4": np.array([[381, 510], [355, 389], [511, 275], [514, 516]]),
    "poly5": np.array([[-510, 90], [-427.5, 34.5], [-309, 9], [-351, 239], [-155, 516], [-510, 516]])
}

# Fonction pour vérifier si un point est à l'intérieur d'un polygone
def is_inside_polygon(point, polygon):
    return polygon.contains_point(point)

from scipy.spatial.distance import euclidean
# Fonction pour calculer la distance entre un point et un segment
def point_to_segment_distance(point, segment_start, segment_end):
    px, py = point
    sx, sy = segment_start
    ex, ey = segment_end
    # Vecteur du segment
    segment_vector = np.array([ex - sx, ey - sy])
    # Vecteur du point au début du segment
    point_vector = np.array([px - sx, py - sy])
    # Projection du vecteur point_vector sur le segment_vector
    segment_length_squared = np.dot(segment_vector, segment_vector)
    if segment_length_squared == 0:
        # Le segment est un point
        return euclidean(point, segment_start)
    t = max(0, min(1, np.dot(point_vector, segment_vector) / segment_length_squared))
    # Point projeté sur le segment
    projection = np.array([sx, sy]) + t * segment_vector
    # Distance entre le point et sa projection
    return euclidean(point, projection)

# Fonction principale pour calculer la distance au polygone
distSmooth=15
def point_to_polygons_distance(point, polygons):
    min_distance = float('inf')
    isInside=False
    for vertices in polygons.values():
        polygon = Polygon(vertices)
        isInside =isInside or  is_inside_polygon(point, polygon)
        # Calculer la distance minimale entre le point et les arêtes du polygone
        num_vertices = len(vertices)
        for i in range(num_vertices):
            segment_start = vertices[i]
            segment_end = vertices[(i + 1) % num_vertices]
            distance = point_to_segment_distance(point, segment_start, segment_end)
            min_distance = min(min_distance, distance)
    if (isInside):
        if (min_distance<distSmooth):
            return 1-0.5*(math.exp(-2*(min_distance/distSmooth)*(min_distance/distSmooth)))
        else:
            return 1
    else:
        if (min_distance>distSmooth):
            return 0.0
        else:
            return 0.5*math.exp(-2*((distSmooth+min_distance)/distSmooth)*((distSmooth+min_distance)/distSmooth))
# Exemple d'utilisation
#point = (0, 0)
#distance = point_to_polygons_distance(point, landscapes)







