from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel, txtDiffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import  mkdir
from os.path import isdir
import os
from mse.MESH_IMPORT_GMSH.view import viewTopo
import numpy as np
from dolfinx.io import VTKFile
from dolfinx.fem import form
from mse.state import state
from mse.linAlgTools import vector
from ufl import grad,dot
import math 
import toolsMRR 

toolsMRR.buildMRRStudy(toolsMRR.model_homogen)

#read datas
toolsMRR.aCapture.loadData("simulatedData.npy")

toolsMRR.aStudy.setParam(aParam=np.array([50,0.05,0.1]))
toolsMRR.simulator.doSimu()
toolsMRR.simulator.exportSimuToVtk()
computeMass(toolsMRR.ds2d,mass)
print(mass)
