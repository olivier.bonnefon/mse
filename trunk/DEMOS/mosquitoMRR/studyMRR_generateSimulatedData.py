#demo showing who import safran data 
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel, txtDiffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import  mkdir
from os.path import isdir
import os
#from mse.MESH_IMPORT_GMSH.view import viewTopo
import numpy as np
from dolfinx.io import VTKFile
from dolfinx.fem import form
from mse.state import state
from mse.linAlgTools import vector
from ufl import grad,dot
import math 
import toolsMRR 

toolsMRR.buildMRRStudy(toolsMRR.model_heterogen_10000,10000.0)


########################
# FIRST ckeck d_pi U
########################
print(toolsMRR.refParamsHeterogen)
toolsMRR.aStudy.setParam(aParam=toolsMRR.refParamsHeterogen)
toolsMRR.simulator.doSimu()
#toolsMRR.simulator.exportSimuToVtk()
toolsMRR.aCapture.computeCaptured()
toolsMRR.aCapture.saveComptage("simulatedData1")
toolsMRR.aCapture.saveComptage("simulatedData2")
toolsMRR.aCapture.saveComptage("simulatedData3")
toolsMRR.aCapture.saveComptage("simulatedData4")
toolsMRR.aCapture.loadData("simulatedData")
toolsMRR.aCapture.computeLL()
print(toolsMRR.aCapture.dLL)

toolsMRR.aStudy.end()


