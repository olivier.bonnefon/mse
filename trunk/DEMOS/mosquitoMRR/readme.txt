1)   studyTraps.py
	it will build the mesh and masks for homogen or heterogen diff

2) studyMRR_generateSimulatedData.py (choice model type and number of indiv inside)
	it will build simulated datas

3)  studyMRR_checkLL.py, studyMRR_checkLL_10000.py, studyMRR_checkLL_Heterogen_10000.py
	it will compute variations by finite dif and sensitive analysis

4) run optimization : 

studyMRR_optim.py

studyMRR_4_loopOptim_heterogen.py :work on EDP simulated data with heterogen model and 4 data set of 10000 indiv (data set are equal because coming from EDP)
