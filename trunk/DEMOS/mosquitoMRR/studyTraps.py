#demo showing who import safran data 
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import  mkdir
from os.path import isdir
import os
#from mse.MESH_IMPORT_GMSH.view import viewTopo
import numpy as np
from mse.COVARIABLE.covariable import covariableFromExpression, covariableCst, builderCovariableFromRaster
import toolsMRR 
import trapAndLandscapeMRR
toolsMRR.buildGeometrie(toolsMRR.model_heterogen_10000)

covF=[]
AllT="0.0"
for numT in range(len(trapAndLandscapeMRR.Xtraps)): 
    aCov=covariableFromExpression(toolsMRR.ds2d,"F"+str(numT))
    covF.append(aCov)
    strexp="np.exp(-((x[0]-"+str(trapAndLandscapeMRR.Xtraps[numT])+")*(x[0]-"+str(trapAndLandscapeMRR.Xtraps[numT])+")+(x[1]-"+ str(trapAndLandscapeMRR.Ytraps[numT])+")*(x[1]-"+str(trapAndLandscapeMRR.Ytraps[numT])+"))/"+str(trapAndLandscapeMRR.radius2)+")"
    print(str(strexp))
    aCov.setValue(strexp)
    mass=aCov.computeMass()
    scale=np.pi*trapAndLandscapeMRR.radius2/mass
    print(scale)
    aCov.setValue(str(scale)+"*"+strexp)
    aCov.save()
    AllT=AllT+"+"+str(scale)+"*"+strexp
    aCov.exportVtk()

covTraps=covariableFromExpression(toolsMRR.ds2d,"FT")
covTraps.setValue(AllT)
covTraps.save()
covTraps.exportVtk()
from matplotlib.patches import Polygon
import matplotlib.path as mpltPath
covInside=covariableFromExpression(toolsMRR.ds2d,"ZH")

import numpy as np
for ind,XYZ in enumerate(toolsMRR.ds2d.vSpace.tabulate_dof_coordinates()):
    coord=np.array((XYZ[0],XYZ[1]))
    covInside.femCov.x.array[ind]=trapAndLandscapeMRR.point_to_polygons_distance(coord,trapAndLandscapeMRR.landscapes)


covInside.exportVtk()
covInside.save()

##export raster for EDS
#rasterInside=np.zeros((1000, 1000))
#xmin=-500
#xmax=500
#ymin=-500
#ymax=500
#dx=2
#dy=2
##rasterInside(ix,iy) = F(xmin+ix*dx,ymin+iy*dy)
#for ix in range(500):
#    print("doing ix=",ix)
#    for iy in range (500):
#        rasterInside[ix,iy]=point_to_polygons_distance(np.array((xmin+dx*ix,ymin+dy*iy)),landscapes)
#np.save("rasterInside",rasterInside)

from dolfinx.io import VTKFile

#for visualisation
from mse.COVARIABLE.covariable import covariableFromCovFile,covariable
toolsMRR.aStudy.end()


