import numpy as np
import matplotlib.pyplot as plt

captureEDP=np.load("simulatedData.npy")
captureEDS=np.load("./EDS/capturedHomo.npy")

# Calcul des cumuls pour les deux campagnes
capture1Cumulees = np.cumsum(captureEDS, axis=1)
capture2Cumulees = np.cumsum(captureEDP, axis=1)
# Créer une figure avec 21 sous-graphiques (3 lignes, 7 colonnes)
fig, axes = plt.subplots(3, 7, figsize=(20, 10))

# Parcourir chaque piège et tracer sur les subplots
for i in range(21):
    row, col = divmod(i, 7)  # Calculer la position du subplot (ligne, colonne)
    ax = axes[row, col]
    ax.plot(capture1Cumulees[i], label='EDS', color='blue', linestyle='-', marker='o')
    ax.plot(capture2Cumulees[i], label='EDP', color='green', linestyle='--', marker='x')
    ax.set_title('Piège {i+1}')
    ax.grid(True)

# Ajouter une légende globale
fig.legend(['EDS', 'EDP'], loc='upper center', ncol=2)
# Ajouter des labels globaux
fig.text(0.5, 0.04, 'Pas de temps', ha='center')
fig.text(0.04, 0.5, 'cumulée capturée', va='center', rotation='vertical')
# Ajuster les espacements
plt.tight_layout(rect=[0, 0.05, 1, 0.95])
# Sauvegarder la figure dans un fichier PDF
plt.savefig("comparaison_EDP_EDS_HOMOGENE_regroupees.pdf")



