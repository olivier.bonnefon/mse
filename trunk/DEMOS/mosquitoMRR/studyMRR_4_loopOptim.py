from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel, txtDiffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import  mkdir
from os.path import isdir
import os
#from mse.MESH_IMPORT_GMSH.view import viewTopo
import numpy as np
from dolfinx.io import VTKFile
from dolfinx.fem import form
from mse.state import state
from mse.linAlgTools import vector
from ufl import grad,dot
import math 
import toolsMRR 

toolsMRR.buildMRRStudy(toolsMRR.model_homogen_10000,10000)



from scipy.optimize import minimize, OptimizeResult
#a call back function to see optimization steps
globalCount=0
#read datas
#aCapture.loadData("simulatedData.npy")
#toolsMRR.aCapture.loadData("EDS/capturedHomo10000_")
toolsMRR.aCapture.loadData("DATA/data")
def aCallableOpt(intermediate_result: OptimizeResult):
    """a callable use in intermediate odo a kdf  ozf zkf sofnqqk oajn
    """
    global globalCount
    globalCount+=1
    print("****************\nIn iteration"+str(globalCount)+": \n",intermediate_result)
boundsInit=[(10,2000),(0.02,1.0),(0.1, 20.0)]
bounds=[(10,10000),(0.02,1.0),(0.1, 100.0)]

initParams = np.array([25,.4,0.05])
file1 = open("trajOptim.txt", "w")
file1.close()
while(True):
    globalCount=0
    for k in range(toolsMRR.nbParams):
        initParams[k]=np.random.uniform(boundsInit[k][0], boundsInit[k][1]) 
    #optimization computation of variation using adjoint system built within MSE
    optParam = minimize(toolsMRR.simuAndComputeVandGradV, initParams, method="L-BFGS-B", bounds=bounds, jac=True, tol=1e-12, callback=aCallableOpt, options={"maxiter":200})
    print(optParam.hess_inv)
    file1 = open("trajOptim.txt", "a")
    file1.write("end optim\n")
    file1.close()

