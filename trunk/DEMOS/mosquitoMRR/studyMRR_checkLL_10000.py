from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel, txtDiffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import  mkdir
from os.path import isdir
import os
#from mse.MESH_IMPORT_GMSH.view import viewTopo
import numpy as np
from dolfinx.io import VTKFile
from dolfinx.fem import form
from mse.state import state
from mse.linAlgTools import vector
from ufl import grad,dot
import math 
import toolsMRR 
import os
toolsMRR.buildMRRStudy(toolsMRR.model_homogen_10000,10000.0)
repSimSav=toolsMRR.aStudy.simuDir+"../SIMSAV/"

def computeDiffSIM_SIMSAV(numP,epsilon):
    aExplorTraj=explorTrajectory(toolsMRR.aStudy,toolsMRR.aStudy.simuDir)
    aExplorTraj.rewind()
    res=0.0
    while(aExplorTraj.replay()):
        toolsMRR.ds2d.uk.load(repSimSav+"ds0",aExplorTraj.curStep)
        toolsMRR.ds2d.uk.scale(-1.0)
        toolsMRR.ds2d.uk.axpy(toolsMRR.ds2d.utm1,1.0)
        toolsMRR.ds2d.uk.scale(1/epsilon)
        toolsMRR.ds2d.utm1.load(repSimSav+"/w0_"+str(numP),aExplorTraj.curStep)
        toolsMRR.ds2d.uk.axpy(toolsMRR.ds2d.utm1,-1.0)
        res+=toolsMRR.ds2d.uk.norm()
    return res


########################
# FIRST ckeck d_pi U
########################
toolsMRR.aCapture.loadData("simulatedData")
LLRef,dLLRef=toolsMRR.simuAndComputeVandGradV(toolsMRR.refParams)
#toolsMRR.simulator.exportSimuToVtk()
print("LL="+str(LLRef))
print("dLL=sould be null")
print(dLLRef)
params=np.array([0.0,0.0,0.0])
scalParams=np.array([100.0,0.1,1.0])
#compute LL and dLL at paramsi
refComptage=np.copy(toolsMRR.aCapture.comptages)
refdComptage=np.copy(toolsMRR.aCapture.dComptages)
diffComptage=np.copy(toolsMRR.aCapture.comptages)
for paramsi in (np.array([300,0.05,1.0]),toolsMRR.refParams,np.array([19*19*0.5,0.1,2/3])): 
    params[:]=paramsi[:]
    print("#########################################################")
    print("test grad at =",params)
    LLRef,dLLRef=toolsMRR.simuAndComputeVandGradV(params)
    os.system("rm -rf "+repSimSav)
    os.system("cp -r "+toolsMRR.aStudy.simuDir+" "+repSimSav)
    np.copyto(refComptage,toolsMRR.aCapture.comptages)
    np.copyto(refdComptage,toolsMRR.aCapture.dComptages)
    print("LLref="+str(LLRef))
    print("dLLref="+str(dLLRef))
    print("#########################################################")
    nbE=3
    ErrsW=np.zeros((nbE,toolsMRR.nbParams))
    ErrsCaptures=np.zeros((nbE,toolsMRR.nbParams))
    Errs=np.zeros((nbE,toolsMRR.nbParams))
    for i in range(toolsMRR.nbParams):
        print("----------------------- d_",i)
        epsilon=0.001
        for ne in range(nbE):
            params[:]=paramsi[:]
            params[i]+=epsilon
            print("- run simu at ",params)
            toolsMRR.aStudy.setParam(aParam=params)
            toolsMRR.simulator.doSimu()
            ErrsW[ne,i]=computeDiffSIM_SIMSAV(i,epsilon)
            toolsMRR.aCapture.computeCaptured(False)
            np.copyto(diffComptage,(1/epsilon)*(toolsMRR.aCapture.comptages-refComptage))
            ErrsCaptures[ne,i]=np.linalg.norm(diffComptage-refdComptage[i])
            toolsMRR.aCapture.computeLL(False)
            Errs[ne,i]=(toolsMRR.aCapture.LL-LLRef)/epsilon - dLLRef[i]
            print("- LL : ",LLRef,toolsMRR.aCapture.LL)
            print("- dLL : ",dLLRef[i],(toolsMRR.aCapture.LL-LLRef)/epsilon )
            epsilon*=0.5
    print(ErrsW)
    print(ErrsCaptures)
    print(Errs)

