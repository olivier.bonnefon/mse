from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel, txtDiffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import  mkdir
from os.path import isdir
import os
#from mse.MESH_IMPORT_GMSH.view import viewTopo
import numpy as np
from dolfinx.io import VTKFile
from dolfinx.fem import form
from mse.state import state
from mse.linAlgTools import vector
from ufl import grad,dot
import math 
import toolsMRR 

toolsMRR.buildMRRStudy(toolsMRR.model_homogen)



from scipy.optimize import minimize, OptimizeResult
#a call back function to see optimization steps
globalCount=0
#read datas
#aCapture.loadData("simulatedData.npy")
toolsMRR.aCapture.loadData("EDS/capturedHomo.npy")
file1 = open("trajOptim.txt", "w")
file1.close()

def aCallableOpt(intermediate_result: OptimizeResult):
    """a callable use in intermediate odo a kdf  ozf zkf sofnqqk oajn
    """
    global globalCount
    globalCount+=1
    print("****************\nIn iteration"+str(globalCount)+": \n",intermediate_result)

initParams = np.array([25,.5,0.05])
initParams = np.array([100,1.,0.3])
#optimization computation of variation using adjoint system built within MSE
optParam = minimize(toolsMRR.simuAndComputeVandGradV, initParams, method="L-BFGS-B", bounds=[(1,10000),(0.01,1),(0.0001, 2.0)], jac=True, tol=1e-8, callback=aCallableOpt, options={"maxiter":2})
print(optParam.hess_inv)
file1 = open("trajOptim.txt", "a")
file1.write("inv_hess:"+str(optParam.hess_inv.todense()[0][0])+" "+str(optParam.hess_inv.todense()[1][1])+" "+str(optParam.hess_inv.todense()[2][2]))
file1.close()

