import numpy as np
import matplotlib.pyplot as plt
captureEDP=[]
captureEDS=[]
for k in range(4):
    #captureEDS.append(np.load("./EDS/capturedHomo10000_"+str(k+1)+".npy"))
    captureEDS.append(np.load("./EDS/capturedHeterogen10000_"+str(k+1)+".npy"))
    captureEDP.append(np.load("./simulatedData"+str(k+1)+".npy"))

# Calcul des cumuls pour les deux campagnes
capture1Cumulees = np.cumsum(captureEDS[0]+captureEDS[1]+captureEDS[2]+captureEDS[3], axis=1)
capture2Cumulees = np.cumsum(captureEDP[0]+captureEDP[1]+captureEDP[2]+captureEDP[3], axis=1)
# Créer une figure avec 21 sous-graphiques (3 lignes, 7 colonnes)
fig, axes = plt.subplots(3, 7, figsize=(20, 10))

# Parcourir chaque piège et tracer sur les subplots
for i in range(21):
    row, col = divmod(i, 7)  # Calculer la position du subplot (ligne, colonne)
    ax = axes[row, col]
    ax.plot(capture1Cumulees[i], label='SPDE', color='blue', linestyle='-', marker='o')
    ax.plot(capture2Cumulees[i], label='PDE', color='green', linestyle='--', marker='x')
    ax.set_title("trap "+str(i+1))
    ax.grid(True)

# Ajouter une légende globale
fig.legend(['SPDE', 'PDE'], loc='upper center', ncol=2)
# Ajouter des labels globaux
fig.text(0.5, 0.04, 'times', ha='center')
fig.text(0.04, 0.5, 'number of captures', va='center', rotation='vertical')
# Ajuster les espacements
plt.tight_layout(rect=[0, 0.05, 1, 0.95])
# Sauvegarder la figure dans un fichier PDF
plt.savefig("comparaison_EDP_EDS_HETEROGEN_4_10000_regroupees.pdf")



