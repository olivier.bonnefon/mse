from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel, txtDiffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import  mkdir
from os.path import isdir
import os
#from mse.MESH_IMPORT_GMSH.view import viewTopo
import numpy as np
from dolfinx.io import VTKFile
from dolfinx.fem import form,assemble_scalar
from mse.state import state
from mse.linAlgTools import vector
from ufl import grad,dot
import math 

model_type=0
#TRAPS DESCRPTIONS

radius2=10.0*10.0
Xtraps=np.array([-141.42, -138.08, -184.78, 50, 57.4   , -38.27, 19.13   , 0    , 76.54  , -393.92, -153.07, -95.67, -57.4   , 400    , 282.84, 212.13  , 168.68 , 100    , -35.36, -282.84, -95.67])
Ytraps=np.array([141.42 , 57.4   ,  -76.54, 0.,  138.58,  92.39,  -46.19 , -100., -184.78,  -99.46, -369.55, -230.97, -138.58,    0.  , -282.84, -212.13,  107.46,    0.  ,   35.36,  282.84,  230.97])

nbrTraps=21
nbrDates=20
class dynamicalSystemWithSensitivity(dynamicalSystem):
    def __init__(self,aStudy,aGeoDim,aMeshFile,nbComps=1, obsProc=None) -> None:
        super().__init__(aStudy,aGeoDim,aMeshFile,nbComps,obsProc)
        if (not computeGradU):
            return
        self.wpi=[]
        self.utm1Sav=state(self)
        for i in range(nbParams):
            self.wpi.append(state(self))
        self.initialState[0]="np.exp(-((x[0]**2+x[1]**2)/4.0))"
    def applyInitialState(self):
        super().applyInitialState()
        m=assemble_scalar(form(self.utm1.comps[0]*self.dx))
        print("mass= "+str(m))
        self.utm1.scale(1/m)
        print(self.utm1.comps[0].x.array.min(),self.utm1.comps[0].x.array.max())
        if (not computeGradU):
            return
        for w in self.wpi:
            w.zero()
    def save(self,prefixFile,nSTep):
        super().save(prefixFile,nSTep)
        if (not computeGradU):
            return
        for i in range(nbParams):
            self.wpi[i].save(prefixFile+"w"+str(self.id)+"_"+str(i),nSTep)
    def load(self,prefixFile,nSTep):
        super().load(prefixFile,nSTep)
        if (not computeGradU):
            return
        for i in range(self.study.nbParam):
            self.wpi[i].load(prefixFile+"w"+str(self.id)+"_"+str(i),nSTep)
    def exportStateVtk(self,nStep):
       super().exportStateVtk(nStep)
       if (not computeGradU):
            return
       for i in range(self.study.nbParam):
            self.wpi[i].exportVtk(nStep)
            

class mrrSimulator(simulator):
    def __init__(self,t0,t1,aStudy) -> None:
        super().__init__(t0,t1,aStudy)
    def exportSimuToVtk(self):
        if (computeGradU):
            mrrDs=self.study.system.dynamicalSystems[0]
            for i in range(self.study.nbParam):
                mrrDs.wpi[i].initFilesToExportVtk(self.study.exportDir+"w"+str(mrrDs.id)+"_"+str(i))
        super().exportSimuToVtk()
        if (computeGradU):
            for i in range(self.study.nbParam):
                mrrDs.wpi[i].closeFilesToExportVtk()



from dolfinx.fem.petsc import  assemble_vector                                                                                      

class impliciteTS(implicitLinearTimeScheme):
    def __init__(self) -> None:
        super().__init__()
        self.theta=.5
    def setSystem(self,system):
        super().setSystem(system)
        if (not computeGradU):
            return
        self.rhspi=[]
        self.rhspitm1=[]
        for i in range(nbParams):
            av=vector()
            self.rhspi.append(av)
            av.setDim(self.system.nbDofs)
            av=vector()
            self.rhspitm1.append(av)
            av.setDim(self.system.nbDofs)
        self.myds=self.system.dynamicalSystems[0]
        self.formpi=[]
        #diff=math.log(10)*pow(10,self.system.study.param[0])
        #self.formpi.append(form( -diff*dot(grad(self.myds.utm1.comps[0]),grad(self.myds.v))*self.myds.dx))
        if (model_type==model_heterogen_10000):
            #self.formpi.append(form( -1.0*dot(grad(self.myds.utm1.comps[0]),grad(self.myds.v))*self.myds.dx))
            #self.formpi.append(form( -1.0*dot(grad(self.myds.utm1.comps[0]),grad(self.myds.v))*self.myds.dx))
            self.formpi.append(form( -1.0*dot(grad(self.myds.dicCov["ZH"]*self.myds.utm1.comps[0]),grad(self.myds.v))*self.myds.dx))
            self.formpi.append(form( -1.0*dot(grad((1-self.myds.dicCov["ZH"])*self.myds.utm1.comps[0]),grad(self.myds.v))*self.myds.dx))
        else:
            self.formpi.append(form( -1.0*dot(grad(self.myds.utm1.comps[0]),grad(self.myds.v))*self.myds.dx))
        self.formpi.append(form( -1.0*self.myds.utm1.comps[0]*self.myds.v*self.myds.dx))
        self.formpi.append(form( -1.0*self.myds.utm1.comps[0]*self.myds.dicCov["FT"]*self.myds.v*self.myds.dx))
    def doATimeStep(self):
        #first build rhs for theta method
        for i in range(nbParams-2,nbParams):
            self.rhspitm1[i]=vector(assemble_vector(self.formpi[i]))
        #print(self.myds.utm1.comps[0].x.array.min(),self.myds.utm1.comps[0].x.array.max())
        super().doATimeStep()
        #print(self.myds.utm1.comps[0].x.array.min(),self.myds.utm1.comps[0].x.array.max())
        #print(assemble_scalar(form(self.myds.utm1.comps[0]*self.myds.dx)))
        if (computeGradU):
            self.myds.utm1Sav.setFromState(self.myds.utm1)
            for i in range(nbParams):
                self.rhspi[i]=vector(assemble_vector(self.formpi[i]))
                #if (i==1):
                #    print(self.myds.utm1.comps[0].x.array.min())
                #    print(self.myds.utm1.comps[0].x.array.max())
                #    self.rhspi[i].print()
                #    exit(0)
                if (i>nbParams-3):
                    self.rhspi[i].scale(self.theta)
                    self.rhspi[i].axpy(1.0-self.theta,self.rhspitm1[i])
            for i in range(nbParams):
                self.myds.utm1.setFromState(self.myds.wpi[i])
                self.myds.uk.setFromState(self.myds.wpi[i])
                for aModel in self.myds.sourcesTerm:
                    aModel.updateFtm1()
                super().buildRhs()
                self.rhspi[i].axpy(1.0,self.rhs)
                #if (i==1):
                #    self.rhs.print()
                #    exit(0)
                self.solver.solve(self.rhspi[i],self.sol)
                self.myds.wpi[i].vectorToState(self.sol,0)
            self.myds.utm1.setFromState(self.myds.utm1Sav)

class capture():
    def __init__(self,study,N0=1.0) -> None:
        #self.comptages[i,j] contains number of captured in mask F_i during time [j,j+1]
        self.N0=N0
        self.dataComptages=None
        self.LL=0.0
        self.dLL=np.zeros((nbParams))
        self.comptages=np.zeros((nbrTraps,nbrDates))
        self.dComptages=np.zeros((nbParams,nbrTraps,nbrDates))
        #self.dComptages=[]
        #for l in range(3):
        #    self.dComptages.append(np.zeros((21,20)))
        self.study=aStudy
        self.ds=aStudy.system.dynamicalSystems[0]
        self.covFi=[]
        for i in range(nbrTraps):
            self.covFi.append(covariableFromCovFile("F"+str(i),self.ds))
    # j in {0,..,19}
    # self.comptages[j] will contains the numbers of trapped indiv during t in [j,j+1]
    # to have a numbers captured corresponding to the numerical scheme, 
    # you have to consider the theta method used
    # for a time step at time=t
    # compute the contribution of this time Nt = assemble_scalar(Fi*ut)
    # find j the nearest integer to T
    # contrib=[0,0]
    # contribution during [j-1,j]
    #   contrib[0]= 0.5*length([t-dt, t+dt] inter [j-1,j])
    # contribution during [j,j+1]
    #   contrib[1]= 0.5*length([t-dt, t+dt] inter [j,j+1])
    # self.comptages[j-1]=contrib[0]*Nt
    # self.comptages[j]=contrib[1]*Nt
    def lenthIntersec(self,j1,j2,t,dt):
        #compute the lenth([t-dt, t+dt] inter [j1,j2])
        #assume j1<j2
        l=min(j2,t+dt)-max(j1,t-dt)
        if(l<0):
            l= 0
        #print("lenthIntersec "+str(j1)+" "+str(j2)+" "+ str(t)+" "+str(l))
        return l
    def nearestInt(self,t):
        if (math.fabs(t-math.floor(t))>math.fabs(t-math.ceil(t))):
            return math.ceil(t)
        else:
            return math.floor(t)
    def computeCaptured(self,computeGrad=True):
        dt=self.study.simulator.doOneStep.dt
        self.comptages.fill(0.0)
        if (computeGrad):
            self.dComptages.fill(0.0)
        aExplorTraj=explorTrajectory(aStudy,aStudy.simuDir)
        aExplorTraj.rewind()
        gamma=self.study.param[self.study.nbParam-1]
        print("gamma=",gamma)
        while(aExplorTraj.replay()):
            time=aExplorTraj.T
            #print("time :"+str(time))
            curStep=aExplorTraj.curStep
            j=self.nearestInt(time)
            contrib=[0.0,0.0]
            contrib[0]=self.lenthIntersec(j-1,j,time,dt)
            contrib[1]=self.lenthIntersec(j,j+1,time,dt)
            #print("capt ",self.ds.utm1.comps[0].x.array.min(),self.ds.utm1.comps[0].x.array.max())
            for i in range(nbrTraps):
                #print("fi ",self.covFi[i].femCov.x.array.max())
                NT=gamma*assemble_scalar(
                        form(self.covFi[i].femCov*
                         self.ds.utm1.comps[0]*self.ds.dx))
                #print("NT=",NT)
                if (j-1>-0.01):
                    self.comptages[i,j-1]+=0.5*contrib[0]*NT
                if (j+1<20.001):
                    self.comptages[i,j]+=0.5*contrib[1]*NT
            if (computeGrad):
                for numP in range(nbParams):
                    ds2d.uk.load(aStudy.absoluteStudyDir+"SIMU/w0_"+str(numP),curStep)
                    for i in range(nbrTraps):
                        dNT=gamma*assemble_scalar(
                             form(self.covFi[i].femCov*
                             self.ds.uk.comps[0]*self.ds.dx))
                        if (numP==nbParams-1):
                            dNT+=assemble_scalar(
                                form(self.covFi[i].femCov*
                                self.ds.utm1.comps[0]*self.ds.dx))
                        if (j-1>-0.01):
                            self.dComptages[numP,i,j-1]+=0.5*contrib[0]*dNT
                        if (j+1<20.001):
                            self.dComptages[numP,i,j]+=0.5*contrib[1]*dNT
    def saveComptage(self,fileName):
        np.save(fileName,self.N0*self.comptages)
        np.save("d"+fileName,self.N0*self.dComptages)
    def loadData(self,fileName):
        self.dataComptages=np.load(fileName)
    #compute LL using self.comptage, self.dadaComptages and self.dComptages
    def computeLL(self,computeGrad=True):
       self.LL=0.0
       if (computeGrad):
           self.dLL.fill(0.0)
       for i in range(nbrTraps):
           for j in range(nbrDates):
               data=self.dataComptages[i,j]
               lambda_=self.N0*self.comptages[i,j]
               #print("lambda_="+str(lambda_))
               self.LL-=-lambda_+data*np.log(lambda_)
               if (computeGrad):
                   for kk in range(nbParams):
                      if (lambda_!=0):
                          self.dLL[kk]-=self.N0*self.dComptages[kk,i,j]*(-1.0+data/lambda_)
               #for ii in range(data):
               #    self.LL-=np.log(ii)
               #P(data)=expected^data exp(-expected) /data!
               #log(P(data)= data*log(expected)-expected+sum_i=1^data log(i)

class capture4(capture):
    def __init__(self,study,N0=1.0) -> None:
        super().__init__(study,N0)
        self.dataComptages=[]
    def loadData(self,fileName):
        self.dataComptages=[]
        for i in range(4):
            self.dataComptages.append(np.load(fileName+str(i+1)+".npy"))
    def computeLL(self,computeGrad=True):
       self.LL=0.0
       if (computeGrad):
           self.dLL.fill(0.0)
       for i in range(nbrTraps):
           for j in range(nbrDates):
               data=0.0
               for k in range(4):
                   data+=self.dataComptages[k][i,j]
               lambda_=self.N0*self.comptages[i,j]
               #print("lambda_="+str(lambda_))
               self.LL-=-4*lambda_+data*np.log(lambda_)
               if (computeGrad):
                   for kk in range(nbParams):
                      if (lambda_!=0):
                          self.dLL[kk]-=self.N0*self.dComptages[kk,i,j]*(-4.0+data/lambda_)

from mse.COVARIABLE.covariable import covariableFromExpression, covariableCst, builderCovariableFromRaster,covariableFromCovFile
nbParams=3
studName="STUDY_MRR"
model_homogen=0
model_heterogen_10000=1
model_homogen_10000=2
computeGradU=1
nbComps=1
aStudy=None
ds2d=None
aCapture=None
simulator=None
#ref params used for simulated datas
refParams = np.array([19*19*0.5,0.1,2.0/3.0])
refParamsHeterogen = np.array([50*50*0.5,15*15*0.5,0.1,2.0/3.0])
bestLL=1e6
optimParams = np.array([.0,.0,.0,.0])
import math
def meshSizeCallback(dim, tag, x, y,z,lc):
    print(x)
    print(y)
    print(z)
    print(lc)
    dist=0
    if (x<-500):
        dist+=-500-x
    if (x>500):
        dist+=x-500
    if (y<-500):
        dist+=-500-y
    if (y>500):
        dist+=y-500
    dist=dist/2000.0
    return 10+200* (1.0-math.exp(-15*dist*dist))
#
#build geometrie, useful to prepar covariables, masks traps and diffusion
def buildGeometrie(model_type_param):
    global nbParams, aStudy,ds2d, aCapture, simulator,studName,model_type
    model_type=model_type_param
    if (model_type==model_homogen):
        nbParams=3
        studName="STUDY_MRR_HOMOGEN"
    elif (model_type==model_homogen_10000):
        nbParams=3
        studName="STUDY_MRR_HOMOGEN_10000"
    else:
        nbParams=4
        studName="STUDY_MRR_HETEROGEN_10000"
    #a study
    aStudy=study(studName, nbParam=nbParams)
    # the study zone
    aStudy.setGeometry("MRR",10,meshSizeCallback)
    #buid covariable for traps
    ds2d=dynamicalSystemWithSensitivity(aStudy,2,"surface1.xdmf",nbComps=1)
#build simulator 
def buildMRRStudy(model_type_param,N0=1000000.0):
    global nbParams, aStudy,ds2d, aCapture, simulator ,studName, model_type
    model_type=model_type_param
    if (model_type==model_homogen):
        nbParams=3
        studName="STUDY_MRR_HOMOGEN"
    elif (model_type==model_homogen_10000):
        nbParams=3
        studName="STUDY_MRR_HOMOGEN_10000"
    else:
        nbParams=4
        studName="STUDY_MRR_HETEROGEN_10000"
    #a study
    aStudy=study(studName, nbParam=nbParams)
    # the study zone
    aStudy.setGeometry("MRR")
    #buid covariable for traps
    ds2d=dynamicalSystemWithSensitivity(aStudy,2,"surface1.xdmf",nbComps=1)
    ds2d.addHeterogenCov("FT",covariableFromCovFile("FT",ds2d))
    if (model_type==model_homogen or model_type==model_homogen_10000):
        txtDiffusionModel(ds2d,"diff2dIsotropP1")
        mrrMod=txtModel(ds2d,"mrr")
    else:
        ds2d.addHeterogenCov("ZH",covariableFromCovFile("ZH",ds2d))
        ds2d.dicHeterogenCov["ZH"].exportVtk()
        txtDiffusionModel(ds2d,"diff2dHeterogenIsotropP1P2")
        mrrMod=txtModel(ds2d,"mrrH")
    #because it is a linear time #cst system:
    aTS=impliciteTS()
    aTS.computeMatOnce=1
    simulator=mrrSimulator(0,20,aStudy)
    TStep=timeDoOneStep(aStudy,aTS,0.5)
    if (model_type==model_homogen):
        aCapture=capture(aStudy,N0)
    else:
        aCapture=capture4(aStudy,N0)
#aCapture.loadData("EDS/capturedHomo.npy")
def simuAndComputeVandGradV(params):
    global bestLL,optimParams
    aStudy.setParam(aParam=params)
    simulator.doSimu()
    aCapture.computeCaptured()
    aCapture.computeLL()
    file1 = open("trajOptim.txt", "a")
    mess=str(params)+" "+str(aCapture.LL)+" "+str(aCapture.dLL)+" "+str(np.linalg.norm(aCapture.dLL))+"\n"
    print(mess)
    file1.write(mess)
    file1.close()
    if (aCapture.LL<bestLL):
        bestLL=aCapture.LL
        for i in range(nbParams):
            optimParams[i]=params[i]
    return aCapture.LL, aCapture.dLL



