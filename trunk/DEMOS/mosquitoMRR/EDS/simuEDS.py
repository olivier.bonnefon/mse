import numpy as np
from scipy.stats import norm
from scipy.stats import expon
from numpy import random, linalg
from matplotlib.patches import Polygon
import matplotlib.path as mpltPath
import os
import trapAndLandscapeMRR

#np.random.seed(35)
#centers of traps 
realtrap = np.array([(-141.42, 141.42), (-138.08, 57.4), (-184.78, -76.54), (50, 0),
                     (57.4,138.58),(-38.27, 92.39), (19.13, -46.19), (0,-100),
                     (76.54, -184.78), (-393.92, -99.46),(-153.07,-369.55),
                     (-95.67, -230.97), (-57.4, -138.58), (400,0), (282.84,-282.84),
                     (212.13,-212.13), (168.68, 107.46), (100, 0), (-35.36, 35.36),
                     (-282.84, 282.84), (-95.67, 230.97)])
#number of trapes
P = len(realtrap) 

numberOfTrappedIndivs=np.zeros(P)
#number of mousquito
N=10000

Tend=20
curT=0.0
nt=Tend*10
dt=Tend/nt
CoordIndiv= np.zeros((N, 2))
AliveIndiv=np.ones(N)

#Generate the datasets for both cases 
nu = 1/10
lifetime = np.random.exponential(1/nu, size = N)

#traps radius size, effect until 10*r
r=10

#Brownian def
sigma=19

#Brownian motion for the homogeneous case
def brownian_motion():
    global CoordIndiv
    CoordIndiv+=norm.rvs(size=CoordIndiv.shape,scale=sigma*np.sqrt(dt))

sigma1=50
sigma2=15
def heterogen_brownian_motion():
    global CoordIndiv
    for c in CoordIndiv:
        isInside=trapAndLandscapeMRR.point_to_polygons_distance(c,trapAndLandscapeMRR.landscapes) 
        sigmah=isInside*sigma1+(1-isInside)*sigma2
        c+=norm.rvs(size=2,scale=sigmah*np.sqrt(dt)) 


gamma = 2/3
def capture():
    distances = np.linalg.norm(CoordIndiv[:, np.newaxis, :] - realtrap[np.newaxis, :, :], axis=2)
    #distance[0] contains distance betweens indiv 0 to all traps
    #distance[i,j] i: index of indiv, j: index of trap
    index_closest_traps = np.argmin(distances, axis=1)
    # Calculer les distances aux pièges les plus proches
    distances_closest = distances[np.arange(N), index_closest_traps]
    # Condition : distance < 10 * r
    mask = distances_closest < 10 * r
    # Calcul de fgamma pour les individus concernés
    fgamma = np.zeros(N)
    fgamma[mask] = gamma * np.exp(-(distances_closest[mask] / r) ** 2)
    # Calcul de la probabilité de capture
    capture_probs = np.zeros(N)
    capture_probs[mask] = 1 - np.exp(-fgamma[mask] * dt)
    # Génération des probabilités aléatoires
    random_probs = np.random.uniform(0, 1, size=N)
    # Condition de capture
    capture_mask = (random_probs < capture_probs) & (curT < lifetime) & (AliveIndiv == 1)
    # Mettre à jour les individus capturés
    AliveIndiv[capture_mask] = 0
    # Compter les captures par piège
    np.add.at(numberOfTrappedIndivs, index_closest_traps[capture_mask], 1)

countCaptured=np.zeros((P,Tend))


def doTimeStep():
    brownian_motion()
    capture()
def doTimeStepHeterogen():
    heterogen_brownian_motion()
    capture()




def doSimu(heterogen=False):
   global curT, countCaptured
   curT=0.0
   for day in range(Tend):
       print(day)
       numberOfTrappedIndivs.fill(0)
       while (curT < day+1):
           if (heterogen):
               doTimeStepHeterogen()
           else:
               doTimeStep()
           curT+=dt
       countCaptured[:,day]=numberOfTrappedIndivs
heterogen=True
doSimu(heterogen)

mask=(20.0 < lifetime) & (AliveIndiv == 1)
print("number of mostiquo alive at the end="+str(np.count_nonzero(mask == True)))
baseName="capturedHomo"
if (heterogen):
    baseName="capturedHeterogen"
np.save(baseName+"10000",countCaptured)
with open(baseName+"_10000_params.txt","w") as file:
    if (heterogen):
        file.write("N "+str(N)+"\nsigma12 "+str(sigma1)+" "+str(sigma2)+"\nnu "+str(nu)+"\ngamma "+str(gamma)+"\n")
    else:
        file.write("N "+str(N)+"\nsigma "+str(sigma)+"\nnu "+str(nu)+"\ngamma "+str(gamma)+"\n")





#for i in range(21):
#    for j in range(20):
#        print(l[i,j],end="\t")
#    print("")
#    for j in range(20):
#        print(ll[i,j],end="\t")
#    print("")
#    print("")

#for i in range(21):
#    for j in range(20):
#        if (ll[i,j]>0):
#            print(l[i,j],ll[i,j], ll[i,j]/l[i,j])
#


