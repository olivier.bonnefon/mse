# Demo for the resolution of the system adjoint, using backward simulator, timeScheme backward and timeDoOnestepBackward
#
# First we will do a simulation, second we will create the adjoint system and third do simulation to resolve the adjoint system
#  the model will be a logistic growth with param
#
#On se retrouve dans un probleme equivalent de ColabMarc avec (D,R,K) = (1,p1,p2) et sans le taux de mortalité

from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.systemAdjoint import systemAdjoint 
from mse.obsProcess import quadraticDiff
from mse.simulator import simulator,simulatorBackward
from mse.timeDoOneStep import timeDoOneStep,timeDoOneStepBackward
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme,implicitLinearBackwardTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import  mkdir
from os.path import isdir, exists
import numpy as np
import matplotlib.pyplot as plt
import shutil
from scipy.optimize import minimize, OptimizeResult


#La geometrie
nbComps=1
WITH_AFFINE_GROWTH=1
testNLTS=1


#une etude
studName="STUDY8"

aStudy=study(studName, nbParam=2)
aStudy.setGeometry("SQUARE",0.4)

#un systeme
aSystem=system(aStudy)
#un systeme dynamique
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
#ds2d1.initialState[0]="0.0+0.0*"+ds2d1.initialState[0]
ds2d1.initialStateAdjoint[0]="0.0+0.0*"+ds2d1.initialStateAdjoint[0]

#la diffusion
aDiffModel=diffusionModel(ds2d1)
#ds2d1.addSecondOrderTerm(aDiffModel)
D = 5.
aDiffModel.setDiffCoef(D)
#if (WITH_AFFINE_GROWTH):
    #affineGrowth(ds2d1,R,K)
m=txtModel(ds2d1,"logisticGrowthParam")
#    m.computeMatOnce=1
#    m.computeRhsOnce=1
aTS=None
#schema en temps implicite lineaire
#if (testNLTS):
aTS=implicitNonLinearTimeScheme()
#else:
#    aTS=implicitLinearTimeScheme()
#    aTS.computeMatOnce=1
aTS.theta=1
aTS.maxIt=5
#simu in t \in [0,1]
t0=0.
t1=2.
simulator(t0,t1,aStudy)
#based on fix time step 0.05
aTStep=timeDoOneStep(aStudy,aTS,0.05)


### Adjoint system ###
# we do first simu with param => will be the ref
# then we do an other simu with other param => it will be the u_a
#
# REF #
aStudy.setParam([1.,.5])
aStudy.simulator.doSimu()
path=aStudy.absoluteStudyDir+"/UREF/"
if(exists(path)):
    shutil.rmtree(path)
shutil.copytree(aStudy.simuDir, path)
#quadraticDiff
quaDiff=quadraticDiff(ds2d1)
# plot
doPlot=False
paramPath=[]
globalCount=0

def aCallableOpt(intermediate_result: OptimizeResult):
    """a callable use in intermediate odo a kdf  ozf zkf sofnqqk oajn 
    """
    print("In iteration: \n",intermediate_result)
    paramPath.append(intermediate_result.x.copy())

def findParamOpt(defaultParam):
    """ call function to find param opt
    """
    global globalCount
    # SIMU #
    print("********\nParam a_sim =",defaultParam)
    aStudy.setParam(defaultParam)
    aStudy.simulator.doSimu()
    #print gradV
    quaDiff.computeGradV()
    print("GradV = ",quaDiff.gradJ)
    #we call function to find paramOpt
    optParam = minimize(quaDiff.sysAdj.valJAndGradJ, defaultParam, method="L-BFGS-B", bounds=[(0.1,3)], jac=True, tol=None, callback=aCallableOpt, options={"maxiter":7})
    print (optParam)
    tmpParamPath = paramPath[-1-optParam.nit:]
    tmpParamPath.insert(0,np.array(defaultParam))
    tmpParamPath = np.array(tmpParamPath)
    print("path of param: ",tmpParamPath)
    #fig to show
    if (doPlot):
        plt.figure()
        plt.plot(tmpParamPath[:,0],tmpParamPath[:,1],'b+-')
        plt.plot([1.],[1.],'r+')
        plt.xlabel("Parameter p1")
        plt.ylabel("Parameter p2")
        plt.axis((0.5,1.5,0.5,1.5))
        globalCount+=1
        plt.title("parameter for test "+str(globalCount))
        plt.savefig(studName+"/pathParameter"+str(globalCount)+".png")
### list of param to test
paramInit = [([1.2,.6]),([1.2,1.]),([1,0.54]),([1,1.3]),([1.4,1.3]),([0.64,0.73])]
### call of function to find paramOPt
findParamOpt(paramInit[0])
