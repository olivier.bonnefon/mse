# study with 2 compatiments
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel, txtDiffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitNonLinearTimeScheme,timeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from mse.linAlgTools import matrix, vector
from mse.COVARIABLE.covariable import (covariableFromExpression,
                                       covariableCst,
                                       builderCovariableFromRaster)
from petsc4py import PETSc

from os import  mkdir
from os.path import isdir
import numpy as np
from dolfinx.fem import Function, assemble_scalar,form
# debugger
import pdb
# Study of predator living in edges



"""
The study is about field pred, hedge pred and a pest.




"""
# Surcharge class Matrix pour matrix dense
class myDenseMatrix(matrix):
    def __init__(self, aMat=None) -> None:
        """The constructor"""
        super().__init__(aMat)
    def setDim(self, aN, aM):
        """It build a DENSE matrix.

        .. WARNING::
            
            do not use if u can do a sparse matrix

        :param int aN: number of row
        :param int aM: number of column
        """
        self.N=aN
        self.M=aM
        self.petscMat.createDense([aN,aM])
        self.petscMat.setUp()
        self.petscMat.assemble()

###
# Overload timescheme
class myImplicitLinearTimeScheme(implicitLinearTimeScheme):
    def __init__(self) -> None:
        super().__init__()
        # create file to save contrib
        # NOTE: to do ????
        #f = open("contribPred.txt", 'w')
        #f.close()
        self.bF = self.system.dynamicalSystems[0].dicHeterogenCov["BF"]
        self.predHedge = 0.
        self.predField = 0.
    def doATimeStep(self):
        """
        we overload timeScheme.doTimeStep to:
        1. update the Cov \"ZS\" before the doTimeStep
        2. calculate the contribution of predator after doTimeStep
        """
        ### Update Cov
        #
        aDs2d = self.system.dynamicalSystems[0]
        arrayPJ =np.dot(
                arrayKM,
                aDs2d.utm1.comps[0].x.array[:]
                )
        arrayJH = np.zeros(aDs2d.nbDofsPerComp)
        for ds1d in self.system.dynamicalSystems[1:]:
            # Calcul of Zh
            arrayH = ds1d.utm1.comps[2].x.array[:]
            ds1d.dicHeterogenCov["ZH"].comps[2].x.array[:] = np.multiply(
                    arrayH, arrayPJ
                    )
            # Calcul of ZP
            arrayJH += np.dot(
                    arraysKtMb[ds1d.id - 1],
                    ds1d.utm1.comps[2].x.array[:]
                    )
        ds2d.dicHeterogenCov["ZP"].comps[0].x.array[:] = np.multiply(
                aDs2d.utm1.comps[2].x.array[:],
                arrayJH
                )

        #arrayP = ds2d.utm1[0].x.array[:]
        #ds1d.dicHeterogenCov["ZH"].comps[2].x.array[:] =
        #np.multiply(
        #        np.dot(arrayKM, arrayP),
        #        ds1d.comps[1]
        #        )
        ### call doTimeStep
        super().doATimeStep()
        #
        ### calcul of contrib pred
        # TODO
        # at each step, we save the predation of both pred
        # NOTE: we supposed contrib of H-PRED is equivalent of
        # Int_dOmega int_Omega B P J(??) dx ds
        # = Int_Omega int_dOmega B P J(??) ds dx
        #
        #
        state2D = self.system.dynamicalSystems[0].utm1
        self.predField += self.bF * assembleScalar(state2D[0] * state2D[1] * aDs2d.dx)
        for ds1d in self.system.dynamicalSystems[1:]:
            # array W: w_i = u_i*v_i
            arrayW = ds1d.utm1.comps[0].x.array[:]*ds1d.utm1[1].x.array[:]
            ds1d.bufState.comps[0].x.array[:] = np.dot(arrayKM, arrayW)
            predHedge += assemble_scalar(form(ds1d.bufState.comps[0] * ds1d.dx))
        #with open("contribPred.txt", 'a') as file:


# End Overload

# Overload timeDoOneStep to end simu when pest < seuil
#NOTE: Overload simu !!
class myDoOneStep(timeDoOneStep):
    def doOneStep(self):
        # if pest density is lower than x
        if(assemble_scalar(
            form(
                self.study.system.dynamicalSystems[0].utm1.comps[0]
                * self.study.system.dynamicalSystems[0].dx
                )
            ):
            # stop simu:
        super().doOneStep()
        pass

# End Overload
nbComps = 3
nbParam = 0
nbParcelles = 20
nbEdges = 70
finalTime = 50
nbParam = 2
sol = 2 # 1 or 2
#
dP = 1.
dF = 1.
dH = 1.
bF = 1.
bH = 1.
mF = 1.
mH = 1.

# a study
studName = "STUDY_IMPACT_HEDGE_AND_FIELD_PRED"
aStudy = study(studName, nbParam=nbParam)
# the study zone
#aStudy.setGeometry("SQUARE",0.4)
aStudy.setGeometry("SQUARE",3)


# a system
aSystem=system(aStudy)
# dynamical system
ds2d = dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
ds1dtab = [
        dynamicalSystem(aStudy, 1, "edge"+str(i+1)+".xdmf", nbComps=nbComps)
        for i in range(4)
        ]

# set parameter
aStudy.setParam((dF, bH))

### Calculus of J ###
# Dof of 2d space

dofDs2dX = ds2d.vSpace.tabulate_dof_coordinates()[:,0]
dofDs2dY = ds2d.vSpace.tabulate_dof_coordinates()[:,1]
# number of DOF 2d
nbDOF2d = len(dofDs2dX)
nbDof1d = sum(
        len(
            ds1d.vSpace.tabulate_dof_coordinates()[:-1,0]
            )
        for ds1d in ds1dtab
        )
# array of value of int(exp^-d(y_i,z) )dz
Fy = np.zeros(nbDOF2d, dtype=float)
# loop for each edges
for ds1d in ds1dtab:
    # Dof of 1d space
    # NOTE: we do not take the last DOF of each ds1d
    # so as not count the corner twice
    dofDs1dX = ds1d.vSpace.tabulate_dof_coordinates()[:-1,0]
    dofDs1dY = ds1d.vSpace.tabulate_dof_coordinates()[:-1,1]
    nbDOF1d = len(dofDs1dX)
    print("nbDOF1d = ",nbDOF1d)
    # print("type of dofDs1dX = ",type(dofDs1dX)) #= numpy.ndarray
    # temp array of value exp(...)
    dist = Function(ds1d.vSpace)
    dist.x.array[0] = 0.
    # loop for each DOF 2d NOTE: need ???
    for i in range(nbDOF2d):
        tmpExpDist = np.exp(-np.power(dofDs1dX - dofDs2dX[i], 2)-np.power(dofDs1dY - dofDs2dY[i], 2))
        dist.x.array[1:] = tmpExpDist[:]
        Fy[i] += assemble_scalar(form(dist*ds1d.dx))

print("Fy[0:10] = ",Fy[0:10])
print("Fy[mid:mid+1010] = ",Fy[int(nbDOF2d/2):int(nbDOF2d/2)+10])
reciprocFy = np.reciprocal(Fy)

print("RecFy[0:10] = ",reciprocFy[0:10])
print("recFy[mid:mid+1010] = ",reciprocFy[int(nbDOF2d/2):int(nbDOF2d/2)+10])

print("len reciprocF = ",len(reciprocFy))

print("nbDOF2d = ",nbDOF2d)

### save of K*M in numpyArray
# DOF 1d
dof1Dx = np.concatenate([ds1d.vSpace.tabulate_dof_coordinates()[:-1,0] for ds1d in ds1dtab])
dof1Dy = np.concatenate([ds1d.vSpace.tabulate_dof_coordinates()[:-1,1] for ds1d in ds1dtab])
# Array of Matrix K
arrayMatrixK = np.exp(
        -np.power(dof1Dx[:, np.newaxis] -dof2Dx[np.newaxis,:] ,2)
        -np.power(dof1Dy[:, np.newaxis] -dof2Dy[np.newaxis,:] ,2)
        )
        * reciprocFy(np.newaxis, :)

# We want to mult K by mass matrix 2d
if sol==1:
    # Sol 1
    # We create a temp matrix (sad) wich contain the mult of K and M
    tmpPestcMatKM = PETSc.Mat().createDense(
            arrayMatrixK.shape, array=arrayMatrixK
            ).matMult(
                    ds2d.massMat.petscMat
                    )
    # We save the matrix on a numpy array
    arrayKM = tmpPestcMatKM.getValues(range(0, tmpPestcMatKM.getSize()[0]), range(0,  tmpPestcMatKM.getSize()[1]))
    #
    # NOTE: too much matrix and array create...
    # 2 dense petsc matrix + 2 numpy array
    #
elif sol == 2:
    # Sol 2
    # the mass matrix is transformed into a numpy array
    arrayMassM = ds2d.massMat.petscMat.getValues(
            range(
                0,
                ds2d.massMat.petscMat.getSize()[0]
                ),
            range(
                0,
                ds2d.massMat.petscMat.getSize()[1]
                )
            )
    # We multiply K by M
    arrayKM = np.matMult(arrayMatrixK, arrayMassM)
    # Matrix KM_partialOmega
    arraysKtMb = [
            # product Matrix K^t matrix mass
            np.matMult(
                # transpose of K
                arrayMatrixK.transpose()[
                    # With good index
                    ind:ind + ds1d.nbDofsPerComp
                    ],
                # Mass Matrix of boundary, from petsc to array
                ds1d.massMat.petscMat.getValues(
                    range(
                        0,
                        ds1d.massMat.petscMat.getSize()[0]
                        ),
                    range(
                        0,
                        ds1d.massMat.petscMat.getSize()[1]
                        )
                    )
                )
            for ind,ds1d in zip(
                np.cumsum([0] + [ds1d.nbDofsPerComp for ds1d in ds1dtab[:-1]]),
                ds1dtab
                )
            ]
    #arraysKtMb = [None]*4
    #tmp = 0
    #for i in range(4):
    #    arraysKtMb[i] = np.matMult(
    #        arrayMatrixK.transpose()[tmp:tmp+ds1d.nbDofsPerComp],
    #        ds1d.massMat.petscMat.getValues(
    #            range(
    #                0,
    #                ds1d.massMat.petscMat.getSize()[0]
    #                ),
    #            range(
    #                0,
    #                ds1d.massMat.petscMat.getSize()[1]
    #                )
    #            )
    #        )
    #    tmp+=nbDofsPerComp
           

else:
    print("Error: sol is different of 1 or 2")
    exit(1)
### End calculus KM ###

# Set initiale State
ds2d.initialState[0] = "1.0+0.0*"+ds2d.initialState[0]
ds2d.initialState[1] = "1.0+0.0*"+ds2d.initialState[1]
ds2d.initialState[2] = "0.0+0.0*"+ds2d.initialState[1]

### Set Cov 2D ###
# pest disffusion
CovDp = covaribaleFromExpression(ds2d, "DP")
CovDp.setValue(dP)
ds2d.addHeterogenCov("DP", CovDp)
# ??? field predator
CovBF = covaribaleFromExpression(ds2d, "BF")
CovBF.setValue(bF)
ds2d.addHeterogenCov("BF", CovBF)
# Pest consuption by hedge predator
# NOTE: the value of ZH will be update on
# the overload function of TimeScheme
covZP = covaribaleFromExpression(ds1d, "ZP")
CovZP.setValue(0.)
ds2d.addHeterogenCov("ZP", CovDH)
### Diffusion model 2d
txtDiffusionModel(ds2d, "diff2d3CompsD")
### Source term 2D
txtModel(ds2d, "pestPredHF2D")

# Loop for each ds1d
for ds1d in ds1dtab:
    ### Set Cov 1D ###
    # Diffusion hedgePred
    CovDH = covaribaleFromExpression(ds1d, "DH")
    CovDH.setValue(dH)
    ds1d.addHeterogenCov("DH", CovDH)
    # Pest consuption by hedge predator
    # NOTE: the value of ZH will be update on
    # the overload function of TimeScheme
    covZH = covaribaleFromExpression(ds1d, "ZH")
    CovZH.setValue(0.)
    ds1d.addHeterogenCov("ZH", CovZH)
    ### Diffusion model 1D ###
    txtDiffusionModel(ds1d, "diff1d3CompsD")
    ### Source term 1D ###
    txtModel(ds2d, "pestPredHF1D")




# loop for each edges
# Dof of 2d space
dofDs2dX = ds2d.vSpace.tabulate_dof_coordinates()[:,0]
dofDs2dY = ds2d.vSpace.tabulate_dof_coordinates()[:,1]
fHY = np.zeros(dofDs2dY.size)
# loop for each Hedges
for i,ds1d in enumerate(ds1dtab):
    # Dof of 1d space
    dofDs1dX = ds1d.vSpace.tabulate_dof_coordinates()[:,0]
    dofDs1dY = ds1d.vSpace.tabulate_dof_coordinates()[:,1]
    dist = Function(ds1d.vSpace)
    # Loop for each DOF of 2d Space
    # NOTE: need ?????
    for j in range(fHY.size):
        # calcul of exp(dist)
        dist.x.array[:] = np.exp(-np.square(dofDs2dX[j]*dofDs1dX[:])-np.square(dofDs2dY[j]*dofDs1dY[:]))
        # assembleSacalar
        fHY[j] += assemble_scalar(form(dist*ds1d.dx))


aTS=None
# time algorithm
aTS=implicitNonLinearTimeScheme()
#simu in t \in [0,10]
simulator(0,10,aStudy)
#based on fix time step 0.05
aTStep=timeDoOneStep(aStudy,aTS,0.05)
#run the simulation
aStudy.simulator.doSimu()


#for visu, export simulation steps to vtk
aStudy.simulator.exportSimuToVtk()
##loop on simu to compute mass
#nSteps=aStudy.simulator.curStep+1
#import np as np
#memMass=np.zeros((ds2d.nbComps)*nSteps,dtype=np.double).reshape((ds2d.nbComps,nSteps))
#mass=np.zeros((ds2d.nbComps),dtype=np.double)
#
#cmp=0
#aExplorTraj=explorTrajectory(aStudy,aStudy.simuDir)
#aExplorTraj.rewind()
#while(aExplorTraj.replay()):
#    computeMass(ds2d,mass)
#    memMass[0][cmp]=mass[0]
#    memMass[1][cmp]=mass[1]
#    print("mass "+str(aExplorTraj.curStep)+" = ", end=" ")
#    for m in mass:
#        print(m,end=", ")
#    print("")
#    cmp+=1
#
#aStudy.end()
#import matplotlib.pyplot as plt
#f = plt.figure()
#plt.plot(memMass[0],memMass[1])
#plt.show()
#
#f.savefig(aStudy.absoluteStudyDir+"preyPredHedge.pdf", bbox_inches='tight')
