import mse.MESH_IMPORT_GMSH.read as reader
from mse.MESH_IMPORT_GMSH.view import viewTopo
from mse.toolsEnv import computerEnv,MSEPrint

aTopo=reader.geomTopoReader(computerEnv.geomDir+"SQUARE/")
aViewer=viewTopo(aTopo)
aViewer.plotAll()
aViewer.show()

aTopo=reader.geomTopoReader(computerEnv.geomDir+"PAYSAGE20/")
aViewer=viewTopo(aTopo)
aViewer.plotAll()
aViewer.show()

