#demo to plot the geometry imported
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import  mkdir
from os.path import isdir


nbComps=1
testNLTS=0
studName="STUDY13_"
viewTopo=1
from mse.MESH_IMPORT_GMSH.view import viewTopo
for (GEONAME,tol) in zip(("FRANCE","SIMPLE3_POLYLINE","FRANCE_ROUTES"),(10000,0.2,10000)):
    #a study
    aStudy=study(studName+GEONAME)
    # only view topo:
    aStudy.setGeometry(GEONAME)
    #remove unused point and edges
    aStudy.topo.exportMinimal()
    aViewTopo=viewTopo(aStudy.topo)
    aViewTopo.plotAll()
    #aViewTopo.plotWires([1])
    aViewTopo.show()
    # build mesh
    aStudy.setGeometry(GEONAME,tol)
    aStudy.end()

