#demo building covariables 
#
#
#

from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import model,txtModel, growth, affineGrowth
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory

from os import  mkdir
from os.path import isdir
#model.verbose(True)

nbComps=1
testNLTS=0


#une etude
studName="STUDY6_COV"
#a study
aStudy=study(studName)
# the study zone
aStudy.setGeometry("SQUARE",0.4)




#un systeme
aSystem=system(aStudy)
#un systeme dynamique
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
#ds2d1.initialState[0]="0.2+0.0*"+ds2d1.initialState[0]

from mse.COVARIABLE.covariable import covariableFromExpression, covariableCst, builderCovariableFromRaster
aCovExpreK=covariableFromExpression(ds2d1,"K")
aCovExpreK.setValue("1+np.sin(2*x[0])*np.sin(2*x[1])")
aCovExpreK.exportVtk()
aCovCst=covariableCst(ds2d1,"R")
aCovCst.setValue(1.0)
aCovCst.exportVtk()
aBuilderCovRaster=builderCovariableFromRaster("W",ds2d1,"../mseMatTools/RASTER/testRaster")
aBuilderCovRaster.doVtk=1
aBuilderCovRaster.build()
ds2d1.addHomogenCov("R",aCovCst)
ds2d1.addHeterogenCov("K",aCovExpreK)
aAffineGrotwthSource=txtModel(ds2d1,"affineGrowthRK")
aDiffModel=diffusionModel(ds2d1)
aDiffModel.setDiffCoef(1.0)

aTS=implicitLinearTimeScheme()
#simu in t \in [0,1]
simulator(0,1,aStudy)
#based on fix time step 0.05
aTStep=timeDoOneStep(aStudy,aTS,0.05)
#run the simulation
aStudy.simulator.doSimu()


#for visu, export simulation steps to vtk
aStudy.simulator.exportSimuToVtk()

#loop on simu to compute mass
import numpy as np
mass=np.zeros((ds2d1.nbComps),dtype=np.double)
aExplorTraj=explorTrajectory(aStudy,aStudy.simuDir)
aExplorTraj.rewind()
while(aExplorTraj.replay()):
    computeMass(ds2d1,mass)
    print("mass "+str(aExplorTraj.curStep)+" = ",end="")
    for m in mass:
        print(m,end=", ")
    print("")
aStudy.end()
