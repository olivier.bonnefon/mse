"""
Example of parameter estimation
==================================
This example show how to estimate parameter for KPP-Fisher equation with scypy.
The model is like:
   \partial_t u(t,x,y) =  p1*u(t,x,y) * (1-u(t,x,y)/p2) + \\nabla p1*u(t,x,y)
       (x,y) in \Omega (\Omega in R^2), t in [T_begin, T_end], and pi parameter to define (in R)
   u(0,x,y) = u_0(x,y)
   \partial_n u(t,x,y) = 0  \\forall t and for (x,y) in boudary
 In this example, we do a simulation to create the reference"""


# %%
# The import of all library

from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel, txtDiffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.systemAdjoint import systemAdjoint 
from mse.obsProcess import quadraticDiff
from mse.simulator import simulator,simulatorBackward
from mse.timeDoOneStep import timeDoOneStep,timeDoOneStepBackward
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme,implicitLinearBackwardTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import  mkdir
from os.path import isdir, exists
import numpy as np
import shutil
from scipy.optimize import minimize, OptimizeResult


# %%
# ****** Can be change by user
# Boolean, if we need to create a reference
needRef = True
# Number of compement
nbComps=1
# Number of parameter
nbParam=2
# Dimension
dim=2
# Name gives to file result
studName="STUDY10"
# Name of directory's Mesh
#NOTE: to choose your own mesh, create a directory with your files in  the directory "BD_GEOMETRY"
myDirMesh = "SQUARE"
# Time for simulation, t \in [t0,t1]
t0=0.
t1=2.
# TimeStep for simulation
stepT = 0.05
# Initiale State,
#The initiale state must be write in a str, with x[0] for x coordinate and x[1] for y coordinate; with python notation.
#If you need specific function, use numpy (the import is done, numpy as np)
#By default ds2d1.initialStateAdjoint[0] = "np.exp(-(x[0]**2+x[1]**2))"
#For example: "x[0]*12.5 + np.exp(-x[1]**2)"
#NOTE: Even if your initiale state is homogenous in space, you must have to write x[0] and x[1]
#for example: (x[0]+x[1]) * 0 + cst
#myInitState = "0.0+1.0*np.exp(-(x[0]**2+x[1]**2))"
# model (without the diffusion term)
# check Readme for more detail about file model
myModel = "logisticGrowthRParam"
# Diffusion Model
nameSendOrdTerm="diff2dIsotropP1"

# ****** End of user changes
# %%
# Creation of the study
aStudy=study(studName, nbParam)

# Define the mesh
aStudy.setGeometry(myDirMesh,0.4)

# Dynamical system
ds2d1=dynamicalSystem(aStudy,dim,"surface1.xdmf",nbComps=nbComps)



# %%
# Diffusion
aDiffModel=txtDiffusionModel(ds2d1, nameSendOrdTerm)
# Model
m=txtModel(ds2d1,myModel)
aTS=None
#schema en temps implicite lineaire
aTS=implicitNonLinearTimeScheme()
aTS.theta=1
aTS.maxIt=5
simulator(t0,t1,aStudy)
#based on fix time step 0.05
aTStep=timeDoOneStep(aStudy,aTS,stepT)


# %%
# We do first simu with param
#
# DATA Simulation #
if (needRef):
    #FIXME: can we change the time step ? dt=0.1 ?
    #aTStep.dt=0.1
    paramRef = [5., 1.]
    aStudy.setParam(paramRef)
    print("Build ref for example with param = ",paramRef)
    aStudy.simulator.doSimu()
    path=aStudy.absoluteStudyDir+"/UREF/"
    if(exists(path)):
        shutil.rmtree(path)
    shutil.copytree(aStudy.simuDir, path)
    #aTStep.dt=stepT
#quadraticDiff
quaDiff=quadraticDiff(ds2d1)
# plot
doPlot=True
# %%
paramPath=[]
globalCount=0

def aCallableOpt(intermediate_result: OptimizeResult):
    """a callable use in intermediate odo a 
    """
    global globalCount
    globalCount+=1
    print("****************\nIn iteration"+str(globalCount)+": \n",intermediate_result)
    print("V = ",quaDiff.computeV())
    print("GradV = ",quaDiff.computeGradV())
    paramPath.append(intermediate_result.x.copy())

def findParamOpt(defaultParam):
    """ call function to find param opt
    """
    global globalCount
    # SIMU #
    #we call function to find paramOpt
    optParam = minimize(quaDiff.sysAdj.valJAndGradJ, defaultParam, method="L-BFGS-B", bounds=[(0.01,12.),(0.1,6.)], jac=True, tol=1e-8, callback=aCallableOpt, options={"maxiter":35})
    print (optParam)
    tmpParamPath = paramPath[-1-optParam.nit:]
    tmpParamPath.insert(0,np.array(defaultParam))
    tmpParamPath = np.array(tmpParamPath)
    print("path of param: ",tmpParamPath)
    print("paramRef = ",paramRef)
    if (doPlot):
        import matplotlib.pyplot as plt
        plt.figure()
        plt.plot(tmpParamPath[:,0],tmpParamPath[:,1],'b+-')
        plt.plot([5.],[1.],'r+')
        plt.xlabel("Parameter D")
        plt.ylabel("Parameter R")
        plt.axis((0.1,12.,0.5,6.5))
        globalCount+=1
        plt.title("parameter for test "+str(globalCount))
        plt.savefig(studName+"/pathParameter"+str(globalCount)+".png")

# %%
# call of function to find paramOPt
findParamOpt([10,5.])
