#study with non linear system (fisher-kpp)
#
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitNonLinearTimeScheme,timeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory

from os import  mkdir
from os.path import isdir


nbComps=1

#une etude
studName="STUDY4_KPP"

#a study
aStudy=study(studName)
# the study zone
aStudy.setGeometry("SQUARE",0.4)



#un systeme
aSystem=system(aStudy)
#un systeme dynamique
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
#ds2d1.initialState[0]="0.2+0.0*"+ds2d1.initialState[0]
if (nbComps>1):
    ds2d1.initialState[1]="0.5*"+ds2d1.initialState[0]
#la diffusion
aDiffModel=diffusionModel(ds2d1)
#ds2d1.addSecondOrderTerm(aDiffModel)
aDiffModel.setDiffCoef(1.0)
m=txtModel(ds2d1,"logistic")

#schema en temps implicite non lineaire

aTS=implicitNonLinearTimeScheme()
aTS.criteron=1e-6
simulator(0,10,aStudy)
aTStep=timeDoOneStep(aStudy,aTS,0.05)
timeScheme.verboseMode=1
aStudy.simulator.doSimu()


#for visu, export simulation steps to vtk
aStudy.simulator.exportSimuToVtk()

#loop on simu to compute mass
import numpy as np
mass=np.zeros((ds2d1.nbComps),dtype=np.double)
aExplorTraj=explorTrajectory(aStudy,aStudy.simuDir)
aExplorTraj.rewind()
while(aExplorTraj.replay()):
    computeMass(ds2d1,mass)
    print("mass "+str(aExplorTraj.curStep)+" = ", end=" ")
    for m in mass:
        print(m,end=", ")
    print("")

aStudy.end()
