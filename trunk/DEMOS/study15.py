from mse.study import study
from mse.dynamicalSystem import dynamicalSystem, computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel, txtDiffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import (implicitLinearTimeScheme,
                                        implicitNonLinearTimeScheme)
from mse.INTERACTIONS.interaction import (interaction2D1D,
                                          interaction1D1D,
                                          interactionEdgesConnect)
from mse.toolsEnv import computerEnv, MSEPrint
from mse.explorTrajectory import explorTrajectory
from mse.COVARIABLE.covariable import (covariableFromExpression,
                                       covariableCst,
                                       builderCovariableFromRaster)
from os import mkdir
from os.path import isdir, isfile
import numpy as np
from dolfinx.fem import form, assemble_scalar


# study based to the model
# with interaction parcelles and bordures
nbComps = 2
WITH_LOGISTIC = 1
WITH_INT_1D1D = 1
testNLTS = 1
studName = "STUDY15_DIFFUSION"
if (WITH_LOGISTIC):
    studName = studName+"_AND_LOGISTIC"
if (WITH_INT_1D1D):
    studName = studName+"_AND_INT_1D1D"
nbParam = 0
nbParcelles = 20
nbEdges = 70
valueD = 10
finalTime = 40
# innoculation sur les parcelles

# derived class of DS to override "beforeDoOneStep"
class myDynamicalSystem(dynamicalSystem):
    nbTreatment = 0
    def __init__(self, aStudy, aGeoDim, aMeshFile, nbComps=1, obsProc=None):
        super().__init__(aStudy, aGeoDim, aMeshFile, nbComps, obsProc)
        # surface of geom, NOTE: need ? exist ?
        self.surface = assemble_scalar(form(1.*self.dx))
        # duration of a treatment
        self.traitmentDuration = 0.1
        # time of treatment, if < 0, no treatment
        self.endTreatment = -1
        # boolean in treatment or not NOTE: need ?
        self.inTreatment = False
    def beforeOneStep(self, fromTime, toTime):
        """We overload this function to change the charge Capacity K
        cf: pdf
        """
        # check if treatment and how many times
        #print("ds"+str(self.id) + " in treatment ? ", self.inTreatment)
        # if traitment and if time out
        if self.inTreatment and self.endTreatment < fromTime:
            # end traitment,
            self.inTreatment = False
            # K from 0.01 to 20
            self.dicHomogenCov["K"].setValue(20.)
            print("End treatment in ds",self.id)
        # if no traitment
        elif not(self.inTreatment):
            # Calcul of density
            density = assemble_scalar(form(self.utm1.comps[0]*self.dx)) / self.surface
            #print("\tdensity =",density)
            # if density > 0.1
            if density > 0.1:
                # we are in treatment
                self.inTreatment = True
                # we add treatment in count
                print(f"nb treatment before: {myDynamicalSystem.nbTreatment}", end = ", ")
                myDynamicalSystem.nbTreatment+=1
                print(f"nb treatment after: {myDynamicalSystem.nbTreatment}", end = ", ")
                # time end treatment
                self.endTreatment = fromTime + self.traitmentDuration
                # do traitment: k from 20 to 0.01
                self.dicHomogenCov["K"].setValue(0.01)
                print("Treatment do in ds",self.id)


# a study
aStudy = study(studName, nbParam)
# the study zone
aStudy.setGeometry("PAYSAGE20", 0.9)
# NOTE: parcelle 11 & 13 differente dans code et dans pdf
# a system
aSystem = system(aStudy)

# dynamical system
# DS 2D
ds2dTab = [
    myDynamicalSystem(aStudy, 2, "surface"+str(i+1)+".xdmf", nbComps=nbComps)
    if i !=0 and i != 10
    else dynamicalSystem(aStudy, 2, "surface"+str(i+1)+".xdmf", nbComps=nbComps)
    for i in range(nbParcelles)
]

# DS 1D
ds1dTab = [
    dynamicalSystem(aStudy, 1, "edge"+str(i+1)+".xdmf", nbComps=nbComps)
    if isfile(aStudy.meshesDir+"edge"+str(i+1)+".xdmf")
    else None
    for i in range(nbEdges)
]

# Indice corespondant au parcelles cultures et parcelles ravageurs
indCult = [
    val for ind, val in enumerate(range(nbParcelles))
    if ind != 0 and ind != 10
]
indPred = [0, 10]


# Cultures
aTabCovCstK = [None]*len(ds2dTab)  # tab wich contain covariable K
aTabCovCstD = [None]*len(ds1dTab)  # tab wich contain covariable D
aTabSource2D = [None]*nbParcelles  # tab wich contain sourceTerm 2d
for ind in indCult:
    #### ds 2D ####
    ds = ds2dTab[ind]
    # Covariable
    aTabCovCstK[ind] = covariableCst(ds, "K")
    aTabCovCstK[ind].setValue(20.)
    ds.addHomogenCov("K", aTabCovCstK[ind])
    # Set initiale State
    ds.initialState[0] = "0.0+0.0*" + ds.initialState[0]
    ds.initialState[1] = ".37+0.0*" + ds.initialState[1]
    # Set Diffusion coef
    diffusionModel(ds).setDiffCoef(1.0)
    # Set sourceTerm Logistic
    aTabSource2D[ind] = txtModel(ds, "cultPestPred")
    # Value of D, the diffusion of prey in boundary


# Pred
for ind in indPred:
    #### ds 2D ####
    ds = ds2dTab[ind]
    # Set initiale State
    ds.initialState[0] = "0.0+0.0*"+ds.initialState[0]
    ds.initialState[1] = ".37+0.0*"+ds.initialState[1]
    # Set Diffusion coef
    diffusionModel(ds).setDiffCoef(1.0)
    # Set sourceTerm Logistic
    aTabSource2D[ind] = txtModel(ds, "reservePestPred")


# Edges
for ind, ds in enumerate(ds1dTab):
    #### ds 1D ####
    if (isinstance(ds, dynamicalSystem)):
        # Covariable
        aTabCovCstD[ind] = covariableCst(ds, "D")
        aTabCovCstD[ind].setValue(valueD)
        ds.addHomogenCov("D", aTabCovCstD[ind])
        # Set initiale State
        ds.initialState[0] = "0.0+0.0*" + ds.initialState[0]
        ds.initialState[1] = "0.0+0.0*" + ds.initialState[1]
        # Set diffusion coef
        txtDiffusionModel(ds, "diff1d2CompIsotropCstD")


# INTERACTIONS:
for indDs, ds2D in enumerate(ds2dTab):
    for indEdge in aStudy.topo.Wires[indDs].edges:
        interaction2D1D(aStudy, ds2D, ds1dTab[indEdge-1])


# Connect edges
# TODO
# Loop for each edges-1
for ind, ds1d1 in enumerate(ds1dTab[:-1]):
    # Loop for each other edges
    for ds1d2 in ds1dTab[ind:]:
        # Create connection edges.
        # if edges are not connected in meshes, interaction will do nothing.
        if isinstance(ds1d1, dynamicalSystem) and isinstance(ds1d2, dynamicalSystem):
            interactionEdgesConnect(aStudy, ds1d1, ds1d2)

aTS = None
# select the time algorithm
aTS = implicitNonLinearTimeScheme()
# simu in t \in [0,1]
simulator(0, finalTime, aStudy)
# NOTE: need to define timestep: different result with 0.05 and 0.1
# based on fix time step 0.05
aTStep = timeDoOneStep(aStudy, aTS, 0.05)
# run the simulation
aStudy.simulator.doSimu()


# for visu, export simulation steps to vtk
aStudy.simulator.exportSimuToVtk()

#loop on simu to compute mass
ds2d = ds2dTab[1]
mass = np.zeros((ds2dTab[1].nbComps),dtype=np.double)
aExplorTraj = explorTrajectory(aStudy,aStudy.simuDir)
aExplorTraj.rewind()
while(aExplorTraj.replay()):
   for ds2d in ds2dTab[0:2]:
       computeMass(ds2d, mass)
       print(f'mass of ds2d {ds2d.id:2} t{aExplorTraj.curStep:3d} = ',end="")
       for m in mass:
           print(f'{m:.4f}',end=", ")
           print(f'mean = {m/assemble_scalar(form(1.*ds2d.dx)):.4f}', end = ", ")
       print("")
#     for ds1d in ds1dTab[:2]:
#         computeMass(ds1d, mass)
#         print(f'mass of ds1d {ds1d.id:2} t{aExplorTraj.curStep:3d} = ',end="")
#         for m in mass:
#             print(f'{m:.4f}',end=", ")
#             print(f'mean = {m/assemble_scalar(form(1.*ds1d.dx)):.4f}', end = ", ")
#         print("")

aStudy.end()
print("nb treatment: ", myDynamicalSystem.nbTreatment)
