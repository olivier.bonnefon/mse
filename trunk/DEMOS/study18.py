# study with 2 compatiments
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel, txtDiffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitNonLinearTimeScheme,timeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from mse.linAlgTools import matrix, vector
from mse.COVARIABLE.covariable import (covariableFromExpression,
                                       covariableCst,
                                       builderCovariableFromRaster)
from petsc4py import PETSc

from os import  mkdir
from os.path import isdir
import numpy as np
from dolfinx.fem import Function, assemble_scalar,form
# debugger
import pdb
# Study of predator living in edges

nbComps=2
nbParam = 0
nbParcelles = 20
nbEdges = 70
valueD = 1
DRav = 1.
DPred = 20.
finalTime = 50

#une etude
studName="STUDY18_PREYPRED"
#a study
aStudy=study(studName)
# the study zone
#aStudy.setGeometry("SQUARE",0.4)
aStudy.setGeometry("SQUARE",3)


# Surcharge class Matrix pour matrix dense

class myDenseMatrix(matrix):
    def __init__(self, aMat=None) -> None:
        """The constructor"""
        super().__init__(aMat)
    def setDim(self, aN, aM):
        """It build a DENSE matrix.

        .. WARNING::
            
            do not use if u can do a sparse matrix

        :param int aN: number of row
        :param int aM: number of column 
        """
        self.N=aN
        self.M=aM
        self.petscMat.createDense([aN,aM])
        self.petscMat.setUp()
        self.petscMat.assemble()

#un systeme
aSystem=system(aStudy)
#un systeme dynamique
ds2d = dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
ds1dtab = [
        dynamicalSystem(aStudy, 1, "edge"+str(i+1)+".xdmf", nbComps=nbComps)
        for i in range(4)
        ]

# Dof of 2d space
dofDs2dX = ds2d.vSpace.tabulate_dof_coordinates()[:,0]
dofDs2dY = ds2d.vSpace.tabulate_dof_coordinates()[:,1]
# number of DOF 2d
nbDOF2d = len(dofDs2dX)
# array of value of int(exp^-d(y_i,z) )dz
Fy = np.zeros(nbDOF2d, dtype=float)
# loop for each edges
for ds1d in ds1dtab:
    # Dof of 1d space
    # NOTE: we do not take the last DOF of each ds1d
    # so as not count the corner twice
    dofDs1dX = ds1d.vSpace.tabulate_dof_coordinates()[:-1,0]
    dofDs1dY = ds1d.vSpace.tabulate_dof_coordinates()[:-1,1]
    nbDOF1d = len(dofDs1dX)
    print("nbDOF1d = ",nbDOF1d)
    # print("type of dofDs1dX = ",type(dofDs1dX)) #= numpy.ndarray
    # temp array of value exp(...)
    dist = Function(ds1d.vSpace)
    dist.x.array[0] = 0.
    # loop for each DOF 2d NOTE: need ???
    for i in range(nbDOF2d): 
        tmpExpDist = np.exp(-np.power(dofDs1dX - dofDs2dX[i], 2)-np.power(dofDs1dY - dofDs2dY[i], 2))
        dist.x.array[1:] = tmpExpDist[:]
        Fy[i] += assemble_scalar(form(dist*ds1d.dx))

print("Fy[0:10] = ",Fy[0:10])
print("Fy[mid:mid+1010] = ",Fy[int(nbDOF2d/2):int(nbDOF2d/2)+10])
reciprocFy = np.reciprocal(Fy)

print("RecFy[0:10] = ",reciprocFy[0:10])
print("recFy[mid:mid+1010] = ",reciprocFy[int(nbDOF2d/2):int(nbDOF2d/2)+10])

print("len reciprocF = ",len(reciprocFy))

print("nbDOF2d = ",nbDOF2d)

### creation of matrix k ###
# type petsc matrix
matrixK = myDenseMatrix()
# type of matrix
# NOTE: dense matrix
# dim matrix = nbDOF1D x nbDof2d
#NOTE: see note of DOF ds1d for nb DOF1D
nbRowK, nbColK = (
        sum(
        len(ds1d.vSpace.tabulate_dof_coordinates()[:-1,0])
        for ds1d in ds1dtab
        )
        ,
        nbDOF2d)
print("nbRowK, nbColK = ",(nbRowK, nbColK))
dof1Dx = np.concatenate([ds1d.vSpace.tabulate_dof_coordinates()[:-1,0] for ds1d in ds1dtab])
dof1Dy = np.concatenate([ds1d.vSpace.tabulate_dof_coordinates()[:-1,1] for ds1d in ds1dtab])

print("len dof1Dx, dof1Dy = ", (len(dof1Dx),len(dof1Dy)))
matrixK.setDim(nbRowK, nbColK)
# loop for assemble
rstart, rend = matrixK.petscMat.getOwnershipRange()
#rstart, rend = matrixK.petscMat.getSize()

print("rstart,rend = ",(rstart,rend))
for row in range(rstart, rend):
    matrixK.petscMat.setValues(
            row,
            list(range(nbDOF2d)),
            np.exp(
                (- np.power(dof1Dx[row] - dofDs2dX, 2)
                - np.power(dof1Dy[row] - dofDs2dY, 2))
                *reciprocFy
                )
            )
# matrix assembly process
matrixK.petscMat.assemblyBegin()
matrixK.petscMat.assemblyEnd()

#matrixK.petscMat.view()

# NOTE: verifier mesure de s
# multiplier matrice K par matrice de masse M (ds2d.massMat) et V=[1] (vector(1))
vectKMV = PETSc.Vec().create()
vectKMV.setSizes(nbRowK)
vectKMV.setFromOptions()
vectResult = vectKMV.copy()
#
vect1 = PETSc.Vec().create()
vect1.setSizes(nbColK)
vect1.setFromOptions()
vect1.set(1.)
c = matrixK.petscMat.matMult(ds2d.massMat.petscMat)
c.mult(vect1, vectKMV)
vectKMV.view()
vectResult.view()

# Creation matrice masse du bords

matMbord = matrix()
matMbord.setDim(nbRowK,nbRowK)
beginN = 0
for ds1d in ds1dtab[:-1]:
    matMbord.assembleMat(ds1d.massMat, beginN, beginN)
    beginN += ds1d.massMat.N - 1
# to add last mass matrix execpt last line
breakpoint()
ds1d = ds1dtab[-1]
nR, nC = ds1d.massMat.petscMat.getSize()
indexSet = PETSc.IS().createGeneral(list(range(nR-1)))
matMassLastBord = matrix(ds1d.massMat.petscMat.createSubMatrix(indexSet,indexSet))
matMbord.assembleMat(matMassLastBord, beginN, beginN)
# on multilie par la matrice de masse du bords
matMbord.petscMat.mult(vectKMV, vectResult)


result = vectResult.sum()
vectKMV.view()
vectResult.view()
print("result = ",result)
exit(0)


# Set initiale State
ds2d.initialState[0] = "5.0+0.0*"+ds2d.initialState[0]
ds2d.initialState[1] = "1.0+0.0*"+ds2d.initialState[1]

# Set Diffusion coef
CovD1 = covariableCst(ds2d, "A")
CovD1.setValue(DRav)
ds2d.addHomogenCov("A", CovD1)
CovD2 = covariableCst(ds2d, "E")
CovD2.setValue(DPred)
ds2d.addHomogenCov("E", CovD2)
txtDiffusionModel(ds2d, "diff2dIsotropD1D2")




# set predation term
##### TODO ######
# calul of F_y = Int_H exp(-d(z,y)) dz
# TODO
# NOTE:
# surcharge timescheme.buildRHS et ajouter K M W ?

# loop for each edges
# Dof of 2d space
dofDs2dX = ds2d.vSpace.tabulate_dof_coordinates()[:,0]
dofDs2dY = ds2d.vSpace.tabulate_dof_coordinates()[:,1]
fHY = np.zeros(dofDs2dY.size)
# loop for each Hedges
for i,ds1d in enumerate(ds1dtab):
    # Dof of 1d space
    dofDs1dX = ds1d.vSpace.tabulate_dof_coordinates()[:,0]
    dofDs1dY = ds1d.vSpace.tabulate_dof_coordinates()[:,1]
    dist = Function(ds1d.vSpace)
    # Loop for each DOF of 2d Space
    # NOTE: need ?????
    for j in range(fHY.size):
        # calcul of exp(dist)
        dist.x.array[:] = np.exp(-np.square(dofDs2dX[j]*dofDs1dX[:])-np.square(dofDs2dY[j]*dofDs1dY[:]))
        # assembleSacalar
        fHY[j] += assemble_scalar(form(dist*ds1d.dx))


aTS=None
# time algorithm
aTS=implicitNonLinearTimeScheme()
#simu in t \in [0,10]
simulator(0,10,aStudy)
#based on fix time step 0.05
aTStep=timeDoOneStep(aStudy,aTS,0.05)
#run the simulation
aStudy.simulator.doSimu()


#for visu, export simulation steps to vtk
aStudy.simulator.exportSimuToVtk()
##loop on simu to compute mass
#nSteps=aStudy.simulator.curStep+1
#import np as np
#memMass=np.zeros((ds2d.nbComps)*nSteps,dtype=np.double).reshape((ds2d.nbComps,nSteps))
#mass=np.zeros((ds2d.nbComps),dtype=np.double)
#
#cmp=0
#aExplorTraj=explorTrajectory(aStudy,aStudy.simuDir)
#aExplorTraj.rewind()
#while(aExplorTraj.replay()):
#    computeMass(ds2d,mass)
#    memMass[0][cmp]=mass[0]
#    memMass[1][cmp]=mass[1]
#    print("mass "+str(aExplorTraj.curStep)+" = ", end=" ")
#    for m in mass:
#        print(m,end=", ")
#    print("")
#    cmp+=1
#
#aStudy.end()
#import matplotlib.pyplot as plt
#f = plt.figure()
#plt.plot(memMass[0],memMass[1])
#plt.show()
#
#f.savefig(aStudy.absoluteStudyDir+"preyPredHedge.pdf", bbox_inches='tight')
