# simulation 2D, with linear system
# test linear and non-linear time stepping
#
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from os import  mkdir
from os.path import isdir


nbComps=1
WITH_AFFINE_GROWTH=0
testNLTS=0
studName="STUDY1_DIFFUSION"
if (WITH_AFFINE_GROWTH):
    studName=studName+"_AND_AFFINE_GROWTH"

#a study
aStudy=study(studName)
# the study zone
aStudy.setGeometry("SQUARE",0.4)
#a dynamical  system 
ds2d1=dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
#ds2d1.initialState[0]="0.2+0.0*"+ds2d1.initialState[0]
if (nbComps>1):
    ds2d1.initialState[1]="0.5*"+ds2d1.initialState[0]
#

#add diffusion motion
aDiffModel=diffusionModel(ds2d1)
aDiffModel.setDiffCoef(1.0)
if (WITH_AFFINE_GROWTH):
    m=txtModel(ds2d1,"affineGrowth")
    m.computeMatOnce=1
    m.computeRhsOnce=1

aTS=None
# select the time algorithm
if (testNLTS):
    aTS=implicitNonLinearTimeScheme()
else:
    aTS=implicitLinearTimeScheme()
#simu in t \in [0,1]
simulator(0,1,aStudy)
#based on fix time step 0.05
aTStep=timeDoOneStep(aStudy,aTS,0.05)
#run the simulation
aStudy.simulator.doSimu()


#for visu, export simulation steps to vtk
aStudy.simulator.exportSimuToVtk()

#loop on simu to compute mass
import numpy as np
mass=np.zeros((ds2d1.nbComps),dtype=np.double)
aExplorTraj=explorTrajectory(aStudy,aStudy.simuDir)
aExplorTraj.rewind()
while(aExplorTraj.replay()):
    computeMass(ds2d1,mass)
    print("mass "+str(aExplorTraj.curStep)+" = ",end="")
    for m in mass:
        print(m,end=", ")
    print("")
aStudy.end()
