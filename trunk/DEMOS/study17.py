# study with 2 compatiments
from mse.study import study
from mse.dynamicalSystem import dynamicalSystem,computeMass
from mse.DISPERSIONS.diffusionModel import diffusionModel, txtDiffusionModel
from mse.MODELS.models import txtModel
from mse.system import system
from mse.simulator import simulator
from mse.timeDoOneStep import timeDoOneStep
from mse.TIMESCHEMES.timeScheme import implicitNonLinearTimeScheme,timeScheme
from mse.toolsEnv import computerEnv,MSEPrint
from mse.explorTrajectory import explorTrajectory
from mse.COVARIABLE.covariable import (covariableFromExpression,
                                       covariableCst,
                                       builderCovariableFromRaster)

from os import  mkdir
from os.path import isdir

# Study of predator living in field
# COV
DRav = 1.5
DPred = 1.2
valL = 1.
valM = 1.
valB = 1.

nbComps=2

#une etude
studName="STUDY17_PREYPRED"
#a study
aStudy=study(studName)
# the study zone
aStudy.setGeometry("SQUARE",0.4)

#un systeme
aSystem=system(aStudy)
#un systeme dynamique
ds2d = dynamicalSystem(aStudy,2,"surface1.xdmf",nbComps=nbComps)
# need ?
# ds1tab = [
#         dynamicalSystem(aStudy, 1, "edge"+str(i+1)+".xdmf", nbComps=nbComps)
#         for i in range(4)
#         ]
# 
# Set initiale State
ds2d.initialState[0] = "5.0+0.0*"+ds2d.initialState[0]
ds2d.initialState[1] = "1.0+0.0*"+ds2d.initialState[1]
# Set sourceTerm
CovM = covariableCst(ds2d, "M")
CovM.setValue(valM)
ds2d.addHomogenCov("M", CovM)
CovL = covariableCst(ds2d, "L")
CovL.setValue(valL)
ds2d.addHomogenCov("L", CovL)
CovB = covariableCst(ds2d, "B")
CovB.setValue(valB)
ds2d.addHomogenCov("B", CovB)
m = txtModel(ds2d, "predField")
# Set Diffusion coef
CovD1 = covariableCst(ds2d, "A")
CovD1.setValue(DRav)
ds2d.addHomogenCov("A", CovD1)
CovD2 = covariableCst(ds2d, "E")
CovD2.setValue(DPred)
ds2d.addHomogenCov("E", CovD2)
txtDiffusionModel(ds2d, "diff2dIsotropD1D2")

aTS=None
# time algorithm
aTS=implicitNonLinearTimeScheme()
#simu in t \in [0,10]
simulator(0,10,aStudy)
#based on fix time step 0.05
aTStep=timeDoOneStep(aStudy,aTS,0.05)
#run the simulation
aStudy.simulator.doSimu()


#for visu, export simulation steps to vtk
aStudy.simulator.exportSimuToVtk()
#loop on simu to compute mass
nSteps=aStudy.simulator.curStep+1
import numpy as np
memMass=np.zeros((ds2d.nbComps)*nSteps,dtype=np.double).reshape((ds2d.nbComps,nSteps))
mass=np.zeros((ds2d.nbComps),dtype=np.double)

cmp=0
aExplorTraj=explorTrajectory(aStudy,aStudy.simuDir)
aExplorTraj.rewind()
while(aExplorTraj.replay()):
    computeMass(ds2d,mass)
    memMass[0][cmp]=mass[0]
    memMass[1][cmp]=mass[1]
    print("mass "+str(aExplorTraj.curStep)+" = ", end=" ")
    for m in mass:
        print(m,end=", ")
    print("")
    cmp+=1

aStudy.end()
import matplotlib.pyplot as plt
f = plt.figure()
plt.plot(memMass[0],memMass[1])
plt.show()

f.savefig(aStudy.absoluteStudyDir+"preyPredField.pdf", bbox_inches='tight')
