import MESH_IMPORT_GMSH.read as reader
import MESH_IMPORT_GMSH.buildMeshes as buildMeshes

from study import study
from dynamicalSystem import dynamicalSystem,computeMass
from DISPERSIONS.diffusionModel import diffusionModel
from MODELS.models import txtModel
from system import system
from simulator import simulator
from timeDoOneStep import timeDoOneStep
from TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from toolsEnv import computerEnv,MSEPrint
nbComps=1
WITH_AFFINE_GROWTH=1
testNLTS=0

reader.verbose=0
buildMeshes.verbose=0
#La geometrie
aTopo=reader.geomTopoReader(computerEnv.geomDir+"GLS/")
aMeshesDir=computerEnv.meshesDir+"GLS/"
#build meshes can be only once
buildMeshes.buildXdmf(aTopo,aMeshesDir,4000)

#une etude
studName="STUDY_GLS"
if (WITH_AFFINE_GROWTH):
    studName=studName+"_AND_AFFINE_GROWTH"

aStudy=study(studName,aTopo,aMeshesDir)


#un systeme
aSystem=system(aStudy)
#un systeme dynamique
ds2d1=dynamicalSystem(aStudy,1,2,aMeshesDir+"surface1.xdmf",nbComps=nbComps)
#ds2d1.initialState[0]="0.2+0.0*"+ds2d1.initialState[0]
if (nbComps>1):
    ds2d1.initialState[1]="0.5*"+ds2d1.initialState[0]
#

#la diffusion
aDiffModel=diffusionModel(ds2d1)
#ds2d1.addSecondOrderTerm(aDiffModel)
aDiffModel.setDiffCoef(1.0)
if (WITH_AFFINE_GROWTH):
    #affineGrowth(ds2d1,R,K)
    m=txtModel(ds2d1,computerEnv.srcDir+"/trunk/MODELS/affineGrowth.txt")
    m.computeMatOnce=1
    m.computeRhsOnce=1

aTS=None
#schema en temps implicite lineaire
if (testNLTS):
    aTS=implicitNonLinearTimeScheme()
else:
    aTS=implicitLinearTimeScheme()
    aTS.computeMatOnce=1

simulator(0,1,0.05,aStudy)
aTStep=timeDoOneStep(aStudy,aTS)

aStudy.simulator.doSimu()


#for visu, export simulation steps to vtk
aStudy.simulator.exportSimuToVtk()

#loop on simu to compute mass
import numpy as np
mass=np.zeros((ds2d1.nbComps),dtype=np.double)
aStudy.simulator.rewind()
while(aStudy.simulator.replay()):
    computeMass(ds2d1,mass)
    print("mass "+str(aStudy.simulator.curStep)+" = ",end="")
    for m in mass:
        print(m,end=", ")
    print("")

aStudy.end()
