#input : image 3 canneaux RGB PopGLS.png
#output : images 1 canal for pop brut and convol pop1chanel.png
#         images 1 canal for grad pop brut and convoled
import numpy as np
from PIL import Image



PointsColors=np.array([[0 ,   166,  0],
                      [71,   190,  0],
                      [171,  216,  0],
                      [233 , 190,  56],
                      [238 , 186,  162],
                       [255 ,255, 255]])
PointsValues=np.array([10,8,6,4,2,0])

def RGBToPop(RGB):
    nearest=0
    dnears=100000
    for i in range(len(PointsColors)):
        C=PointsColors[i]
        dist=np.dot(RGB-C,RGB-C)
        if (dist<dnears):
            dnears=dist
            nearest=i
    iprev=nearest-1
    if (iprev>0):
        C=PointsColors[iprev]
        diprev=np.dot(RGB-C,RGB-C)
    else:
        diprev=10000000
    inext=nearest+1
    if (inext<len(PointsColors)-1):
        C=PointsColors[inext]
        dinext=np.dot(RGB-C,RGB-C)
    else:
        dinext=10000000
    secNearest=iprev
    if (dinext<diprev):
        secNearest=inext
    print(str(nearest)+" "+str(secNearest))
    secDnear=np.dot(RGB-PointsColors[secNearest],RGB-PointsColors[secNearest])
    return (secDnear*PointsValues[nearest]+dnears*PointsValues[secNearest])/(secDnear+dnears)

im = Image.open("PopGLS.png")
imarray = np.array(im)
ndx=imarray.shape[0]
ndy=imarray.shape[1]

vArray=np.zeros(ndx*ndy).reshape(ndx,ndy)

gradArray=np.zeros(ndx*ndy).reshape(ndx,ndy)
gradConvolArray=np.zeros(ndx*ndy).reshape(ndx,ndy)


for i in range(ndx):
    for j in range (ndy):
        vArray[i][j]=0.1*RGBToPop(imarray[i,j])

imb= Image.fromarray((np.uint8(255*vArray)))
imb.save("pop1chanel.png")

for i in range(ndx):
    for j in range (ndy):
        varpop=0
        nvar=0
        if (i<ndx-1):
            varpop+=abs(vArray[i][j]-vArray[i+1][j])
            nvar+=1
        if (i>0):
            varpop+=abs(vArray[i][j]-vArray[i-1][j])
            nvar+=1
        if (j<ndy-1):
            varpop+=abs(vArray[i][j]-vArray[i][j+1])
            nvar+=1
        if (j>0):
            varpop+=abs(vArray[i][j]-vArray[i][j-1])
            nvar+=1
        gradArray[i][j]=varpop/nvar

maxGrad=gradArray.max()
        
imb= Image.fromarray((np.uint8((255.0/maxGrad)*gradArray)))
imb.save("gradpop1chanel.png")

def convelAndSave(arrayOfValues,name):
    workCopy=arrayOfValues.copy()
    for convolHalfDist in [5,10,20]:
        for i in range(ndx):
            for j in range (ndy):
                nval=0
                val=0
                for k in range(-convolHalfDist,convolHalfDist+1):
                    if ((i+k)<0 or (i+k)>=ndx):
                        continue
                    for l in range(-convolHalfDist,convolHalfDist+1):
                        if ((j+l)<0 or (j+l)>=ndy):
                            continue
                        val+=arrayOfValues[i+k][j+l]
                        nval+=1
                workCopy[i][j]=val/(nval)
        maxConvGrad=workCopy.max()         
        imb= Image.fromarray((np.uint8((255.0/maxConvGrad)*workCopy)))
        imb.save(name+"_convol_"+str(convolHalfDist)+".png")

convelAndSave(gradArray,"gradPop")
convelAndSave(vArray,"pop")
