from dolfinx.io import XDMFFile,VTKFile
from dolfinx.fem import FunctionSpace,form
from mpi4py import MPI
from ufl import Measure,TrialFunction,TestFunction
from dolfinx.fem import Function, assemble_scalar
from dolfinx.fem.petsc import assemble_matrix
import numpy as np

meshDir="/home/olivierb/nonlocal_2d1d/trunk/MESHES/GLS/"
meshFile="surface1.xdmf"

with XDMFFile(MPI.COMM_WORLD,meshDir+meshFile,"r") as xdmf:
    dolfinMesh= xdmf.read_mesh(name="Grid")

#from dolfinx import mesh
#dolfinMesh = mesh.create_rectangle(comm=MPI.COMM_WORLD,
#                            points=((0, 0), (1000, 1000)), n=(75, 75),
#                            cell_type=mesh.CellType.triangle)

    
dx=Measure('dx',dolfinMesh)
vSpace=FunctionSpace(dolfinMesh,("CG",1))
XYZ=vSpace.tabulate_dof_coordinates().copy()
xmax=XYZ[:,0].max()
xmin=XYZ[:,0].min()
ymax=XYZ[:,1].max()
ymin=XYZ[:,1].min()
pop=Function(vSpace)

nbDofsPerComp=vSpace.tabulate_dof_coordinates().shape[0]
u= TrialFunction(vSpace)
v= TestFunction(vSpace)

#      R     G    B
# max :0    166  0   10
#      71   190  0   8
#      171  216  0   6
#      233  190  56  4
#      238  186  162 2 
# min :255 255 255   0

PointsColors=np.array([[0 ,   166,  0],
                      [71,   190,  0],
                      [171,  216,  0],
                      [233 , 190,  56],
                      [238 , 186,  162],
                       [255 ,255, 255]])
PointsValues=np.array([10,8,6,4,2,0])

def RGBToPop(RGB):
    nearest=0
    dnears=100000
    for i in range(len(PointsColors)):
        C=PointsColors[i]
        dist=np.dot(RGB-C,RGB-C)
        if (dist<dnears):
            dnears=dist
            nearest=i
    iprev=nearest-1
    if (iprev>0):
        C=PointsColors[iprev]
        diprev=np.dot(RGB-C,RGB-C)
    else:
        diprev=10000000
    inext=nearest+1
    if (inext<len(PointsColors)-1):
        C=PointsColors[inext]
        dinext=np.dot(RGB-C,RGB-C)
    else:
        dinext=10000000
    secNearest=iprev
    if (dinext<diprev):
        secNearest=inext
    print(str(nearest)+" "+str(secNearest))
    secDnear=np.dot(RGB-PointsColors[secNearest],RGB-PointsColors[secNearest])
    return (secDnear*PointsValues[nearest]+dnears*PointsValues[secNearest])/(secDnear+dnears)

def readPopToDolfinx():
    from PIL import Image
    im = Image.open("PopGLS.png")
    imarray = np.array(im)
    ndx=imarray.shape[0]
    ndy=imarray.shape[1]
    rasterDx=(1+ymax-ymin)/ndx
    rasterDy=(1+xmax-xmin)/ndy
    def buildRMaskPicture(x):
        xindex=int((ymax-x[1])/rasterDx)
        yindex=int((x[0]-xmin)/rasterDy)
        return RGBToPop(imarray[xindex,yindex])
    vbuildRMask=np.vectorize(buildRMaskPicture,signature="(d)->()")
    pop.x.array[:]=vbuildRMask(XYZ)
    vtkfile_R = VTKFile(MPI.COMM_WORLD,'Popref.pvd',"w")
    vtkfile_R.write_function(pop, 0)
    vtkfile_R.close()

readPopToDolfinx()
