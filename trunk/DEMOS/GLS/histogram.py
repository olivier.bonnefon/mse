import numpy as np

step=0.1
fromVal=0.
toVal=4


def buildHistogram(XX):
    nelem=XX
    prevcount=0
    curval=fromVal+step
    cmp=0
    res=np.zeros(int((toVal-fromVal)/step)+1)
    while(curval<=toVal):
        count=0;
        for x in XX:
            if (float(x)<curval):
                count+=1
        res[cmp]=count-prevcount
        print(str(curval-step)+" "+str(curval)+" "+str(res[cmp]))
        prevcount=count
        curval+=step
        cmp+=1
    return res
