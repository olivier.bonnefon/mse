#
# input : 1 chanel picture val and grad (brut and convoled)
#         pop1chanel.png pop_convol_5.png pop_convol_10.png
#
#
# output : meshes (to see mesh : gmsh toto.med / or paraview toto.sdmf)
#          without mesh size measure : MESHES/GLS 
#          with mesh size dependind of pop :  MESHES/GLSADAPTEDPOP

import MESH_IMPORT_GMSH.read as reader
import MESH_IMPORT_GMSH.buildMeshes as buildMeshes

from study import study
from dynamicalSystem import dynamicalSystem,computeMass
from DISPERSIONS.diffusionModel import diffusionModel
from MODELS.models import txtModel
from system import system
from simulator import simulator
from timeDoOneStep import timeDoOneStep
from TIMESCHEMES.timeScheme import implicitLinearTimeScheme,implicitNonLinearTimeScheme
from toolsEnv import computerEnv,MSEPrint
from dolfinx.io import XDMFFile,VTKFile
from mpi4py import MPI
from ufl import Measure
from dolfinx.fem import FunctionSpace
from dolfinx.fem import Function
import numpy as np
from PIL import Image
import os 

reader.verbose=0
buildMeshes.verbose=0
#La geometrie
aTopo=reader.geomTopoReader(computerEnv.geomDir+"GLS/")
aMeshesDir=computerEnv.meshesDir+"GLS/"

#build meshes can be only once
meshSize=5000
buildMeshes.buildXdmf(aTopo,aMeshesDir,meshSize)

meshFile="surface1.xdmf"

with XDMFFile(MPI.COMM_WORLD,aMeshesDir+meshFile,"r") as xdmf:
    dolfinMesh= xdmf.read_mesh(name="Grid")

dx=Measure('dx',dolfinMesh)
vSpace=FunctionSpace(dolfinMesh,("CG",1))
XYZ=vSpace.tabulate_dof_coordinates().copy()
xmax=XYZ[:,0].max()
xmin=XYZ[:,0].min()
ymax=XYZ[:,1].max()
ymin=XYZ[:,1].min()
pop=Function(vSpace)



im = Image.open("pop1chanel.png")
vPop = np.array(im)
ndx=vPop.shape[0]
ndy=vPop.shape[1]
#because sym picture
rasterDx=(1+ymax-ymin)/ndx
rasterDy=(1+xmax-xmin)/ndy




if (1):
    import histogram as histo
    histo.fromVal=0
    histo.toVal=256
    histo.step=5
    hh=histo.buildHistogram(vPop.reshape(vPop.shape[0]*vPop.shape[1]))
if (1):
    def  buildRMaskPicture(x):
        xindex=int((ymax-x[1])/rasterDx)
        yindex=int((x[0]-xmin)/rasterDy)
        return vPop[xindex][yindex]
    vbuildRMask=np.vectorize(buildRMaskPicture,signature="(d)->()")
    pop.x.array[:]=vbuildRMask(XYZ)
    vtkfile_R = VTKFile(MPI.COMM_WORLD,'Popref.pvd',"w")
    vtkfile_R.write_function(pop, 0)
    vtkfile_R.close()


popMin=75
popMax=200

meshSize=10000
minSize=0.15*meshSize
def meshSizeCallbackPop(dim, tag, x, y, z, lc):
    xindex=int((ymax-y)/rasterDx)
    yindex=int((x-xmin)/rasterDy)
    if (xindex<0):
        xindex=0
    if (xindex>=ndx):
        xindex=ndx-1
    if (yindex<0):
        yindex=0
    if (yindex>=ndy):
        yindex=ndy-1
    pop=vPop[xindex][yindex]
    res=((pop-popMin)*minSize + (popMax-pop)*meshSize)/(popMax-popMin)
    if (res>meshSize):
        res=meshSize
    if (res<minSize):
        res=minSize
    return res


for img in ["pop1chanel.png","pop_convol_5.png","pop_convol_10.png"]:
    aMeshesDir=computerEnv.meshesDir+"GLS_POP_ADAPTED"+img.strip(".png")+"/"
    try:
        os.mkdir(aMeshesDir)
    except OSError as error: 
        pass 
    im = Image.open(img)
    vPop = np.array(im)
    buildMeshes.buildXdmf(aTopo,aMeshesDir,meshSize,meshSizeCallbackPop)

popMin=20
popMax=100

for img in ["gradPop_convol_5.png","gradPop_convol_10.png","gradPop_convol_20.png"]:
    aMeshesDir=computerEnv.meshesDir+"GLS_POP_ADAPTED"+img.strip(".png")+"/"
    try:
        os.mkdir(aMeshesDir)
    except OSError as error: 
        pass 
    im = Image.open(img)
    vPop = np.array(im)
    buildMeshes.buildXdmf(aTopo,aMeshesDir,meshSize,meshSizeCallbackPop)

