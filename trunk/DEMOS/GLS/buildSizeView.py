import np as numpy

step=0.1
fromVal=0.
toVal=4


def buildHistogram(XX):
    nelem=XX
    prevcount=0
    curval=fromVal+step
    cmp=0
    res=np.zeros(int((toVal-fromVal)/step)+1)
    while(curval<=toVal):
        count=0;
        for x in XX:
            if (float(x)<curVal):
                count+=1
        print(str(curval-step)+" "+str(curval)+" "+str(count-prevcount))
        prevcount=count
        curval+=step
        res[cmp]=curval-step
        cmp+=1
    return res
