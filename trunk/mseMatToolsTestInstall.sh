echo "========================================================"
echo "===== 3) install mseMatTools :=========================="
echo "========================================================"

cd $TRUNK_MSE
mkdir mseMatTools/build
cd mseMatTools/build
cmake .. -DCMAKE_INSTALL_PREFIX=$TRUNK_MSE/TEST_INSTAL/installmseMatTools
make install

echo "and compil with option -DCMAKE_BUILD_TYPE=Debug"

echo "========================================================"
echo "===== 4) install python interface pymseMatTools ========"
echo "========================================================"

cd $TRUNK_MSE
mkdir mseMatTools/Frontend/build
cd mseMatTools/Frontend/build
cmake .. -DMSE_MAT_TOOLS_INSTALL_PATH=$TRUNK_MSE/TEST_INSTAL/installmseMatTools -DCMAKE_INSTALL_PREFIX=$TRUNK_MSE/TEST_INSTAL/installpymseMatTools
make
export PYTHONPATH=$PYTHONPATH:$TRUNK_MSE/TEST_INSTAL/installpymseMatTools/lib/python3.10/site-packages/
echo "to check this:"
python3 -c "import pymseMatTools.mseMatTools as mt"

cd $TRUNK_MSE

