USE OF MSE
==========

It is possible ti use MSE without building it.

JUST USE MSE:
-------------

The simplest method is to use guix to create an MSE environment based on an archived version of MSE. 
The first step consists in downloading the last MSE-sources from forgemia. 
Next, the following command builds the MSE environnement:
.. code-block:: bash

	cd mse/trunk
	guix time-machine -C ../guix/channelsMSE.scm -- shell -C --share=/tmp --preserve='^DISPLAY$' -m ../guix/manifestDolfinxv06.scm -m ../guix/manifestMSE.scm -L ../guix/recettes/

The guix option  --share=/tmp --preserve='^DISPLAY$' are useful for graphic export (for paraview) 

To install guix and get a brief overview, look at this link "https://gitlab.inria.fr/fuentes/guix4n00bs". 


BUILDING AND INSTALL all components of MSE from sources using guix
------------------------------------------------------------------
For developers, it can be useful to build MSE from sources. Again, the simplest is to use guix. The first step is to get sources and launch the environment in the mse/trunk directory:   

.. code-block:: bash

	guix time-machine -C ../guix/channelsMSE.scm -- shell -C --share=/tmp --preserve='^DISPLAY$'  -m ../guix/manifestDolfinxv06.scm  -m ../guix/manifestCompilAllMSEComponents.scm -L ../guix/recettes/

The guix option  --share=/tmp --preserve='^DISPLAY$' are useful for graphic export (for debug with ddd) 

Within this environment, Mse installation needs 5 compilations steps (4 cmake + 1 setup.py) resume in following commands. The copy-paste works, thanks guix.

.. code-block:: python

    echo "ensure you are in mse/trunk directory ?" 
    
    TRUNK_MSE=$(pwd)/   
    mkdir TEST_INSTAL  
    rm -Rf TEST_INSTAL/*  
    
    echo "remove previuos builds"
    rm -rf mseMatTools/build/* mseMatTools/Frontend/build/* mseMapTools/build/* mseMapTools/Frontend/build/* 

    echo "1) install mseMatTools :"
    
    cd $TRUNK_MSE  
    mkdir mseMatTools/build  
    cd mseMatTools/build  
    cmake .. -DCMAKE_INSTALL_PREFIX=$TRUNK_MSE/TEST_INSTAL/installmseMatTools  
    make install  
    
    echo "and compil with option -DCMAKE_BUILD_TYPE=Debug"  
    
    echo "2) install python interface pymseMatTools"  
    
    cd $TRUNK_MSE  
    mkdir mseMatTools/Frontend/build  
    cd mseMatTools/Frontend/build  
    cmake .. -DMSE_MAT_TOOLS_INSTALL_PATH=$TRUNK_MSE/TEST_INSTAL/installmseMatTools -DCMAKE_INSTALL_PREFIX=$TRUNK_MSE/TEST_INSTAL/installpymseMatTools  
    make  
    export PYTHONPATH=$PYTHONPATH:$TRUNK_MSE/TEST_INSTAL/installpymseMatTools/lib/python3.10/site-packages/   
    echo "to check this:"  
    python3 -c "import pymseMatTools.mseMatTools as mt"  
    
    echo "3) install mseMapTools " 
    cd $TRUNK_MSE  
    mkdir mseMapTools/build  
    cd mseMapTools/build  
    cmake .. -DCMAKE_INSTALL_PREFIX=$TRUNK_MSE/TEST_INSTAL/installmseMapTools  
    make install  
    echo "4) install python interface pymseMapTools"  
    cd $TRUNK_MSE  
    
    mkdir mseMapTools/Frontend/build  
    cd mseMapTools/Frontend/build  
    cmake .. -DMSE_MAP_TOOLS_INSTALL_PATH=$TRUNK_MSE/TEST_INSTAL/installmseMapTools -DCMAKE_INSTALL_PREFIX=$TRUNK_MSE/TEST_INSTAL/installpymseMapTools  
    make  
    export PYTHONPATH=$PYTHONPATH:$TRUNK_MSE/TEST_INSTAL/installpymseMapTools/lib/python3.10/site-packages/   
    
    echo "to check this:"  
    python3 -c "import pymseMapTools.mseMapTools as mp"  
   
    echo "5) install mseSafranTools "
    cd $TRUNK_MSE
    rm -rf mseSafranTools/build/* mseSafranTools/FrontEnd/build/*  
    mkdir mseSafranTools/build
    cd mseSafranTools/build
    cmake .. -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=$TRUNK_MSE/TEST_INSTAL/installmseSafranTools
    make install
    echo "6) install python interface pymseSafranTools"
    cd $TRUNK_MSE
    
    mkdir mseSafranTools/Frontend/build
    cd mseSafranTools/Frontend/build
    cmake .. -DMSE_SAFRAN_TOOLS_INSTALL_PATH=$TRUNK_MSE/TEST_INSTAL/installmseSafranTools -DCMAKE_INSTALL_PREFIX=$TRUNK_MSE/TEST_INSTAL/installpymseSafranTools
    make
    export PYTHONPATH=$PYTHONPATH:$TRUNK_MSE/TEST_INSTAL/installpymseSafranTools/lib/python3.10/site-packages/

    echo "to check this:"
    python3 -c "import pymseSafranTools.mseSafranTools as mp"

    echo "7) python install:"  
    
    cd $TRUNK_MSE  
    echo "clean local python build :  " 
    rm -rf dist build src/MSE.egg-info/ $TRUNK_MSE/TEST_INSTAL/installMse/ 
    echo "- run the python installation"  
   
    python3 setup.py build
    python3 setup.py install --prefix=$TRUNK_MSE/TEST_INSTAL/installMse/ --no-compile --single-version-externally-managed --root=/
    ls $TRUNK_MSE/TEST_INSTAL/installMse/lib/python3.10/site-packages/mse/  
    echo "The path to local install is add to python path:"  
    export PYTHONPATH=$PYTHONPATH:$TRUNK_MSE/TEST_INSTAL/installMse/lib/python3.10/site-packages/
    echo "To ckeck that mse is available:"  
    python3 -c "import mse"  


doc generation
--------------


Documention is built using sphinx. The documention building needs to build and install MSE within the guix environnement describe above.

.. code-block:: bash

    cd docs
    mkdir build
    cd build
    copy-figuresrm -rf *
    cmake ..
    make html
    make copy-figures

The generated doc is in result is in ./build/html


