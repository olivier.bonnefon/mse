cmake_minimum_required(VERSION 3.13)
# use pkg-config to find PETSC
project(MSE_MAP_TOOLS LANGUAGES C )
find_package(PkgConfig REQUIRED)

set(ENV{PKG_CONFIG_PATH} "$ENV{PKG_CONFIG_PATH}")

add_library(mseMapTools SHARED src/mseMapTools.c)
set_target_properties(mseMapTools PROPERTIES PUBLIC_HEADER "src/mseMapTools.h")
add_executable(testMseMap src/testMseMap.c)
target_link_libraries(testMseMap mseMapTools)
target_include_directories(mseMapTools
  PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src>
  $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)
target_link_libraries(mseMapTools PUBLIC ${MAP_TOOLS_LINK_LIBRARIES} m)
message(STATUS "OB" ${CMAKE_CURRENT_SOURCE_DIR} " " ${CMAKE_INSTALL_INCLUDEDIR} " " ${CMAKE_INSTALL_LIBDIR})

enable_testing()
add_test(testMseMap testMseMap)
#add_subdirectory(Frontend)

install(TARGETS mseMapTools
  EXPORT ${PROJECT_NAME}Targets
  INCLUDES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})

#install (
#	DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/src
#	DESTINATION  ${CMAKE_INSTALL_LIBDIR}/../include/
#	FILES_MATCHING
#	PATTERN *.h
#)


