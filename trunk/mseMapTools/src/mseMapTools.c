#include "stdio.h"
#include "math.h"
#include "mseMapTools.h"
#include "lat4251perDay.h"
#include <stdlib.h>
//#define DEBUG_MAP
#define NAME_SIZE 2048
void buildMapc(double * in_array1D, int size1d, double * in_array2D,int size2d, int * out_array,int sizeOut,double  aTol){
    int i1d,i2d;
#ifdef DEBUG_MAP
    printf("call buildMapc size 1d size 2d sizeOut: %i %i %i\n",size1d,size2d,sizeOut);
#endif
    double * pl2d;
    for (i1d=0;i1d<sizeOut;i1d++){
        pl2d=in_array2D;
        *out_array=-1;
        for (i2d=0;i2d<(size2d/3);i2d++){
#ifdef DEBUG_MAP
	printf("comparing index 1d %i, 2d %i, 1D %e %e , 2D %e %e\n",i1d,i2d,*in_array1D,*(in_array1D+1),*pl2d,*(pl2d+1));	
#endif
            if (fabs(*in_array1D-*pl2d)<aTol && fabs(*(in_array1D+1)-*(pl2d+1))<aTol){
                *out_array=i2d;
#ifdef DEBUG_MAP
                printf("%i %i\n",i1d,i2d);
#endif
                break;
            }
            pl2d++;
            pl2d++;
            pl2d++;
        }
        out_array++;
        in_array1D++;
        in_array1D++;
        in_array1D++;
    }
}


/*
 *
 *computePhotoPeriod compute the photoperiod at XYZ. 
 * the result is one output file per day
 * If therhold > 0 
 *   then therhold is consider has a time in hours
 *   the outfile contains 1  where the photoperiod > therhold
 * else
 *   the outfile contains the photoperiod in minute  
 */

void computePhotoPeriod(double *XYZ,int nXYZ,char* lMseCovName,char* mseCovDir,double therholdP ){
	double latMin=42.3;
	double latMax=51;
	double yMin=1706747;
	double yMax=2676078;
	char fileName[2048];
	int nPt=nXYZ/3;
	sprintf(fileName,"%s/time%s.txt",mseCovDir,lMseCovName);
	FILE *fTime=fopen(fileName, "w");
	for (int date=0;date<365;date++){
		fprintf(fTime,"%i %i\n",date,date);
		sprintf(fileName,"%s/%s%i.bin",mseCovDir,lMseCovName,date);
		FILE *fichier = fopen(fileName, "wb");
		fwrite((char*)&nPt,sizeof(int),1,fichier); 
		for(int i=0;i<nPt;i++){
			double lY =XYZ[i*3+1];
			if (lY<yMin || lY > yMax){
				printf("mseMapTools, computePhotoPeriod,%i Y %e out of range %e %e\n",i,lY,yMin,yMax);
		       		break;
			}
			double lat = latMin + (lY-yMin)*(latMax-latMin)/(yMax-yMin);
			int ny=floor(lat-42);
			double coef=(lat-42)-ny;
//			printf("ny=%i coef=%e\n",ny,coef);
			if (ny<0){
				ny=0;
				coef=0;
			}
			if (ny>9){
				ny=9;
				coef=1;
			}
			double p=((1-coef)*dayTophoto[ny*365+date]+coef*dayTophoto[(ny+1)*365+date]);
//			printf("photoperiod %i ny=%i lat=%e coef=%e  %e heures\n",
//					date,ny,lat,coef,p);
			double val=0;
			if (therholdP>0){
				if(p>therholdP)
					val=1;
				fwrite((char*)&val, sizeof(double), 1,fichier);
			}else
				fwrite((char*)&p, sizeof(double), 1,fichier);
		}
		fclose(fichier);
	}
	fclose(fTime);
}
void computePhotoPeriodC(double *XYZ,int nXYZ,char* lMseCovName,char* mseCovDir,double therholdP ){
	double latMin=42.3;
	double latMax=51;
	double yMin=1706747;
	double yMax=2676078;
	char fileName[2048];
	int nPt=nXYZ/3;
	for (int date=0;date<365;date++){
		sprintf(fileName,"%s/%s%i.bin",mseCovDir,lMseCovName,date);
		FILE *fichier = fopen(fileName, "wb");
		fwrite((char*)&nPt,sizeof(int),1,fichier); 
		for(int i=0;i<nPt;i++){
			double lY =XYZ[i*3+1];
			if (lY<yMin || lY > yMax){
				printf("mseMapTools, computePhotoPeriod,%i Y %e out of range %e %e\n",i,lY,yMin,yMax);
		       		break;
			}
			double lat = latMin + (lY-yMin)*(latMax-latMin)/(yMax-yMin);
			double delta=(M_PI/180.0)*23.4*sin(2*M_PI*(284+date)/365.0);       	
			double cosH0=-tan(delta)*tan((M_PI/180.0)*lat);
			double p=(2./15.)*180.0*acos(cosH0)/M_PI;
			double val=0;
			printf("photoperiod %i lat=%e  delta=%e cosH0=%e  %e heures\n",
					date,lat,delta,cosH0,p);
			if (p>therholdP*60)
				val=1;
			fwrite((char*)&val, sizeof(double), 1,fichier);
		}
		fclose(fichier);
	}
}
int saveMyTimeVect(char * dirName,char* simulatorName, double * pVal, int nVal, int numDate,double simdate ){
	char fileName[NAME_SIZE];
	sprintf(fileName,"%s/%s%i.bin",dirName,simulatorName,numDate);
	FILE *fichier = fopen(fileName, "wb");
	if (!fichier){
		printf("mseSafranTools, saveMyTimeVect: can't open file :%s\n",fileName);
		return 1;
	}
	fwrite((char*)&nVal,sizeof(int),1,fichier);
	fwrite((char*)pVal, sizeof(double), nVal,fichier);
	fclose(fichier);
	sprintf(fileName,"%s/time%s.txt",dirName,simulatorName); 
	if (numDate==0)
		fichier = fopen(fileName,"w");
	else
		fichier = fopen(fileName,"a");
	fprintf(fichier,"%i %e\n",numDate, simdate);
	fclose(fichier);
	return 0;
}

//GDD(,numDay)=sigma_{n=0}^{n=numDay-1) max(T(,n)-therholdT)
//GDDT(,numDay) = 1 if GDD(,numDay)>therholdGDD)
//		= 0 else
// assume dirNameT contains a Tname binaries files with time'Tname'.txt file 
// build GDDT data in dirNameGDD directory
int computeGDD(char * dirNameGDD,char * GddName, char * dirNameT, char * Tname,double therholdT, double therholdGdd){
//1 first get data size
//1.1 get first index in time file
	char fileName[NAME_SIZE];
	sprintf(fileName,"%s/time%s.txt",dirNameT,Tname);
	FILE* fichier = fopen(fileName,"r");
	if (!fichier){
		printf("ERROR mseSafranTools computeGDD can't open file %s\n",fileName);
		return 1;
	}	
	char *line = NULL;
	size_t len;
	if (getline(&line, &len, fichier)==-1){
		printf("ERROR mseSafranTools computeGDD can't read line in file %s\n",fileName);
		return 1;
	}
	int numDate=0;
	fclose(fichier);
	sscanf(line,"%i",&numDate);
//1.2: open binary file and get size
	sprintf(fileName,"%s/%s%i.bin",dirNameT,Tname,numDate);
	fichier = fopen(fileName, "rb");	
	if (!fichier){
		printf("ERROR mseSafranTools computeGDD can't open binary file %s\n",fileName);
		return 1;
	}
	int sizeData,sd;
	fread(&sizeData,sizeof(int),1,fichier);
	fclose(fichier);
	if (sizeData<1){
		printf("ERROR mseSafranTools computeGDD read a null or neg size of data\n");
		return 1;
	}
//alloc memory
	double *pData=(double *) calloc(sizeData, sizeof(double));
	double *pGDD=(double *) calloc(sizeData, sizeof(double));
	double *pGDDTHEROLDED=(double *) calloc(sizeData, sizeof(double));
	double simDate;
	char gddTName[NAME_SIZE];
	sprintf(gddTName,"GDD%s",Tname);
//compute gdd and gdd therholded	
	sprintf(fileName,"%s/time%s.txt",dirNameT,Tname);
	FILE* fichierT = fopen(fileName,"r");
	ssize_t read;
	while ((read = getline(&line, &len, fichierT)) != -1) {
		//printf("info mseSafranTools, read line %s\n",line);
		sscanf(line,"%i%lf",&numDate,&simDate);
		//printf("info mseSafranTools, read %i %lf\n",numDate,simDate);
		sprintf(fileName,"%s/%s%i.bin",dirNameT,Tname,numDate);
		FILE * fichierB = fopen(fileName, "rb");	
		if (!fichierB){
			printf("ERROR mseSafranTools computeGDD: binary file %s cant be opened\n",fileName);
			return 1;
		}
		fread(&sd,sizeof(int),1,fichierB);
		fread(pData,sizeof(double),sizeData,fichierB);
		double* pGDDl=pGDD;
		double* pGDDTl=pGDDTHEROLDED;
		double* pDatal=pData;
		for (int k=0;k<sizeData;k++){
			if (!*pGDDTl){
				*pGDDl+=(*pDatal>therholdT)?*pDatal-therholdT:0;
				*pGDDTl=(*pGDDl > therholdGdd)?1:0;
			}
			pGDDl++;pGDDTl++;pDatal++;

		}
		saveMyTimeVect(dirNameGDD,gddTName,pGDDTHEROLDED,sizeData,numDate,simDate);
		fclose(fichierB);
	}
	if (line)
		free(line);
	fclose(fichierT);
	free(pGDD);
	free(pGDDTHEROLDED);
	return 0;

}


