#include "mseMapTools.h"
#define N1D 5
#define N2D 10
int main(){
double coord1d[N1D*3];
double coord2d[N2D*3];
int index1Dto2D[N1D];
int j;
int index1Dto2Dref[N1D];
int cur=0;
index1Dto2Dref[0]=2;
index1Dto2Dref[1]=1;
index1Dto2Dref[2]=9;
index1Dto2Dref[3]=0;
index1Dto2Dref[4]=3;
coord1d[cur]=0; coord1d[cur+1]=0; coord1d[cur+2]=0;cur+=3;
coord1d[cur]=1; coord1d[cur+1]=2; coord1d[cur+2]=3;cur+=3;
coord1d[cur]=0; coord1d[cur+1]=4; coord1d[cur+2]=0;cur+=3;
coord1d[cur]=0; coord1d[cur+1]=5; coord1d[cur+2]=0;cur+=3;
coord1d[cur]=6; coord1d[cur+1]=0; coord1d[cur+2]=0;cur+=3;
cur=0;
for (j=0;j<N2D*3;j++)
	coord2d[j]=9.0;

for (j=0;j<N1D;j++){
	coord2d[3*index1Dto2Dref[j]]=coord1d[3*j];
	coord2d[3*index1Dto2Dref[j]+1]=coord1d[3*j+1];
	coord2d[3*index1Dto2Dref[j]+2]=coord1d[3*j+2];
}
buildMapc(coord1d,3*N1D,coord2d,3*N2D,index1Dto2D,N1D,0.01);
for (j=0;j<N1D;j++){
	if (index1Dto2D[j]!=index1Dto2Dref[j])
		return 1;
}
return 0;
}
