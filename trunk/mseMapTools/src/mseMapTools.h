#ifndef MSEMAPTOOLS_H
#define MSEMAPTOOLS_H
#ifdef __cplusplus
extern "C" {
#endif


void buildMapc(double * in_array1D, int size1d, double * in_array2D,int size2d, int * out_array,int sizeOut,double  aTol);
void computePhotoPeriod(double *XYZ,int nXYZ,char* lMseCovName,char* mseCovDir,double therholdP );
int computeGDD(char * dirNameGDD,char * GddName, char * dirNameT, char * Tname,double therholdT, double therholdGdd);
int saveMyTimeVect(char * dirName,char* simulatorName, double * pVal, int nVal, int numDate,double simdate );

#ifdef __cplusplus
}
#endif
#endif
