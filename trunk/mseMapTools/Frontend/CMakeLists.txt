cmake_minimum_required(VERSION 3.13)
project(PY_MAP_TOOLS)
set(CMAKE_SWIG_FLAGS)
find_package(SWIG REQUIRED)
include(UseSWIG)
find_package(Python3 REQUIRED COMPONENTS Interpreter Development)
SET(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/../../cmake)
include(python_install)

find_package(Numpy)
message (STATUS "OB  MSE_MAP_TOOLS_INSTALL_PATH" ${MSE_MAP_TOOLS_INSTALL_PATH})
find_library(MSEMAPTOOLS_LIBRARY NAMES mseMapTools  HINTS ${MSE_MAP_TOOLS_INSTALL_PATH}/lib )
find_path(MSEMAPTOOLS_INCLUDE_DIR NAMES mseMapTools.h HINTS ${MSE_MAP_TOOLS_INSTALL_PATH}/include )
message (STATUS "OB MSEMAPTOOLS_INCLUDE_DIR " ${MSEMAPTOOLS_INCLUDE_DIR})
message (STATUS "OB MSEMAPTOOLS_LIBRARY " ${MSEMAPTOOLS_LIBRARY})
configure_file(${CMAKE_SOURCE_DIR}/mseMapTools.i.in ${CMAKE_SOURCE_DIR}/mseMapTools.i)
set_property(SOURCE mseMapTools.i PROPERTY SWIG_MODULE_NAME mseMapTools)


swig_add_library(pymseMapTools
  TYPE MODULE
  LANGUAGE python
  OUTPUT_DIR ${CMAKE_CURRENT_BINARY_DIR}/pymseMapTools
  SOURCES mseMapTools.i)


if (Numpy_FOUND)
  message(STATUS "Numpy Found ${Numpy_FOUND}")
  message(STATUS "Numpy_INCLUDE_DIRS = ${Numpy_INCLUDE_DIRS}")
else()
  message(FATAL_ERROR "Numpy Not Found!!!")
endif()
target_include_directories(pymseMapTools
  PRIVATE
  ${PYTHON_INCLUDE_PATH}
  ${Python3_INCLUDE_DIRS}
  ${Numpy_INCLUDE_DIRS}
  ${CMAKE_CURRENT_SOURCE_DIR}/src
  ../src
  )
set(PYTHON_PROJECT pymseMapTools)
message(STATUS "Python project: ${PYTHON_PROJECT}")
set(PYTHON_PROJECT_DIR ${PROJECT_BINARY_DIR}/python/${PYTHON_PROJECT})
message(STATUS "Python project build path: ${PYTHON_PROJECT_DIR}")

file(GENERATE OUTPUT ${PYTHON_PROJECT_DIR}/__init__.py CONTENT "__version__ = \"${PROJECT_VERSION}\"\n")

# Look for python module wheel
search_python_module(
  NAME setuptools
  PACKAGE setuptools)

search_python_module(
  NAME wheel
  PACKAGE wheel)
set_property(TARGET pymseMapTools PROPERTY SWIG_USE_TARGET_INCLUDE_DIRECTORIES ON)

target_link_libraries(pymseMapTools PRIVATE ${MSEMAPTOOLS_LIBRARY})
message (STATUS "OB ${CMAKE_CURRENT_BINARY_DIR}")

# Files to install with Python
set(SETUP_PY_IN ${CMAKE_CURRENT_SOURCE_DIR}/python/setup.py.in)
set(SETUP_PY_OUT ${CMAKE_CURRENT_BINARY_DIR}/python/setup.py)
configure_file(${SETUP_PY_IN} ${SETUP_PY_OUT})


add_custom_command(
	OUTPUT python/dist/timestamp
	COMMAND ${CMAKE_COMMAND} -E remove_directory dist
	COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_BINARY_DIR}/_pymseMapTools.so ${CMAKE_CURRENT_BINARY_DIR}/python/pymseMapTools/_pymseMapTools.so
	COMMAND ${CMAKE_COMMAND} -E copy ${CMAKE_CURRENT_BINARY_DIR}/pymseMapTools/mseMapTools.py ${CMAKE_CURRENT_BINARY_DIR}/python/pymseMapTools/mseMapTools.py
	COMMAND echo "OB will run ${Python3_EXECUTABLE} setup.py bdist_wheel"
	COMMAND ${Python3_EXECUTABLE} setup.py bdist_wheel

	DEPENDS
		pymseMapTools
	BYPRODUCTS
		python/${PYTHON_PROJECT}
	    	python/${PYTHON_PROJECT}.egg-info
    		python/build
    		python/dist
	WORKING_DIRECTORY python
	COMMAND_EXPAND_LISTS)

add_custom_target(python_package ALL
  DEPENDS
    python/dist/timestamp
    COMMAND echo "OB will  run ${Python3_EXECUTABLE} -m pip install  --no-index --find-links=${CMAKE_CURRENT_BINARY_DIR}/python/dist mse --prefix=${CMAKE_INSTALL_PREFIX}"
    COMMAND  ${Python3_EXECUTABLE} -m pip install  --no-index --find-links=${CMAKE_CURRENT_BINARY_DIR}/python/dist pymseMapTools --prefix=${CMAKE_INSTALL_PREFIX}
    COMMENT "running pip install"
  )

install(TARGETS pymseMapTools
	DESTINATION "${CMAKE_INSTALL_PREFIX}/dummy"
	)
enable_testing()
add_test( NAME testMseMapTools
	COMMAND ${Python3_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/testMseMapTools.py
	WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
#	add_custom_command(TARGET pymseMapTools POST_BUILD
#			COMMAND "${Python3_EXECUTABLE} -m pip install --no-index --prefix=${CMAKE_INSTALL_PREFIX}")

#set_target_properties(pymapTools PROPERTIES
#   INSTALL_RPATH "$ORIGIN:$ORIGIN/../../${PYTHON_PROJECT}/.libs"
# )                                                             
