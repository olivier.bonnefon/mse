# 
# This file is useful for developer. Users of the mse library are advised to consult the documentation available at https://mse.biosp.org/ for its usage.
#
# This script allows for :
# 1) to compil all mse components using current sources 
# 2) test the install procedure in view of adjust the guix cook
#
# to use it:
#
# 1 -> set software environnement using guix:
# cd mse/trunck
# guix time-machine -C ../guix/channelsMSE.scm -- shell -C --share=/tmp --preserve='^DISPLAY$' -m ../guix/manifestDolfinxv06.scm  -m ../guix/manifestCompilAllMSEComponents.scm -L ../guix/recettes/
# 2 -> source ./mseTestInstall.sh
#
#



echo "ensure you are in mse/trunk directory ?"
TRUNK_MSE=$(pwd)/
mkdir TEST_INSTAL
rm -Rf TEST_INSTAL/*

echo "remove previous builds"
rm -rf mseMatTools/build/* mseMatTools/Frontend/build/* mseMapTools/build/* mseMapTools/Frontend/build/ *mseSafranTools/build/* mseSafranTools/Frontend/build/*

#mseMapTools install
source ./mseMapToolsTestInstall.sh

#mseMatTools install
source ./mseMatToolsTestInstall.sh
#mseSafranTools install
source ./mseSafranToolsTestInstall.sh

#MSE python install
echo "========================================================"
echo "===== 7) mse python install:============================"
echo "========================================================"

cd $TRUNK_MSE
echo "clean local python build :  "
rm -rf dist build src/MSE.egg-info/ $TRUNK_MSE/TEST_INSTAL/installMse/
echo "- run the python installation"

python3 setup.py build
python3 setup.py install --prefix=$TRUNK_MSE/TEST_INSTAL/installMse/ --no-compile --single-version-externally-managed --root=/
ls $TRUNK_MSE/TEST_INSTAL/installMse/lib/python3.10/site-packages/mse/
echo "The path to local install must be add to python path:"
export PYTHONPATH=$PYTHONPATH:$TRUNK_MSE/TEST_INSTAL/installMse/lib/python3.10/site-packages/
echo "To ckeck that mse is available:"
python3 -c "import mse"

